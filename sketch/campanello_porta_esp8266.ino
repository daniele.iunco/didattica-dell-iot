/*
 * Versione per ESP8266.
 * 
 * Definisco un tempo ATTESA_MINIMA_CAMBIO_STATO_MS per evitare rilevamenti
 * errati causa eventi ambientali, tempo di blocco della serratura, etc.
 * 
 * su ESP8266 in modalità deep sleep il risveglio è possibile solo generando un
 * segnale di reset con componenti esterni, uso quindi la modalità light sleep
 */

#include <ESP8266WiFi.h>
 
#define PIN_RELE_REED D2
#define PIN_BUZZER D5
#define ATTESA_MINIMA_CAMBIO_STATO_MS 500

void setSleepWaitHigh() { //sleep in attesa di livello alto
  WiFi.mode(WIFI_OFF);
  wifi_fpm_set_sleep_type(LIGHT_SLEEP_T);
  gpio_pin_wakeup_enable(GPIO_ID_PIN(PIN_RELE_REED), GPIO_PIN_INTR_HILEVEL);
  wifi_fpm_set_wakeup_cb(callbackAperturaPorta); // callback al risveglio
  wifi_fpm_open();
  // attesa in sleep massima con risveglio da evento GPIO
  wifi_fpm_do_sleep(0xFFFFFFF);
  delay(10); // attesa mentre va in sleep
}

void setSleepWaitLow() { //sleep in attesa di livello basso
  WiFi.mode(WIFI_OFF);
  wifi_fpm_set_sleep_type(LIGHT_SLEEP_T);
  gpio_pin_wakeup_enable(GPIO_ID_PIN(PIN_RELE_REED), GPIO_PIN_INTR_LOLEVEL);
  //wifi_fpm_set_wakeup_cb(callbackChiusuraPorta);
  wifi_fpm_open();
  // attesa in sleep massima con risveglio da evento GPIO
  wifi_fpm_do_sleep(0xFFFFFFF);
  delay(10); // attesa mentre va in sleep
}

volatile bool ignoraEvento = false;
volatile unsigned long ultimaAperturaPorta = 0;

void callbackAperturaPorta(){
  if (ignoraEvento) return;
  if (ultimaAperturaPorta > 0) return;
  ultimaAperturaPorta = millis();
}

void notificaApertura(){
  digitalWrite(PIN_BUZZER, HIGH);
  delay(1000);
  digitalWrite(PIN_BUZZER, LOW);
}

void setup() {
  pinMode(PIN_RELE_REED, INPUT_PULLUP);
  pinMode(PIN_BUZZER, OUTPUT);
}

void loop() {
  if (ultimaAperturaPorta > 0){
    // se è trascorso abbastanza tempo posso proseguire
    if (millis() > ultimaAperturaPorta + ATTESA_MINIMA_CAMBIO_STATO_MS) {
      if (digitalRead(PIN_RELE_REED) == HIGH){
        notificaApertura();
      } else {
        // porta non più aperta, possibili micro spostamenti della porta
        // del magnete a causa di eventi atmosferici, devo ignorare l'evento.
        ultimaAperturaPorta = 0;
        setSleepWaitHigh();
        return;
      }
    } else { // ancora in attesa
      delay(1); 
      return;
    }
  }

  // porta aperta e notifica emessa, o porta aperta dall'accensione, attendo chiusura
  // ignoro altri eventi per non generare notifiche senza disattivare interrupt
  ignoraEvento = true;
  while (digitalRead(PIN_RELE_REED) == HIGH) {
    setSleepWaitLow();
  }
  delay(ATTESA_MINIMA_CAMBIO_STATO_MS);
  ignoraEvento = false;

  // se dopo il delay la porta si è riaperta, considero
  // l'evento una nuova apertura da verificare
  if (digitalRead(PIN_RELE_REED) == HIGH) {
    ultimaAperturaPorta = millis();
    return;
  }
  
  setSleepWaitHigh();
}
