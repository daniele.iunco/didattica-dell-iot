/*
 * Versione per Arduino (ATmega328P).
 * 
 * Definisco un tempo ATTESA_MINIMA_CAMBIO_STATO_MS per evitare rilevamenti
 * errati causa eventi ambientali, tempo di blocco della serratura, etc.
 * 
 */

#include "LowPower.h"

#define PIN_RELE_REED 2
#define PIN_BUZZER 5
#define ATTESA_MINIMA_CAMBIO_STATO_MS 500

volatile bool ignoraEvento = false;
volatile unsigned long ultimaAperturaPorta = 0;

void isrAperturaPorta(){
  if (ignoraEvento) return;
  if (ultimaAperturaPorta > 0) return;
  ultimaAperturaPorta = millis();
}

void notificaApertura(){
  digitalWrite(PIN_BUZZER, HIGH);
  delay(1000);
  digitalWrite(PIN_BUZZER, LOW);
}

void setup() {
  pinMode(PIN_RELE_REED, INPUT_PULLUP);
  pinMode(PIN_BUZZER, OUTPUT);
  attachInterrupt(digitalPinToInterrupt(PIN_RELE_REED), isrAperturaPorta, RISING);
}

void loop() {
  if (ultimaAperturaPorta > 0){
    // se è trascorso abbastanza tempo posso proseguire
    if (millis() > ultimaAperturaPorta + ATTESA_MINIMA_CAMBIO_STATO_MS) {
      if (digitalRead(PIN_RELE_REED) == HIGH){
        notificaApertura();
      } else {
        // porta non più aperta, possibili micro spostamenti del magnete
        // causa eventi atmosferici, devo ignorare l'evento.
        ultimaAperturaPorta = 0;
        LowPower.powerDown(SLEEP_FOREVER, ADC_OFF, BOD_OFF);
        return;
      }
    } else { // ancora in attesa
      delay(1); 
      return;
    }
  }

  // porta aperta e notifica emessa, o porta aperta dall'accensione, attendo chiusura
  // ignoro altri eventi per non generare notifiche senza disattivare interrupt
  ignoraEvento = true; 
  while (digitalRead(PIN_RELE_REED) == HIGH) {
    // N.B. un eventuale evento fa terminare l'attesa
    LowPower.powerDown(SLEEP_1S, ADC_OFF, BOD_OFF);
  }
  delay(ATTESA_MINIMA_CAMBIO_STATO_MS);
  ignoraEvento = false;

  // se dopo il delay la porta si è riaperta,
  // considero l'evento una nuova apertura da verificare
  if (digitalRead(PIN_RELE_REED) == HIGH) {
    ultimaAperturaPorta = millis();
    return;
  }
  
  LowPower.powerDown(SLEEP_FOREVER, ADC_OFF, BOD_OFF);
}
