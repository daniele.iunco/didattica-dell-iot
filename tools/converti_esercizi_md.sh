#!/bin/bash

tesiPath=".."

if [[ "$tesiPath" == */ ]]
then
    echo "Errore"
    exit 1
fi


if [ -d $tesiPath ] 
then
    echo "Percorso relativo '$tesiPath'"
    echo "Percorso assoluto:"
    realpath $tesiPath;
else
    echo "errore: '$tesiPath' non esiste"
    exit 1
fi

read -p "Ok (s/n)? " -r
if [[ $REPLY =~ ^[Ss]$ ]]
then
    echo "procedo"
else
    echo "Operazione annullata"
    exit 1
fi

texContentPath=$tesiPath"/tex-content"
sourcepath=$texContentPath"/esercizi-md"
destpath=$texContentPath"/esercizi-tex-auto"
autoimportfile=$destpath"/auto-import.tex"
fileSearchPathAndPattern=$sourcepath"/*.md"
fileToDeleteSearchPathAndPattern=$destpath"/*.auto.tex"

echo "Percorso dei file generati automaticamente: '$destpath'"
echo "File per l'importazione (sovrascritto): '$autoimportfile'"

echo "Questi file saranno cancellati:"

if ls $fileToDeleteSearchPathAndPattern 1> /dev/null 2> /dev/null; then
    for file in $fileToDeleteSearchPathAndPattern
    do
        echo "- '$file'"
    done
    read -p "Continuare e cancellare i file (s/n)? " -r
    if [[ $REPLY =~ ^[Ss]$ ]]
    then
        for file in $fileToDeleteSearchPathAndPattern
        do
            rm $file
    	echo "File '$file' cancellato"
        done
    else
        echo "Operazione annullata"
        exit 1
    fi
else
    echo "Nessun file da cancellare"
fi


#modifiche all'output con sed
#############################
#cose da rimuovere:
#sottosezione nella tabella dei contenuti:			riga con addcontentsline....
#etichetta predefinita:						\label{sec:esercizioNNN....
#parametri di compactitem:					\itemsep1pt........tutta la riga

#cose da cambiare:
#parametri allineamento immagini:				da [htbp] a [H]
#elenchi:							{itemize} in {compactitem}
#subsection in titoloparagrafo (comando personalizzato TeX)	da \subsection{ a \titoloparagrafo{

echo "%file generato automaticamente" > $autoimportfile
for file in $fileSearchPathAndPattern
do
    destfile="$(basename -- $file)"
    destfile="${destfile/.md}.auto.tex"
    destfullpath="$destpath"/"$destfile"
    echo "'$(basename -- $file)' -> '$destfullpath'"

pandoc --no-wrap -f markdown-auto_identifiers $file --to=latex | sed -e '/addcontentsline/d' | sed -e 's/\\label{sec:esercizioNNN/\%\\label{sec:esercizioNNN/' | sed -e 's/\[htbp\]/\[H\]/g' | sed -e 's/{itemize}/{compactitem}/g'  | sed '/\\itemsep1pt*/d'   | sed -e 's/\\subsection\*{/\\titoloparagrafo{/g' > $destfullpath
echo "\subimport{.}{$destfile}">>$autoimportfile
done

