import java.util.HashMap;
import java.io.*;
import java.util.Scanner;

/**
 * @author danielej
 * 
 * soluzione provvisoria per fare una ricerca di esercizi validi in base agli argomenti svolti di teoria
 * ciascun file di esercizio contiene dei metadati scritti in modo da essere ignorati nella conversione in .tex   
 * N.B. questo codice non elabora i metadati, ma cerca semplicemente la stringa contenente i prerequisiti nei file di ciascun esercizio
 */

public class Ricerca {

	public static void main(String[] args) {
		if (args.length != 2) {
			System.out.println("Parametri errati, previsto: <percorso directory file esercizi> <chiavi argomenti fatti separati da ,>");
			System.out.println("Esempio:");
			System.out.println("java -jar ricerca-esercizi.jar ../tex-content/esercizi-md/ chiave1,chiave2,chiave3");
			return;
		}
		
		HashMap<String, String> justtotest = new HashMap<String, String>();
		String keys[] = args[1].split(",");
		for (int i=0; i<keys.length;i++) {
			justtotest.put(keys[i].toLowerCase(), null);
		}
		
		File myDir = new File(args[0]);
		if (!myDir.isDirectory()) {
			System.out.println("\"" + args[0] + "\" non è una directory valida");
			return;
		}

		File[] listOfFiles = myDir.listFiles();
		for (File myFile : listOfFiles) {
			if (!myFile.isDirectory()) 
				if (eserciziovalido(justtotest, myFile.getAbsolutePath()))
					System.out.println(myFile.getAbsolutePath().substring(myFile.getAbsolutePath().lastIndexOf("/")+1) + ": OK");
		}
	    
	}
	

	public static boolean eserciziovalido(HashMap<String, String> justtotest, String filepath) {
		boolean result = true;
	    try {
	        File myObj = new File(filepath);
	        Scanner myReader = new Scanner(myObj);
	        while (myReader.hasNextLine() && result) {
	          String data = myReader.nextLine();
	          if (data.startsWith("<prerequisites>")) {
	        	  if (data.indexOf(">")> 0 && data.lastIndexOf("<") > data.indexOf(">")) {
		        	  String sInnerText = data.substring(data.indexOf(">")+1,data.lastIndexOf("<"));
		        	  String parts[] = sInnerText.split(",");
		        	  for (int i=0; i < parts.length && result; i++) {
		        		  if (!justtotest.containsKey(parts[i].trim().toLowerCase()))
		        			  result = false;
		        	  }
	        	  }
	          }
	          
	        }
	        myReader.close();
	      } catch (FileNotFoundException e) {
	        System.out.println("File \"" + filepath + "\" non trovato.");
	      }

	    return result;
	}
	
	
}
