/*******************************************************************************
 * Copyright (C) 2021 Daniele Iunco
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package widgets;

import interfaces.*;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;
import java.awt.Color;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.awt.event.ActionEvent;
import java.awt.Component;
import javax.swing.Box;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;


//TODO lblLabel provvisoria con JTextArea perch� JLabel non gestisce testo su pi� righe, da cambiare
//TODO serialVersionUID

/**
 * Classe che estende JPanel per consentire la gestione di un testo multi linea con pulsante incolla ed editor in finestra
 * si collega a un oggetto con metodi setter e getter per un valore String
 * @author Daniele Iunco
 * @version 1.0
 *
 */
public class SwingTextWithPasteAutoWidth extends JPanel implements IContentsMonitored {
	private static final long serialVersionUID = 1L;
	private final JTextArea text;
	private String sLabelText = "Label";
	private String initialValue;
	private final IDialogChangeSubscriber parentWindow;
	private JTextArea lblLabel;

	private final Object linkedObject;
	private final String linkedPropertyName;
	private final Color defaultBgColor;

	//per gestire l'abilitazione delle notifiche
	private boolean notifyEvents = false;

	/**
	 * Costruttore
	 * 
	 */
	public SwingTextWithPasteAutoWidth(IDialogChangeSubscriber parentWindow, String label, Object linkedObject, String linkedPropertyName) {
		this.parentWindow = parentWindow;
		this.sLabelText = label;
		this.linkedObject = linkedObject;
		this.linkedPropertyName = linkedPropertyName;
		setBorder(null);
		
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 1.0};
		gridBagLayout.columnWeights = new double[]{0.0, 1.0, 0.0};
		setLayout(gridBagLayout);
		
		JButton btnPaste = new JButton("Incolla");
		btnPaste.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					String data = (String)Toolkit.getDefaultToolkit().getSystemClipboard().getData(DataFlavor.stringFlavor);
					if (data != null) {
						text.insert(data, text.getCaretPosition());
						notifyChanges();
					}
				} catch (HeadlessException | UnsupportedFlavorException | IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} 
			}
		});

		lblLabel = new JTextArea("");
		lblLabel.setRows(2);
		lblLabel.setBackground(getBackground());
		lblLabel.setLineWrap(true);
		lblLabel.setBorder(new EmptyBorder(2, 2, 2, 2));
		JScrollPane scrollpaneLabel = new JScrollPane(lblLabel);
		scrollpaneLabel.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		scrollpaneLabel.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollpaneLabel.setBorder(null);
		
		GridBagConstraints gbc_scrollpaneLabel = new GridBagConstraints();
		gbc_scrollpaneLabel.insets = new Insets(0, 0, 5, 5);
		gbc_scrollpaneLabel.anchor = GridBagConstraints.NORTHWEST;
		gbc_scrollpaneLabel.gridx = 0;
		gbc_scrollpaneLabel.gridy = 0;
		gbc_scrollpaneLabel.weightx = 0;
		gbc_scrollpaneLabel.weighty = 0;
		gbc_scrollpaneLabel.fill = GridBagConstraints.BOTH;
		gridBagLayout.setConstraints(scrollpaneLabel,gbc_scrollpaneLabel) ;
		add(scrollpaneLabel, gbc_scrollpaneLabel);
		
		lblLabel.setText(label);
		
		Component verticalStrut = Box.createVerticalStrut(50);
		GridBagConstraints gbc_verticalStrut = new GridBagConstraints();
		gbc_verticalStrut.insets = new Insets(0, 0, 5, 0);
		gbc_verticalStrut.gridx = 2;
		gbc_verticalStrut.gridy = 0;
		add(verticalStrut, gbc_verticalStrut);

		GridBagConstraints gbc_btnPaste = new GridBagConstraints();
		gbc_btnPaste.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnPaste.anchor = GridBagConstraints.WEST;
		gbc_btnPaste.insets = new Insets(0, 0, 5, 5);
		gbc_btnPaste.gridx = 0;
		gbc_btnPaste.gridy = 1;
		add(btnPaste, gbc_btnPaste);
		
		JButton btnEdit = new JButton("Modifica");
		btnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				openEditor();
			}
		});
		GridBagConstraints gbc_btnEdit = new GridBagConstraints();
		gbc_btnEdit.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnEdit.anchor = GridBagConstraints.WEST;
		gbc_btnEdit.insets = new Insets(0, 0, 5, 5);
		gbc_btnEdit.gridx = 0;
		gbc_btnEdit.gridy = 2;
		add(btnEdit, gbc_btnEdit);

		text = new JTextArea();
		text.setLineWrap(true);
		text.getDocument().addDocumentListener(new DocumentListener() {

	        @Override
	        public void removeUpdate(DocumentEvent e) {
				setBgColor();
				notifyChanges();
	        }

	        @Override
	        public void insertUpdate(DocumentEvent e) {
				setBgColor();
				notifyChanges();
	        }

	        @Override
	        public void changedUpdate(DocumentEvent arg0) {
				setBgColor();
				notifyChanges();
	        }
	    });
		
		defaultBgColor = text.getBackground();
		
		JScrollPane scrollpaneText = new JScrollPane(text);
		
		GridBagConstraints gbc_scrollpaneText = new GridBagConstraints();
		gbc_scrollpaneText.insets = new Insets(0, 0, 0, 5);
		gbc_scrollpaneText.gridheight = 5;
		gbc_scrollpaneText.anchor = GridBagConstraints.NORTHWEST;
		gbc_scrollpaneText.gridx = 1;
		gbc_scrollpaneText.gridy = 0;
		gbc_scrollpaneText.weightx = 1;
		gbc_scrollpaneText.weighty = 1;
		gbc_scrollpaneText.fill = GridBagConstraints.BOTH;
		gridBagLayout.setConstraints(scrollpaneText,gbc_scrollpaneText) ;
		add(scrollpaneText, gbc_scrollpaneText);
		
		Component horizontalStrut = Box.createHorizontalStrut(100);
		GridBagConstraints gbc_horizontalStrut = new GridBagConstraints();
		gbc_horizontalStrut.insets = new Insets(0, 0, 0, 5);
		gbc_horizontalStrut.gridx = 0;
		gbc_horizontalStrut.gridy = 4;
		add(horizontalStrut, gbc_horizontalStrut);
	}

	@Override
	public boolean hasChanges() {
		return text.getText().equals(initialValue) ? false : true;
	}

	void openEditor() {
		Window parentWindow = SwingUtilities.windowForComponent(this); 
		Frame parentFrame = null;
		if (parentWindow instanceof Frame) parentFrame = (Frame)parentWindow;
		final SwingTextEditorDialog myReq = new SwingTextEditorDialog(parentFrame, "Edit " + sLabelText, text.getText());
		myReq.setVisible(true);
		String sSelected = myReq.getValue();
		if (sSelected != null && sSelected.length() > 0) text.setText(sSelected);
	}
	
	@Override
	public void setNotChanged() {
		boolean currNotify = notifyEvents;
		notifyEvents = false;
		initialValue = text.getText();
		setBgColor();
		notifyEvents = currNotify;
	}

	@Override
	public void setBgColor() {
		text.setBackground(text.getText().equals(initialValue) ? defaultBgColor : Color.YELLOW);
	}

	/**
	 * sostituisce il valore e resetta lo stato dell'oggetto in non modificato
	 * @param keys
	 */
	public void setText(String value) {
		initialValue = value;
		text.setText(value);
		notifyChanges();
	}

	public String getText() {
		return text.getText();
	}

	/**
	 * Notifica le modifiche
	 */
	private void notifyChanges() {
		if (notifyEvents && parentWindow != null) parentWindow.changed(this);
	}

	@Override
	public void syncUnderlyingObject() {
		IContentsMonitored.invokeSetter(linkedObject, linkedPropertyName, getText());
	}

	@Override
	public void setNotifications(boolean enabled) {
		notifyEvents = enabled;
	}
	
	@Override
	public void loadFromUnderlyingObject() {
		setText((String)IContentsMonitored.invokeGetter(linkedObject, linkedPropertyName));
	}
	
}
