/*******************************************************************************
 * Copyright (C) 2021 Daniele Iunco
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package widgets;

import javax.swing.JPanel;

import java.awt.GridLayout;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.event.ListSelectionListener;

import common.*;

import javax.swing.event.ListSelectionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

//TODO conversione fatta male della vecchia classe, da rifare, ci si deve appoggiare a una lista che eredita da AbstractListModel 
//TODO serialVersionUID

/**
 * Widget che usa JList per gestire oggetti ItemFormativoEsercizioAdv collegati ad una stringa di testo
 * @author Daniele Iunco
 *
 */
public class SwingExerciseList extends JPanel {

	private static final long serialVersionUID = 1L;

	public final static String SELECTED_INDEX_CHANGED_EVENT = "selectedIndexChanged";
	
	private final java.util.List<Map.Entry<String, ItemFormativoEsercizioAdv>> refExerciseList;

	private JList<ItemFormativoEsercizioAdv> myJlist;
	private DefaultListModel<ItemFormativoEsercizioAdv> list;
	private int iOldIndex = -1;

	@Override
	public void addKeyListener(KeyListener obj){
		myJlist.addKeyListener(obj);
	}
		
	private PropertyChangeSupport changes = new PropertyChangeSupport(this);

    public void addPropertyChangeListener(PropertyChangeListener myListener) {
        changes.addPropertyChangeListener(myListener);
       }
    public void removePropertyChangeListener(PropertyChangeListener myListener) {
        changes.removePropertyChangeListener(myListener);
       }
    
	/**
	 * Costruttore
	 */
	public SwingExerciseList() {
		refExerciseList = new java.util.ArrayList<Map.Entry<String, ItemFormativoEsercizioAdv>>();

		setLayout(new GridLayout(1, 0, 0, 0));
		
		list = new DefaultListModel<ItemFormativoEsercizioAdv>();
		myJlist = new JList<ItemFormativoEsercizioAdv>(list);
		myJlist.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
		        changes.firePropertyChange(SELECTED_INDEX_CHANGED_EVENT, iOldIndex, myJlist.getSelectedIndex());
		        iOldIndex = myJlist.getSelectedIndex();
			}
		});
		add(myJlist);
		
		myJlist.setCellRenderer(new ItemFormativoEsercizioAdvCellRenderer());
	}

	public String[] getSelection() {
		return new String[] {refExerciseList.get(myJlist.getSelectedIndex()).getKey()};
	}

	public ItemFormativoEsercizioAdv getFirstSelectedObject() {
		if (myJlist.getSelectedIndex() == -1) return null;
		return refExerciseList.get(myJlist.getSelectedIndex()).getValue();
	}
	
	@Override
	public void removeAll() {
		list.removeAllElements();
		refExerciseList.clear();
	}
	
	
	public void add(String xmlFilePath, PathStructure pathStructure) {
		try {
			ItemFormativoEsercizioAdv myNew = new ItemFormativoEsercizioAdv(xmlFilePath, !Tools.fileExists(xmlFilePath), pathStructure);
			list.addElement(myNew);
			refExerciseList.add(new AbstractMap.SimpleImmutableEntry<String, ItemFormativoEsercizioAdv>(xmlFilePath, myNew));
		}
		catch (Exception e1) {
			e1.printStackTrace();
		}
		finally {
			
		}
	}

	public void add(Map.Entry<String, ItemFormativoEsercizioAdv> item) {
		try {
			list.addElement(item.getValue());
			refExerciseList.add(item);
		}
		catch (Exception e1) {
			e1.printStackTrace();
		}
		finally {
			
		}
	}

	public void add(String xmlFilePath, ItemFormativoEsercizioAdv item) {
		try {
			list.addElement(item);
			refExerciseList.add(new AbstractMap.SimpleImmutableEntry<String, ItemFormativoEsercizioAdv>(xmlFilePath, item));
		}
		catch (Exception e1) {
			e1.printStackTrace();
		}
		finally {
			
		}
	}
	
	public int getSelectionIndex() {
		return myJlist.getSelectedIndex();
	}
	
	public int getItemCount() {
		return list.getSize();
	}

	public ItemFormativoEsercizioAdv[] getAllItems() {
		if (refExerciseList.size() == 0) return new ItemFormativoEsercizioAdv[] {};
		
		List<ItemFormativoEsercizioAdv> result = new ArrayList<ItemFormativoEsercizioAdv>();
		for (Entry<String, ItemFormativoEsercizioAdv> entryFound : refExerciseList) {
			result.add(entryFound.getValue());
		}
		
		ItemFormativoEsercizioAdv resultArray[] = new ItemFormativoEsercizioAdv[result.size()];
	    
		resultArray = result.toArray(resultArray);
		return resultArray;
	}

	
	
}
