/*******************************************************************************
 * Copyright (C) 2021 Daniele Iunco
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package widgets;

import interfaces.*;

import java.awt.Color;

import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;

//TODO serialVersionUID

/**
 * Classe che estende JSpinner per consentire la gestione dello stato di contenuto modificato/non modificato
 * @author Daniele Iunco
 * @version 1.0
 *
 */
public class SwingSpinner extends JPanel implements IContentsMonitored {
	private static final long serialVersionUID = 1L;
	private int initialValue;
	private final IDialogChangeSubscriber parentWindow;

	private final Object linkedObject;
	private final String linkedPropertyName;
	private final JSpinner spinner;
	private final Color defaultBgColor;
	
	//per gestire l'abilitazione delle notifiche
	private boolean notifyEvents = false;

	/**
	 * Create the panel.
	 */
	public SwingSpinner(IDialogChangeSubscriber parentWindow, String label, Object linkedObject, String linkedPropertyName) {
		this.parentWindow=parentWindow;
		this.initialValue=0;
		this.linkedObject = linkedObject;
		this.linkedPropertyName = linkedPropertyName;
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWeights = new double[]{0.3, 0.7};
		gridBagLayout.rowWeights = new double[]{0.0};
		setLayout(gridBagLayout);
		
		JLabel lblLabel = new JLabel(label);
		GridBagConstraints gbc_lblLabel = new GridBagConstraints();
		gbc_lblLabel.anchor = GridBagConstraints.WEST;
		gbc_lblLabel.insets = new Insets(0, 0, 0, 5);
		gbc_lblLabel.gridx = 0;
		gbc_lblLabel.gridy = 0;
		add(lblLabel, gbc_lblLabel);
		
		spinner = new JSpinner();
		spinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				spinner_stateChanged();
			}
		});
		GridBagConstraints gbc_spinner = new GridBagConstraints();
		gbc_spinner.fill = GridBagConstraints.HORIZONTAL;
		gbc_spinner.anchor = GridBagConstraints.NORTHWEST;
		gbc_spinner.gridx = 1;
		gbc_spinner.gridy = 0;
		add(spinner, gbc_spinner);
		
		defaultBgColor = spinner.getBackground();

	}
	
	public int getNumber() {
		SpinnerNumberModel myModel = (SpinnerNumberModel) spinner.getModel();
		return myModel.getNumber().intValue();
	}

	public void setNumber(int value){
		this.initialValue=value;
		SpinnerNumberModel myModel = (SpinnerNumberModel) spinner.getModel();
		myModel.setValue(value);
		setBgColor();
		notifyChanges();
	}

	@Override
	public boolean hasChanges() {
		return (getNumber()!=initialValue);
	}

	@Override
	public void setNotChanged() {
		boolean currNotify = notifyEvents;
		notifyEvents = false;
		initialValue = getNumber();
		setBgColor();
		notifyEvents = currNotify;
	}

	@Override
	public void setBgColor() {
		setBackground(getNumber()==initialValue ? defaultBgColor : Color.YELLOW);
	}

	/**
	 * Notifica le modifiche
	 */
	private void notifyChanges() {
		if (notifyEvents && parentWindow != null)
			parentWindow.changed(this);
	}

	@Override
	public void syncUnderlyingObject() {
		IContentsMonitored.invokeSetter(linkedObject, linkedPropertyName, getNumber());
	}

	@Override
	public void setNotifications(boolean enabled) {
		notifyEvents = enabled;
	}
	
	@Override
	public void loadFromUnderlyingObject() {
		Integer numFound = (Integer)IContentsMonitored.invokeGetter(linkedObject, linkedPropertyName);
		if (numFound != null) setNumber(numFound.intValue());
	}

	private void spinner_stateChanged() {
		setBgColor();
		notifyChanges();
	}
	
	/**
	 * Incrementa di 1 e imposta l'oggetto come modificato senza inviare la notifica
	 */
	public void addAndSetChangedNoNotify() {
		notifyEvents=false;
		SpinnerNumberModel myModel = (SpinnerNumberModel) spinner.getModel();
		myModel.setValue(getNumber()+1);
		setBgColor();
		notifyEvents=true;
	}
	
}
