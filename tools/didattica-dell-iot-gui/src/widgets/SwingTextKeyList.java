/*******************************************************************************
 * Copyright (C) 2021 Daniele Iunco
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package widgets;

import common.*;

import interfaces.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import java.awt.Color;
import java.awt.Frame;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.JButton;

import java.awt.Insets;
import java.awt.Window;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;


//TODO serialVersionUID

/**
 * Classe che estende JPanel per consentire la gestione di una lista di tag separati da virgola, definiti in un file esterno, con supporto per la notifica delle modifiche
 * @author Daniele Iunco
 * @version 1.0
 * 
 */
public class SwingTextKeyList extends JPanel implements IContentsMonitored {
	private static final long serialVersionUID = 1L;
	private final IDialogChangeSubscriber parentWindow;

	private JTextArea text;
	private IReferenceMonitor window;
	
	private String keyValueFilePath;
	private String initialValue;

	private final Object linkedObject;
	private final String linkedPropertyName;
	private final Color defaultBgColor;

	//per gestire l'abilitazione delle notifiche
	private boolean notifyEvents = false;

	
	/**
	 * Costruttore
	 * @param parentWindow
	 * @param keyValueFilePath
	 * @param window
	 * @param initialValue
	 * @throws FileNotFoundException
	 */
	public SwingTextKeyList(IDialogChangeSubscriber parentWindow, String keyValueFilePath, IReferenceMonitor window, String label, Object linkedObject, String linkedPropertyName) throws FileNotFoundException {
		this.parentWindow=parentWindow;
		this.window=window;
		this.keyValueFilePath=keyValueFilePath;
		this.initialValue = "";
		this.linkedObject = linkedObject;
		this.linkedPropertyName = linkedPropertyName;

		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 1.0};
		setLayout(gridBagLayout);
		
		
		JButton btnChange = new JButton("Cambia");
		btnChange.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnChange_click();
			}
		});
		
		JLabel lblLabel = new JLabel(label);
		GridBagConstraints gbc_lblLabel = new GridBagConstraints();
		gbc_lblLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblLabel.gridx = 0;
		gbc_lblLabel.gridy = 0;
		add(lblLabel, gbc_lblLabel);
		GridBagConstraints gbc_btnChange = new GridBagConstraints();
		gbc_btnChange.insets = new Insets(0, 0, 5, 0);
		gbc_btnChange.gridx = 2;
		gbc_btnChange.gridy = 0;
		add(btnChange, gbc_btnChange);
		
		JPanel panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.gridheight = 2;
		gbc_panel.insets = new Insets(0, 0, 0, 5);
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 1;
		gbc_panel.gridy = 0;
		add(panel, gbc_panel);
		panel.setLayout(new GridLayout(0, 1, 0, 0));
		
		text = new JTextArea();
		text.setLineWrap(true);
		if (initialValue != null) text.setText(initialValue);
		text.getDocument().addDocumentListener(new DocumentListener() {

	        @Override
	        public void removeUpdate(DocumentEvent e) {
				setBgColor();
				notifyChanges();
	        }

	        @Override
	        public void insertUpdate(DocumentEvent e) {
				setBgColor();
				notifyChanges();
	        }

	        @Override
	        public void changedUpdate(DocumentEvent arg0) {
				setBgColor();
				notifyChanges();
	        }
	    });

		defaultBgColor = text.getBackground();

		JScrollPane sp = new JScrollPane(text);
		
		panel.add(sp);

	}

	/**
	 * ci sono modifiche?
	 */
	@Override
	public boolean hasChanges() {
		return text.getText().equals(initialValue) ? false : true;
	}

	/**
	 * resetta lo stato in non modificato
	 */
	@Override
	public void setNotChanged() {
		boolean currNotify = notifyEvents;
		notifyEvents = false;
		initialValue = text.getText();
		setBgColor();
		notifyEvents = currNotify;
	}
	
	/**
	 * applica il colore corretto
	 */
	@Override
	public void setBgColor() {
		text.setBackground(text.getText().equals(initialValue) ? defaultBgColor : Color.YELLOW);
	}

	private void btnChange_click() {
		try {
			KeyValuePairsFile objFile = new KeyValuePairsFile(this.keyValueFilePath,window);
			List<String> currVals = getValue();
			
			Window parentWindow = SwingUtilities.windowForComponent(this); 
			Frame parentFrame = null;
			if (parentWindow instanceof Frame) parentFrame = (Frame)parentWindow;
			SwingKeySelectorDialog window = new SwingKeySelectorDialog(parentFrame,"Seleziona tag",objFile,currVals.toArray(new String[currVals.size()]));
			window.setVisible(true);
			java.util.List<String> result = window.getValues();
			if (result != null) replaceCurrentValue(result);
		} catch (FileAlreadyExistsException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	/**
	 * chiavi separate da virgola
	 * @return
	 */
	public String getValueString() {
		return String.join(",", getValue());
	}
	
	/**
	 * sostituisce il valore con uno nuovo da elaborare
	 * @param rawKeyString
	 */
	public void setValue(String rawKeyString) {
		if (rawKeyString==null || rawKeyString.length()==0) {
			initialValue = null;
			text.setText("");
			notifyChanges();
			return;
		}
		List<String> parsed = Arrays.asList(rawKeyString.split(","));
		StringBuilder sb = new StringBuilder();
		for (int i=0;i<parsed.size();i++ ) {
			//trim strings
			if (parsed.get(i).trim().length() > 0) {
				if (sb.length()>0) sb.append(",");
				sb.append(parsed.get(i).trim());
			}
		}
		initialValue = sb.toString();
		text.setText(initialValue);
		notifyChanges();
	}

	/**
	 * sostituisce il valore
	 * @param keys
	 */
	private void replaceCurrentValue(java.util.List<String> keys) {
		if (keys==null || keys.size()==0) {
			text.setText("");
			return;
		}
		StringBuilder sb = new StringBuilder();
		for (int i=0;i<keys.size();i++ ) {
			//trim strings
			if (keys.get(i).trim().length() > 0) {
				if (sb.length()>0) sb.append(",");
				sb.append(keys.get(i).trim());
			}
		}
		text.setText(sb.toString());
		notifyChanges();
	}

	/**
	 * sostituisce il valore e resetta lo stato dell'oggetto in non modificato
	 * @param keys
	 */
	public void setValue(String[] keys) {
		if (keys==null || keys.length==0) {
			initialValue = null;
			text.setText("");
			return;
		}
		StringBuilder sb = new StringBuilder();
		for (int i=0;i<keys.length;i++ ) {
			//trim strings
			if (keys[i].trim().length() > 0) {
				if (sb.length()>0) sb.append(",");
				sb.append(keys[i].trim());
			}
		}
		initialValue = sb.toString();
		text.setText(initialValue);
		notifyChanges();
	}

	/**
	 * sostituisce il valore e resetta lo stato dell'oggetto in non modificato
	 * @param keys
	 */
	public void setValue(List<String> keys) {
		if (keys==null || keys.size()==0) {
			initialValue = null;
			text.setText("");
			return;
		}
		StringBuilder sb = new StringBuilder();
		for (int i=0;i<keys.size();i++ ) {
			//trim strings
			if (keys.get(i).trim().length() > 0) {
				if (sb.length()>0) sb.append(",");
				sb.append(keys.get(i).trim());
			}
		}
		initialValue = sb.toString();
		text.setText(initialValue);
		notifyChanges();
	}
	
	/**
	 * restituisce una lista di stringhe
	 * @return
	 */
	public List<String> getValue() {
		ArrayList<String> parsed = new ArrayList<String>();
		
		if (text.getText().length() < 1) return parsed;
		
		String[] rawResult = text.getText().split(",");
		
		for (int i=0;i<rawResult.length;i++ ) {
			//trim strings
			if (rawResult[i].trim().length() > 0)
				parsed.add(rawResult[i].trim());
		}
		return parsed;
	}

	/**
	 * Notifica le modifiche
	 */
	private void notifyChanges() {
		if (notifyEvents && parentWindow != null) parentWindow.changed(this);
	}

	@Override
	public void setNotifications(boolean enabled) {
		notifyEvents = enabled;
	}
	
	@Override
	public void syncUnderlyingObject() {
		IContentsMonitored.invokeSetter(linkedObject, linkedPropertyName, getValueString());
	}

	@Override
	public void loadFromUnderlyingObject() {
		setValue((String)IContentsMonitored.invokeGetter(linkedObject, linkedPropertyName));
	}
	
}
