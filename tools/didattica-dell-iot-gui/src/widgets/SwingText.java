/*******************************************************************************
 * Copyright (C) 2021 Daniele Iunco
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package widgets;

import interfaces.*;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.GridBagLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;


//TODO serialVersionUID

/**
 * Classe che estende JPanel per consentire la gestione di un testo con etichetta a sinistra
 * si collega a un oggetto con metodi setter e getter per un valore String
 * @author Daniele Iunco
 * @version 1.0
 *
 */
public class SwingText extends JPanel implements IContentsMonitored {
	private static final long serialVersionUID = 1L;

	private final JTextField text;
	private String initialValue;
	private final IDialogChangeSubscriber parentWindow;
	private JTextArea lblLabel;

	private final Object linkedObject;
	private final String linkedPropertyName;
	private final Color defaultBgColor;

	//per gestire l'abilitazione delle notifiche
	private boolean notifyEvents = false;

	/**
	 * Costruttore
	 * 
	 */
	public SwingText(IDialogChangeSubscriber parentWindow, String label, Object linkedObject, String linkedPropertyName) {
		this.parentWindow = parentWindow;
		this.linkedObject = linkedObject;
		this.linkedPropertyName = linkedPropertyName;
		setBorder(null);
		
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.rowWeights = new double[]{0.0};
		gridBagLayout.columnWeights = new double[]{0.0, 1.0};
		setLayout(gridBagLayout);

		lblLabel = new JTextArea("");
		lblLabel.setBackground(getBackground());
		lblLabel.setLineWrap(true);
		lblLabel.setBorder(new EmptyBorder(2, 2, 2, 2));
		JScrollPane scrollpaneLabel = new JScrollPane(lblLabel);
		scrollpaneLabel.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		scrollpaneLabel.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollpaneLabel.setBorder(null);
		
		GridBagConstraints gbc_scrollpaneLabel = new GridBagConstraints();
		gbc_scrollpaneLabel.anchor = GridBagConstraints.NORTHWEST;
		gbc_scrollpaneLabel.gridx = 0;
		gbc_scrollpaneLabel.gridy = 0;
		gbc_scrollpaneLabel.weightx = 0;
		gbc_scrollpaneLabel.weighty = 0;
		gbc_scrollpaneLabel.fill = GridBagConstraints.BOTH;
		gridBagLayout.setConstraints(scrollpaneLabel,gbc_scrollpaneLabel) ;
		add(scrollpaneLabel, gbc_scrollpaneLabel);
		
		lblLabel.setText(label);

		text = new JTextField();
		text.getDocument().addDocumentListener(new DocumentListener() {

	        @Override
	        public void removeUpdate(DocumentEvent e) {
				setBgColor();
				notifyChanges();
	        }

	        @Override
	        public void insertUpdate(DocumentEvent e) {
				setBgColor();
				notifyChanges();
	        }

	        @Override
	        public void changedUpdate(DocumentEvent arg0) {
				setBgColor();
				notifyChanges();
	        }
	    });
		defaultBgColor = text.getBackground();
		
		GridBagConstraints gbc_text = new GridBagConstraints();
		gbc_text.anchor = GridBagConstraints.NORTHWEST;
		gbc_text.gridx = 1;
		gbc_text.gridy = 0;
		gbc_text.weightx = 1;
		gbc_text.weighty = 1;
		gbc_text.fill = GridBagConstraints.BOTH;
		gridBagLayout.setConstraints(text,gbc_text) ;
		add(text, gbc_text);
		
	}

	@Override
	public boolean hasChanges() {
		return text.getText().equals(initialValue) ? false : true;
	}
	
	@Override
	public void setNotChanged() {
		boolean currNotify = notifyEvents;
		notifyEvents = false;
		initialValue = text.getText();
		setBgColor();
		notifyEvents = currNotify;
	}

	@Override
	public void setBgColor() {
		text.setBackground(text.getText().equals(initialValue) ? defaultBgColor : Color.YELLOW);
	}

	/**
	 * sostituisce il valore e resetta lo stato dell'oggetto in non modificato
	 * @param keys
	 */
	public void setText(String value) {
		initialValue = value;
		text.setText(value);
		notifyChanges();
	}

	public String getText() {
		return text.getText();
	}

	/**
	 * Notifica le modifiche, se shell lo supporta
	 */
	private void notifyChanges() {
		if (notifyEvents && parentWindow != null) parentWindow.changed(this);
	}

	@Override
	public void setNotifications(boolean enabled) {
		notifyEvents = enabled;
	}
	
	@Override
	public void syncUnderlyingObject() {
		IContentsMonitored.invokeSetter(linkedObject, linkedPropertyName, getText());
	}

	@Override
	public void loadFromUnderlyingObject() {
		setText((String)IContentsMonitored.invokeGetter(linkedObject, linkedPropertyName));
	}
	
}
