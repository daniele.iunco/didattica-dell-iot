/*******************************************************************************
 * Copyright (C) 2021 Daniele Iunco
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package widgets;

import interfaces.*;

import javax.swing.JPanel;
import javax.swing.JCheckBox;
import java.awt.Color;
import java.awt.GridLayout;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;

//TODO serialVersionUID

public class SwingCheckBox extends JPanel implements IContentsMonitored {

	private static final long serialVersionUID = 1L;
	private boolean initialValue;
	private JCheckBox btnCheckButton;
	private final IDialogChangeSubscriber parentWindow;

	private final Object linkedObject;
	private final String linkedPropertyName;
	private final Color defaultBgColor;
	
	//per gestire l'abilitazione delle notifiche
	private boolean notifyEvents = false;

	/**
	 * Costruttore
	 */
	public SwingCheckBox(IDialogChangeSubscriber parentWindow, boolean initialValue, String text, Object linkedObject, String linkedPropertyName) {
		this.parentWindow = parentWindow;
		this.linkedObject = linkedObject;
		this.linkedPropertyName = linkedPropertyName;

		setLayout(new GridLayout(0, 1, 0, 0));
		
		defaultBgColor = getBackground();
		
		btnCheckButton = new JCheckBox(text);
		btnCheckButton.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				setBgColor();
			}
		});
		add(btnCheckButton);

		this.initialValue = initialValue;
		btnCheckButton.setSelected(initialValue);
	}

	public void setText(String value) {
		btnCheckButton.setText(value);
	}
	
	public void setValue(boolean value) {
		this.initialValue = value;
		btnCheckButton.setSelected(value);
		setBgColor();
		notifyChanges();
	}

	public void setValue(Boolean value) {
		if (value == null) {
			this.initialValue = false;
			btnCheckButton.setSelected(false);
			setBgColor();
			notifyChanges();
			
		} else {
			this.initialValue = value;
			btnCheckButton.setSelected(value);
			setBgColor();
			notifyChanges();
		}
	}

	public boolean getValue() {
		return btnCheckButton.isSelected();
	}

	@Override
	public boolean hasChanges() {
		return getValue() != initialValue;
	}

	@Override
	public void setNotChanged() {
		boolean currNotify = notifyEvents;
		notifyEvents = false;
		initialValue = getValue();
		setBgColor();
		notifyEvents = currNotify;
	}

	@Override
	public void setBgColor() {
		btnCheckButton.setBackground(getValue() == initialValue ? defaultBgColor : Color.YELLOW);
	}

	/**
	 * Notifica le modifiche
	 */
	private void notifyChanges() {
		if (notifyEvents && parentWindow != null) parentWindow.changed(this);
	}

	@Override
	public void setNotifications(boolean enabled) {
		notifyEvents = enabled;
	}
	
	@Override
	public void syncUnderlyingObject() {
		IContentsMonitored.invokeSetter(linkedObject, linkedPropertyName, getValue());
	}

	@Override
	public void loadFromUnderlyingObject() {
		setValue((Boolean)IContentsMonitored.invokeGetter(linkedObject, linkedPropertyName));
	}
	
}
