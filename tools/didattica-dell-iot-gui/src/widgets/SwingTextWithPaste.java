/*******************************************************************************
 * Copyright (C) 2021 Daniele Iunco
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package widgets;

import interfaces.*;

import javax.swing.JPanel;
import javax.swing.BoxLayout;
import java.awt.Dimension;

//TODO lblLabel provvisoria con JTextArea perch� JLabel non gestisce testo su pi� righe, da cambiare
//TODO serialVersionUID

/**
 * Contenitore provvisorio per dare una misura a SwingTextWithPaste (ora rinominato in SwingTextWithPasteAutoWidth) quando � in un JPanel con JScollpane
 * @author Daniele Iunco
 * @version 1.0
 *
 */
public class SwingTextWithPaste extends JPanel implements IContentsMonitored {
	private static final long serialVersionUID = 1L;

	private SwingTextWithPasteAutoWidth content;

	/**
	 * Costruttore
	 * 
	 */
	public SwingTextWithPaste(IDialogChangeSubscriber parentWindow, String label, Object linkedObject, String linkedPropertyName) {
		setBorder(null);
		setPreferredSize(new Dimension(570,150));
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		
		content = new SwingTextWithPasteAutoWidth(parentWindow, label, linkedObject, linkedPropertyName);
		add(content);
	
	}

	@Override
	public boolean hasChanges() {
		return content.hasChanges();
	}

	@Override
	public void setNotChanged() {
		content.setNotChanged();
	}

	@Override
	public void setBgColor() {
		content.setBgColor();
	}

	public void setText(String value) {
		content.setText(value);
	}

	public String getText() {
		return content.getText();
	}

	@Override
	public void setNotifications(boolean enabled) {
		content.setNotifications(enabled);
	}
	
	@Override
	public void syncUnderlyingObject() {
		content.syncUnderlyingObject();
	}

	@Override
	public void loadFromUnderlyingObject() {
		content.loadFromUnderlyingObject();
	}
	
}
