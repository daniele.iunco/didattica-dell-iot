/*******************************************************************************
 * Copyright (C) 2021 Daniele Iunco
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package widgets;

import interfaces.*;

import javax.swing.JPanel;

import java.awt.GridBagLayout;
import javax.swing.JLabel;

import java.awt.Color;
import java.awt.GridBagConstraints;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.Insets;
import java.util.List;

import javax.swing.JList;
import javax.swing.border.LineBorder;

import common.Tools;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

// TODO serialVersionUID

/**
 * Classe che estende JPanel per consentire la gestione di un array di stringhe utilizzando un ArrayList come contenitore
 * si collega a un oggetto con metodi setter e getter per un valore List<String>
 * @author Daniele Iunco
 * @version 1.0
 * 
 */
public class SwingListWithEditor extends JPanel implements IContentsMonitored {

	private static final long serialVersionUID = 1L;
	private JList<String> myJList;
	private DefaultListModel<String> list;
	private JLabel lblLabel;
	private java.util.List<String> initialValue;
	private final IDialogChangeSubscriber parentWindow;

	private final Object linkedObject;
	private final String linkedPropertyName;
	private final Color defaultBgColor;
	private final boolean allowEmptyStrings;

	//per gestire l'abilitazione delle notifiche
	private boolean notifyEvents = false;

	/**
	 * Create the panel.
	 */
	public SwingListWithEditor(IDialogChangeSubscriber parentWindow, boolean allowEmptyStrings, String label, Object linkedObject, String linkedPropertyName) {
		this.parentWindow = parentWindow;
		initialValue = new java.util.ArrayList<String>();
		this.linkedObject = linkedObject;
		this.linkedPropertyName = linkedPropertyName;
		this.allowEmptyStrings = allowEmptyStrings;

		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		lblLabel = new JLabel(label);
		GridBagConstraints gbc_lblLabel = new GridBagConstraints();
		gbc_lblLabel.anchor = GridBagConstraints.WEST;
		gbc_lblLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblLabel.gridx = 0;
		gbc_lblLabel.gridy = 0;
		add(lblLabel, gbc_lblLabel);
		
		list = new DefaultListModel<String>();
		myJList = new JList<String>(list);
		myJList.setBorder(new LineBorder(Color.LIGHT_GRAY));
		GridBagConstraints gbc_list = new GridBagConstraints();
		gbc_list.gridheight = 3;
		gbc_list.insets = new Insets(0, 0, 5, 5);
		gbc_list.fill = GridBagConstraints.BOTH;
		gbc_list.gridx = 0;
		gbc_list.gridy = 1;
		add(myJList, gbc_list);
		
		JButton btnAdd = new JButton("Aggiungi");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnAdd_Click();
			}
		});
		GridBagConstraints gbc_btnAdd = new GridBagConstraints();
		gbc_btnAdd.insets = new Insets(0, 0, 5, 0);
		gbc_btnAdd.gridx = 1;
		gbc_btnAdd.gridy = 1;
		add(btnAdd, gbc_btnAdd);
		
		JButton btnEdit = new JButton("Modifica");
		btnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnEdit_Click();
			}
		});
		GridBagConstraints gbc_btnEdit = new GridBagConstraints();
		gbc_btnEdit.insets = new Insets(0, 0, 5, 0);
		gbc_btnEdit.gridx = 1;
		gbc_btnEdit.gridy = 2;
		add(btnEdit, gbc_btnEdit);
		
		JButton btnDelete = new JButton("Cancella");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnDelete_Click();
			}
		});
		GridBagConstraints gbc_btnDelete = new GridBagConstraints();
		gbc_btnDelete.anchor = GridBagConstraints.NORTH;
		gbc_btnDelete.gridx = 1;
		gbc_btnDelete.gridy = 3;
		add(btnDelete, gbc_btnDelete);

		defaultBgColor = myJList.getBackground();
	}

	void btnAdd_Click() {
		String result = Tools.inputBox(this, "Aggiungi", linkedPropertyName, "");
		if (result != null && (allowEmptyStrings || result.trim().length() > 0)) {
			list.addElement(result.trim());
			notifyChanges();
		}
		setBgColor();
	}
	
	void btnEdit_Click() {
		if (myJList.getSelectedIndex() < 0) return;
		String result = Tools.inputBox(this, "Modifica", linkedPropertyName, myJList.getSelectedValue());
		if (result != null && (allowEmptyStrings || result.trim().length() > 0)) {
			list.set(myJList.getSelectedIndex(), result.trim());
			notifyChanges();
		}
		setBgColor();
	}
	
	void btnDelete_Click() {
		if (myJList.getSelectedIndex() < 0) return;
		if (!Tools.yesOrNoBox(this, "Cancellare l'elemento?", myJList.getSelectedValue())) return;
			
		list.remove(myJList.getSelectedIndex());
		setBgColor();
		notifyChanges();
	}
	
	public void setLabel(String text) {
		lblLabel.setText(text);
	}

	/**
	 * sostituisce il valore lasciando l'oggetto impostato come non modificato
	 * @param src
	 * @param keepSelectedIndex mantiene l'eventuale riga selezionata
	 */
	public void setValue(String[] src, boolean keepSelectedIndex) {
		int iOldIndex = myJList.getSelectedIndex();
		list.removeAllElements();
		initialValue.clear();
		if (src==null) return;
		for (int i=0; i < src.length; i++) {
			list.addElement(src[i]);
			initialValue.add(src[i]);
		}
		myJList.setSelectedIndex(iOldIndex > -1 && iOldIndex < list.getSize() ? iOldIndex : list.getSize()-1);
		notifyChanges();
	}

	public void clear() {
		list.removeAllElements();
		initialValue.clear();
		notifyChanges();
	}
	
	/**
	 * sostituisce il valore lasciando l'oggetto come non modificato
	 * @param src
	 * @param keepSelectedIndex mantiene l'eventuale riga selezionata
	 */
	public void setValue(java.util.List<String> src, boolean keepSelectedIndex) {
		int iOldIndex = myJList.getSelectedIndex();
		list.removeAllElements();
		initialValue.clear();
		if (src==null) return;
		for (int i=0; i < src.size(); i++) {
			list.addElement(src.get(i));
			initialValue.add(src.get(i));
		}
		myJList.setSelectedIndex(iOldIndex > -1 && iOldIndex < list.getSize() ? iOldIndex : list.getSize()-1);
		notifyChanges();
	}

	public String[] getValue() {
		if (list.getSize() < 1) return null;
		
		String[] result = new String[list.getSize()];
		
		for (int i=0; i < list.getSize(); i++)
			result[i] = list.get(i);
		
		return result;
	}

	
	public void getListValue(java.util.List<String> result) {
		result.clear();
		
		if (list.getSize() < 1) return;
		
		for (int i=0; i < list.getSize(); i++)
			result.add(list.get(i));
		
	}
	
	@Override
	public boolean hasChanges() {
		boolean bChanged = false;
		// 0 0 = no changes
		if (initialValue.size() == 0 && list.getSize() == 0)  // 0 0
			bChanged = false;
		else if (initialValue.size() == 0 && list.getSize() > 0)  // 0 1
			bChanged = true;
		else if (initialValue.size() > 0 && list.getSize() == 0) // 1 0
			bChanged = true;
		else if (initialValue.size() > 0 && list.getSize() > 0) // 1 1
			if (list.getSize() != initialValue.size())
				bChanged = true;
			else //same size, must compare
				for (int i=0; i < initialValue.size(); i++)
					if (!list.get(i).equals(initialValue.get(i)))
					{
						bChanged = true;
						break;
					}
		return bChanged;
	}

	@Override
	public void setNotChanged() {
		boolean currNotify = notifyEvents;
		notifyEvents = false;
		setValue(getValue(), true);
		setBgColor();
		notifyEvents = currNotify;
	}

	@Override
	public void setBgColor() {
		myJList.setBackground(hasChanges() ? Color.YELLOW : defaultBgColor);
	}

	/**
	 * Notifica le modifiche
	 */
	private void notifyChanges() {
		if (notifyEvents && parentWindow != null) parentWindow.changed(this);
	}

	@Override
	public void setNotifications(boolean enabled) {
		notifyEvents = enabled;
	}
	
	@Override
	public void syncUnderlyingObject() {
		java.util.List<String> underlyingList = (List<String>) IContentsMonitored.invokeGetter(linkedObject, linkedPropertyName);
		getListValue(underlyingList);
	}

	@Override
	public void loadFromUnderlyingObject() {
		java.util.List<String> underlyingList = (List<String>) IContentsMonitored.invokeGetter(linkedObject, linkedPropertyName);
		setValue(underlyingList, true);
	}
	
}
