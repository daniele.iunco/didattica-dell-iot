/*******************************************************************************
 * Copyright (C) 2021 Daniele Iunco
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package widgets;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Frame;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import javax.swing.JTextArea;
import java.awt.GridLayout;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

//TODO serialVersionUID

/**
 * Finestra di dialogo chiamata dai widget del package per l'editing di una stringa.
 * @author Daniele Iunco
 * @version 1.0
 *
 */
class SwingTextEditorDialog extends JDialog {
	private static final long serialVersionUID = 1L;
	
	private JTextArea text;
	private final JPanel contentPanel = new JPanel();
	private String returnValue;
	private static final String ACTION_OK ="OK";
	private static final String ACTION_CANCEL ="Cancel";
	
	/**
	 * Create the dialog.
	 * @param windowTitle
	 * @param defaultValue
	 */
	public SwingTextEditorDialog(Frame owner, String windowTitle, String defaultValue) {
		super(owner, true);
		setBounds(100, 100, 800, 600);
		setTitle(windowTitle);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new GridLayout(0, 1, 0, 0));
		{
			text = new JTextArea();
			text.setLineWrap(true);
			text.setText(defaultValue);
			JScrollPane sp = new JScrollPane(text);
			contentPanel.add(sp);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Salva");
				okButton.setActionCommand(ACTION_OK);
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						myActionPerformed(e);
					}
				});
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Annulla");
				cancelButton.setActionCommand(ACTION_CANCEL);
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						myActionPerformed(e);
					}
				});
				buttonPane.add(cancelButton);
			}
		}
	}

	public String getValue() {
		return returnValue;
	}

	public void myActionPerformed(ActionEvent e) {
        String action = e.getActionCommand();
        if (action.equals(ACTION_OK)) {
        	returnValue = text.getText();
        	dispose();
        } else if (action.equals(ACTION_CANCEL)) {
        	dispose();
        }
    }
}
