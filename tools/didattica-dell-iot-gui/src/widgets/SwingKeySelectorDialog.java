/*******************************************************************************
 * Copyright (C) 2021 Daniele Iunco
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package widgets;

import common.*;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Frame;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import java.awt.GridLayout;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.swing.JList;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.awt.event.ActionEvent;

//TODO serialVersionUID


/**
 * Finestra di dialogo chiamata dai widget del package per gestire chiavi da un file esterno.
 * @author Daniele Iunco
 * @version 1.0
 *
 */
class SwingKeySelectorDialog extends JDialog {
	
	private static final long serialVersionUID = 1L;
	//TODO poco efficiente ma meglio di iterazione ripetuta n volte sulla lista di n elementi
	private final Map<String,String> currentKeys;
	protected java.util.List<String> result;
	
	private KeyValuePairsFile keys;
	private final JPanel contentPanel = new JPanel();
	private JList<String> list;
	private DefaultListModel<String> listKeys;
	
	/**
	 * Create the dialog.
	 */
	public SwingKeySelectorDialog(Frame owner, String windowTitle, KeyValuePairsFile keys, String[] initialValue) throws FileAlreadyExistsException, IOException {
		super(owner, true);

		this.keys=keys;
		this.currentKeys = new HashMap<String,String>();
		
		if (initialValue!=null)
			for(int i=0; i < initialValue.length; i++) {
				this.currentKeys.put(initialValue[i], initialValue[i]);
			}

		if (windowTitle!= null)
			setTitle(windowTitle);
		else
			setTitle("Gestione coppie chiave-valore");

		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new GridLayout(0, 1, 0, 0));
		listKeys = new DefaultListModel<String>();
		list = new JList<String>(listKeys);
		JScrollPane sp = new JScrollPane(list);
		contentPanel.add(sp);
		
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						processSelection();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Annulla");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		reloadAll();
	}

	void reloadAll() {
    	Iterator<ChiaveValore> myIterator = keys.iterator();
     	int iIndex = -1;
     	int iIndexSel = -1;
    	
     	int iSel[] = new int[currentKeys.size()];
     	while (myIterator.hasNext()) {
    		iIndex++;
    		String sKey = myIterator.next().key;
    		if (currentKeys.containsKey(sKey)) {
    			iIndexSel++;
    			iSel[iIndexSel] = iIndex;
    		}
    		listKeys.addElement(sKey);
    	}
     	list.setSelectedIndices(iSel);
	}
	
	void processSelection() {
		if (list.getSelectedIndex() < 0) return;
		result = list.getSelectedValuesList();
		dispose();
	}
	
	public java.util.List<String> getValues(){
		return result;
	}
}
