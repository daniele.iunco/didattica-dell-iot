/*******************************************************************************
 * Copyright (C) 2021 Daniele Iunco
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package widgets;

import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import javax.swing.JButton;

import java.awt.GridBagLayout;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

//TODO serialVersionUID

public class SwingSearchKeyBox extends JPanel {
	private static final long serialVersionUID = 1L;
	private JTextField text;
	private List<String> myItems;
	private boolean bDiscardStartEndWhitespaces;
	
	/**
	 * Create the panel.
	 */
	public SwingSearchKeyBox(boolean bDiscardStartEndWhitespaces) {
		this.bDiscardStartEndWhitespaces=bDiscardStartEndWhitespaces;
		myItems = new ArrayList<String>();
				
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWeights = new double[]{1.0, 0.0};
		gridBagLayout.rowWeights = new double[]{0.0};
		setLayout(gridBagLayout);
		
		text = new JTextField();
		text.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) openEditDialog();
			}
		});
		text.setEditable(false);
		GridBagConstraints gbc_text = new GridBagConstraints();
		gbc_text.fill = GridBagConstraints.HORIZONTAL;
		gbc_text.insets = new Insets(0, 0, 0, 5);
		gbc_text.gridx = 0;
		gbc_text.gridy = 0;
		add(text, gbc_text);
		text.setColumns(10);
		
		JButton btnEdit = new JButton("Modifica");
		btnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				openEditDialog();
			}
		});
		GridBagConstraints gbc_btnEdit = new GridBagConstraints();
		gbc_btnEdit.anchor = GridBagConstraints.NORTHWEST;
		gbc_btnEdit.gridx = 1;
		gbc_btnEdit.gridy = 0;
		add(btnEdit, gbc_btnEdit);

	}

	private void openEditDialog() {
		Window parentWindow = SwingUtilities.windowForComponent(this); 
		Frame parentFrame = null;
		if (parentWindow instanceof Frame) parentFrame = (Frame)parentWindow;
		
		SwingTextEditorDialog myReq = new SwingTextEditorDialog(parentFrame, "Edit", getText());
		myReq.setVisible(true);
		String sSelected = myReq.getValue();
		if (sSelected != null) {
			setText(sSelected);
			
		}
	}

	public String getText() {
		String result = myItems.stream().map(Object::toString).collect(Collectors.joining("\n"));
		return result;
	}

	public void setText(String value) {
		myItems.clear();
		if (value == null) return;
		
		String sLines[] = value.split("\\r?\\n");
		StringBuilder newText = new StringBuilder();
		for (int i=0; i < sLines.length; i++) {
			if (bDiscardStartEndWhitespaces) sLines[i] = sLines[i].trim();
			if (sLines[i].length() > 0) {
				if (i != 0) newText.append(";");
				newText.append(sLines[i]);
				myItems.add(sLines[i]);
			}
		}
		text.setText(newText.toString());
	}
	
	public List<String> getValues() {
		return myItems;
	}

}
