/*******************************************************************************
 * Copyright (C) 2021 Daniele Iunco
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package widgets;

import interfaces.*;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

//TODO serialVersionUID

public class SwingCombo extends JPanel implements IContentsMonitored {

	private static final long serialVersionUID = 1L;

	private JComboBox<String> comboBox;
	
	private String initialValue;
	
	private final IDialogChangeSubscriber parentWindow;

	private final Object linkedObject;
	private final String linkedPropertyName;
	private Color defaultBgColor;

	//per gestire l'abilitazione delle notifiche
	private boolean notifyEvents = false;

	/**
	 * Costruttore
	 */
	public SwingCombo(IDialogChangeSubscriber parentWindow, String label, String[] values, Object linkedObject, String linkedPropertyName) {
		//objCombo = new JComboBox<String>(values);
		
		this.parentWindow = parentWindow;
		this.initialValue="";
		this.linkedObject = linkedObject;
		this.linkedPropertyName = linkedPropertyName;
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		JLabel lblNewLabel = new JLabel(label);
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.fill = GridBagConstraints.BOTH;
		gbc_lblNewLabel.insets = new Insets(0, 0, 0, 5);
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 0;
		add(lblNewLabel, gbc_lblNewLabel);
		
		comboBox = new JComboBox<String>(values);
		GridBagConstraints gbc_comboBox = new GridBagConstraints();
		gbc_comboBox.fill = GridBagConstraints.BOTH;
		gbc_comboBox.gridx = 1;
		gbc_comboBox.gridy = 0;
		add(comboBox, gbc_comboBox);
		
		comboBox.addActionListener (new ActionListener () {
		    public void actionPerformed(ActionEvent e) {
				setBgColor();
				notifyChanges();
		    }
		});
		
		//add(objCombo);
		defaultBgColor = getBackground();
	}
	
	public void setText(String value){
		comboBox.setSelectedItem(value);
		this.initialValue=value;
		setBgColor();
		notifyChanges();
	}

	public String getText() {
		return (String) comboBox.getSelectedItem();
	}
	
	@Override
	public boolean hasChanges() {
		boolean sameValue = false;
		if (initialValue == null)
			sameValue = getText() == null;
		else
			sameValue = initialValue.equals(getText());

		return !sameValue;
	}

	@Override
	public void setNotChanged() {
		boolean currNotify = notifyEvents;
		notifyEvents = false;
		initialValue = getText();
		setBgColor();
		notifyEvents = currNotify;
	}

	@Override
	public void setBgColor() {
		
		setBackground(hasChanges() ? Color.YELLOW : defaultBgColor);
	}

	/**
	 * Notifica le modifiche, se shell lo supporta
	 */
	private void notifyChanges() {
		if (notifyEvents && parentWindow != null) parentWindow.changed(this);
	}

	@Override
	public void setNotifications(boolean enabled) {
		notifyEvents = enabled;
	}
	
	@Override
	public void syncUnderlyingObject() {
		IContentsMonitored.invokeSetter(linkedObject, linkedPropertyName, getText());
	}

	@Override
	public void loadFromUnderlyingObject() {
		setText((String)IContentsMonitored.invokeGetter(linkedObject, linkedPropertyName));
	}
	
}
