/*******************************************************************************
 * Copyright (C) 2021 Daniele Iunco
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package widgets;

import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

//TODO serialVersionUID

/**
 * Widget per ottenere una lista modificabile con scrollbar con metodi per l'interfacciamento 
 * @author Daniele Iunco
 *
 */
public class SwingList extends JPanel {
	private static final long serialVersionUID = 1L;
	private JList<String> myJlist;
	private DefaultListModel<String> list;

	/**
	 * Create the panel.
	 */
	public SwingList() {
		
		setLayout(new GridLayout(1, 0, 0, 0));
		
		list = new DefaultListModel<String>();
		myJlist = new JList<String>(list);
		
		JScrollPane sp = new JScrollPane(myJlist);
		add(sp);
		
	}

	public int getSelectedIndex() {
		return myJlist.getSelectedIndex();
	}

	public int[] getSelectionIndeces() {
		return myJlist.getSelectedIndices();
	}
	
	//TODO probabilmente c'� un modo pi� semplice per ottenere l'array di stringhe
	public String[] getSelection() {
		ArrayList<String> result = new ArrayList<String>();
		
		if (getSelectedIndex()>-1) {
			int iSelectedIndices[] = myJlist.getSelectedIndices();
			for (int i=0; i<iSelectedIndices.length; i++)
				result.add(list.get(iSelectedIndices[i]));
		}
		
		return result.toArray(new String[result.size()]);
	}

	@Override
	public void removeAll() {
		list.removeAllElements();
	}
	
	public void add(String key) {
		// TODO Auto-generated method stub
		list.addElement(key);
	}
	
	

}
