/*******************************************************************************
 * Copyright (C) 2021 Daniele Iunco
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package interfaces;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;

/**
 * Interfaccia per widget che hanno metodi per tenere traccia dello stato di modifica dei contenuti
 * se un widget implementa l'interfaccia, allora il contenitore pu� verificare se ci sono modifiche ai dati.
 * @author Daniele Iunco
 * @version 1.0
 * 
 */
public interface IContentsMonitored {


	/**
	 * Notifica le modifiche, se parentWindow lo supporta
	 */
	public default void notifyChanges(Object parentWindow) {
		if (!hasChanges()) return;
		if (parentWindow == null) return;
		if (!IContainerMonitorContents.class.isAssignableFrom(parentWindow.getClass())) return;
		((IContainerMonitorContents) parentWindow).changed(this);
	}

	
	/**
	 * Forza l'aggiornamento del colore visualizzato, applicando quello previsto per lo stato dell'oggetto
	 */
	public void setBgColor();

	/**
	 * Restituisce true se il contenuto � stato modificato, false altrimenti
	 */
	public boolean hasChanges();
	
	/**
	 * Setta lo stato dell'oggetto in non modificato, qualsiasi sia il contenuto
	 */
	public void setNotChanged();
	
	/**
	 * Aggiorna il campo collegato chiamando il relativo setter
	 */
	public void syncUnderlyingObject();
	
	/**
	 * Carica il campo collegato chiamando il relativo getter
	 */
	public void loadFromUnderlyingObject();
	
	
	/**
	 * Attiva o disattiva la gestione delle notifiche dell'evento di modifica monitorato
	 */
	public void setNotifications(boolean enabled);
	
    public static void invokeSetter(Object obj, String propertyName, Object variableValue)
    {
        PropertyDescriptor pd;
        try {
            pd = new PropertyDescriptor(propertyName, obj.getClass());
            java.lang.reflect.Method setter = pd.getWriteMethod();
            setter.invoke(obj,variableValue);
        } catch (IntrospectionException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            e.printStackTrace();
        }
 
    }
    
    public static Object invokeGetter(Object obj, String variableName)
    {
        try {
            PropertyDescriptor pd = new PropertyDescriptor(variableName, obj.getClass());
            java.lang.reflect.Method getter = pd.getReadMethod();
            Object value = getter.invoke(obj);
            return value;
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | IntrospectionException e) {
            e.printStackTrace();
            return null;
        }
    }
}
