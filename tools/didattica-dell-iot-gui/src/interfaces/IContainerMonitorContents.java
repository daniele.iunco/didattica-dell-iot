/*******************************************************************************
 * Copyright (C) 2021 Daniele Iunco
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package interfaces;

/**
 * 
 * Interfaccia per finestre di dialogo contenenti widget che possono notificare la modifica dei contenuti
 * se la finestra implementa l'interfaccia, e si � passato un riferimento alla finestra ai widget istanziati
 * allora questi ultimi possono notificare le modifiche alla finestra.
 * @author Daniele Iunco
 * @version 1.0
 *
 */
public interface IContainerMonitorContents {
	public void changed(IContentsMonitored sender);
}
