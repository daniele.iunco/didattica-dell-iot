/*******************************************************************************
 * Copyright (C) 2021 Daniele Iunco
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package common;

import java.io.File;

public class PathStructure {
	
	public final FileSystemDirectory projectRoot;
	public final FileSystemDirectory imagePath;
	public final FileSystemDirectory mdPath;
	public final FileSystemDirectory texPath;
	public final FileSystemDirectory xmlPath;
	public final FileSystemFile tagsFile;
	public final FileSystemFile prerequisiteFile;
	public final FileSystemDirectory sketchPath;
	public final FileSystemDirectory gvPath;
  
	public Tuple<Boolean, String> validate() {
		StringBuilder log = new StringBuilder();
		boolean fail = false;
		
		if (!projectRoot.isValid()) {
			log.append("Err: " + projectRoot.getItemPath() + "\n");
			fail = true;
		}
		if (!imagePath.isValid()){
			log.append("Err: " + imagePath.getItemPath() + "\n");
			fail = true;
		}
		//non pi� usato
		/*if (!mdPath.isValid()){
			log.append("Err: " + mdPath.getItemPath() + "\n");
			fail = true;
		}*/
		if (!xmlPath.isValid()){
			log.append("Err: " + xmlPath.getItemPath() + "\n");
			fail = true;
		}
		if (!tagsFile.isValid()){
			log.append("Err: " + tagsFile.getItemPath() + "\n");
			fail = true;
		}
		if (!prerequisiteFile.isValid()){
			log.append("Err: " + prerequisiteFile.getItemPath() + "\n");
			fail = true;
		}
		if (!sketchPath.isValid()){
			log.append("Err: " + sketchPath.getItemPath() + "\n");
			fail = true;
		}
		if (!gvPath.isValid()){
			log.append("Err: " + gvPath.getItemPath() + "\n");
			fail = true;
		}
		return new Tuple<Boolean, String>(!fail, log.toString());
	}

	
	public boolean isValid() {
		return validate().item1;
	}

	public PathStructure(String rootPath) {
		projectRoot = new FileSystemDirectory(rootPath, "");
		imagePath = new FileSystemDirectory(rootPath + File.separator , "tex-content" + File.separator + "esercizi-md" + File.separator + "images");
		mdPath = new FileSystemDirectory(rootPath + File.separator , "esercizi-md");
		xmlPath = new FileSystemDirectory(rootPath + File.separator , "esercizi-xml");
		texPath = new FileSystemDirectory(rootPath + File.separator , "esercizi-tex");
		tagsFile = new FileSystemFile(rootPath + File.separator , "coppie-chiave-valore-tag.txt");
		prerequisiteFile = new FileSystemFile(rootPath + File.separator , "coppie-chiave-valore-prerequisiti.txt");
		sketchPath = new FileSystemDirectory(rootPath + File.separator , "sketch");
		gvPath = new FileSystemDirectory(rootPath + File.separator , "grafi");
	}
	
	private abstract class FileSystemItem {
	    private String itemBasePath;
	    String itemRelativePath;
	    
	    private FileSystemItem(String itemBasePath, String itemRelativePath) {
	    	this.itemBasePath=itemBasePath;
	    	this.itemRelativePath=itemRelativePath;
	    }

	    public String getItemPath() {
	    	if (itemRelativePath==null)
	    		return this.itemBasePath;
	    	else
	    		return this.itemBasePath + itemRelativePath; 
	    }
	   
	    public abstract boolean isValid();
	    public String relativePath() {
	    	if (itemRelativePath==null)
	    		return "";
	    	else
	    		return itemRelativePath;
	    }
		
	}
	
	public class FileSystemFile extends FileSystemItem {
		
	    private FileSystemFile(String itemBasePath, String itemRelativePath) {
			super(itemBasePath, itemRelativePath);
		}

		@Override
	    public boolean isValid() {
	    	return Tools.fileExists(getItemPath());
	    }

		@Override
		public String toString(){
			return getItemPath();
		}
	}
	
	public class FileSystemDirectory extends FileSystemItem {

		public FileSystemDirectory(String itemBasePath, String itemRelativePath) {
			super(itemBasePath, itemRelativePath);
		}

		@Override
	    public boolean isValid() {
	    	return Tools.directoryExists(getItemPath());
	    }
		
		@Override
		public String toString(){
			return getItemPath();
		}
		
		/**
		 * restituisce il percorso relativo della directory (o file) subItemPath, subItemPath deve essere un file o directory ma NON un percorso assoluto
		 * @param subItemPath
		 * @return
		 */
		public String mapRelativeSubPath(String subItemPath, String customPathSeparator) {
			if (itemRelativePath==null || itemRelativePath.length() == 0)
				return subItemPath;
			else
				if (subItemPath.startsWith(customPathSeparator))
					return itemRelativePath + subItemPath;
				else
					return itemRelativePath + customPathSeparator + subItemPath;
		}
	}
	
	

}
