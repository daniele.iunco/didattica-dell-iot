//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.3.1 
// Vedere <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2021.05.05 alle 05:51:02 PM CEST 
//

package common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per datiRisorsaCollegata complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="datiRisorsaCollegata"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="nomeFile" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="testoDescrittivo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="identificativoFile" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="fScaleWidth" type="{http://www.w3.org/2001/XMLSchema}float" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "datiRisorsaCollegata", propOrder = {
    "nomeFile",
    "testoDescrittivo",
    "identificativoFile",
    "fScaleWidth"
})
public class DatiRisorsaCollegata {

    @XmlElement(required = true)
    protected String nomeFile;
    protected String testoDescrittivo;
    protected String identificativoFile;
    protected Float fScaleWidth;

    /**
     * Recupera il valore della propriet� nomeFile.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeFile() {
        return nomeFile;
    }

    /**
     * Imposta il valore della propriet� nomeFile.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeFile(String value) {
        this.nomeFile = value;
    }

    /**
     * Recupera il valore della propriet� testoDescrittivo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTestoDescrittivo() {
        return testoDescrittivo;
    }

    /**
     * Imposta il valore della propriet� testoDescrittivo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTestoDescrittivo(String value) {
        this.testoDescrittivo = value;
    }

    /**
     * Recupera il valore della propriet� identificativoFile.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificativoFile() {
        return identificativoFile;
    }

    /**
     * Imposta il valore della propriet� identificativoFile.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificativoFile(String value) {
        this.identificativoFile = value;
    }

    /**
     * Recupera il valore della propriet� fScaleWidth.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getFScaleWidth() {
        return fScaleWidth;
    }

    /**
     * Imposta il valore della propriet� fScaleWidth.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setFScaleWidth(Float value) {
        this.fScaleWidth = value;
    }

}
