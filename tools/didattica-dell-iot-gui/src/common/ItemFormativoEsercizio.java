//
// Questo file Ŕ stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.3.1 
// Vedere <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Qualsiasi modifica a questo file andrÓ persa durante la ricompilazione dello schema di origine. 
// Generato il: 2021.05.04 alle 11:22:25 AM CEST 
//


package common;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per itemFormativoEsercizio complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="itemFormativoEsercizio"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="mdprerequisitelist" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="mdcomponentlist" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="mdwhy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="mdexpectedresult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="mdextras" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="mderraticbehavior" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="mdcommonerrors" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="mdlessonlearned" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="mdsolution" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="mdtodolist" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="mdpbldetails" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="risorseCollegate" type="{}risorseCollegateType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="nomeDescrittivo" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="identificatoreArgomento" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="prerequisiti" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="tags" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="fondamentale" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="mdPblproblem" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="mdPblnewlearninggoal" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="tempoRichiesto" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *       &lt;attribute name="tipoTempo" type="{}tipoTempo" /&gt;
 *       &lt;attribute name="versione" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "itemFormativoEsercizio", propOrder = {
    "mdprerequisitelist",
    "mdcomponentlist",
    "mdwhy",
    "mdexpectedresult",
    "mdextras",
    "mderraticbehavior",
    "mdcommonerrors",
    "mdlessonlearned",
    "mdsolution",
    "mdtodolist",
    "mdpbldetails",
    "risorseCollegate"
})
@XmlRootElement(name="itemFormativoEsercizio")
public class ItemFormativoEsercizio {

    protected List<String> mdprerequisitelist;
    protected List<String> mdcomponentlist;
    protected String mdwhy;
    protected String mdexpectedresult;
    protected String mdextras;
    protected String mderraticbehavior;
    protected String mdcommonerrors;
    protected String mdlessonlearned;
    protected String mdsolution;
    protected List<String> mdtodolist;
    protected String mdpbldetails;
    protected List<RisorseCollegateType> risorseCollegate;
    @XmlAttribute(name = "nomeDescrittivo", required = true)
    protected String nomeDescrittivo;
    @XmlAttribute(name = "identificatoreArgomento", required = true)
    protected String identificatoreArgomento;
    @XmlAttribute(name = "prerequisiti", required = true)
    protected String prerequisiti;
    @XmlAttribute(name = "tags", required = true)
    protected String tags;
    @XmlAttribute(name = "fondamentale")
    protected Boolean fondamentale;
    @XmlAttribute(name = "mdPblproblem")
    protected Boolean mdPblproblem;
    @XmlAttribute(name = "mdPblnewlearninggoal")
    protected Boolean mdPblnewlearninggoal;
    @XmlAttribute(name = "tempoRichiesto")
    protected Integer tempoRichiesto;
    @XmlAttribute(name = "tipoTempo")
    protected String tipoTempo;
    @XmlAttribute(name = "versione")
    protected Integer versione;


	/**
	 * Metodo vuoto
	 * @param ignore
	 */
 public void setMdtodolist(List<String> ignore) {
     return;
 }
		
	/**
	 * Metodo vuoto
	 * @param ignore
	 */
 public void setMdprerequisitelist(List<String> ignore) {
     return;
 }
		
	/**
	 * Metodo vuoto
	 * @param ignore
	 */
 public void setMdcomponentlist(List<String> ignore) {
     return;
 }
		
	/**
	 * Metodo vuoto
	 * @param ignore
	 */
 public void setRisorseCollegate(List<String> ignore) {
     return;
 }
 
    /**
     * Gets the value of the mdprerequisitelist property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the mdprerequisitelist property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMdprerequisitelist().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getMdprerequisitelist() {
        if (mdprerequisitelist == null) {
            mdprerequisitelist = new ArrayList<String>();
        }
        return this.mdprerequisitelist;
    }

    /**
     * Gets the value of the mdcomponentlist property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the mdcomponentlist property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMdcomponentlist().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getMdcomponentlist() {
        if (mdcomponentlist == null) {
            mdcomponentlist = new ArrayList<String>();
        }
        return this.mdcomponentlist;
    }

    /**
     * Recupera il valore della proprietÓ mdwhy.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMdwhy() {
        return mdwhy;
    }

    /**
     * Imposta il valore della proprietÓ mdwhy.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMdwhy(String value) {
        this.mdwhy = value;
    }

    /**
     * Recupera il valore della proprietÓ mdexpectedresult.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMdexpectedresult() {
        return mdexpectedresult;
    }

    /**
     * Imposta il valore della proprietÓ mdexpectedresult.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMdexpectedresult(String value) {
        this.mdexpectedresult = value;
    }

    /**
     * Recupera il valore della proprietÓ mdextras.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMdextras() {
        return mdextras;
    }

    /**
     * Imposta il valore della proprietÓ mdextras.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMdextras(String value) {
        this.mdextras = value;
    }

    /**
     * Recupera il valore della proprietÓ mderraticbehavior.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMderraticbehavior() {
        return mderraticbehavior;
    }

    /**
     * Imposta il valore della proprietÓ mderraticbehavior.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMderraticbehavior(String value) {
        this.mderraticbehavior = value;
    }

    /**
     * Recupera il valore della proprietÓ mdcommonerrors.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMdcommonerrors() {
        return mdcommonerrors;
    }

    /**
     * Imposta il valore della proprietÓ mdcommonerrors.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMdcommonerrors(String value) {
        this.mdcommonerrors = value;
    }

    /**
     * Recupera il valore della proprietÓ mdlessonlearned.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMdlessonlearned() {
        return mdlessonlearned;
    }

    /**
     * Imposta il valore della proprietÓ mdlessonlearned.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMdlessonlearned(String value) {
        this.mdlessonlearned = value;
    }

    /**
     * Recupera il valore della proprietÓ mdsolution.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMdsolution() {
        return mdsolution;
    }

    /**
     * Imposta il valore della proprietÓ mdsolution.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMdsolution(String value) {
        this.mdsolution = value;
    }

    /**
     * Gets the value of the mdtodolist property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the mdtodolist property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMdtodolist().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getMdtodolist() {
        if (mdtodolist == null) {
            mdtodolist = new ArrayList<String>();
        }
        return this.mdtodolist;
    }

    /**
     * Recupera il valore della proprietÓ mdpbldetails.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMdpbldetails() {
        return mdpbldetails;
    }

    /**
     * Imposta il valore della proprietÓ mdpbldetails.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMdpbldetails(String value) {
        this.mdpbldetails = value;
    }

    /**
     * Gets the value of the risorseCollegate property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the risorseCollegate property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRisorseCollegate().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RisorseCollegateType }
     * 
     * 
     */
    public List<RisorseCollegateType> getRisorseCollegate() {
        if (risorseCollegate == null) {
            risorseCollegate = new ArrayList<RisorseCollegateType>();
        }
        return this.risorseCollegate;
    }

    /**
     * Recupera il valore della proprietÓ nomeDescrittivo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeDescrittivo() {
        return nomeDescrittivo;
    }

    /**
     * Imposta il valore della proprietÓ nomeDescrittivo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeDescrittivo(String value) {
        this.nomeDescrittivo = value;
    }

    /**
     * Recupera il valore della proprietÓ identificatoreArgomento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificatoreArgomento() {
        return identificatoreArgomento;
    }

    /**
     * Imposta il valore della proprietÓ identificatoreArgomento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificatoreArgomento(String value) {
        this.identificatoreArgomento = value;
    }

    /**
     * Recupera il valore della proprietÓ prerequisiti.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrerequisiti() {
        return prerequisiti;
    }

    /**
     * Imposta il valore della proprietÓ prerequisiti.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrerequisiti(String value) {
        this.prerequisiti = value;
    }

    /**
     * Recupera il valore della proprietÓ tags.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTags() {
        return tags;
    }

    /**
     * Imposta il valore della proprietÓ tags.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTags(String value) {
        this.tags = value;
    }

    /**
     * Recupera il valore della proprietÓ fondamentale.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isFondamentale() {
        return fondamentale;
    }

    /**
     * Imposta il valore della proprietÓ fondamentale.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFondamentale(Boolean value) {
        this.fondamentale = value;
    }

    /**
     * Recupera il valore della proprietÓ mdPblproblem.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMdPblproblem() {
        return mdPblproblem;
    }

    /**
     * Imposta il valore della proprietÓ mdPblproblem.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMdPblproblem(Boolean value) {
        this.mdPblproblem = value;
    }

    /**
     * Recupera il valore della proprietÓ mdPblnewlearninggoal.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMdPblnewlearninggoal() {
        return mdPblnewlearninggoal;
    }

    /**
     * Imposta il valore della proprietÓ mdPblnewlearninggoal.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMdPblnewlearninggoal(Boolean value) {
        this.mdPblnewlearninggoal = value;
    }

    /**
     * Recupera il valore della proprietÓ tempoRichiesto.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTempoRichiesto() {
        return tempoRichiesto;
    }

    /**
     * Imposta il valore della proprietÓ tempoRichiesto.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTempoRichiesto(Integer value) {
        this.tempoRichiesto = value;
    }

    /**
     * Recupera il valore della proprietÓ tipoTempo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoTempo() {
        return tipoTempo;
    }

    /**
     * Imposta il valore della proprietÓ tipoTempo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoTempo(String value) {
        this.tipoTempo = value;
    }

    /**
     * Recupera il valore della proprietÓ versione.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getVersione() {
        return versione;
    }

    /**
     * Imposta il valore della proprietÓ versione.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setVersione(Integer value) {
        this.versione = value;
    }

}
