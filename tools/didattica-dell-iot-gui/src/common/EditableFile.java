/*******************************************************************************
 * Copyright (C) 2021 Daniele Iunco
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package common;

import interfaces.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.util.IllegalFormatException;


//TODO questo sistema � provvisorio e non � quello corretto, usare il pattern publish-subscribe

/**
 * Classe astratta, per gestire un file modificabile con notifica della modifica alla finestra principale, se specificata
 * @author Daniele Iunco
 * @version 1.0
 *
 */
public abstract class EditableFile {
	private Object window;
	
	
	//TODO provvisorio per poter modificare e salvare copie del file cambiando underlyingFile, valutare implementazione valida
	protected String underlyingFile;

	/**
	 * 
	 * @param filepath
	 * @param window finestra che deve ricevere la notifica di file modificato
	 * @throws FileNotFoundException
	 * @throws IllegalFormatException
	 */
	public EditableFile(String filepath, Object window) throws FileNotFoundException, IllegalFormatException {
		this.window = window;
		this.underlyingFile=filepath;
		load();
	}

    public abstract void load() throws FileNotFoundException, IllegalFormatException;
	protected abstract void privsave() throws FileAlreadyExistsException,IOException;
	
	public void save() throws FileAlreadyExistsException,IOException{
		privsave();
		if (window == null) return;
		if (!IReferenceMonitor.class.isAssignableFrom(window.getClass())) return;
		((IReferenceMonitor)window).FileUpdated(underlyingFile);
	}
}
