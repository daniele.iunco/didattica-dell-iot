//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.3.1 
// Vedere <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2021.05.04 alle 11:22:25 AM CEST 
//


package common;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the common package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ItemFormativoEsercizio_QNAME = new QName("", "itemFormativoEsercizio");
    private final static QName _RisorseCollegateTypeDatiRisorsaCollegata_QNAME = new QName("", "datiRisorsaCollegata");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: common
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ItemFormativoEsercizio }
     * 
     */
    public ItemFormativoEsercizio createItemFormativoEsercizio() {
        return new ItemFormativoEsercizio();
    }

    /**
     * Create an instance of {@link RisorseCollegateType }
     * 
     */
    public RisorseCollegateType createRisorseCollegateType() {
        return new RisorseCollegateType();
    }

    /**
     * Create an instance of {@link DatiRisorsaCollegata }
     * 
     */
    public DatiRisorsaCollegata createDatiRisorsaCollegata() {
        return new DatiRisorsaCollegata();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ItemFormativoEsercizio }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ItemFormativoEsercizio }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "itemFormativoEsercizio")
    public JAXBElement<ItemFormativoEsercizio> createItemFormativoEsercizio(ItemFormativoEsercizio value) {
        return new JAXBElement<ItemFormativoEsercizio>(_ItemFormativoEsercizio_QNAME, ItemFormativoEsercizio.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DatiRisorsaCollegata }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DatiRisorsaCollegata }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "datiRisorsaCollegata", scope = RisorseCollegateType.class)
    public JAXBElement<DatiRisorsaCollegata> createRisorseCollegateTypeDatiRisorsaCollegata(DatiRisorsaCollegata value) {
        return new JAXBElement<DatiRisorsaCollegata>(_RisorseCollegateTypeDatiRisorsaCollegata_QNAME, DatiRisorsaCollegata.class, RisorseCollegateType.class, value);
    }

}
