/*******************************************************************************
 * Copyright (C) 2021 Daniele Iunco
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package common;

import interfaces.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileAlreadyExistsException;
import java.util.ArrayList;
import java.util.HashMap;
import java.lang.IllegalArgumentException;
import java.util.Iterator;
import java.util.Scanner;

/**
 * Classe per gestire un file di coppie chiave-valore su disco, modificabile come se fosse un HashMap
 * TODO fatta male, da rifare
 * @author Daniele Iunco
 * @version 1.0
 *
 */
public class KeyValuePairsFile extends EditableFile {

	//serve a forzare la conservazione di una copia per ogni modifica dei file, settare true quando si fanno test
	private static final boolean KEEP_ALL_CHANGES_AS_BACKUP = false; 
	
	public KeyValuePairsFile(String filepath, IReferenceMonitor window) throws FileNotFoundException, IllegalArgumentException {
		super(filepath, window);
	}

	private HashMap<String, ChiaveValore> keyvaluepairlist;
	private ArrayList<ChiaveValore> underlyinglist;

	public boolean containsKey(String key) {
		return keyvaluepairlist.containsKey(key);
	}

	public Iterator<ChiaveValore> iterator(){
		return underlyinglist.iterator();
	}
	
	public boolean addNew(String key, String value) throws FileAlreadyExistsException, IOException {
		if (keyvaluepairlist.containsKey(key)) return false;
		ChiaveValore myNew = new ChiaveValore(key,value);
		keyvaluepairlist.put(key, myNew);
		underlyinglist.add(myNew);
		save();
		return true;
	}
	
	public boolean removeItem(String key) throws FileAlreadyExistsException, IOException {
		if (!keyvaluepairlist.containsKey(key)) return false;
		ChiaveValore myRemove = keyvaluepairlist.get(key);
		keyvaluepairlist.remove(key);
		underlyinglist.remove(myRemove);
		save();
		return true;
	}
	
	public ChiaveValore getItem(String key) {
		if (!keyvaluepairlist.containsKey(key)) return null;
		return keyvaluepairlist.get(key);
	}

	@Override
    public void load() throws FileNotFoundException, IllegalArgumentException
    {
    	keyvaluepairlist = new HashMap<String, ChiaveValore>();
    	underlyinglist = new ArrayList<ChiaveValore>();
		File myFile = new File(underlyingFile);
		Scanner myReader = new Scanner(myFile, "UTF-8");
		try {
			while (myReader.hasNextLine()) {
				ChiaveValore newItem = new ChiaveValore(myReader.nextLine());
				keyvaluepairlist.put(newItem.key, newItem);
				underlyinglist.add(newItem);
			}
		}
		catch (Exception e) {
			throw new IllegalArgumentException();
		}
		finally {
			myReader.close();
		}
    }

    @Override
    protected void privsave() throws FileAlreadyExistsException,IOException
    {
    	//TODO: se non si vogliono tenere le varie versioni togliere la copia del vecchio file, oppure numerare le versioni in modo corretto
		String pathBakTmp = underlyingFile + ".bak." + java.util.UUID.randomUUID().toString();
		if (Tools.fileExists(pathBakTmp)) throw new FileAlreadyExistsException(pathBakTmp);
		File src = new File(underlyingFile);
		File bak = new File(pathBakTmp);
		//TODO: dettaglio errore
		if (!src.renameTo(bak)) throw new IOException();
		
		OutputStreamWriter myWriter = new OutputStreamWriter(new FileOutputStream(underlyingFile), StandardCharsets.UTF_8);
    	Iterator<ChiaveValore> myIterator = underlyinglist.iterator();
    	while (myIterator.hasNext()) {
    		myWriter.write(myIterator.next().toString());
    		if (myIterator.hasNext()) myWriter.write("\r\n");
    	}
		myWriter.close();
		
		if (!KEEP_ALL_CHANGES_AS_BACKUP) bak.delete();
    }

    @Override
    public String toString() {
    	StringBuilder myOut = new StringBuilder();
    	Iterator<ChiaveValore> myIterator = underlyinglist.iterator();
    	while (myIterator.hasNext()) {
    		if (myOut.length()>0) myOut.append("\r\n");
    		myOut.append(myIterator.next().toString());
    	}
        return myOut.toString();
    }

}
