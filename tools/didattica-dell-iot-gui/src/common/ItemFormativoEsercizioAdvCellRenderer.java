/*******************************************************************************
 * Copyright (C) 2021 Daniele Iunco
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package common;

import java.awt.Color;
import java.awt.Component;
import java.io.File;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import javax.swing.border.Border;


//TODO serialVersionUID
public class ItemFormativoEsercizioAdvCellRenderer extends JLabel 
                                 implements ListCellRenderer<ItemFormativoEsercizioAdv>
	{
	private static final long serialVersionUID = 1L;
	private Border selectedBorder = null;

   public ItemFormativoEsercizioAdvCellRenderer()
   {
	   //TODO ora l'esistenza di una versione diversa rispetto al file .tex su disco � indicata con il colore giallo, trovare come mettere un'icona
	   //setIconTextGap(10); //in caso di icona

      // Swing labels default to being transparent; the container's color
      // shows through. To change a Swing label's background color, you must
      // first make the label opaque (by passing true to setOpaque()). Later,
      // you invoke setBackground(), passing the new color as the argument.

      setOpaque(true);

      // This border is placed around a cell that is selected and has focus.

      //esempio per evidenziare con un bordo l'elemento selezionato
      //selectedBorder = BorderFactory.createLineBorder(Color.YELLOW, 1);
      setText("Loading...");
   }

   @Override
   public Component getListCellRendererComponent(JList<? extends ItemFormativoEsercizioAdv> list,
		   ItemFormativoEsercizioAdv value,
                                                 int index,
                                                 boolean isSelected,
                                                 boolean cellHasFocus)
   {
      setText(value.getTitleForList());
      //setIcon(value.getFlagIcon()); //in caso di icona

      if (isSelected)

      {
         setBackground(list.getSelectionBackground());
         setForeground(list.getSelectionForeground());
      }
      else
      {
    	  String currXml = value.getXmlAbsolutePath();
    	  if (currXml == null) { //oggetto non abbinato a un file Xml, potrebbe essere un nuovo oggetto o un oggetto in cui il file xml non � ancora stato caricato, non ha versione
    		  setBackground(list.getBackground());
    	  } else if (value.risorseCollegateConCampiMancanti()) {
    		  setBackground(Color.ORANGE);
    	  } else {
    		  Integer currVersion = value.masterItem.getVersione();
    		  
        	  String sTextFileName = currXml.substring(currXml.lastIndexOf(File.separator)+1, currXml.lastIndexOf(".")) + ".tex";
        	  String sTexAbsolutePath = value.getPathStructure().texPath + File.separator + sTextFileName;

        	  try
        	  {
	        	  if (currVersion == null) {
	        		  setBackground(list.getBackground());
	        	  } else if (ItemFormativoEsercizioAdv.getLaTeXVersion(sTexAbsolutePath)!= currVersion.intValue()) {
	        		  setBackground(Color.YELLOW);
	        	  } else
	        		  setBackground(list.getBackground());
        	  }
        	  catch (Exception e) {
        		  e.printStackTrace();
        		  setBackground(Color.red);
        	  }
    	  }
    	  
          setForeground(list.getForeground());
      }

      setFont(list.getFont());

      setEnabled(list.isEnabled());

      if (isSelected && cellHasFocus && selectedBorder != null)
         setBorder(selectedBorder);
      else
         setBorder(null);

      return this;
   }
}