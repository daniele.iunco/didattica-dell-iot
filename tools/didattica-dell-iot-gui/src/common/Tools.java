/*******************************************************************************
 * Copyright (C) 2021 Daniele Iunco
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package common;

import java.awt.Component;
import java.awt.Frame;
import java.awt.Window;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileAlreadyExistsException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

/**
 * Metodi utili di vario tipo
 * @author Daniele Iunco
 * @version 1.0
 */
public abstract class Tools {

	/**
	 * Verifica l'esistenza del file
	 * @param path
	 * @return
	 */
	public static boolean fileExists(String path) {
		File myFile = new File(path);
		return myFile.exists() && !myFile.isDirectory();
	}

	/**
	 * Verifica l'esistenza della directory
	 * @param path
	 * @return
	 */
	public static boolean directoryExists(String path) {
		File myFile = new File(path);
		return myFile.exists() && myFile.isDirectory();
	}
	
	public static boolean isImage(String filePath) {
		if (filePath == null) return false;
		if (filePath.toLowerCase().endsWith(".png") || filePath.toLowerCase().endsWith(".png"))
			return true;
		return false;
	}

	/**
	 * Converte un oggetto List<String> contenente una o pi� stringhe in una stringa con un valore per riga
	 * @param listItems
	 * @return
	 */
	public static String arrayToMultiLineString(List<String> listItems) {
		StringBuilder result = new StringBuilder();
		if (listItems != null && listItems.size() > 0) {
			for (int i=0; i<listItems.size(); i++)
				result.append(listItems.get(i) + "\n");
		}
		return result.toString();
	}
	
    /**
     * restituisce true se almeno una delle stringhe in textList contiene almeno una delle stringhe in seasearchStrings
     * @param text
     * @param searchStrings
     * @return
     */
	public static boolean matchAnyStringInArray(List<String> textList, List<String> searchStrings) {
    	if (textList == null || textList.size() == 0 || searchStrings == null || searchStrings.size() == 0) return false;
    	
    	for (int i=0; i < textList.size(); i++) {
    		if (matchAnyStringInArray(textList.get(i), searchStrings)) {
    			return true;
    		}
    	}
    	
    	return false;
    }
    
    
    //TODO soluzione provvisoria per fare un confronto case insensitive, non va bene, usare regex o altro specifico per Java
    /**
     * restituisce true se text contiene almeno una delle stringhe in seasearchStrings
     * @param text
     * @param searchStrings
     * @return
     */
	public static boolean matchAnyStringInArray(String text, List<String> searchStrings) {
    	if (text == null || searchStrings == null || searchStrings.size() == 0) return false;
    	
    	for (int i=0; i < searchStrings.size(); i++) {
    		if (text.toLowerCase().contains(searchStrings.get(i).toLowerCase())) {
    			return true;
    		}
    	}
    	return false;
    }

	/**
	 * Scrive su file la stringa passata come parametro
	 * @param outputFileAbsolutePath
	 * @throws IOException 
	 */
	public static void saveTextToFile(String fileContents, String outputFileAbsolutePath) throws IOException {
		OutputStreamWriter myWriter = new OutputStreamWriter(new FileOutputStream(outputFileAbsolutePath), StandardCharsets.UTF_8);
    	myWriter.write(fileContents);
		myWriter.close();
	}
	
	
	/**
	 * Crea una finetra di dialogo per una notifica con il solo pulsante OK
	 * @param parent
	 * @param text
	 */
	public static void messageInfoBox(Component parent, String text) {
		JOptionPane.showMessageDialog(parent, text);
		return;
	}
		
	/**
	 * Crea una finetra di dialogo per chiedere una risposta con opzioni Si/No
	 * @param parent
	 * @param title
	 * @param message
	 * @param defaultText
	 * @return
	 */
	public static boolean yesOrNoBox(Component parent, String title, String message) {
		Window parentWindow = SwingUtilities.windowForComponent(parent); 
		Frame parentFrame = null;
		if (parentWindow instanceof Frame) parentFrame = (Frame)parentWindow;
		
	    int dialogResult = JOptionPane.showConfirmDialog (parentFrame, message, title, JOptionPane.YES_NO_OPTION);

	    return dialogResult == JOptionPane.YES_OPTION;
	}
	
	/**
	 * Crea una finetra di dialogo per chiedere un input testuale all'utente
	 * @param parent
	 * @param title
	 * @param message
	 * @param defaultText
	 * @return
	 */
	public static String inputBox(JDialog parent, String title, String message, String defaultText) {
		Window parentWindow = SwingUtilities.windowForComponent(parent); 
		Frame parentFrame = null;
		if (parentWindow instanceof Frame) parentFrame = (Frame)parentWindow;
		return inputBox(parentFrame, title, message, defaultText);
	}

	/**
	 * Crea una finetra di dialogo per chiedere un input testuale all'utente, versione con String
	 * @param parent
	 * @param title
	 * @param message
	 * @param defaultText
	 * @return
	 */
	public static String inputBox(Component parent, String title, String message, String defaultText) {
		String result = (String)JOptionPane.showInputDialog(
				parent,
				message, 
				title,            
	               JOptionPane.PLAIN_MESSAGE,
	               null,            
	               null, 
	               defaultText
	            );
		return result;
	}
	
	/**
	 * Crea una finetra di dialogo per chiedere un input testuale all'utente, versione con Float
	 * @param parent
	 * @param title
	 * @param message
	 * @param defaultText
	 * @return
	 */
	public static Float inputBox(Component parent, String title, String message, Float defaultValueFloat) {
		String sDefaultValue = "";
		if (defaultValueFloat != null) sDefaultValue = String.valueOf(defaultValueFloat.floatValue());
		String result = (String)JOptionPane.showInputDialog(
				parent,
				message, 
				title,            
	               JOptionPane.PLAIN_MESSAGE,
	               null,            
	               null, 
	               sDefaultValue
	            );
		if (result == null) return null;
		if (result.length() == 0) return null;
		try
		{
			float resultFloat = Float.parseFloat(result);
			return resultFloat;
		} catch (NumberFormatException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	//promemoria
	//JFrame ownerFrame = (JFrame) SwingUtilities.getWindowAncestor(this);
	//Window parentWindow = SwingUtilities.windowForComponent(this); 
	//Frame parentFrame = null;
	//if (parentWindow instanceof Frame) parentFrame = (Frame)parentWindow;

	public static List<String> stringToList(String rawKeyString) {
		if (rawKeyString==null || rawKeyString.length()==0) {
			return null;
		}
		List<String> parsed = Arrays.asList(rawKeyString.split(","));
		return parsed;
	}
	
	/**
	 * legge il contenuto di un file di testo, carica ogni riga in una HashMap con chiave=riga valore=riga
	 * @param filePath
	 * @return
	 */
	public static Map<String,String> textFileLinesToMap(String filePath){
		Map<String, String> result = new HashMap<String, String>(); 
		if (!fileExists(filePath)) return result;

		try {
			BufferedReader myReader = new BufferedReader(new FileReader(filePath));
			String line;
			while ((line = myReader.readLine()) != null) {
				//se c'� una riga ripetuta la devo ignorare 
				if (!result.containsKey(line)) result.put(line, line);
			}
			myReader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * rinomina il file sourceFile in destFile, se sourceFile non esiste non fa nulla, se destFile esiste solleva un'eccezione FileAlreadyExistsException, o, in caso di altri errori IOException
	 * @param sourceFile
	 * @param destFile
	 * @throws IOException 
	 */
	public static void renameFileIfExists(String sourceFile, String destFile) throws IOException {
		if (!fileExists(sourceFile)) return;
		if (fileExists(destFile)) throw new FileAlreadyExistsException(destFile);
		
		File toRename = new File(sourceFile);
		File bak = new File(destFile);
		//TODO dettaglio errore
		boolean renameResult = toRename.renameTo(bak);
		if (!renameResult) throw new IOException();
	}

	public static boolean hasText(String text) {
		if (text == null) return false;
		if (text.length() == 0) return false;
		return true;
	}
	
	public static String[] fromKeysToValues(String[] keys, KeyValuePairsFile dictionaryFile) {
		List<String> result = new ArrayList<String>();
		
		for (int i=0; i<keys.length; i++)
			if (dictionaryFile.containsKey(keys[i]))
				result.add(dictionaryFile.getItem(keys[i]).value);
			else
				result.add(keys[i]);

		return result.toArray(new String[result.size()]);
	}


	public static String createGVFileBT(String bottom, String[] top) {
		ArrayList<String> converted = new ArrayList<String>();
		converted.addAll(Arrays.asList(top));
		return createGVFileBT(bottom, converted);
		/*
		StringBuilder result = new StringBuilder();
		final String bottomQuoted = bottom.contains(" ") ? "\"" + bottom + "\"" : bottom;

		result.append("digraph " + bottom + "\n");
		result.append("{\n");
		result.append("    graph [rankdir = \"BT\", center=true, nodesep=0.02, ranksep=0.2]\n");
		result.append("\n");
		result.append("    node [shape=rectangle,style=filled,fillcolor=green] \n");   
		result.append("    \n");
		result.append("    " + bottomQuoted + "\n");
		result.append("    \n");
		result.append("    node [shape=oval,fillcolor=yellow]\n");
		for (String key : top) {
			result.append("    " + bottomQuoted + " -> " + (key.contains(" ") ? "\"" + key + "\"" : key) + "\n");
		}
		result.append("}\n");
		return result.toString();*/
	}
	
	public static String uuidNoDash() {
		String sUUID = java.util.UUID.randomUUID().toString().replace('-', 'x');
		return sUUID;
	}
	

	public static String createGVFileBT(String bottom, List<String> top) {
		StringBuilder result = new StringBuilder();
		final String bottomQuoted = bottom.contains(" ") ? "\"" + bottom + "\"" : bottom;

		final String nodeTop = "nodeTop";
		
		result.append("digraph " + bottom + "\n");
		result.append("{\n");
		result.append("    graph [rankdir = \"BT\"]\n");
		result.append("\n");
		result.append("    node [shape=rectangle,style=filled,fillcolor=green] \n");   
		result.append("    " + nodeTop + " [label=\"\\etichettanodoesercizio{" + bottomQuoted + "}\"]\n");
		result.append("    \n");
		result.append("    node [shape=oval,fillcolor=yellow]\n");
		
		StringBuilder nodeDef = new StringBuilder();
		
		StringBuilder nodeRelations = new StringBuilder();
		
		int iKeyChar = 0;
		for (String key : top) {
			char thisKey = (char)(iKeyChar+65); //da A a Z'
			String nodeName = "node" + String.valueOf(thisKey);
			nodeDef.append("    " + nodeName + " [label=\"\\etichettanodoprerequisito{" + key + "}\"]\n");
			nodeRelations.append("    " + nodeTop + " -> " + nodeName + "\n");
			iKeyChar++;
		}
		
		result.append(nodeDef);
		result.append("\n");
		
		result.append(nodeRelations);
		result.append("\n");
		
		result.append("}\n");
		return result.toString();
	}
	
	// VECCHIA VERSIONE (LR)
	public static String createGVFileLR(String left, String[] right) {
		StringBuilder result = new StringBuilder();
		final String leftQuoted = left.contains(" ") ? "\"" + left + "\"" : left;

		result.append("digraph " + left + "\n");
		result.append("{\n");
		result.append("    graph [rankdir = \"LR\"]\n");
		result.append("\n");
		result.append("    node [shape=rectangle,style=filled] \n");   
		result.append("    \n");
		result.append("    " + leftQuoted + "\n");
		result.append("    \n");
		result.append("    node [shape=oval,fillcolor=yellow]\n");
		for (String key : right) {
			result.append("    " + leftQuoted + " -> " + (key.contains(" ") ? "\"" + key + "\"" : key) + "\n");
		}
		result.append("}\n");
		return result.toString();
	}

	public static String createGVFileLR(String left, List<String> right) {
		StringBuilder result = new StringBuilder();
		final String leftQuoted = left.contains(" ") ? "\"" + left + "\"" : left;

		result.append("digraph " + left + "\n");
		result.append("{\n");
		result.append("    graph [rankdir = \"LR\"]\n");
		result.append("\n");
		result.append("    node [shape=rectangle,style=filled] \n");   
		result.append("    \n");
		result.append("    " + leftQuoted + "\n");
		result.append("    \n");
		result.append("    node [shape=oval,fillcolor=yellow]\n");
		for (String key : right) {
			result.append("    " + leftQuoted + " -> " + key + "\n");
		}
		result.append("}\n");
		return result.toString();
	}
}
