/*******************************************************************************
 * Copyright (C) 2021 Daniele Iunco
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

package common;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;

//TODO per generare i file nei vari formati sarebbe preferibile usare direttamente un BufferedWriter senza passare dallo StringBuilder intermedio e il relativo toString();

/**
 * Classe con metodi per funzionalit� aggiuntive per oggetti ItemFormativoEsercizio
 * si pu� istanziare leggendo il valore dei campi da un file xml oppure da un oggetto ItemFormativoEsercizio
 * tra cui metodi del tipo "public String to{formato}String(..." per l'esportazione in {formato}
 * 
 * TODO nel metodo missingMandatoryFields, definire quali campi vanno sempre valorizzati e la cui assenza deve far apparire nel titolo il prefisso "[ERROR]" 
 * 
 * @author Daniele Iunco
 * @version 1.0
 *
 */
public class ItemFormativoEsercizioAdv {
	public ItemFormativoEsercizio masterItem;
	private String xmlAbsolutePath;
	private final PathStructure pathStructure;
	
	public PathStructure getPathStructure() {
		return this.pathStructure;
	}
	
	public String getXmlAbsolutePath() {
		return xmlAbsolutePath;
	}
	
    public ItemFormativoEsercizioAdv(ItemFormativoEsercizio masterItem, PathStructure pathStructure){
    	this.masterItem = masterItem;
    	this.pathStructure = pathStructure;
    	xmlAbsolutePath = null;
    }

    /**
     * Istanzia l'oggetto senza un file XML, serve a creare un oggetto non abbinato a un file XML, ad esempio un nuovo esercizio, si pu� comunque caricare un file successivamente
     */
    public ItemFormativoEsercizioAdv(PathStructure pathStructure) {
    	this.masterItem = new ItemFormativoEsercizio();
    	this.pathStructure = pathStructure;
    	xmlAbsolutePath = null;
    }

    public boolean risorseCollegateConCampiMancanti() {
    	if (masterItem.getRisorseCollegate() == null || masterItem.getRisorseCollegate().size() == 0) return false;
    	
		List<RisorseCollegateType> risorseCollegate = masterItem.getRisorseCollegate();
		for (int i=0; i < risorseCollegate.size(); i++) {
			DatiRisorsaCollegata objDati = risorsaDaRisorsa(risorseCollegate.get(i));
			if (Tools.isImage(objDati.nomeFile)) {
				if (objDati.getTestoDescrittivo() == null || objDati.getTestoDescrittivo().length() == 0)
					return true;
			}
			else if (objDati.getIdentificativoFile() == null || objDati.getIdentificativoFile().length() == 0) 
				return true;
		}
		return false;
    }
    
    /**
     * Istanzia l'oggetto abbinando da subito un file XML, da creare o gi� esistente 
     * @param xmlAbsolutePath
     * @param createNew
     * @throws FileAlreadyExistsException
     * @throws FileNotFoundException
     */
    public ItemFormativoEsercizioAdv(String xmlAbsolutePath, boolean createNew, PathStructure pathStructure) throws FileAlreadyExistsException, FileNotFoundException {
    	this.masterItem = new ItemFormativoEsercizio();
    	this.pathStructure = pathStructure;
		loadFile(xmlAbsolutePath, createNew);
    }
    
	/**
	 * Carica nell'oggetto ItemFormativoEsercizio sottostante un file, o crea un nuovo oggetto vuoto abbinato a un nuovo file
	 */
	public void loadFile(String xmlAbsolutePath, boolean createNew) throws FileNotFoundException, FileAlreadyExistsException {
		if (!Tools.fileExists(xmlAbsolutePath) == !createNew) 
			if (!createNew) throw new FileNotFoundException(xmlAbsolutePath);
			else throw new FileAlreadyExistsException(xmlAbsolutePath);

		this.xmlAbsolutePath = xmlAbsolutePath;
		if (createNew) {
			this.masterItem = new ItemFormativoEsercizio();
			return;
		} else {
			try {
				JAXBContext jaxbContext = JAXBContext.newInstance( ItemFormativoEsercizio.class );
				Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
				Object result = jaxbUnmarshaller.unmarshal(new File(xmlAbsolutePath));
				@SuppressWarnings("unchecked") //se sto caricando davvero un ItemFormativoEsercizio non si verificano errori
				JAXBElement<ItemFormativoEsercizio> result2 = (JAXBElement<ItemFormativoEsercizio>)result;
				this.masterItem = (ItemFormativoEsercizio) result2.getValue();
		    	this.xmlAbsolutePath = xmlAbsolutePath;
			}
			catch (Exception e1) {
				e1.printStackTrace();
			}
		}
	}
	
    public String getTitleForList() {
    	return (missingMandatoryFields() ? "[ERROR][" : "[") + masterItem.identificatoreArgomento + "] " + masterItem.nomeDescrittivo;
    }

    /**
     * verifica se i prerequisiti dell'esercizio sono soddisfatti da quelli passati come parametro  
     * @param selectedPrerequisites se null o array vuoto nessuna verifica, risultato true
     * @return
     */
	public boolean matchPrerequisites(String[] selectedPrerequisites) {
		if (selectedPrerequisites == null || selectedPrerequisites.length == 0) return true;
		boolean result = false;
		String parts[] = masterItem.prerequisiti.split(",");

		HashMap<String,String> selectedPrerequisitesHashMap = new HashMap<String,String>();
		for (String key : selectedPrerequisites) {
			selectedPrerequisitesHashMap.put(key.toLowerCase(), key);
		}
		/*for (int i=0; i < parts.length && result; i++) {
			if (!selectedPrerequisitesHashMap.containsKey(parts[i].trim().toLowerCase()))
				result = false;
		}*/
		for (int i=0; i < parts.length; i++) {
			if (selectedPrerequisitesHashMap.containsKey(parts[i].trim().toLowerCase()))
				return true;
		}
	    return result;
    }
    
	/**
	 * 
	 * @param titleKeys 			se non null, il risultato deve contenere una delle stringhe
	 * @param componentsKeys 		se non null, il risultato deve contenere una delle stringhe
	 * @param anyFieldKeys 			se non null, il risultato deve contenere una delle stringhe
	 * @param pblNewLearningGoal 	verificato solo se true
	 * @param pblProblem			verificato solo se true
	 * @param onlyWithMissingFields	verificato solo se true
	 * @return true se corrisponde ai criteri di ricerca
	 */
    public boolean match(List<String> titleKeys, List<String> componentsKeys, List<String> anyFieldKeys, boolean pblNewLearningGoal, boolean pblProblem, boolean onlyWithMissingFields) {
    	if (titleKeys != null && titleKeys.size() > 0 && !Tools.matchAnyStringInArray(masterItem.nomeDescrittivo, titleKeys)) return false;
    	if (componentsKeys != null && componentsKeys.size() > 0 && !Tools.matchAnyStringInArray(masterItem.getMdcomponentlist(), componentsKeys)) return false;
    	if (pblNewLearningGoal && !masterItem.isMdPblnewlearninggoal()) return false;
    	if (pblProblem && !masterItem.isMdPblproblem()) return false;
    	if (onlyWithMissingFields && !missingMandatoryFields()) return false;
    	
    	
    	ArrayList<String> stringsToSearch = new ArrayList<String>();
    	
    	if (anyFieldKeys != null && anyFieldKeys.size() > 0) {
    		
    		if (masterItem.risorseCollegate!=null)
    			//TODO
	
			if (masterItem.mdwhy!=null)
				stringsToSearch.add(masterItem.mdwhy);
			
			if (masterItem.mdextras!=null)
				stringsToSearch.add(masterItem.mdextras);
			
			if (masterItem.mdexpectedresult!=null)
				stringsToSearch.add(masterItem.mdexpectedresult);
	
			if (masterItem.mdcommonerrors!=null)
				stringsToSearch.add(masterItem.mdcommonerrors);
			
			if (masterItem.mderraticbehavior!=null)
				stringsToSearch.add(masterItem.mderraticbehavior);
			
			if (masterItem.mdlessonlearned!=null)
				stringsToSearch.add(masterItem.mdlessonlearned);
			
			if (masterItem.mdsolution!=null)
				stringsToSearch.add(masterItem.mdsolution);
			
			if (masterItem.mdtodolist!=null)
				stringsToSearch.addAll(masterItem.mdtodolist);
			
			if (masterItem.mdprerequisitelist!=null)
				stringsToSearch.addAll(masterItem.mdprerequisitelist);
			
			if (masterItem.mdcomponentlist!=null)
				stringsToSearch.addAll(masterItem.mdcomponentlist);
			
			if (masterItem.mdpbldetails!=null)
				stringsToSearch.add(masterItem.mdpbldetails);
			
			return Tools.matchAnyStringInArray(stringsToSearch, anyFieldKeys);
    	}

    	
		return true;
    }
    
    /**
     * 
     * @return true se alcuni campi necessari non sono valorizzati
     */
	public boolean missingMandatoryFields() {
		if (masterItem.mdwhy == null || masterItem.mdwhy.length() == 0) return true;
		if (masterItem.mdexpectedresult == null || masterItem.mdexpectedresult.length() == 0) return true;
		//if (masterItem.mderraticbehavior == null || masterItem.mderraticbehavior.length() == 0) return true;
		if (masterItem.mdlessonlearned == null || masterItem.mdlessonlearned.length() == 0) return true;
		//if (masterItem.hrsolution == null || masterItem.hrsolution.length() == 0) return true;
		if (masterItem.getMdtodolist() != null && masterItem.getMdtodolist().size() > 0) return true;
		return false;	
	}

	private String subSectionHtmlBlock(String title, List<String> listItems, String postText) {
		StringBuilder result = new StringBuilder();
		if (title != null && title.length() > 0) result.append("<h2 style=\"margin-top: 20px\">" + title + ":</h2>\n");
		if (listItems != null && listItems.size() > 0) {
			result.append("<ul>\n");
			for (int i=0; i<listItems.size(); i++)
				result.append("<li>" + listItems.get(i) + "</li>\n");
			result.append("</ul>\n");
		}
		if (postText != null && postText.length() > 0)
			result.append(postText.replace("\n", "<br />\n") + "<br />\n");
		return result.toString();
	}
	
	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
	
		result.append(masterItem.nomeDescrittivo + "\n");

		if (masterItem.getMdprerequisitelist()!= null)
			result.append(Tools.arrayToMultiLineString(masterItem.getMdprerequisitelist()) + "\n");
		
		if (masterItem.getMdcomponentlist()!= null)
			result.append(Tools.arrayToMultiLineString(masterItem.getMdcomponentlist()) + "\n");

		if (masterItem.getMdwhy()!= null)
			result.append(masterItem.getMdwhy() + "\n");

		if (masterItem.getMdexpectedresult()!= null)
			result.append(masterItem.getMdexpectedresult() + "\n");

		if (masterItem.getMdextras()!= null)
			result.append(masterItem.getMdextras() + "\n");

		if (masterItem.getMdcommonerrors()!= null)
			result.append(masterItem.getMdcommonerrors() + "\n");

		if (masterItem.getMderraticbehavior()!= null)
			result.append(masterItem.getMderraticbehavior() + "\n");

		if (masterItem.getMdlessonlearned()!= null)
			result.append(masterItem.getMdlessonlearned() + "\n");

		if (masterItem.getMdpbldetails() != null)
			result.append(masterItem.getMdpbldetails() + "\n");

		if (masterItem.getMdsolution()!= null)
			result.append(masterItem.getMdsolution() + "\n");

		return result.toString();
	}
	
	public String getTeXIndexString() {
		StringBuilder result = new StringBuilder();
		String parts[] = masterItem.prerequisiti.split(",");
		for (String part : parts) {
			result.append("\\index{" + escapeTeXChars(part) + "!" + escapeTeXChars(masterItem.getNomeDescrittivo()) + "}");
		}
		return result.toString();
	}
		
	//TODO fare un toLaTeXString()
	/*
	 * definire una serie di comandi LaTeX per poi poterli usare nel toLaTeXString
	 * 
	 * \prerequisito{testoqui123}
	 * \begin{componenti}
	 * \componente{componente}
	 * \end{componenti}
	 * 
	 * etc
	 * 
	 */
	
	private String figuraConTemplateTeX(DatiRisorsaCollegata figura) {
		StringBuilder result = new StringBuilder();
		
		String idFigura;
		if (Tools.hasText(figura.identificativoFile)){
			idFigura = "fig:" + figura.identificativoFile;
		} else {
			idFigura = "fig:" + UUID.randomUUID().toString();
		}

		if (Tools.hasText(figura.testoDescrittivo))
			result.append(figura.testoDescrittivo);
		else
			result.append("Esempio soluzione");
		
		result.append(" in figura \\ref{" + idFigura + "}.\n");
		
		result.append(imageToLaTeXString(figura,idFigura));
		
		return result.toString();
	}
	
	private String generaBloccoFigureTeX() {
		StringBuilder result = new StringBuilder();
		
		if (masterItem.getRisorseCollegate() != null && masterItem.getRisorseCollegate().size() > 0) {
			result.append("\n");
			
			List<RisorseCollegateType> risorseCollegate = masterItem.getRisorseCollegate();
			for (int i=0; i < risorseCollegate.size(); i++)
				if (Tools.isImage(nomeFileDaRisorsa(risorseCollegate.get(i))))
				{
					result.append(figuraConTemplateTeX(risorsaDaRisorsa(risorseCollegate.get(i))));
				}
		}
		
		return result.toString();
	}
	
	private boolean needLabel() {
		//nella versione attuale l'unico cosa che pu� avere un link all'esercizio � il codice in appendice, quindi cerco file .ino
		
		List<RisorseCollegateType> risorseCollegate = masterItem.getRisorseCollegate();
		
		for (int i=0; i < risorseCollegate.size(); i++)
			if ( nomeFileDaRisorsa(risorseCollegate.get(i)).toLowerCase().endsWith(".ino"))
				return true;
		
		return false;
	}

	public Tuple<String,String> toLaTeXString(PathStructure projectPaths, boolean codiceInAppendice, String customPathSeparator) throws IOException {
		StringBuilder result = new StringBuilder();
		StringBuilder resultAppendice = new StringBuilder();
		
		//TODO metodo per ridurre il codice poco performante!
		
		int iVersioneFix = -1;
		if (masterItem.getVersione() != null) 
			iVersioneFix = masterItem.getVersione().intValue();
		else
			iVersioneFix = -1;
		
		result.append("%versione=" + String.valueOf(iVersioneFix) + "\n");
		
		result.append("\\section{" + (missingMandatoryFields() ? "[TODO]" : "") + escapeTeXChars(masterItem.nomeDescrittivo) + "}\n");
		if (needLabel()) result.append("\\label{" + masterItem.identificatoreArgomento + "}\n");
		result.append(getTeXIndexString());

		result.append("\\titoloparagrafo{Prerequisiti:}\n");

		/*result.append("\\begin{compactitem}\n");
		if (masterItem.getMdprerequisitelist()!= null)
			for (int i=0;i<masterItem.getMdprerequisitelist().size();i++ )
				result.append("\\item " + escapeTeXChars(masterItem.getMdprerequisitelist().get(i).trim()) + "\n");
		result.append("\\end{compactitem}\n");
		result.append("\n");*/

		//result.append("%\\adjustbox{center,max width=\\textwidth,max height=.33\\textheight}\n");
		result.append("\\adjustbox{max width=\\textwidth}{\n");
		result.append("\\begin{dot2tex}[dot,options=-t raw]\n");
		result.append("\\input{" + pathStructure.gvPath.mapRelativeSubPath(gvFileName(), customPathSeparator) + "}\n");
		result.append("\\end{dot2tex}\n");
		result.append("}\n");
		result.append("\n");

		result.append("\\titoloparagrafo{Componenti:}\n");
		result.append("\\begin{compactitem}\n");
		if (masterItem.getMdcomponentlist()!= null)
			for (int i=0;i<masterItem.getMdcomponentlist().size();i++ )
				result.append("\\item " + escapeTeXChars(masterItem.getMdcomponentlist().get(i).trim()) + "\n");
		result.append("\\end{compactitem}\n");
		result.append("\n");
		
		
		if (Tools.hasText(masterItem.getMdexpectedresult())) {
			result.append("\n");
			result.append("\\titoloparagrafo{Descrizione e risultato atteso:}\n");
			result.append(masterItem.getMdexpectedresult() == null || masterItem.getMdexpectedresult().length() == 0 ? "N/A" : escapeTeXChars(masterItem.getMdexpectedresult()));
			result.append("\n");
		}

		if (Tools.hasText(masterItem.getMdwhy())) {
			result.append("\n");
			result.append("\\titoloparagrafo{Scopo dell'esercizio:}\n");
			result.append(escapeTeXChars(masterItem.getMdwhy()));
			result.append("\n");
		}

		if (Tools.hasText(masterItem.getMdextras())) {
			result.append("\n");
			result.append("\\titoloparagrafo{Eventuali funzionalit� aggiuntive:}\n");
			result.append(masterItem.getMdextras() == null || masterItem.getMdextras().length() == 0 ? "N/A" : escapeTeXChars(masterItem.getMdextras()));
			result.append("\n");
		}

		if (Tools.hasText(masterItem.getMdcommonerrors())) {
			result.append("\n");
			result.append("\\titoloparagrafo{Errori comuni:}\n");
			result.append(masterItem.getMdcommonerrors() == null || masterItem.getMdcommonerrors().length() == 0 ? "N/A" : escapeTeXChars(masterItem.getMdcommonerrors()));
			result.append("\n");
		}
		
		if (Tools.hasText(masterItem.getMderraticbehavior())) {
			result.append("\n");
			result.append("\\titoloparagrafo{Possibile comportamento erratico:}\n");
			result.append(masterItem.getMderraticbehavior() == null || masterItem.getMderraticbehavior().length() == 0 ? "N/A" : escapeTeXChars(masterItem.getMderraticbehavior()));
			result.append("\n");
		}

		if (masterItem.mdPblproblem != null && masterItem.mdPblproblem)
		{
			result.append("\n");
			result.append("\\titoloparagrafo{Validit� per PBL:}\n");
			result.append("Problema da definire e analizzare: Si.\n");
			if (masterItem.mdPblnewlearninggoal != null && masterItem.mdPblnewlearninggoal)
				result.append("Necessaria individuazione nuovi obiettivi di apprendimento: Si.\n");
			else
				result.append("Necessaria individuazione nuovi obiettivi di apprendimento: No.\n");
			
			result.append("\\titoloparagrafo{Dettagli obiettivo di apprendimento atteso:}\n");
			result.append(masterItem.getMdpbldetails() == null || masterItem.getMdexpectedresult().length() == 0 ? "N/A" : escapeTeXChars(masterItem.getMdpbldetails()));
			result.append("\n");
		}
		
		if (Tools.hasText(masterItem.getMdlessonlearned())) {
			result.append("\n");
			result.append("\\titoloparagrafo{Risultato di apprendimento:}\n");
			result.append(masterItem.getMdlessonlearned() == null || masterItem.getMdlessonlearned().length() == 0 ? "N/A" : escapeTeXChars(masterItem.getMdlessonlearned()));
			result.append("\n");
		}

		if (Tools.hasText(masterItem.getMdsolution())) {
			result.append("\n");
			result.append("\\titoloparagrafo{Indicazioni per la soluzione:}\n");
			result.append(masterItem.getMdsolution() == null || masterItem.getMdexpectedresult().length() == 0 ? "N/A" : escapeTeXChars(masterItem.getMdsolution()));
			String bloccoFigure=generaBloccoFigureTeX();
			if (bloccoFigure != null && bloccoFigure.length() > 0) {
				result.append(bloccoFigure);
				result.append("\n");
			}
			result.append("\n");
		}

		if (masterItem.getRisorseCollegate() != null && masterItem.getRisorseCollegate().size() > 0) {
			result.append("\n");
			
			List<RisorseCollegateType> risorseCollegate = masterItem.getRisorseCollegate();
			
			StringBuilder resultCodice = new StringBuilder();
			StringBuilder resultCodiceAppendice = new StringBuilder();
			
			for (int i=0; i < risorseCollegate.size(); i++)
			{
				if ( nomeFileDaRisorsa(risorseCollegate.get(i)).toLowerCase().endsWith(".ino")) {
					DatiRisorsaCollegata risorsaIno = risorsaDaRisorsa(risorseCollegate.get(i));
					if (codiceInAppendice) {
						resultCodice.append("\\hyperref[" + escapeTeXChars(risorsaIno.getIdentificativoFile()) + "]{Codice di esempio: " + escapeTeXChars(risorsaIno.getNomeFile()) + "}\n");
						
						if (resultCodiceAppendice.length() > 0) resultCodiceAppendice.append("\n\n");
						resultCodiceAppendice.append("\\titoloparagrafo{File " + escapeTeXChars(nomeFileDaRisorsa(risorseCollegate.get(i))) + ":}\n");
						resultCodiceAppendice.append("\\label{" + escapeTeXChars(risorsaIno.getIdentificativoFile()) +"}\n");
						resultCodiceAppendice.append("\\begin{lstlisting}[\n");
						resultCodiceAppendice.append("basicstyle=\\tiny\n");
						resultCodiceAppendice.append("]\n");
						resultCodiceAppendice.append(Files.readString(Path.of(projectPaths.sketchPath.getItemPath() + File.separator + nomeFileDaRisorsa(risorseCollegate.get(i)))) + "\n");
						resultCodiceAppendice.append("\\end{lstlisting}\n");
					}
					else {
						resultCodice.append("\\titoloparagrafo{" + escapeTeXChars(nomeFileDaRisorsa(risorseCollegate.get(i))) + ":}\\\\\n");
						resultCodice.append("\\begin{lstlisting}[\n");
						resultCodice.append("basicstyle=\\tiny\n");
						resultCodice.append("]\n");
						resultCodice.append(Files.readString(Path.of(projectPaths.sketchPath.getItemPath() + File.separator + nomeFileDaRisorsa(risorseCollegate.get(i)))) + "\n");
						resultCodice.append("\\end{lstlisting}\n");
					}
				}
			}

			if (resultCodice.length() > 0)
			{
				result.append("\n");
				result.append("\\titoloparagrafo{Codice di esempio:}\n");
				result.append(resultCodice);
				result.append("\n");
			}
			
			if (resultCodiceAppendice.length() > 0)
			{
				resultAppendice.append("\n");
				resultAppendice.append("\\section{Esercizio ``\\hyperref[" + masterItem.identificatoreArgomento + "]{" + escapeTeXChars(masterItem.nomeDescrittivo) + "}''}\n");
				resultAppendice.append(resultCodiceAppendice);
				result.append("\n");
			}
			
			
		}
		
		if (masterItem.getMdtodolist() != null) {
			List<String> lstTodo = masterItem.getMdtodolist();
			for (int i=0; i < lstTodo.size(); i++)
					result.append("\\todo{" + escapeTeXChars(lstTodo.get(i)) + "}\n");
		}
		
		return new Tuple<String,String> (result.toString(), resultAppendice.toString());
	}
	
	/**
	 * restituisce il numero di versione del file, -1 se il file non esiste
	 * @param filePath
	 * @return
	 * @throws IOException
	 */
	public static int getLaTeXVersion(String filePath) throws IOException {
		int result = -1;
		
		if (!Tools.fileExists(filePath)) return result;
		
		BufferedReader myReader = new BufferedReader(new FileReader(filePath));
		String firstLine = myReader.readLine();
		myReader.close();
		
		final String prefix = "%versione=";
		// il formato della prima riga � %versione=numero
		
		if (firstLine.startsWith(prefix))
			result = Integer.valueOf(firstLine.substring(prefix.length()));
		
		return result;
	}
	
	private String imageToLaTeXString(DatiRisorsaCollegata risorsa) {
		if (risorsa == null) return "";
		float widthScale= 0.9f;
		if (risorsa.getFScaleWidth() != null)
			widthScale = risorsa.getFScaleWidth().floatValue();
		return imageToLaTeXString( risorsa.getNomeFile(),  risorsa.getTestoDescrittivo(),  risorsa.getIdentificativoFile(), widthScale);
	}

	private String imageToLaTeXString(DatiRisorsaCollegata risorsa, String idUnicoPersonalizzato) {
		if (risorsa == null) return "";
		float widthScale= 0.9f;
		if (risorsa.getFScaleWidth() != null)
			widthScale = risorsa.getFScaleWidth().floatValue();
		return imageToLaTeXString( risorsa.getNomeFile(),  risorsa.getTestoDescrittivo(), idUnicoPersonalizzato, widthScale);
	}
	
	private String imageToLaTeXString(String nomeFile, String didascalia, String texLabel, float widthScale) {
		StringBuilder result = new StringBuilder();
		
		result.append("\\begin{figure}\n");
		result.append("	\\begin{center}\n");
		result.append("\\includegraphics[width=" + String.valueOf(widthScale) + "\\textwidth]{" + nomeFile.substring(0, nomeFile.lastIndexOf('.')) + "}\n");
		if (didascalia != null && didascalia.length() > 0)
			result.append("		\\caption{" + didascalia + "}\n");
		if (texLabel != null && texLabel.length() > 0)
			result.append("		\\label{" + texLabel + "}\n");
		
		result.append("\\end{center}\n");
		result.append("\\end{figure}\n");
		
		return result.toString();
	}
	
	public String toHtmlString(PathStructure projectPaths) throws IOException {
		StringBuilder result = new StringBuilder();
		
		//TODO poco performante!
		//TODO si dovrebbero anche gestire pi� percorsi possibili tra cui cercare gli elementi dell'array attachmentsPath, le immagini, etc
		
		result.append("<html>\n");
		result.append("    <head>\n");
		result.append("        <title>" + (missingMandatoryFields() ? "[TODO]" : "") + masterItem.nomeDescrittivo + "</title>\n");
		result.append("    </head>\n");
		result.append("    <body>\n");
		result.append("        <h1>" + (missingMandatoryFields() ? "[TODO]" : "") + masterItem.nomeDescrittivo + "</h1>\n");

		if (masterItem.getMdprerequisitelist()!= null)
			result.append(subSectionHtmlBlock("Prerequisiti",masterItem.getMdprerequisitelist(),null));

		if (masterItem.getMdcomponentlist()!= null)
			result.append(subSectionHtmlBlock("Componenti",masterItem.getMdcomponentlist(),null));

		if (masterItem.getMdwhy()!= null)
			result.append(subSectionHtmlBlock("Scopo dell'esercizio",null,masterItem.getMdwhy()));

		if (masterItem.getMdexpectedresult()!= null)
			result.append(subSectionHtmlBlock("Descrizione e risultato atteso",null,masterItem.getMdexpectedresult()));

		if (masterItem.getMdextras()!= null)
			result.append(subSectionHtmlBlock("Eventuali funzionalit� aggiuntive",null,masterItem.getMdextras()));

		if (masterItem.getMdcommonerrors()!= null)
			result.append(subSectionHtmlBlock("Errori comuni",null,masterItem.getMdcommonerrors()));

		if (masterItem.getMderraticbehavior()!= null)
			result.append(subSectionHtmlBlock("Possibile comportamento erratico",null,masterItem.getMderraticbehavior()));

		if (masterItem.getMdlessonlearned()!= null)
			result.append(subSectionHtmlBlock("Risultato di apprendimento",null,masterItem.getMdlessonlearned()));

		if (masterItem.mdPblproblem != null && masterItem.mdPblproblem)
		{
			result.append("<br />\n");
			result.append("<h2 style=\"margin-top: 20px\">Validit� per PBL:</h2>\n");
			result.append("Problema da definire e analizzare: Si<br />\n");
			if (masterItem.mdPblnewlearninggoal != null && masterItem.mdPblnewlearninggoal)
				result.append("Necessaria individuazione nuovi obiettivi di apprendimento: Si<br />\n");
			else
				result.append("Necessaria individuazione nuovi obiettivi di apprendimento: No<br />\n");
			
			result.append("Dettagli obiettivo di apprendimento atteso:<br />\n");
			
			result.append(masterItem.getMdpbldetails() == null || masterItem.getMdexpectedresult().length() == 0 ? "N/A" : masterItem.getMdpbldetails().replace("\n", "<br />\n"));
			result.append("<br />\n");
			
		}

		if (masterItem.getMdsolution()!= null)
			result.append(subSectionHtmlBlock("Indicazioni per la soluzione",null,masterItem.getMdsolution()));
		
		if (masterItem.getRisorseCollegate() != null) {
			List<RisorseCollegateType> risorseCollegate = masterItem.getRisorseCollegate();
			for (int i=0; i < risorseCollegate.size(); i++)
			{
				if (Tools.isImage(nomeFileDaRisorsa(risorseCollegate.get(i)))) {
					StringBuilder fixedAttachmentPath = new StringBuilder();
					if (projectPaths.imagePath != null && projectPaths.imagePath.isValid()) {
						fixedAttachmentPath.append(projectPaths.imagePath);
						if (!projectPaths.imagePath.getItemPath().endsWith(File.separator)) fixedAttachmentPath.append(File.separator); 
						fixedAttachmentPath.append(nomeFileDaRisorsa(risorseCollegate.get(i)));
						result.append("<img src=\"" + fixedAttachmentPath.toString() + "\" alt=\"\" style=\"margin-left:auto;margin-right:auto;max-width: 600px\" /><br />\n");
					}
				}
			}
			for (int i=0; i < risorseCollegate.size(); i++)
			{
				if (nomeFileDaRisorsa(risorseCollegate.get(i)).toLowerCase().endsWith(".ino")) {
					StringBuilder fixedAttachmentPath = new StringBuilder();
					if (projectPaths.sketchPath != null && projectPaths.sketchPath.isValid()) {
						fixedAttachmentPath.append(projectPaths.sketchPath);
						if (!projectPaths.sketchPath.getItemPath().endsWith(File.separator)) fixedAttachmentPath.append(File.separator); 
						fixedAttachmentPath.append(risorseCollegate.get(i));
						result.append("<pre>" +  Files.readString(Path.of(fixedAttachmentPath.toString())) + "</pre>\n");
					}
				}
			}
		}
		
		result.append("    </body>\n");
		result.append("</html>");
		return result.toString();
	}

	public String toMdString(PathStructure projectPaths) throws IOException {
		StringBuilder result = new StringBuilder();
		
		//TODO metodo per ridurre il codice poco performante!
		
		result.append("<!--<metadata>\n");
		result.append("<title>" + masterItem.nomeDescrittivo + "</title>\n");
		result.append("<tags>" + masterItem.tags + "</tags>\n");
		result.append("<prerequisites>" + masterItem.prerequisiti + "</prerequisites>\n");
		result.append("</metadata>-->\n");
		
		result.append((missingMandatoryFields() ? "[TODO]" : "") + masterItem.nomeDescrittivo + "\n");
		result.append("====\n");
		
		result.append("\n");
		result.append("Prerequisiti:{-}\n");
		result.append("----\n");
		if (masterItem.getMdprerequisitelist()!= null)
			for (int i=0;i<masterItem.getMdprerequisitelist().size();i++ )
				result.append("- " + masterItem.getMdprerequisitelist().get(i).trim() + "\n");
				
		result.append("\n");
		result.append("Componenti:{-}\n");
		result.append("----\n");
		if (masterItem.getMdcomponentlist()!= null)
			for (int i=0;i<masterItem.getMdcomponentlist().size();i++ )
				result.append("- " + masterItem.getMdcomponentlist().get(i).trim() + "\n");

		result.append("\n");
		result.append("Descrizione e risultato atteso:{-}\n");
		result.append("----\n");
		result.append(masterItem.getMdexpectedresult() == null || masterItem.getMdexpectedresult().length() == 0 ? "N/A" : masterItem.getMdexpectedresult());
		result.append("\n");

		result.append("\n");
		result.append("Scopo dell'esercizio:{-}\n");
		result.append("----\n");
		result.append(masterItem.getMdwhy());
		result.append("\n");

		result.append("\n");
		result.append("Eventuali funzionalit� aggiuntive:{-}\n");
		result.append("----\n");
		result.append(masterItem.getMdextras() == null || masterItem.getMdextras().length() == 0 ? "N/A" : masterItem.getMdextras());
		result.append("\n");

		result.append("\n");
		result.append("Errori comuni:{-}\n");
		result.append("----\n");
		result.append(masterItem.getMdcommonerrors() == null || masterItem.getMdcommonerrors().length() == 0 ? "N/A" : masterItem.getMdcommonerrors());
		result.append("\n");

		result.append("\n");
		result.append("Possibile comportamento erratico:{-}\n");
		result.append("----\n");
		result.append(masterItem.getMderraticbehavior() == null || masterItem.getMderraticbehavior().length() == 0 ? "N/A" : masterItem.getMderraticbehavior());
		result.append("\n");

		if (masterItem.mdPblproblem != null && masterItem.mdPblproblem)
		{
			result.append("\n");
			result.append("Validit� per PBL:{-}\n");
			result.append("----\n");
			result.append("Problema da definire e analizzare: Si\n");
			if (masterItem.mdPblnewlearninggoal != null && masterItem.mdPblnewlearninggoal)
				result.append("Necessaria individuazione nuovi obiettivi di apprendimento: Si\n");
			else
				result.append("Necessaria individuazione nuovi obiettivi di apprendimento: No\n");
			
			result.append("Dettagli obiettivo di apprendimento atteso:\n");
			result.append(masterItem.getMdpbldetails() == null || masterItem.getMdexpectedresult().length() == 0 ? "N/A" : masterItem.getMdpbldetails());
			result.append("\n");
		}
		
		result.append("\n");
		result.append("Risultato di apprendimento:{-}\n");
		result.append("----\n");
		result.append(masterItem.getMdlessonlearned() == null || masterItem.getMdlessonlearned().length() == 0 ? "N/A" : masterItem.getMdlessonlearned());
		result.append("\n");

		result.append("\n");
		result.append("Indicazioni per la soluzione:{-}\n");
		result.append("----\n");
		result.append(masterItem.getMdsolution() == null || masterItem.getMdexpectedresult().length() == 0 ? "N/A" : masterItem.getMdsolution());
		result.append("\n");
		
		if (masterItem.getRisorseCollegate() != null && masterItem.getRisorseCollegate().size() > 0) {
			result.append("\n");
			
			List<RisorseCollegateType> risorseCollegate = masterItem.getRisorseCollegate();
			for (int i=0; i < risorseCollegate.size(); i++)
				if (Tools.isImage(nomeFileDaRisorsa(risorseCollegate.get(i))))
				{
					if (i > 0) result.append("\n-\n");
					result.append("![Esempio](./images/" + nomeFileDaRisorsa(risorseCollegate.get(i)) + ")\n");
				}
			
			for (int i=0; i < risorseCollegate.size(); i++)
			{
				if (nomeFileDaRisorsa(risorseCollegate.get(i)).toLowerCase().endsWith(".ino")) {
					result.append("\n");
					result.append("Codice di esempio (" + risorseCollegate.get(i) + ")\n");
					result.append("----\n");
					result.append("```cpp\n\n" +  Files.readString(Path.of(projectPaths.sketchPath.getItemPath() + File.separator + risorseCollegate.get(i))) + "```\n\n");
				}
			}
		}
		
		if (masterItem.getMdtodolist() != null) {
			List<String> lstTodo = masterItem.getMdtodolist();
			for (int i=0; i < lstTodo.size(); i++)
					result.append("\\todo{" + lstTodo.get(i) + "}\n");
		}
		
		return result.toString();
	}
	
	/**
	 * provvisorio
	 * @param source
	 * @return
	 */
	private static String escapeTeXChars(String source) {
		// attenzione, la stringa \ deve essere la prima, altrimenti fa l'escape di ogni \
		String[] reserved = new String[] {"\\", "&", "%", "$", "#", "_", "{", "}", "~", "^"};
		String result = source;
		for (int i = 0; i < reserved.length; i++) {
			result = result.replace(reserved[i], "\\" + reserved[i]);
		}
		return result;
	}
	
	public static String nomeFileDaRisorsa(RisorseCollegateType risorsa) {
		DatiRisorsaCollegata trovata = risorsaDaRisorsa(risorsa);
		if (trovata == null) return ""; 
		return trovata.getNomeFile();
	}
	
	/**
	 * trasforma l'eventuale stringa in oggetto DatiRisorsaCollegata, o restituisce l'oggetto DatiRisorsaCollegata
	 * @param risorsa
	 * @return
	 */
	public static DatiRisorsaCollegata risorsaDaRisorsa(RisorseCollegateType risorsa) {
		// RisorseCollegateType pu� contenere 0 o 1 valori String o DatiRisorsaCollegata
		// tuttavia formattando l'xml con indentazione si introducono spazi trattati poi come stringhe di spazi che finiscono in contents
		// li devo ignorare
		List<Serializable>contents = risorsa.getContent();
		if (contents == null || contents.size() == 0) return null;
		for (int i = 0; i < contents.size(); i++)
		{
			if (contents.get(i).getClass().isAssignableFrom(String.class) && ((String)contents.get(i)).trim().length() > 0) {
				DatiRisorsaCollegata result = new DatiRisorsaCollegata();
				result.setNomeFile((String)contents.get(i));
				return result;
			} else
				if (contents.get(i).getClass().isAssignableFrom(JAXBElement.class)) {
					@SuppressWarnings("unchecked") //se sto caricando un file XML corretto, allora qui non si possono verificare errori
					JAXBElement<DatiRisorsaCollegata> risorsaJAXB = (JAXBElement<DatiRisorsaCollegata>)contents.get(i);
					return risorsaJAXB.getValue();
				}
			
		}
		return null;
	}
	
	public String toNodo() {
		return masterItem.identificatoreArgomento.toUpperCase();
	}

	public String toNodoConArchi() {
		StringBuilder sbResult = new StringBuilder();
		List<String> archi = Tools.stringToList(masterItem.getPrerequisiti());
		for (String s : archi) {
			sbResult.append(masterItem.identificatoreArgomento.toUpperCase() + " -> " + s + "\n");
		}
		return sbResult.toString();
	}
	
	public static boolean risorsaVuota(RisorseCollegateType risorsa) {
		List<Serializable>contents = risorsa.getContent();
		if (contents == null || contents.size() == 0) return true;
		if (contents.get(0).getClass().isAssignableFrom(String.class) && ((String)contents.get(0)).trim().length()==0) {
			return true;
		}
		return false;
	}

	/**
	 * ricarica il contenuto dal file xml, se specificato
	 * @throws FileNotFoundException
	 */
	public void reloadFromUnderlyingXml() throws FileNotFoundException {
		if (this.xmlAbsolutePath != null && Tools.fileExists(xmlAbsolutePath))
			try {
				loadFile(xmlAbsolutePath, false);
			} catch (FileAlreadyExistsException e) {
				// avendo specificato createNew = false, questo evento non si pu� verificare
				e.printStackTrace();
			} 
	}

	public String gvFileName() {
		return "esercizio-" + masterItem.getIdentificatoreArgomento() + ".gv";
	}

	public boolean saveFile() {
		if (xmlAbsolutePath==null || xmlAbsolutePath.length() == 0) return false;
		OutputStream os = null;
		try {
			JAXBElement<ItemFormativoEsercizio> jaxbElement = new JAXBElement<ItemFormativoEsercizio>( new QName("", "itemFormativoEsercizio"), ItemFormativoEsercizio.class, masterItem );
			JAXBContext jaxbContext = JAXBContext.newInstance( ItemFormativoEsercizio.class );
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE); 
			os = new FileOutputStream( getXmlAbsolutePath() );
			jaxbMarshaller.marshal( jaxbElement, os );
			return true;
		}
		catch (Exception e1) {
			e1.printStackTrace();
			return false;
		}
		finally {
			if (os != null)
			try
			{
				os.close();
			}catch (IOException e2) {
				
			}
		}
	}

	/**
	 * Incrementa di 1 il numero di versione, se il campo non era valorizzato lo imposta a 1
	 */
	public void incrementaVersione() {
		// TODO Auto-generated method stub
		if (masterItem.getVersione() == null)
			masterItem.setVersione(1);
		else
			masterItem.setVersione(masterItem.getVersione().intValue() + 1);
	}
	
}
