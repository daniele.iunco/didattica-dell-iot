package common;

public class Tuple<Item1, Item2> {
	public final Item1 item1; 
	public final Item2 item2;
	
	public Tuple(Item1 item1, Item2 item2) { 
		this.item1 = item1; 
		this.item2 = item2; 
	} 
}