/*******************************************************************************
 * Copyright (C) 2021 Daniele Iunco
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/


import common.*;

import interfaces.*;

import javax.swing.JDialog;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import java.awt.Insets;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.util.Iterator;

import javax.swing.JTextField;
import javax.swing.JList;
import java.awt.FlowLayout;
import java.awt.Frame;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.BorderFactory;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.BoxLayout;
import javax.swing.border.CompoundBorder;
import javax.swing.border.LineBorder;
import java.awt.Color;

//TODO serialVersionUID

/**
 * 
 * @author Daniele Iunco
 * @version 1.0
 *
 */
public class SwingKeyValuePairEditor extends JDialog {
	private static final long serialVersionUID = 1L;

	private String keyfilepath="";
	private KeyValuePairsFile keys;
	private IReferenceMonitor mainWindow;

	private JLabel lblSelectedKey;
	private JTextField txtEditVal;
	private JTextField txtValue;
	private JTextField txtKey;
	private DefaultListModel<String> listKeys;
	private boolean reloading;
	
	public SwingKeyValuePairEditor(Frame owner, IReferenceMonitor mainWindow, String windowTitle, String keyfilepath) throws FileNotFoundException, IllegalArgumentException {
		super(owner, true);
		setBounds(100, 100, 650, 400);
		if (!Tools.fileExists(keyfilepath))
		{
			throw new FileNotFoundException(keyfilepath);
		}
		this.keyfilepath=keyfilepath;
		this.mainWindow = mainWindow;
		setTitle(windowTitle);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.X_AXIS));
			
		JPanel panel = new JPanel();
		panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

		GridBagConstraints gbl_panel = new GridBagConstraints();
		gbl_panel.gridwidth = 1;
		gbl_panel.fill = GridBagConstraints.BOTH;
		gbl_panel.gridx = 0;
		gbl_panel.gridy = 0;
		getContentPane().add(panel);
		
		
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{1.0, 0.0, 1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 1.0, 1.0, 0.0, Double.MIN_VALUE};
		panel.setLayout(gridBagLayout);
		
		JLabel lblFile = new JLabel("File: " + keyfilepath);
		GridBagConstraints gbc_lblFile = new GridBagConstraints();
		gbc_lblFile.gridwidth = 3;
		gbc_lblFile.anchor = GridBagConstraints.WEST;
		gbc_lblFile.insets = new Insets(0, 0, 5, 0);
		gbc_lblFile.gridx = 0;
		gbc_lblFile.gridy = 0;
		panel.add(lblFile, gbc_lblFile);

		listKeys = new DefaultListModel<String>();
		JList<String> list = new JList<String>(listKeys);
		list.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				if (!reloading) {
					ChiaveValore objVal = keys.getItem((String) listKeys.get(list.getSelectedIndex()));
					lblSelectedKey.setText(objVal.key);
					txtEditVal.setText(objVal.value);
				}
			}
		});
		JScrollPane myScrollPaneList = new JScrollPane(list); 

		GridBagConstraints gbc_listKeys = new GridBagConstraints();
		gbc_listKeys.gridheight = 2;
		gbc_listKeys.gridwidth = 2;
		gbc_listKeys.insets = new Insets(0, 0, 5, 5);
		gbc_listKeys.fill = GridBagConstraints.BOTH;
		gbc_listKeys.gridx = 0;
		gbc_listKeys.gridy = 1;
		panel.add(myScrollPaneList, gbc_listKeys);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel_1 = new GridBagConstraints();
		gbc_panel_1.insets = new Insets(0, 0, 5, 0);
		gbc_panel_1.fill = GridBagConstraints.BOTH;
		gbc_panel_1.gridx = 2;
		gbc_panel_1.gridy = 1;
		gbc_panel_1.weightx = .5;
		panel.add(panel_1, gbc_panel_1);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[]{0, 0, 0};
		gbl_panel_1.rowHeights = new int[]{0, 0, 0, 0, 0};
		gbl_panel_1.columnWeights = new double[]{1.0, 1.0, Double.MIN_VALUE};
		gbl_panel_1.rowWeights = new double[]{0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		panel_1.setLayout(gbl_panel_1);
		
		JLabel lblNewLabel_7 = new JLabel("Modifica");
		GridBagConstraints gbc_lblNewLabel_7 = new GridBagConstraints();
		gbc_lblNewLabel_7.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_7.gridx = 0;
		gbc_lblNewLabel_7.gridy = 0;
		panel_1.add(lblNewLabel_7, gbc_lblNewLabel_7);
		
		JLabel lblNewLabel = new JLabel("Chiave");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 1;
		panel_1.add(lblNewLabel, gbc_lblNewLabel);
		
		lblSelectedKey = new JLabel("Chiave");
		GridBagConstraints gbc_lblSelectedKey = new GridBagConstraints();
		gbc_lblSelectedKey.anchor = GridBagConstraints.WEST;
		gbc_lblSelectedKey.insets = new Insets(0, 0, 5, 0);
		gbc_lblSelectedKey.gridx = 1;
		gbc_lblSelectedKey.gridy = 1;
		panel_1.add(lblSelectedKey, gbc_lblSelectedKey);
		
		JLabel lblNewLabel_2 = new JLabel("Valore");
		GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
		gbc_lblNewLabel_2.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_2.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_2.gridx = 0;
		gbc_lblNewLabel_2.gridy = 2;
		panel_1.add(lblNewLabel_2, gbc_lblNewLabel_2);
		
		txtEditVal = new JTextField();
		GridBagConstraints gbc_txtEditVal = new GridBagConstraints();
		gbc_txtEditVal.insets = new Insets(0, 0, 5, 0);
		gbc_txtEditVal.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtEditVal.gridx = 1;
		gbc_txtEditVal.gridy = 2;
		panel_1.add(txtEditVal, gbc_txtEditVal);
		txtEditVal.setColumns(10);
		
		JPanel grpModifica = new JPanel();
		grpModifica.setBorder(new CompoundBorder());
		GridBagConstraints gbc_grpModifica = new GridBagConstraints();
		gbc_grpModifica.gridwidth = 2;
		gbc_grpModifica.fill = GridBagConstraints.BOTH;
		gbc_grpModifica.gridx = 0;
		gbc_grpModifica.gridy = 3;
		panel_1.add(grpModifica, gbc_grpModifica);
		grpModifica.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JButton btnSave = new JButton("Modifica");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				keys.getItem(lblSelectedKey.getText()).value = txtEditVal.getText();
				try {
					keys.save();
					if (mainWindow!=null) mainWindow.FileUpdated(keyfilepath);
					reloadAll();
				} catch (FileAlreadyExistsException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IllegalArgumentException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		grpModifica.add(btnSave);
		
		JButton btnDelete = new JButton("Elimina");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					keys.removeItem(lblSelectedKey.getText());
					keys.save();
					if (mainWindow!=null) mainWindow.FileUpdated(keyfilepath);
					reloadAll();
				} catch (FileAlreadyExistsException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IllegalArgumentException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		grpModifica.add(btnDelete);
		
		JPanel grpAggiungi = new JPanel();
		grpAggiungi.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_grpAggiungi = new GridBagConstraints();
		gbc_grpAggiungi.fill = GridBagConstraints.BOTH;
		gbc_grpAggiungi.gridx = 2;
		gbc_grpAggiungi.gridy = 2;
		panel.add(grpAggiungi, gbc_grpAggiungi);
		GridBagLayout gbl_grpAggiungi = new GridBagLayout();
		gbl_grpAggiungi.columnWidths = new int[]{0, 0, 0};
		gbl_grpAggiungi.rowHeights = new int[]{0, 0, 0, 0, 0};
		gbl_grpAggiungi.columnWeights = new double[]{1.0, 1.0, Double.MIN_VALUE};
		gbl_grpAggiungi.rowWeights = new double[]{0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		grpAggiungi.setLayout(gbl_grpAggiungi);
		
		JLabel lblNewLabel_6 = new JLabel("Aggiungi");
		GridBagConstraints gbc_lblNewLabel_6 = new GridBagConstraints();
		gbc_lblNewLabel_6.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_6.gridx = 0;
		gbc_lblNewLabel_6.gridy = 0;
		grpAggiungi.add(lblNewLabel_6, gbc_lblNewLabel_6);
		
		JLabel lblNewLabel_3 = new JLabel("Chiave");
		GridBagConstraints gbc_lblNewLabel_3 = new GridBagConstraints();
		gbc_lblNewLabel_3.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_3.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_3.gridx = 0;
		gbc_lblNewLabel_3.gridy = 1;
		grpAggiungi.add(lblNewLabel_3, gbc_lblNewLabel_3);
		
		txtKey = new JTextField();
		GridBagConstraints gbc_txtKey = new GridBagConstraints();
		gbc_txtKey.insets = new Insets(0, 0, 5, 0);
		gbc_txtKey.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtKey.gridx = 1;
		gbc_txtKey.gridy = 1;
		grpAggiungi.add(txtKey, gbc_txtKey);
		txtKey.setColumns(10);
		
		JLabel lblNewLabel_5 = new JLabel("Valore");
		GridBagConstraints gbc_lblNewLabel_5 = new GridBagConstraints();
		gbc_lblNewLabel_5.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_5.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_5.gridx = 0;
		gbc_lblNewLabel_5.gridy = 2;
		grpAggiungi.add(lblNewLabel_5, gbc_lblNewLabel_5);
		
		txtValue = new JTextField();
		GridBagConstraints gbc_txtValue = new GridBagConstraints();
		gbc_txtValue.insets = new Insets(0, 0, 5, 0);
		gbc_txtValue.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtValue.gridx = 1;
		gbc_txtValue.gridy = 2;
		grpAggiungi.add(txtValue, gbc_txtValue);
		txtValue.setColumns(10);
		
		JPanel panel_3 = new JPanel();
		GridBagConstraints gbc_panel_3 = new GridBagConstraints();
		gbc_panel_3.gridwidth = 2;
		gbc_panel_3.fill = GridBagConstraints.BOTH;
		gbc_panel_3.gridx = 0;
		gbc_panel_3.gridy = 3;
		grpAggiungi.add(panel_3, gbc_panel_3);
		panel_3.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JButton btnAddNew = new JButton("Aggiungi");
		btnAddNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!keys.containsKey(txtKey.getText())) {
					try {
						keys.addNew(txtKey.getText(), txtValue.getText());
						keys.save();
						if (mainWindow!=null) mainWindow.FileUpdated(keyfilepath);
						reloadAll();
					} catch (FileAlreadyExistsException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (IllegalArgumentException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
		});
		panel_3.add(btnAddNew);
		
		JButton btnReload = new JButton("Ricarica");
		btnReload.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					reloadAll();
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IllegalArgumentException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		GridBagConstraints gbc_btnReload = new GridBagConstraints();
		gbc_btnReload.insets = new Insets(0, 0, 0, 5);
		gbc_btnReload.gridx = 0;
		gbc_btnReload.gridy = 3;
		panel.add(btnReload, gbc_btnReload);

		reloadAll();
	}

	// ricarica i contenuti
	void reloadAll() throws FileNotFoundException, IllegalArgumentException {
		if (keyfilepath==null || keyfilepath=="") throw new FileNotFoundException("");

		reloading=true;
		keys = new KeyValuePairsFile(keyfilepath, mainWindow);
		listKeys.removeAllElements();
    	Iterator<ChiaveValore> myIterator = keys.iterator();
    	while (myIterator.hasNext()) {
    		listKeys.addElement(myIterator.next().key);
    	}
    	reloading=false;
	}
}
