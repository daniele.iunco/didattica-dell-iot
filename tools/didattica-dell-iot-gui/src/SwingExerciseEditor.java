/*******************************************************************************
 * Copyright (C) 2021 Daniele Iunco
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

import widgets.*;
import common.*;
import interfaces.*;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import java.awt.GridLayout;
import java.awt.Image;

import javax.swing.JTextField;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Toolkit;

import javax.swing.JLabel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.LineBorder;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;

import java.awt.Color;
import javax.swing.WindowConstants;
import java.awt.Component;
import java.awt.Container;

import javax.swing.Box;
import javax.swing.JEditorPane;

import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.nio.file.FileAlreadyExistsException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.SwingUtilities;

//TODO serialVersionUID

/**
 * Finestra per l'editor di esercizi, per ciascun campo dell'oggetto ItemFormativoEsercizio c'� un widget specifico per la modifica che si associa al campo indicato nel costruttore 
 * ciascun widget implementa l'interfaccia IContentsMonitored e va aggiunto alla lista myMonitored.add(objWidget);
 * 
 * TODO la gestione non � fatta bene, invece che aggiornare dell'oggetto ItemFormativoEsercizio qui dal metodo changed, i widget dovrebbero aggiornare per conto loro il campo a cui sono collegati.
 * TODO serve una classe per gestire un gruppo di widget che implementano l'interfaccia IContentsMonitored, con i relativi metodi per gestire le modifiche
 * per tenere il codice di gestione delle modifiche separato da quello dell'interfaccia 
 * 
 * 
 * @author Daniele Iunco
 * @version 1.0
 */
public class SwingExerciseEditor extends JDialog implements IDialogChangeSubscriber {

	private static final long serialVersionUID = 1L;
	private ItemFormativoEsercizioAdv myOpenedFile; 
	private boolean fileLoaded;
	private java.util.List<IContentsMonitored> myMonitored;

	private IReferenceMonitor mainWindow; 
	private PathStructure pathStructure;

	private final JPanel contentPanel;
	private JTextField txtXmlFile;
	private SwingText txtIdentificatoreArgomento;
	private SwingText txtTitle;
	private SwingTextKeyList txtPrerequisites;
	private JEditorPane browser;

	private SwingListWithEditor lstHRTodoList;
	private SwingListWithEditor lstHRPrerequisites;
	private SwingListWithEditor lstHRComponents;
	private SwingTextWithPaste txtHRWhy;
	private SwingTextWithPaste txtHRExpectedResult;
	private SwingTextWithPaste txtHRExtra;
	private SwingTextWithPaste txtHRCommonErrors;
	private SwingTextWithPaste txtHRErraticBehavior;
	private SwingCheckBox chkPblProblem;
	private SwingCheckBox chkPblNewLearningGoal;
	private SwingTextWithPaste txtPblHrDetails;
	private SwingTextWithPaste txtHRSolution;
	private SwingTextWithPaste txtHRLessonLearned;
	private SwingCheckBox chkFondamentale; 
	private SwingListRisorseCollegateWithEditor lstRisorseCollegate;
	private SwingCombo cboTipoTempo;
	private SwingSpinner txtUnitaTempo;
	private SwingSpinner txtVersione;
	private SwingTextKeyList txtTags;
	private Component horizontalStrut;
		
	/**
	 * Costruttore
	 * @param xmlFilePath
	 * @param createNew
	 * @param mainWindow
	 * @param sDefaultIdAttr
	 * @param pathStructure
	 * @throws FileNotFoundException
	 * @throws FileAlreadyExistsException
	 */

	public SwingExerciseEditor(String xmlFilePath, boolean createNew, 
			IReferenceMonitor mainWindow, String sDefaultIdAttr, PathStructure pathStructure) throws FileNotFoundException, FileAlreadyExistsException {
		super();
		this.mainWindow=mainWindow;
		this.pathStructure = pathStructure;
		this.myMonitored = new ArrayList<IContentsMonitored>();

		//il file va caricato all'inizio, in modo che i widget si possano collegare senza trovare riferimenti null
		//loadFile(xmlFilePath, createNew);
		myOpenedFile = new ItemFormativoEsercizioAdv(xmlFilePath, createNew, pathStructure);

		setBounds(100, 100, 1024, 768);
		
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		
		addWindowListener(new WindowAdapter() 
		{
			public void windowClosed(WindowEvent e)
			{
				dispose();
			}
			public void windowClosing(WindowEvent e)
			{
			  handleClosing();
			}
		});
		
		
		JPanel parentPanel = new JPanel();
		getContentPane().setLayout(new BorderLayout());
		parentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(parentPanel, BorderLayout.CENTER);
		parentPanel.setLayout(new GridLayout(0, 1, 0, 0));
		{
			contentPanel = new JPanel();
			contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));

	        parentPanel.add(contentPanel);
			GridBagLayout gbl_contentPanel = new GridBagLayout();
			gbl_contentPanel.columnWidths = new int[] {0, 0, 0};
			gbl_contentPanel.rowHeights = new int[] {0, 0};
			gbl_contentPanel.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
			gbl_contentPanel.rowWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
			contentPanel.setLayout(gbl_contentPanel);
			
			JPanel panelFile = new JPanel();
			panelFile.setBorder(new CompoundBorder(new LineBorder(new Color(192, 192, 192)), new EmptyBorder(5, 5, 5, 5)));
			GridBagConstraints gbc_panelFile = new GridBagConstraints();
			gbc_panelFile.fill = GridBagConstraints.BOTH;
			gbc_panelFile.insets = new Insets(0, 0, 5, 5);
			gbc_panelFile.gridx = 0;
			gbc_panelFile.gridy = 0;
			contentPanel.add(panelFile, gbc_panelFile);
			GridBagLayout gbl_panelFile = new GridBagLayout();
			gbl_panelFile.columnWidths = new int[]{0, 0, 0, 0};
			gbl_panelFile.rowHeights = new int[]{0, 0, 0};
			gbl_panelFile.columnWeights = new double[]{1.0, 0.0, 0.0, Double.MIN_VALUE};
			gbl_panelFile.rowWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
			panelFile.setLayout(gbl_panelFile);
			
			JLabel lblNewLabel_5 = new JLabel("Campo versione modificabile per test, fare attenzione!");
			GridBagConstraints gbc_lblNewLabel_5 = new GridBagConstraints();
			gbc_lblNewLabel_5.anchor = GridBagConstraints.WEST;
			gbc_lblNewLabel_5.insets = new Insets(0, 0, 5, 5);
			gbc_lblNewLabel_5.gridx = 0;
			gbc_lblNewLabel_5.gridy = 0;
			panelFile.add(lblNewLabel_5, gbc_lblNewLabel_5);

			txtVersione = new SwingSpinner(this, "Versione file", myOpenedFile.masterItem, "versione");
			GridBagConstraints gbc_txtVersione = new GridBagConstraints();
			gbc_txtVersione.gridwidth = 2;
			gbc_txtVersione.insets = new Insets(0, 0, 5, 5);
			gbc_txtVersione.gridx = 1;
			gbc_txtVersione.gridy = 0;
			panelFile.add(txtVersione, gbc_txtVersione);
			
			txtXmlFile = new JTextField();
			txtXmlFile.setEditable(false);
			GridBagConstraints gbc_txtXmlFile = new GridBagConstraints();
			gbc_txtXmlFile.fill = GridBagConstraints.HORIZONTAL;
			gbc_txtXmlFile.anchor = GridBagConstraints.WEST;
			gbc_txtXmlFile.insets = new Insets(0, 0, 0, 5);
			gbc_txtXmlFile.gridx = 0;
			gbc_txtXmlFile.gridy = 1;
			panelFile.add(txtXmlFile, gbc_txtXmlFile);
			txtXmlFile.setColumns(20);
			txtXmlFile.setText(xmlFilePath);
			
			JButton btnSave = new JButton("Salva modifiche");
			btnSave.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (hasChanges()) {
						saveFile();
						setNotChanged();
						mainWindow.FileUpdated(xmlFilePath);
					}
				}
			});
			GridBagConstraints gbc_btnSave = new GridBagConstraints();
			gbc_btnSave.insets = new Insets(0, 0, 0, 5);
			gbc_btnSave.gridx = 1;
			gbc_btnSave.gridy = 1;
			panelFile.add(btnSave, gbc_btnSave);
			
			JButton btnDiscardChanges = new JButton("Ignora modifiche e chiudi");
			btnDiscardChanges.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					setNotChanged();
					dispose();
				}
			});
			GridBagConstraints gbc_btnDiscardChanges = new GridBagConstraints();
			gbc_btnDiscardChanges.gridx = 2;
			gbc_btnDiscardChanges.gridy = 1;
			panelFile.add(btnDiscardChanges, gbc_btnDiscardChanges);
			
			browser = new JEditorPane();
			browser.setEditable(false);
			GridBagConstraints gbc_browser = new GridBagConstraints();
			gbc_browser.gridheight = 2;
			gbc_browser.insets = new Insets(0, 0, 5, 0);
			gbc_browser.fill = GridBagConstraints.BOTH;
			gbc_browser.gridx = 1;
			gbc_browser.gridy = 0;
			
	        JScrollPane scrollBrowser = new JScrollPane(browser);    

			contentPanel.add(scrollBrowser, gbc_browser);
			
			JPanel panelExerciseParams = new JPanel();
			panelExerciseParams.setBorder(new CompoundBorder(new LineBorder(new Color(192, 192, 192)), new EmptyBorder(5, 5, 5, 5)));
			GridBagConstraints gbc_panelExerciseParams = new GridBagConstraints();
			gbc_panelExerciseParams.insets = new Insets(0, 0, 5, 5);
			gbc_panelExerciseParams.fill = GridBagConstraints.BOTH;
			gbc_panelExerciseParams.gridx = 0;
			gbc_panelExerciseParams.gridy = 1;
			contentPanel.add(panelExerciseParams, gbc_panelExerciseParams);
			GridBagLayout gbl_panelExerciseParams = new GridBagLayout();
			gbl_panelExerciseParams.columnWidths = new int[]{144, 0};
			gbl_panelExerciseParams.rowHeights = new int[]{14, 0, 0};
			gbl_panelExerciseParams.columnWeights = new double[]{1.0, Double.MIN_VALUE};
			gbl_panelExerciseParams.rowWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
			panelExerciseParams.setLayout(gbl_panelExerciseParams);
			
			JLabel lblMetadatiSpecificiDellesercizio = new JLabel("metadati specifici dell'esercizio");
			GridBagConstraints gbc_lblMetadatiSpecificiDellesercizio = new GridBagConstraints();
			gbc_lblMetadatiSpecificiDellesercizio.insets = new Insets(0, 0, 5, 0);
			gbc_lblMetadatiSpecificiDellesercizio.anchor = GridBagConstraints.NORTHWEST;
			gbc_lblMetadatiSpecificiDellesercizio.gridx = 0;
			gbc_lblMetadatiSpecificiDellesercizio.gridy = 0;
			panelExerciseParams.add(lblMetadatiSpecificiDellesercizio, gbc_lblMetadatiSpecificiDellesercizio);
			
			JScrollPane scrollPane = new JScrollPane();
			GridBagConstraints gbc_scrollPane = new GridBagConstraints();
			gbc_scrollPane.fill = GridBagConstraints.BOTH;
			gbc_scrollPane.gridx = 0;
			gbc_scrollPane.gridy = 1;
			panelExerciseParams.add(scrollPane, gbc_scrollPane);
			
			JPanel panelExerciseParamsBlocks = new JPanel();
			scrollPane.setViewportView(panelExerciseParamsBlocks);
			GridBagLayout gbl_panelMetadataBlocks = new GridBagLayout();
			gbl_panelMetadataBlocks.columnWidths = new int[]{0, 0};
			gbl_panelMetadataBlocks.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
			gbl_panelMetadataBlocks.columnWeights = new double[]{0.0, Double.MIN_VALUE};
			gbl_panelMetadataBlocks.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
			panelExerciseParamsBlocks.setLayout(gbl_panelMetadataBlocks);
			

			int yRow = 1;


			txtIdentificatoreArgomento = new SwingText(this, "identificatoreArgomento", myOpenedFile.masterItem, "identificatoreArgomento");
			GridBagConstraints gbc_txtIdentificatoreArgomento = new GridBagConstraints();
			gbc_txtIdentificatoreArgomento.insets = new Insets(0, 0, 5, 0);
			gbc_txtIdentificatoreArgomento.fill = GridBagConstraints.HORIZONTAL;
			gbc_txtIdentificatoreArgomento.gridx = 0;
			gbc_txtIdentificatoreArgomento.gridy = yRow;
			panelExerciseParamsBlocks.add(txtIdentificatoreArgomento, gbc_txtIdentificatoreArgomento);
			if (sDefaultIdAttr != null) txtIdentificatoreArgomento.setText(sDefaultIdAttr);	
			yRow++;

			txtTitle = new SwingText(this, "nomeDescrittivo", myOpenedFile.masterItem, "nomeDescrittivo");
			GridBagConstraints gbc_txtTitle = new GridBagConstraints();
			gbc_txtTitle.insets = new Insets(0, 0, 5, 0);
			gbc_txtTitle.fill = GridBagConstraints.HORIZONTAL;
			gbc_txtTitle.gridx = 0;
			gbc_txtTitle.gridy = yRow;
			panelExerciseParamsBlocks.add(txtTitle, gbc_txtTitle);
			yRow++;

			txtPrerequisites = new SwingTextKeyList(this, pathStructure.prerequisiteFile.getItemPath(), mainWindow, "Prerequisiti", myOpenedFile.masterItem, "prerequisiti");
			GridBagConstraints gbc_txtPrerequisites = new GridBagConstraints();
			gbc_txtPrerequisites.insets = new Insets(0, 0, 5, 0);
			gbc_txtPrerequisites.fill = GridBagConstraints.HORIZONTAL;
			gbc_txtPrerequisites.gridx = 0;
			gbc_txtPrerequisites.gridy = yRow;
			panelExerciseParamsBlocks.add(txtPrerequisites, gbc_txtPrerequisites);
			yRow++;

			chkFondamentale = new SwingCheckBox(this, false, "Fondamentale", myOpenedFile.masterItem, "fondamentale");
			GridBagConstraints gbc_chkFondamentale = new GridBagConstraints();
			gbc_chkFondamentale.anchor = GridBagConstraints.WEST;
			gbc_chkFondamentale.insets = new Insets(0, 0, 5, 0);
			gbc_chkFondamentale.gridx = 0;
			gbc_chkFondamentale.gridy = yRow;
			panelExerciseParamsBlocks.add(chkFondamentale, gbc_chkFondamentale);
			yRow++;
			
			txtUnitaTempo = new SwingSpinner(this, "Tempo richiesto", myOpenedFile.masterItem, "tempoRichiesto");
			GridBagConstraints gbc_txtUnitaTempo = new GridBagConstraints();
			gbc_txtUnitaTempo.insets = new Insets(0, 0, 5, 0);
			gbc_txtUnitaTempo.gridx = 0;
			gbc_txtUnitaTempo.gridy = yRow;
			panelExerciseParamsBlocks.add(txtUnitaTempo, gbc_txtUnitaTempo);
			yRow++;
			
			cboTipoTempo = new SwingCombo(this, "Unit� di tempo", new String[] {"Ore", "Minuti"}, myOpenedFile.masterItem, "tipoTempo");
			GridBagConstraints gbc_cboTipoTempo = new GridBagConstraints();
			gbc_cboTipoTempo.anchor = GridBagConstraints.WEST;
			gbc_cboTipoTempo.insets = new Insets(0, 0, 5, 0);
			gbc_cboTipoTempo.gridx = 0;
			gbc_cboTipoTempo.gridy = yRow;
			panelExerciseParamsBlocks.add(cboTipoTempo, gbc_cboTipoTempo);
			yRow++;

			txtTags = new SwingTextKeyList(this, pathStructure.tagsFile.getItemPath(), mainWindow, "Tags", myOpenedFile.masterItem, "tags");
			GridBagConstraints gbc_txtTags = new GridBagConstraints();
			gbc_txtTags.anchor = GridBagConstraints.ABOVE_BASELINE;
			gbc_txtTags.insets = new Insets(0, 0, 5, 0);
			gbc_txtTags.gridx = 0;
			gbc_txtTags.gridy = yRow;
			panelExerciseParamsBlocks.add(txtTags, gbc_txtTags);
			yRow++;
			
			lstHRPrerequisites = new SwingListWithEditor(this, false, "Prerequisiti", myOpenedFile.masterItem, "mdprerequisitelist");
			GridBagConstraints gbc_lstHRPrerequisites = new GridBagConstraints();
			gbc_lstHRPrerequisites.fill = GridBagConstraints.HORIZONTAL;
			gbc_lstHRPrerequisites.insets = new Insets(0, 0, 5, 0);
			gbc_lstHRPrerequisites.gridx = 0;
			gbc_lstHRPrerequisites.gridy = yRow;
			panelExerciseParamsBlocks.add(lstHRPrerequisites, gbc_lstHRPrerequisites);
			yRow++;
			
			lstHRComponents = new SwingListWithEditor(this, false, "Componenti", myOpenedFile.masterItem, "mdcomponentlist");
			GridBagConstraints gbc_lstHRComponents = new GridBagConstraints();
			gbc_lstHRComponents.fill = GridBagConstraints.HORIZONTAL;
			gbc_lstHRComponents.insets = new Insets(0, 0, 5, 0);
			gbc_lstHRComponents.gridx = 0;
			gbc_lstHRComponents.gridy = yRow;
			panelExerciseParamsBlocks.add(lstHRComponents, gbc_lstHRComponents);
			yRow++;
			
			txtHRWhy = new SwingTextWithPaste(this, "Motivo", myOpenedFile.masterItem, "mdwhy");
			GridBagConstraints gbc_txtHRWhy = new GridBagConstraints();
			gbc_txtHRWhy.fill = GridBagConstraints.HORIZONTAL;
			gbc_txtHRWhy.insets = new Insets(0, 0, 5, 0);
			gbc_txtHRWhy.gridx = 0;
			gbc_txtHRWhy.gridy = yRow;
			panelExerciseParamsBlocks.add(txtHRWhy, gbc_txtHRWhy);
			yRow++;
			
			txtHRExpectedResult = new SwingTextWithPaste(this, "Descrizione", myOpenedFile.masterItem, "mdexpectedresult");
			GridBagConstraints gbc_txtHRExpectedResult = new GridBagConstraints();
			gbc_txtHRExpectedResult.fill = GridBagConstraints.HORIZONTAL;
			gbc_txtHRExpectedResult.insets = new Insets(0, 0, 5, 0);
			gbc_txtHRExpectedResult.gridx = 0;
			gbc_txtHRExpectedResult.gridy = yRow;
			panelExerciseParamsBlocks.add(txtHRExpectedResult, gbc_txtHRExpectedResult);
			yRow++;
			
			txtHRExtra = new SwingTextWithPaste(this, "Espansione", myOpenedFile.masterItem, "mdextras");
			GridBagConstraints gbc_txtHRExtra = new GridBagConstraints();
			gbc_txtHRExtra.fill = GridBagConstraints.HORIZONTAL;
			gbc_txtHRExtra.insets = new Insets(0, 0, 5, 0);
			gbc_txtHRExtra.gridx = 0;
			gbc_txtHRExtra.gridy = yRow;
			panelExerciseParamsBlocks.add(txtHRExtra, gbc_txtHRExtra);
			yRow++;
			
			txtHRCommonErrors = new SwingTextWithPaste(this, "Errori comuni", myOpenedFile.masterItem, "mdcommonerrors");
			GridBagConstraints gbc_txtHRCommonErrors = new GridBagConstraints();
			gbc_txtHRCommonErrors.fill = GridBagConstraints.HORIZONTAL;
			gbc_txtHRCommonErrors.insets = new Insets(0, 0, 5, 0);
			gbc_txtHRCommonErrors.gridx = 0;
			gbc_txtHRCommonErrors.gridy = yRow;
			panelExerciseParamsBlocks.add(txtHRCommonErrors, gbc_txtHRCommonErrors);
			yRow++;
			
			txtHRErraticBehavior = new SwingTextWithPaste(this, "Comportamento erratico", myOpenedFile.masterItem, "mderraticbehavior");
			GridBagConstraints gbc_txtHRErraticBehavior = new GridBagConstraints();
			gbc_txtHRErraticBehavior.fill = GridBagConstraints.HORIZONTAL;
			gbc_txtHRErraticBehavior.insets = new Insets(0, 0, 5, 0);
			gbc_txtHRErraticBehavior.gridx = 0;
			gbc_txtHRErraticBehavior.gridy = yRow;
			panelExerciseParamsBlocks.add(txtHRErraticBehavior, gbc_txtHRErraticBehavior);
			yRow++;
			
			chkPblProblem = new SwingCheckBox(this, false, "Valido per PBL", myOpenedFile.masterItem, "mdPblproblem");
			GridBagConstraints gbc_chkPblProblem = new GridBagConstraints();
			gbc_chkPblProblem.fill = GridBagConstraints.HORIZONTAL;
			gbc_chkPblProblem.insets = new Insets(0, 0, 5, 0);
			gbc_chkPblProblem.gridx = 0;
			gbc_chkPblProblem.gridy = yRow;
			panelExerciseParamsBlocks.add(chkPblProblem, gbc_chkPblProblem);
			yRow++;
			
			chkPblNewLearningGoal = new SwingCheckBox(this, false, "Obiettivo PBL", myOpenedFile.masterItem, "mdPblnewlearninggoal");
			GridBagConstraints gbc_chkPblNewLearningGoal = new GridBagConstraints();
			gbc_chkPblNewLearningGoal.fill = GridBagConstraints.HORIZONTAL;
			gbc_chkPblNewLearningGoal.insets = new Insets(0, 0, 5, 0);
			gbc_chkPblNewLearningGoal.gridx = 0;
			gbc_chkPblNewLearningGoal.gridy = yRow;
			panelExerciseParamsBlocks.add(chkPblNewLearningGoal, gbc_chkPblNewLearningGoal);
			yRow++;
			
			txtPblHrDetails = new SwingTextWithPaste(this, "Dettagli PBL", myOpenedFile.masterItem, "mdpbldetails");
			GridBagConstraints gbc_txtPblHrDetails = new GridBagConstraints();
			gbc_txtPblHrDetails.fill = GridBagConstraints.HORIZONTAL;
			gbc_txtPblHrDetails.insets = new Insets(0, 0, 5, 0);
			gbc_txtPblHrDetails.gridx = 0;
			gbc_txtPblHrDetails.gridy = yRow;
			panelExerciseParamsBlocks.add(txtPblHrDetails, gbc_txtPblHrDetails);
			yRow++;
			
			txtHRSolution = new SwingTextWithPaste(this, "Indicazioni per soluzione", myOpenedFile.masterItem, "mdsolution");
			GridBagConstraints gbc_txtHRSolution = new GridBagConstraints();
			gbc_txtHRSolution.fill = GridBagConstraints.HORIZONTAL;
			gbc_txtHRSolution.insets = new Insets(0, 0, 5, 0);
			gbc_txtHRSolution.gridx = 0;
			gbc_txtHRSolution.gridy = yRow;
			panelExerciseParamsBlocks.add(txtHRSolution, gbc_txtHRSolution);
			yRow++;
			
			txtHRLessonLearned = new SwingTextWithPaste(this, "Lesson learned", myOpenedFile.masterItem, "mdlessonlearned");
			GridBagConstraints gbc_txtHRLessonLearned = new GridBagConstraints();
			gbc_txtHRLessonLearned.fill = GridBagConstraints.HORIZONTAL;
			gbc_txtHRLessonLearned.gridx = 0;
			gbc_txtHRLessonLearned.gridy = yRow;
			panelExerciseParamsBlocks.add(txtHRLessonLearned, gbc_txtHRLessonLearned);
			yRow++;
			
			lstRisorseCollegate = new SwingListRisorseCollegateWithEditor(this, false, "lstRisorseCollegate", myOpenedFile.masterItem, "risorseCollegate");
			GridBagConstraints gbc_lstRisorseCollegate = new GridBagConstraints();
			gbc_lstRisorseCollegate.fill = GridBagConstraints.HORIZONTAL;
			gbc_lstRisorseCollegate.insets = new Insets(0, 0, 5, 0);
			gbc_lstRisorseCollegate.gridx = 0;
			gbc_lstRisorseCollegate.gridy = yRow;
			panelExerciseParamsBlocks.add(lstRisorseCollegate, gbc_lstRisorseCollegate);
			yRow++;
			
			lstHRTodoList = new SwingListWithEditor(this, false, "lstHRTodoList", myOpenedFile.masterItem, "mdtodolist");
			GridBagConstraints gbc_lstHRTodoList = new GridBagConstraints();
			gbc_lstHRTodoList.insets = new Insets(0, 0, 5, 0);
			gbc_lstHRTodoList.fill = GridBagConstraints.HORIZONTAL;
			gbc_lstHRTodoList.anchor = GridBagConstraints.WEST;
			gbc_lstHRTodoList.gridx = 0;
			gbc_lstHRTodoList.gridy = yRow;
			panelExerciseParamsBlocks.add(lstHRTodoList, gbc_lstHRTodoList);
			yRow++;
			
			Component horizontalStrut_1 = Box.createHorizontalStrut(600);
			GridBagConstraints gbc_horizontalStrut_1 = new GridBagConstraints();
			gbc_horizontalStrut_1.insets = new Insets(0, 0, 0, 5);
			gbc_horizontalStrut_1.gridx = 0;
			gbc_horizontalStrut_1.gridy = 3;
			contentPanel.add(horizontalStrut_1, gbc_horizontalStrut_1);
			
			
			//TODO meglio scansione widget esistenti?
			myMonitored.add(txtVersione);
			myMonitored.add(txtHRWhy);
			myMonitored.add(txtHRExtra);
			myMonitored.add(txtHRExpectedResult);
			myMonitored.add(txtHRLessonLearned);
			myMonitored.add(txtHRErraticBehavior);
			myMonitored.add(txtHRCommonErrors);
			myMonitored.add(txtHRSolution);
			myMonitored.add(chkPblProblem);
			myMonitored.add(chkPblNewLearningGoal);
			myMonitored.add(txtPblHrDetails);
			myMonitored.add(lstHRTodoList);
			myMonitored.add(lstHRPrerequisites);
			myMonitored.add(lstHRComponents);
			myMonitored.add(txtTags);
			myMonitored.add(txtIdentificatoreArgomento);
			myMonitored.add(txtTitle);
			myMonitored.add(txtPrerequisites);
			myMonitored.add(lstRisorseCollegate);
			myMonitored.add(chkFondamentale);
			myMonitored.add(cboTipoTempo);
			myMonitored.add(txtUnitaTempo);
			
			horizontalStrut = Box.createHorizontalStrut(500);
			GridBagConstraints gbc_horizontalStrut = new GridBagConstraints();
			gbc_horizontalStrut.gridx = 0;
			gbc_horizontalStrut.gridy = yRow;
			panelExerciseParamsBlocks.add(horizontalStrut, gbc_horizontalStrut);

			for (IContentsMonitored item : myMonitored) {
				item.loadFromUnderlyingObject();
				if (item != txtVersione) item.setNotifications(true);
			}
			
			syncPreview();
			fileLoaded=true;
		}
	}
	
	public <T extends Component> java.util.List<T> harvestMatches(Container root, Class<T> clazz) {
	    java.util.List<Container> containers = new LinkedList<>();
	    java.util.List<T> harvested = new ArrayList<>();

	    containers.add(root);
	    while (!containers.isEmpty()) {
	        Container container = containers.remove(0);
	        for (Component component : container.getComponents()) {
	            if (clazz.isAssignableFrom(component.getClass())) {
	                harvested.add((T) component);
	            } else if (component instanceof Container) {
	                containers.add((Container) component);
	            }
	        }
	    }

	    return Collections.unmodifiableList(harvested);
	}
	
	void handleClosing() {
		if (hasChanges()) {
			  Component ownerFrame = (Component) SwingUtilities.getWindowAncestor(this);
			  Tools.messageInfoBox(ownerFrame, "Info: L'oggetto � stato modificato");
		  } else {
			  dispose();
		  }
	}
	
	/**
	 * contenuto di sender modificato
	 * @param sender
	 */
	@Override
	public void changed(Object sender){
		if (!fileLoaded) return;
		if (sender != txtVersione && !txtVersione.hasChanges())
			txtVersione.addAndSetChangedNoNotify();
		syncUnderlyingObject();
		syncPreview();
	}
	
	private boolean hasChanges() {
		for (IContentsMonitored item : myMonitored) {
			item.setBgColor();
			if (item.hasChanges())
				return true;
		}
		return false;
	}

	/**
	 * imposta come non modificato
	 */
	private void setNotChanged() {
		for (IContentsMonitored item : myMonitored) {
			item.setNotChanged(); 
		}
	}
	
	/**
	 * applica le modifiche all'oggetto ItemFormativoEsercizio sottostante
	 */
	private void syncUnderlyingObject() {
		if (myOpenedFile==null) return;
		
		for (IContentsMonitored item : myMonitored) {
			item.syncUnderlyingObject();
		}
	}
	
	/**
	 * Salva l'oggetto ItemFormativoEsercizio sottostante su disco sovrascrivendo quello vecchio
	 */
	private void saveFile() {
		syncUnderlyingObject();
		
		if (!myOpenedFile.saveFile()) {
			Tools.messageInfoBox(this, "Salvataggio fallito");
		}
		
	}
	
	void syncPreview() {
	    browser.setContentType("text/html");
		try {

			//TODO JEditorPane dovrebbe caricare le immagini locali da un Hashtable, ma questa soluzione NON funziona, risolvere
			//codice provvisorio, messo sia in in SwingExercisePreview sia in SwingExerciseEditor con opportuni adattamenti

			Dictionary cache=(Dictionary)browser.getDocument().getProperty("imageCache");

			if (cache==null) {
            	cache=new Hashtable<URL, Image>();
            	browser.getDocument().putProperty("imageCache",cache);
			} 
            List<RisorseCollegateType> risorseCollegate = myOpenedFile.masterItem.getRisorseCollegate();
			if (risorseCollegate != null) {
				for (int i=0; i < risorseCollegate.size(); i++)
				{
					String imageFileName = ItemFormativoEsercizioAdv.nomeFileDaRisorsa(risorseCollegate.get(i));
					if (Tools.isImage(imageFileName)) {
						StringBuilder fixedAttachmentPath = new StringBuilder();
						if (pathStructure.imagePath != null && pathStructure.imagePath.isValid()) {
							fixedAttachmentPath.append(pathStructure.imagePath);
							if (!pathStructure.imagePath.getItemPath().endsWith(File.separator)) fixedAttachmentPath.append(File.separator); 
							fixedAttachmentPath.append(imageFileName);
							
							URL imageFile = new File(fixedAttachmentPath.toString()).toURI().toURL();
					        Image image = Toolkit.getDefaultToolkit().createImage(imageFile); 
					        cache.put(imageFile, image);
						}
					}
				}
			}

						
			browser.setText(myOpenedFile.toHtmlString(pathStructure));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			StringWriter stringWriter = new StringWriter();
			e.printStackTrace(new PrintWriter(stringWriter));
			browser.setText("<html><body><pre>" + stringWriter.toString() + "</pre></body></html>");
		}
	}
	
}
