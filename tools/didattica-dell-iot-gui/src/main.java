/*******************************************************************************
 * Copyright (C) 2021 Daniele Iunco
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

import common.*;

import widgets.*;

import interfaces.*;

import java.awt.EventQueue;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import java.awt.Insets;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.border.LineBorder;
import javax.xml.bind.JAXBException;

import java.awt.Color;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileAlreadyExistsException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Iterator;
import java.util.Map;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;
import java.awt.Component;
import javax.swing.Box;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class main implements IReferenceMonitor {
	private PathStructure pathStructure;
	
	private final boolean CODICE_IN_APPENDICE = true;
	
	private SwingExercisePreview previewTool;
	private String lastPreviewFile;
	
	private JFrame frmGestioneEsercizi;
	private JTextField txtProjectRoot;
	private JTextField readonlyTagsFilePath;
	private JTextField readonlyPrerequisitesFilePath;
	private JTextField readonlyXmlFilePath;
	private JTextField readonlyImagePath;
	private JTextField readonlySketchPath;
	private JTextField readonlyGraphPath;
	private JTextField readonlyTeXPath;
	private SwingSearchKeyBox txtSearchTitle;
	private SwingSearchKeyBox txtSearchComponents;
	private SwingSearchKeyBox txtSearchAnyText;
	private JCheckBox chkAutoPreview;
	private SwingExerciseList lstXmlFound;
	private JCheckBox chkSearchPblProblem;
	private JCheckBox chkSearchPblNewLearningGoal;
	private JCheckBox chkSearchMissingField;
	private SwingList lstPrerequisites;
	
	private JButton btnEditTags;
	private JButton btnEditPrerequisites;
	private JButton btnExerciseNew;
	private JButton btnExerciseEdit;
	private JButton btnConvertToRaw;
	private JButton btnExercisePreview;
	private JButton btnReloadOrSearch;
	private JLabel lblNewLabel_14;
	private JButton btnConvertToTeX;
	private Component horizontalStrut;

	private Map<String,ArrayList<ItemFormativoEsercizioAdv>> dicExerciseIDs; 

	private static final String TEX_AUTO_IMPORT_FILE = "auto-import.tex";
	private static final String TEX_CODE_AUTO_IMPORT_FILE = "auto-import-code.tex";

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					main window = new main();
					window.frmGestioneEsercizi.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public main() {
		dicExerciseIDs = new HashMap<String,ArrayList<ItemFormativoEsercizioAdv>>(); 

		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmGestioneEsercizi = new JFrame();
		frmGestioneEsercizi.setTitle("Gestione esercizi");
		frmGestioneEsercizi.setBounds(100, 100, 1000, 620);
		frmGestioneEsercizi.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmGestioneEsercizi.getContentPane().setLayout(new BoxLayout(frmGestioneEsercizi.getContentPane(), BoxLayout.X_AXIS));
		
		
		JPanel panel = new JPanel();
		panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		
		GridBagConstraints gbl_panel = new GridBagConstraints();
		gbl_panel.gridwidth = 1;
		gbl_panel.fill = GridBagConstraints.BOTH;
		gbl_panel.gridx = 0;
		gbl_panel.gridy = 0;
		
		frmGestioneEsercizi.getContentPane().add(panel);
		{
			GridBagLayout gridBagLayout = new GridBagLayout();
			gridBagLayout.columnWidths = new int[] {0, 0};
			gridBagLayout.rowHeights = new int[] {0, 0};
			gridBagLayout.columnWeights = new double[]{0.5, 0.5};
			gridBagLayout.rowWeights = new double[]{0.0, 1.0};
			panel.setLayout(gridBagLayout);
		}
		
		JPanel panel_1 = new JPanel();
		{
			GridBagLayout gridBagLayout = new GridBagLayout();
			gridBagLayout.columnWidths = new int[] {0, 0, 0};
			gridBagLayout.rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
			gridBagLayout.columnWeights = new double[]{0.0, 1.0, 0};
			gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0};
			panel_1.setLayout(gridBagLayout);
		}
		panel_1.setBorder(new CompoundBorder(new LineBorder(new Color(192, 192, 192)), new EmptyBorder(5, 5, 5, 5)));
		GridBagConstraints gbc_panel_1 = new GridBagConstraints();
		gbc_panel_1.insets = new Insets(0, 0, 5, 5);
		gbc_panel_1.fill = GridBagConstraints.BOTH;
		gbc_panel_1.gridx = 0;
		gbc_panel_1.gridy = 0;
		panel.add(panel_1, gbc_panel_1);
		
		JLabel lblNewLabel_6 = new JLabel("Percorsi");
		GridBagConstraints gbc_lblNewLabel_6 = new GridBagConstraints();
		gbc_lblNewLabel_6.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_6.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_6.gridx = 0;
		gbc_lblNewLabel_6.gridy = 0;
		panel_1.add(lblNewLabel_6, gbc_lblNewLabel_6);
		
		horizontalStrut = Box.createHorizontalStrut(100);
		GridBagConstraints gbc_horizontalStrut = new GridBagConstraints();
		gbc_horizontalStrut.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut.gridx = 1;
		gbc_horizontalStrut.gridy = 0;
		panel_1.add(horizontalStrut, gbc_horizontalStrut);
		
		JLabel lblNewLabel = new JLabel("Percorso radice");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 1;
		panel_1.add(lblNewLabel, gbc_lblNewLabel);
		
		txtProjectRoot = new JTextField();
		GridBagConstraints gbc_txtProjectRoot = new GridBagConstraints();
		gbc_txtProjectRoot.insets = new Insets(0, 0, 5, 5);
		gbc_txtProjectRoot.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtProjectRoot.gridx = 1;
		gbc_txtProjectRoot.gridy = 1;
		panel_1.add(txtProjectRoot, gbc_txtProjectRoot);
		txtProjectRoot.setColumns(10);
		
		JButton btnChooseRootPath = new JButton("Seleziona");
		btnChooseRootPath.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selectRootPath();
			}
		});
		GridBagConstraints gbc_btnChooseRootPath = new GridBagConstraints();
		gbc_btnChooseRootPath.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnChooseRootPath.insets = new Insets(0, 0, 5, 0);
		gbc_btnChooseRootPath.gridx = 2;
		gbc_btnChooseRootPath.gridy = 1;
		panel_1.add(btnChooseRootPath, gbc_btnChooseRootPath);
		
		JLabel lblNewLabel_1 = new JLabel("File tag");
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_1.gridx = 0;
		gbc_lblNewLabel_1.gridy = 2;
		panel_1.add(lblNewLabel_1, gbc_lblNewLabel_1);
		
		readonlyTagsFilePath = new JTextField();
		readonlyTagsFilePath.setColumns(10);
		readonlyTagsFilePath.setEditable(false);
		GridBagConstraints gbc_readonlyTagsFilePath = new GridBagConstraints();
		gbc_readonlyTagsFilePath.insets = new Insets(0, 0, 5, 5);
		gbc_readonlyTagsFilePath.fill = GridBagConstraints.HORIZONTAL;
		gbc_readonlyTagsFilePath.gridx = 1;
		gbc_readonlyTagsFilePath.gridy = 2;
		panel_1.add(readonlyTagsFilePath, gbc_readonlyTagsFilePath);
		
		btnEditTags = new JButton("Modifica");
		btnEditTags.setEnabled(false);
		btnEditTags.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnEditTags_Click();
			}
		});
		GridBagConstraints gbc_btnEditTags = new GridBagConstraints();
		gbc_btnEditTags.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnEditTags.insets = new Insets(0, 0, 5, 0);
		gbc_btnEditTags.gridx = 2;
		gbc_btnEditTags.gridy = 2;
		panel_1.add(btnEditTags, gbc_btnEditTags);
		
		JLabel lblNewLabel_2 = new JLabel("File prerequisiti");
		GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
		gbc_lblNewLabel_2.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_2.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_2.gridx = 0;
		gbc_lblNewLabel_2.gridy = 3;
		panel_1.add(lblNewLabel_2, gbc_lblNewLabel_2);
		
		readonlyPrerequisitesFilePath = new JTextField();
		readonlyPrerequisitesFilePath.setColumns(10);
		readonlyPrerequisitesFilePath.setEditable(false);
		GridBagConstraints gbc_readonlyPrerequisitesFilePath = new GridBagConstraints();
		gbc_readonlyPrerequisitesFilePath.insets = new Insets(0, 0, 5, 5);
		gbc_readonlyPrerequisitesFilePath.fill = GridBagConstraints.HORIZONTAL;
		gbc_readonlyPrerequisitesFilePath.gridx = 1;
		gbc_readonlyPrerequisitesFilePath.gridy = 3;
		panel_1.add(readonlyPrerequisitesFilePath, gbc_readonlyPrerequisitesFilePath);

		btnEditPrerequisites = new JButton("Modifica");
		btnEditPrerequisites.setEnabled(false);
		btnEditPrerequisites.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnEditPrerequisites_Click();
			}
		});
		GridBagConstraints gbc_btnEditPrerequisites = new GridBagConstraints();
		gbc_btnEditPrerequisites.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnEditPrerequisites.insets = new Insets(0, 0, 5, 0);
		gbc_btnEditPrerequisites.gridx = 2;
		gbc_btnEditPrerequisites.gridy = 3;
		panel_1.add(btnEditPrerequisites, gbc_btnEditPrerequisites);
		
		JLabel lblNewLabel_3 = new JLabel("File XML");
		GridBagConstraints gbc_lblNewLabel_3 = new GridBagConstraints();
		gbc_lblNewLabel_3.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_3.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_3.gridx = 0;
		gbc_lblNewLabel_3.gridy = 4;
		panel_1.add(lblNewLabel_3, gbc_lblNewLabel_3);
		
		readonlyXmlFilePath = new JTextField();
		readonlyXmlFilePath.setColumns(10);
		readonlyXmlFilePath.setEditable(false);
		GridBagConstraints gbc_readonlyXmlFilePath = new GridBagConstraints();
		gbc_readonlyXmlFilePath.insets = new Insets(0, 0, 5, 5);
		gbc_readonlyXmlFilePath.fill = GridBagConstraints.HORIZONTAL;
		gbc_readonlyXmlFilePath.gridx = 1;
		gbc_readonlyXmlFilePath.gridy = 4;
		panel_1.add(readonlyXmlFilePath, gbc_readonlyXmlFilePath);
		
		JLabel lblNewLabel_5 = new JLabel("File TeX");
		GridBagConstraints gbc_lblNewLabel_5 = new GridBagConstraints();
		gbc_lblNewLabel_5.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_5.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_5.gridx = 0;
		gbc_lblNewLabel_5.gridy = 5;
		panel_1.add(lblNewLabel_5, gbc_lblNewLabel_5);
		
		readonlyTeXPath = new JTextField();
		readonlyTeXPath.setColumns(10);
		readonlyTeXPath.setEditable(false);
		GridBagConstraints gbc_readonlyTeXPath = new GridBagConstraints();
		gbc_readonlyTeXPath.insets = new Insets(0, 0, 5, 5);
		gbc_readonlyTeXPath.fill = GridBagConstraints.HORIZONTAL;
		gbc_readonlyTeXPath.gridx = 1;
		gbc_readonlyTeXPath.gridy = 5;
		panel_1.add(readonlyTeXPath, gbc_readonlyTeXPath);

		JLabel lblNewLabel_7 = new JLabel("Percorso immagini");
		GridBagConstraints gbc_lblNewLabel_7 = new GridBagConstraints();
		gbc_lblNewLabel_7.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_7.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_7.gridx = 0;
		gbc_lblNewLabel_7.gridy = 6;
		panel_1.add(lblNewLabel_7, gbc_lblNewLabel_7);
		
		readonlyImagePath = new JTextField();
		readonlyImagePath.setColumns(10);
		readonlyImagePath.setEditable(false);
		GridBagConstraints gbc_readonlyImagePath = new GridBagConstraints();
		gbc_readonlyImagePath.insets = new Insets(0, 0, 5, 5);
		gbc_readonlyImagePath.fill = GridBagConstraints.HORIZONTAL;
		gbc_readonlyImagePath.gridx = 1;
		gbc_readonlyImagePath.gridy = 6;
		panel_1.add(readonlyImagePath, gbc_readonlyImagePath);

		lblNewLabel_14 = new JLabel("Percorso sketch");
		GridBagConstraints gbc_lblNewLabel_14 = new GridBagConstraints();
		gbc_lblNewLabel_14.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_14.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_14.gridx = 0;
		gbc_lblNewLabel_14.gridy = 7;
		panel_1.add(lblNewLabel_14, gbc_lblNewLabel_14);
		
		readonlySketchPath = new JTextField();
		readonlySketchPath.setColumns(10);
		readonlySketchPath.setEditable(false);
		GridBagConstraints gbc_readonlySketchPath = new GridBagConstraints();
		gbc_readonlySketchPath.insets = new Insets(0, 0, 5, 5);
		gbc_readonlySketchPath.fill = GridBagConstraints.HORIZONTAL;
		gbc_readonlySketchPath.gridx = 1;
		gbc_readonlySketchPath.gridy = 7;
		panel_1.add(readonlySketchPath, gbc_readonlySketchPath);

		JLabel lblNewLabel_15 = new JLabel("Percorso grafi");
		GridBagConstraints gbc_lblNewLabel_15 = new GridBagConstraints();
		gbc_lblNewLabel_15.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_15.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_15.gridx = 0;
		gbc_lblNewLabel_15.gridy = 8;
		panel_1.add(lblNewLabel_15, gbc_lblNewLabel_15);
		
		readonlyGraphPath = new JTextField();
		readonlyGraphPath.setColumns(10);
		readonlyGraphPath.setEditable(false);
		GridBagConstraints gbc_readonlyGraphPath = new GridBagConstraints();
		gbc_readonlyGraphPath.insets = new Insets(0, 0, 5, 5);
		gbc_readonlyGraphPath.fill = GridBagConstraints.HORIZONTAL;
		gbc_readonlyGraphPath.gridx = 1;
		gbc_readonlyGraphPath.gridy = 8;
		panel_1.add(readonlyGraphPath, gbc_readonlyGraphPath);

		JPanel panel_2 = new JPanel();
		{
			GridBagLayout gridBagLayout = new GridBagLayout();
			gridBagLayout.columnWidths = new int[] {0, 0};
			gridBagLayout.rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
			gridBagLayout.columnWeights = new double[]{0.0, 1.0};
			gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
			panel_2.setLayout(gridBagLayout);
		}
		
		JPanel panel_3 = new JPanel();
		{
			GridBagLayout gridBagLayout = new GridBagLayout();
			gridBagLayout.columnWidths = new int[] {0};
			gridBagLayout.rowHeights = new int[] {0, 0, 0, 0};
			gridBagLayout.columnWeights = new double[]{1.0};
			gridBagLayout.rowWeights = new double[]{0.0, 1.0, 0.0, 0.0};
			panel_3.setLayout(gridBagLayout);
		}
		panel_3.setBorder(new CompoundBorder(new LineBorder(new Color(192, 192, 192)), new EmptyBorder(5, 5, 5, 5)));
		GridBagConstraints gbc_panel_3 = new GridBagConstraints();
		gbc_panel_3.gridheight = 2;
		gbc_panel_3.insets = new Insets(0, 0, 0, 5);
		gbc_panel_3.fill = GridBagConstraints.BOTH;
		gbc_panel_3.gridx = 1;
		gbc_panel_3.gridy = 0;
		panel.add(panel_3, gbc_panel_3);
		
		JLabel lblNewLabel_8 = new JLabel("Lista esercizi (F2 per modificare l'ID)");
		GridBagConstraints gbc_lblNewLabel_8 = new GridBagConstraints();
		gbc_lblNewLabel_8.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_8.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel_8.gridx = 0;
		gbc_lblNewLabel_8.gridy = 0;
		panel_3.add(lblNewLabel_8, gbc_lblNewLabel_8);
		
		lstXmlFound = new SwingExerciseList();
		lstXmlFound.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				if (e.getKeyCode() == 113) { //F2
					askChangeId();
				}
			}
		});
		lstXmlFound.addPropertyChangeListener(new PropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent evt) {
				if (evt.getPropertyName().equals(SwingExerciseList.SELECTED_INDEX_CHANGED_EVENT))
					if (chkAutoPreview.isSelected()) previewHtml();
			}
		});
		//TODO implementare il necessario
		lstXmlFound.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2)
					btnEdit_click();
			}
		});

		lstXmlFound.setBorder(new LineBorder(Color.LIGHT_GRAY));
		GridBagConstraints gbc_lstXmlFound = new GridBagConstraints();
		gbc_lstXmlFound.weighty = 1.0;
		gbc_lstXmlFound.insets = new Insets(0, 0, 5, 0);
		gbc_lstXmlFound.fill = GridBagConstraints.BOTH;
		gbc_lstXmlFound.gridx = 0;
		gbc_lstXmlFound.gridy = 1;
		JScrollPane spXml = new JScrollPane(lstXmlFound);
		
		panel_3.add(spXml, gbc_lstXmlFound);
		
		chkAutoPreview = new JCheckBox("Auto anteprima");
		GridBagConstraints gbc_chkAutoPreview = new GridBagConstraints();
		gbc_chkAutoPreview.anchor = GridBagConstraints.WEST;
		gbc_chkAutoPreview.insets = new Insets(0, 0, 5, 0);
		gbc_chkAutoPreview.gridx = 0;
		gbc_chkAutoPreview.gridy = 2;
		panel_3.add(chkAutoPreview, gbc_chkAutoPreview);
		
		JPanel panel_4 = new JPanel();
		GridBagConstraints gbc_panel_4 = new GridBagConstraints();
		gbc_panel_4.insets = new Insets(0, 0, 5, 0);
		gbc_panel_4.fill = GridBagConstraints.BOTH;
		gbc_panel_4.gridx = 0;
		gbc_panel_4.gridy = 3;
		panel_3.add(panel_4, gbc_panel_4);
		panel_4.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		btnExerciseNew = new JButton("Nuovo");
		btnExerciseNew.setEnabled(false);
		btnExerciseNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnNew_click();
			}
		});
		panel_4.add(btnExerciseNew);
		
		btnExerciseEdit = new JButton("Modifica");
		btnExerciseEdit.setEnabled(false);
		btnExerciseEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnEdit_click();
			}
		});
		panel_4.add(btnExerciseEdit);
		
		btnConvertToRaw = new JButton("toString()");
		btnConvertToRaw.setEnabled(false);
		btnConvertToRaw.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				allToString();
			}
		});
		panel_4.add(btnConvertToRaw);
			
		btnConvertToTeX = new JButton("toTeX");
		btnConvertToTeX.setEnabled(false);
		btnConvertToTeX.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					convertToTeX();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		panel_4.add(btnConvertToTeX);
	
		btnExercisePreview = new JButton("Anteprima");
		btnExercisePreview.setEnabled(false);
		btnExercisePreview.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				previewHtml();
			}
		});
		
		panel_4.add(btnExercisePreview);
		panel_2.setBorder(new CompoundBorder(new LineBorder(new Color(192, 192, 192)), new EmptyBorder(5, 5, 5, 5)));
		GridBagConstraints gbc_panel_2 = new GridBagConstraints();
		gbc_panel_2.weightx = 0.1;
		gbc_panel_2.insets = new Insets(0, 0, 0, 5);
		gbc_panel_2.fill = GridBagConstraints.BOTH;
		gbc_panel_2.gridx = 0;
		gbc_panel_2.gridy = 1;
		panel.add(panel_2, gbc_panel_2);
		
		JLabel lblNewLabel_12 = new JLabel("Ricerca");
		GridBagConstraints gbc_lblNewLabel_12 = new GridBagConstraints();
		gbc_lblNewLabel_12.gridwidth = 2;
		gbc_lblNewLabel_12.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_12.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_12.gridx = 0;
		gbc_lblNewLabel_12.gridy = 0;
		panel_2.add(lblNewLabel_12, gbc_lblNewLabel_12);
		
		JLabel lblNewLabel_13 = new JLabel("Titolo");
		GridBagConstraints gbc_lblNewLabel_13 = new GridBagConstraints();
		gbc_lblNewLabel_13.weightx = 0.3;
		gbc_lblNewLabel_13.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_13.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_13.gridx = 0;
		gbc_lblNewLabel_13.gridy = 1;
		panel_2.add(lblNewLabel_13, gbc_lblNewLabel_13);
		
		txtSearchTitle = new SwingSearchKeyBox(true);
		GridBagConstraints gbc_txtSearchTitle = new GridBagConstraints();
		gbc_txtSearchTitle.weightx = 0.7;
		gbc_txtSearchTitle.insets = new Insets(0, 0, 5, 0);
		gbc_txtSearchTitle.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtSearchTitle.gridx = 1;
		gbc_txtSearchTitle.gridy = 1;
		panel_2.add(txtSearchTitle, gbc_txtSearchTitle);

		JLabel lblNewLabel_9 = new JLabel("Componenti");
		GridBagConstraints gbc_lblNewLabel_9 = new GridBagConstraints();
		gbc_lblNewLabel_9.weightx = 0.3;
		gbc_lblNewLabel_9.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_9.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_9.gridx = 0;
		gbc_lblNewLabel_9.gridy = 2;
		panel_2.add(lblNewLabel_9, gbc_lblNewLabel_9);
		
		txtSearchComponents = new SwingSearchKeyBox(true);
		GridBagConstraints gbc_txtSearchComponents = new GridBagConstraints();
		gbc_txtSearchComponents.weightx = 0.7;
		gbc_txtSearchComponents.insets = new Insets(0, 0, 5, 0);
		gbc_txtSearchComponents.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtSearchComponents.gridx = 1;
		gbc_txtSearchComponents.gridy = 2;
		panel_2.add(txtSearchComponents, gbc_txtSearchComponents);

		JLabel lblNewLabel_10 = new JLabel("Testo libero");
		GridBagConstraints gbc_lblNewLabel_10 = new GridBagConstraints();
		gbc_lblNewLabel_10.weightx = 0.3;
		gbc_lblNewLabel_10.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_10.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_10.gridx = 0;
		gbc_lblNewLabel_10.gridy = 3;
		panel_2.add(lblNewLabel_10, gbc_lblNewLabel_10);
		
		txtSearchAnyText = new SwingSearchKeyBox(true);
		GridBagConstraints gbc_txtSearchAnyText = new GridBagConstraints();
		gbc_txtSearchAnyText.weightx = 0.7;
		gbc_txtSearchAnyText.insets = new Insets(0, 0, 5, 0);
		gbc_txtSearchAnyText.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtSearchAnyText.gridx = 1;
		gbc_txtSearchAnyText.gridy = 3;
		panel_2.add(txtSearchAnyText, gbc_txtSearchAnyText);

		chkSearchPblProblem = new JCheckBox("Problema PBL");
		GridBagConstraints gbc_chkSearchPblProblem = new GridBagConstraints();
		gbc_chkSearchPblProblem.anchor = GridBagConstraints.WEST;
		gbc_chkSearchPblProblem.gridwidth = 2;
		gbc_chkSearchPblProblem.insets = new Insets(0, 0, 5, 0);
		gbc_chkSearchPblProblem.gridx = 0;
		gbc_chkSearchPblProblem.gridy = 4;
		panel_2.add(chkSearchPblProblem, gbc_chkSearchPblProblem);
		
		chkSearchPblNewLearningGoal = new JCheckBox("Obiettivo apprendimento PBL");
		GridBagConstraints gbc_chkSearchPblNewLearningGoal = new GridBagConstraints();
		gbc_chkSearchPblNewLearningGoal.anchor = GridBagConstraints.WEST;
		gbc_chkSearchPblNewLearningGoal.gridwidth = 2;
		gbc_chkSearchPblNewLearningGoal.insets = new Insets(0, 0, 5, 0);
		gbc_chkSearchPblNewLearningGoal.gridx = 0;
		gbc_chkSearchPblNewLearningGoal.gridy = 5;
		panel_2.add(chkSearchPblNewLearningGoal, gbc_chkSearchPblNewLearningGoal);
		
		chkSearchMissingField = new JCheckBox("Solo con campi richiesti mancanti");
		GridBagConstraints gbc_chkSearchMissingField = new GridBagConstraints();
		gbc_chkSearchMissingField.anchor = GridBagConstraints.WEST;
		gbc_chkSearchMissingField.gridwidth = 2;
		gbc_chkSearchMissingField.insets = new Insets(0, 0, 5, 0);
		gbc_chkSearchMissingField.gridx = 0;
		gbc_chkSearchMissingField.gridy = 6;
		panel_2.add(chkSearchMissingField, gbc_chkSearchMissingField);
		
		JLabel lblNewLabel_11 = new JLabel("Prerequisiti");
		GridBagConstraints gbc_lblNewLabel_11 = new GridBagConstraints();
		gbc_lblNewLabel_11.weightx = 0.3;
		gbc_lblNewLabel_11.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_11.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_11.gridx = 0;
		gbc_lblNewLabel_11.gridy = 7;
		panel_2.add(lblNewLabel_11, gbc_lblNewLabel_11);
		
		lstPrerequisites = new SwingList();
		
		lstPrerequisites.setBorder(new LineBorder(Color.LIGHT_GRAY));
		GridBagConstraints gbc_lstPrerequisites = new GridBagConstraints();
		gbc_lstPrerequisites.weightx = 0.7;
		gbc_lstPrerequisites.insets = new Insets(0, 0, 5, 0);
		gbc_lstPrerequisites.fill = GridBagConstraints.BOTH;
		gbc_lstPrerequisites.gridx = 1;
		gbc_lstPrerequisites.gridy = 7;
		panel_2.add(lstPrerequisites, gbc_lstPrerequisites);
		
		btnReloadOrSearch = new JButton("Cerca");
		btnReloadOrSearch.setEnabled(false);
		btnReloadOrSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				searchAndLoadXmlFiles();
			}
		});
		GridBagConstraints gbc_btnReloadOrSearch = new GridBagConstraints();
		gbc_btnReloadOrSearch.gridwidth = 2;
		gbc_btnReloadOrSearch.gridx = 0;
		gbc_btnReloadOrSearch.gridy = 8;
		panel_2.add(btnReloadOrSearch, gbc_btnReloadOrSearch);
		
	}

	protected void askChangeId() {
		if (pathStructure == null || !pathStructure.isValid()) return;
		ItemFormativoEsercizioAdv selected = lstXmlFound.getFirstSelectedObject();
		if (selected != null) {
			String currId = selected.masterItem.getIdentificatoreArgomento();
			String newId = Tools.inputBox(frmGestioneEsercizi, "Cambia ID", "Cambia ID dell'esercizio '" + selected.masterItem.getNomeDescrittivo() + "'", currId);
			if (newId != null)
				if (newId.length() > 0)
					if (newId.equals(currId))
						Tools.messageInfoBox(frmGestioneEsercizi, "Non modificato!");
					else if (keyAlreadyUsed(newId, selected).item1)
						Tools.messageInfoBox(frmGestioneEsercizi, "ERRORE: Id '" + newId + "' gi� in uso");
					else {
						selected.incrementaVersione();
						selected.masterItem.setIdentificatoreArgomento(newId);
						if (selected.saveFile())
							reloadIds();
						else
							Tools.messageInfoBox(frmGestioneEsercizi, "Salvataggio fallito");
					}
				else
					Tools.messageInfoBox(frmGestioneEsercizi, "Errore, stringa vuota");
		}
	}

	/**
	 * Carica l'elenco dei prerequisiti
	 */
	private void reloadPrerequisites() {
		if (pathStructure == null || !pathStructure.prerequisiteFile.isValid()) return;
		try {
			lstPrerequisites.removeAll();
			KeyValuePairsFile prerequisites = new KeyValuePairsFile(pathStructure.prerequisiteFile.getItemPath(), this);

    		Iterator<ChiaveValore> myIterator = prerequisites.iterator();
    		while (myIterator.hasNext())
    			lstPrerequisites.add(myIterator.next().key);
    	
		} catch (FileNotFoundException | IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//
	/**
	 * Restituisco una tupla che indica se la chiave � in uso e quali esercizi la stanno usando
	 * TODO la lista di esercizi serve per personalizzare la notifica con maggiori dettagli
	 * N.B. non � possibile escludere che seguito di modifiche manuali sui file xml ci si trovi identificativi duplicati, quindi uso una lista di esercizi, che potrebbe contenere anche pi� di un elemento 
	 * @param key
	 * @param itemToExclude
	 * @return
	 */
	private Tuple<Boolean, List<ItemFormativoEsercizioAdv>> keyAlreadyUsed(String key, ItemFormativoEsercizioAdv itemToExclude){
		if (!dicExerciseIDs.containsKey(key)) return new Tuple<Boolean, List<ItemFormativoEsercizioAdv>>(false, null);
		
		List<ItemFormativoEsercizioAdv> found = dicExerciseIDs.get(key);
		
		List<ItemFormativoEsercizioAdv> result = new ArrayList<ItemFormativoEsercizioAdv>();
		
		for (ItemFormativoEsercizioAdv item : found) {
			if (item != itemToExclude) result.add(item);
		}
		
		if (result.size() > 0)
			return new Tuple<Boolean, List<ItemFormativoEsercizioAdv>>(true, result);
		else
			return new Tuple<Boolean, List<ItemFormativoEsercizioAdv>>(false, null);
	}

	/**
	 * Cerca e carica in elenco gli esercizi corrispondenti ai criteri di ricerca specificati, in assenza di criteri di ricerca si caricano tutti gli esercizi
	 */
	private void searchAndLoadXmlFiles()
	{
		if (pathStructure == null || !pathStructure.isValid()) return;
		
		String[] selectedPrerequisites = lstPrerequisites.getSelection();
		File myXmlDir = new File(pathStructure.xmlPath.getItemPath());
		File myFilesInXmlFolder[] = myXmlDir.listFiles();
			
		lstXmlFound.removeAll();
		dicExerciseIDs.clear();

		for (int i=0; i < myFilesInXmlFolder.length; i++) {
			if (myFilesInXmlFolder[i].isFile() && myFilesInXmlFolder[i].getName().toLowerCase().endsWith(".xml"))
				try {
					String xmlFilePath = myFilesInXmlFolder[i].getAbsolutePath();
					ItemFormativoEsercizioAdv exerciseObj = new ItemFormativoEsercizioAdv(xmlFilePath, false, pathStructure);
					
					ArrayList<ItemFormativoEsercizioAdv> currList;
					if (dicExerciseIDs.containsKey(exerciseObj.masterItem.getIdentificatoreArgomento()))
						currList = dicExerciseIDs.get(exerciseObj.masterItem.getIdentificatoreArgomento());
					else {
						currList = new ArrayList<ItemFormativoEsercizioAdv>();
						dicExerciseIDs.put(exerciseObj.masterItem.getIdentificatoreArgomento(), currList);
					}
					currList.add(exerciseObj);
						
					if (exerciseObj.match(txtSearchTitle.getValues(), txtSearchAnyText.getValues(), txtSearchComponents.getValues(), chkSearchPblNewLearningGoal.isSelected(), chkSearchPblProblem.isSelected(), chkSearchMissingField.isSelected()) && exerciseObj.matchPrerequisites(selectedPrerequisites)) 					
						lstXmlFound.add(new AbstractMap.SimpleImmutableEntry<String, ItemFormativoEsercizioAdv>(xmlFilePath, exerciseObj));
				} catch (FileNotFoundException | FileAlreadyExistsException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
	}

	/**
	 * Ricarica l'HashMap degli identificatori di esercizio
	 */
	private void reloadIds()
	{
		if (pathStructure == null || !pathStructure.isValid()) return;
		
		File myFilesInXmlFolder[] = new File(pathStructure.xmlPath.getItemPath()).listFiles();
			
		dicExerciseIDs.clear();

		for (int i=0; i < myFilesInXmlFolder.length; i++) {
			if (myFilesInXmlFolder[i].isFile() && myFilesInXmlFolder[i].getName().toLowerCase().endsWith(".xml"))
				try {
					String xmlFilePath = myFilesInXmlFolder[i].getAbsolutePath();
					ItemFormativoEsercizioAdv exerciseObj = new ItemFormativoEsercizioAdv(xmlFilePath, false, pathStructure);
					
					ArrayList<ItemFormativoEsercizioAdv> currList;
					if (dicExerciseIDs.containsKey(exerciseObj.masterItem.getIdentificatoreArgomento()))
						currList = dicExerciseIDs.get(exerciseObj.masterItem.getIdentificatoreArgomento());
					else {
						currList = new ArrayList<ItemFormativoEsercizioAdv>();
						dicExerciseIDs.put(exerciseObj.masterItem.getIdentificatoreArgomento(), currList);
					}
					currList.add(exerciseObj);
						
					
				} catch (FileNotFoundException | FileAlreadyExistsException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
	}

	private void btnEdit_click() {
		if (pathStructure == null || !pathStructure.isValid()) return;
		try {
			if (lstXmlFound.getSelectionIndex() > -1) {
				SwingExerciseEditor myForm = new SwingExerciseEditor(lstXmlFound.getSelection()[0], false,
						this, "", pathStructure);
				myForm.setVisible(true);
				}
		} catch (FileAlreadyExistsException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	private String selectedFilePath() {
		if (lstXmlFound.getSelectionIndex() > -1)
			return lstXmlFound.getSelection()[0];
		return null;
	}
	
	void previewHtml() {
		try {
			String myFilePath = selectedFilePath();
			if (myFilePath == null ) return;
			ItemFormativoEsercizioAdv myItem = new ItemFormativoEsercizioAdv(myFilePath, !Tools.fileExists(myFilePath), pathStructure);
			
			if (previewTool == null || !previewTool.isDisplayable()) {
				previewTool = new SwingExercisePreview(pathStructure);
				previewTool.setVisible(true);
			}
			previewTool.loadFile(myItem);

		
		} catch (FileNotFoundException | FileAlreadyExistsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	void btnNew_click() {
		if (pathStructure == null || !pathStructure.isValid()) return;
		try {
			String sSelected = Tools.inputBox(frmGestioneEsercizi, "Nuovo file?", "Nuovo file?", "name.xml");
			if (sSelected == null) return;
			
			if (!sSelected.endsWith(".xml")) sSelected += ".xml";
			
			SwingExerciseEditor myForm = new SwingExerciseEditor(pathStructure.xmlPath + File.separator + sSelected, true, this, "esercizio" + String.valueOf(lstXmlFound.getItemCount()+1), pathStructure);
			myForm.setVisible(true);
			
		} catch (FileAlreadyExistsException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	@Override
	public void FileUpdated(String filepath) {
		if (pathStructure == null || !pathStructure.prerequisiteFile.isValid()) return;

		// TODO Auto-generated method stub
		if (filepath.equals(pathStructure.prerequisiteFile.getItemPath()))
			reloadPrerequisites();
		else if (previewTool != null && previewTool.isDisplayable() && filepath.equals(lastPreviewFile)) {
			if (!previewTool.isVisible()) previewTool.setVisible(true);
			previewTool.reloadFromDisk(lastPreviewFile);
		}
		
	}

	protected void openKeyValuePairEditor(String windowTitle, String filePath) {
		try {
			SwingKeyValuePairEditor window = new SwingKeyValuePairEditor(null,this,windowTitle,filePath);
			window.setVisible(true);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private void btnEditTags_Click() {
		if (pathStructure == null || !pathStructure.tagsFile.isValid()) return;
		openKeyValuePairEditor("Editor tags", pathStructure.tagsFile.getItemPath());
	}

	protected void btnEditPrerequisites_Click() {
		if (pathStructure == null || !pathStructure.prerequisiteFile.isValid()) return;
		openKeyValuePairEditor("Editor prerequisiti", pathStructure.prerequisiteFile.getItemPath());
	}

	/**
	 * genera il file auto-import.tex con tutti i file .tex della directory esercizi-tex escluso l'auto-import stesso.
	 */
	void recreateAutoImportTex() {
		if (pathStructure == null || !pathStructure.texPath.isValid()) return;
		
		try {
			
			File myTeXDir = new File(pathStructure.texPath.getItemPath());
			File myFilesInTeXFolder[] = myTeXDir.listFiles();

			OutputStreamWriter myWriterAutoImport = new OutputStreamWriter(new FileOutputStream(pathStructure.texPath.getItemPath() + File.separator + TEX_AUTO_IMPORT_FILE), StandardCharsets.UTF_8);
			myWriterAutoImport.write("%file generato automaticamente\n");
			OutputStreamWriter myWriterAutoImportCode = new OutputStreamWriter(new FileOutputStream(pathStructure.texPath.getItemPath() + File.separator + TEX_CODE_AUTO_IMPORT_FILE), StandardCharsets.UTF_8);
			myWriterAutoImportCode.write("%file generato automaticamente\n");

			for (int i=0; i < myFilesInTeXFolder.length; i++) {
				if (myFilesInTeXFolder[i].isFile() ) {
					if (myFilesInTeXFolder[i].getName().toLowerCase().endsWith(".code.tex") && !myFilesInTeXFolder[i].getName().equals(TEX_CODE_AUTO_IMPORT_FILE))
						myWriterAutoImportCode.write("\\subimport{.}{" + myFilesInTeXFolder[i].getName() + "}\n");
					else if (myFilesInTeXFolder[i].getName().toLowerCase().endsWith(".tex") && !myFilesInTeXFolder[i].getName().equals(TEX_AUTO_IMPORT_FILE))
						myWriterAutoImport.write("\\subimport{.}{" + myFilesInTeXFolder[i].getName() + "}\n");
				}
			}
			myWriterAutoImport.close();
			myWriterAutoImportCode.close();
	    	
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

	void recreateGVFiles() throws IOException {
		if (pathStructure == null || !pathStructure.isValid()) return;
		try {
			KeyValuePairsFile prerequisites = new KeyValuePairsFile(pathStructure.prerequisiteFile.getItemPath(), this);

			File myXmlDir = new File(pathStructure.xmlPath.getItemPath());
			File myFilesInXmlFolder[] = myXmlDir.listFiles();

			Iterator<ChiaveValore> myIterator = prerequisites.iterator();
    		while (myIterator.hasNext())
    		{
    			String thisKey = myIterator.next().key;
    			String[] selectedPrerequisites = new String[] {thisKey};
    			List<String> exercises = new ArrayList<String>();
    			
    			for (int i=0; i < myFilesInXmlFolder.length; i++) {
    				if (myFilesInXmlFolder[i].isFile() && myFilesInXmlFolder[i].getName().toLowerCase().endsWith(".xml"))
    					try {
    						String xmlFilePath = myFilesInXmlFolder[i].getAbsolutePath();
    						ItemFormativoEsercizioAdv exerciseObj = new ItemFormativoEsercizioAdv(xmlFilePath, false, pathStructure); 
    						
    						if (exerciseObj.matchPrerequisites(selectedPrerequisites)) 					
    							exercises.add(exerciseObj.masterItem.getIdentificatoreArgomento().toUpperCase()); //il codice � un campo obbligatorio, non ne controllo l'esistenza
    					} catch (FileNotFoundException | FileAlreadyExistsException e) {
    						// TODO Auto-generated catch block
    						e.printStackTrace();
    					}
    			}
    			
    			OutputStreamWriter myWriter = new OutputStreamWriter(new FileOutputStream(pathStructure.gvPath.getItemPath() + File.separator + "prerequisito-" + thisKey + ".gv"), StandardCharsets.UTF_8);
    			myWriter.write(Tools.createGVFileLR(thisKey, exercises));
    			myWriter.close();

    		}
    		
			for (int i=0; i < myFilesInXmlFolder.length; i++) {
				if (myFilesInXmlFolder[i].isFile() && myFilesInXmlFolder[i].getName().toLowerCase().endsWith(".xml"))
					try {
						String xmlFilePath = myFilesInXmlFolder[i].getAbsolutePath();
						ItemFormativoEsercizioAdv exerciseObj = new ItemFormativoEsercizioAdv(xmlFilePath, false, pathStructure); 
						
						//String thisGV = Tools.createGVFile(exerciseObj.masterItem.getIdentificatoreArgomento().toUpperCase(), Tools.fromKeysToValues(exerciseObj.masterItem.getPrerequisiti().split(","), prerequisites));
						String thisGV = Tools.createGVFileBT(exerciseObj.masterItem.getIdentificatoreArgomento().toUpperCase(), exerciseObj.masterItem.getPrerequisiti().split(","));
						
						OutputStreamWriter myWriter = new OutputStreamWriter(new FileOutputStream(pathStructure.gvPath.getItemPath() + File.separator + exerciseObj.gvFileName()), StandardCharsets.UTF_8);
		    			myWriter.write(thisGV);
		    			myWriter.close();
						
					} catch (FileNotFoundException | FileAlreadyExistsException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
  		
			

    			
		} catch (FileNotFoundException | IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Converte i file in formato LaTeX, ma solo se non esiste il file .tex oppure, nel caso esista, solo se � cambiata la versione
	 * alla fine rigenera il file auto-import.tex
	 * @throws IOException 
	 */
	void convertToTeX() throws IOException {
		if (pathStructure == null || !pathStructure.isValid()) return;
		
		String sSeparator = File.separator;
		
		String newSeparator = Tools.inputBox(frmGestioneEsercizi, "Separatore di percorso?", sSeparator, sSeparator);
		if (newSeparator != null && newSeparator.length() > 0)
			sSeparator=newSeparator;
				
		ItemFormativoEsercizioAdv[] src = lstXmlFound.getAllItems();
		for (int i=0; i < src.length; i++) {
			ItemFormativoEsercizioAdv current = src[i];
			current.reloadFromUnderlyingXml();
			String sTextFileName = src[i].getXmlAbsolutePath();
			sTextFileName = sTextFileName.substring(sTextFileName.lastIndexOf(File.separator)+1, sTextFileName.lastIndexOf(".")) + ".tex";
			String outputPath = pathStructure.texPath.getItemPath() + File.separator + sTextFileName;
			
			String sTextAppendixFileName = src[i].getXmlAbsolutePath();
			sTextAppendixFileName = sTextAppendixFileName.substring(sTextAppendixFileName.lastIndexOf(File.separator)+1, sTextAppendixFileName.lastIndexOf(".")) + ".code.tex";
			String outputAppendixPath = pathStructure.texPath.getItemPath() + File.separator + sTextAppendixFileName;
			
			
			try {
				Tuple<String,String> texResult = current.toLaTeXString(pathStructure, CODICE_IN_APPENDICE, sSeparator);
				
				if (!Tools.fileExists(outputPath) && !Tools.fileExists(outputAppendixPath)) { //il file dell'esercizio .tex e dell'appendice non esistono, devo creare i file
					Tools.saveTextToFile(texResult.item1, outputPath);
					if (texResult.item2 != null && texResult.item2.length() > 0)
						Tools.saveTextToFile(texResult.item2, outputAppendixPath);
				}
				else { //almeno uno dei due file esiste, se � cambiata la versione rinomino ci� che esiste
					int foundFileVersionOnDisk = ItemFormativoEsercizioAdv.getLaTeXVersion(outputPath);
					Integer rawVersione = current.masterItem.getVersione();
					int iVersione = -1;
					if (rawVersione != null) iVersione = rawVersione.intValue();
					if (iVersione != foundFileVersionOnDisk) { 
						String sSuffix = java.util.UUID.randomUUID().toString() + ".bak";
						Tools.renameFileIfExists(outputPath, outputPath + "." + String.valueOf(foundFileVersionOnDisk) + "." + sSuffix);
						Tools.renameFileIfExists(outputAppendixPath, outputAppendixPath + "." + String.valueOf(foundFileVersionOnDisk) + "." + sSuffix);
						
						Tools.saveTextToFile(texResult.item1, outputPath);
						if (texResult.item2 != null && texResult.item2.length() > 0)
							Tools.saveTextToFile(texResult.item2, outputAppendixPath);
					}
				}
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		recreateGVFiles();
		recreateAutoImportTex();
		
	}
	
	void allToString() {
		
	}
	
	/*
	
	void allToString() {

		try {
			KeyValuePairsFile prerequisites;
			prerequisites = new KeyValuePairsFile(txtPrereq.getText(), this);

			Iterator<ChiaveValore> myIterator = prerequisites.iterator();
			
			while (myIterator.hasNext())
				allToString(exerciseSearch(myIterator.next().key), myIterator.next().key);
			
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	void allToString00(boolean auto) {
		StringBuilder allToGrafo = new StringBuilder();
		StringBuilder allToGrafo2 = new StringBuilder();
		

		java.util.List<Map.Entry<String, ItemFormativoEsercizioAdv>> src = exerciseSearch();

		StringBuilder allToString = new StringBuilder();
				
		for (int i=0; i < src.size(); i++) {
			if (i>0) allToString.append("\n");
			allToString.append(src.get(i).getValue().toString());
		}
		
		
		for (int i=0; i < src.size(); i++) {
			if (i>0) allToGrafo.append("\n");
			allToGrafo.append(src.get(i).getValue().toNodo());
		}

		allToGrafo.append ("\n");
		
		for (int i=0; i < src.size(); i++) {
			if (i>0) allToGrafo2.append("\n");
			allToGrafo2.append(src.get(i).getValue().toNodoConArchi());
		}
		
		String defFileName = "file.txt";
		if (lstPrerequisites.getSelectedIndex() > -1) {
			defFileName = lstPrerequisites.getSelection()[0] + ".txt";
		}
		
		defFileName = pathStructure.projectRoot + File.separator + defFileName + ".gv";
				
		String sAbsolutePath = Tools.inputBox(frmGestioneEsercizi, "Percorso assoluto file di destinazione?", "Salva tutto in un file di testo", defFileName);
		if (Tools.fileExists(sAbsolutePath)) {
			Tools.messageInfoBox(frmGestioneEsercizi, "File gi� esistente");
			return;
		}

    	try {
    		OutputStreamWriter myWriter = new OutputStreamWriter(new FileOutputStream(sAbsolutePath), StandardCharsets.UTF_8);
    		myWriter.write("# qui elenco tutti gli esercizi\n");
    		myWriter.write("node [shape=rectangle,style=filled]\n");
			myWriter.write(allToGrafo.toString());
			myWriter.write("\n\n# qui elenco tutte le dipendenze\n");
			myWriter.write("node [shape=oval,fillcolor=yellow]\n");
			myWriter.write(allToGrafo2.toString());
			//myWriter.write(allToString.toString());
			Tools.messageInfoBox(frmGestioneEsercizi, "Salvato");
	 		myWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	void allToString(java.util.List<Map.Entry<String, ItemFormativoEsercizioAdv>> src, String key) {
		StringBuilder allToGrafo = new StringBuilder();
		StringBuilder allToGrafo2 = new StringBuilder();
		
		for (int i=0; i < src.size(); i++) {
			if (i>0) allToGrafo.append("\n");
			allToGrafo.append(src.get(i).getValue().toNodo());
		}

		allToGrafo.append ("\n");
		
		for (int i=0; i < src.size(); i++) {
			if (i>0) allToGrafo2.append("\n");
			allToGrafo2.append(src.get(i).getValue().toNodoConArchi());
		}
		
		String defFileName = pathStructure.projectRoot + File.separator + "grafo" + File.separator + key + ".gv";
				
		try {
    		OutputStreamWriter myWriter = new OutputStreamWriter(new FileOutputStream(defFileName), StandardCharsets.UTF_8);
    		myWriter.write("# qui elenco tutti gli esercizi\n");
    		myWriter.write("node [shape=rectangle,style=filled]\n");
			myWriter.write(allToGrafo.toString());
			myWriter.write("\n\n# qui elenco tutte le dipendenze\n");
			myWriter.write("node [shape=oval,fillcolor=yellow]\n");
			myWriter.write(allToGrafo2.toString());
			//myWriter.write(allToString.toString());
			Tools.messageInfoBox(frmGestioneEsercizi, "Salvato");
	 		myWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	*/
	
	
	
	/**
	 * Chiede all'utente il percorso radice, elabora gli altri e li scrive nelle caselle di testo
	 */
	void selectRootPath() {
		String result;

		final JFileChooser fc = new JFileChooser();
		if (Tools.directoryExists(txtProjectRoot.getText())) fc.setCurrentDirectory(new File(txtProjectRoot.getText()));
		fc.setFileSelectionMode( JFileChooser.DIRECTORIES_ONLY);
		int returnVal = fc.showOpenDialog(frmGestioneEsercizi);
		if (returnVal == JFileChooser.APPROVE_OPTION)
			result = fc.getSelectedFile().getAbsolutePath();
		else
			return;
		
		pathStructure = new PathStructure(result);
		Tuple<Boolean, String> validationResult = pathStructure.validate();

		btnEditTags.setEnabled(validationResult.item1);
		btnEditPrerequisites.setEnabled(validationResult.item1);
		btnExerciseNew.setEnabled(validationResult.item1);
		btnExerciseEdit.setEnabled(validationResult.item1);
		btnConvertToRaw.setEnabled(validationResult.item1);
		btnConvertToTeX.setEnabled(validationResult.item1);
		btnExercisePreview.setEnabled(validationResult.item1);
		btnReloadOrSearch.setEnabled(validationResult.item1);

		if (!validationResult.item1) {
			Tools.messageInfoBox(frmGestioneEsercizi, validationResult.item2);
			return;
		}

		txtProjectRoot.setText(pathStructure.projectRoot.getItemPath());
		readonlyTagsFilePath.setText(pathStructure.tagsFile.getItemPath());
		readonlyPrerequisitesFilePath.setText(pathStructure.prerequisiteFile.getItemPath());
		readonlyXmlFilePath.setText(pathStructure.xmlPath.getItemPath());
		readonlyTeXPath.setText(pathStructure.texPath.getItemPath());
		readonlyImagePath.setText(pathStructure.imagePath.getItemPath());
		readonlySketchPath.setText(pathStructure.sketchPath.getItemPath());
		readonlyGraphPath.setText(pathStructure.gvPath.getItemPath());
		
		reloadPrerequisites();
		
		searchAndLoadXmlFiles();
	}

	
/**
 * di seguito eventuali pezzi di codice tolto	
 */
	
	
	/*
	void convertToMd() {
		//Disattivato, eventualmente da modificare per gestire le versioni dei file
		java.util.List<Map.Entry<String, ItemFormativoEsercizioAdv>> src = exerciseSearch();
		for (int i=0; i < src.size(); i++) {
			String outputPath = txtMdPath.getText() + File.separator + src.get(i).getKey().substring(src.get(i).getKey().lastIndexOf(File.separator)+1, src.get(i).getKey().lastIndexOf(".")) + ".md";
			try {
				ItemFormativoEsercizioAdv.saveToFile(src.get(i).getValue().toMdString(pathStructure), outputPath);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}*/
	
	
}
