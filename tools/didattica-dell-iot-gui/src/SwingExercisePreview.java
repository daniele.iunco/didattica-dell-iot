/*******************************************************************************
 * Copyright (C) 2021 Daniele Iunco
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/


import common.*;

import interfaces.*;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.FileAlreadyExistsException;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.xml.bind.JAXBException;


//TODO serialVersionUID

/**
 * Finestra di anteprima dell'esercizio
 * @author Daniele Iunco
 * @version 1.0
 *
 */
public class SwingExercisePreview extends JDialog {
	private static final long serialVersionUID = 1L;
	private ItemFormativoEsercizioAdv myOpenedFile; 
	private JEditorPane browser;
	private PathStructure pathStructure;

	private final JPanel contentPanel = new JPanel();

	/**
	 * Create the dialog.
	 */
	public SwingExercisePreview(PathStructure pathStructure) {
		setTitle("Anteprima esercizio");
		this.pathStructure = pathStructure;
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

		setBounds(100, 100, 1024, 768);
		
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Chiudi");
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
		
		browser = new JEditorPane();
		browser.setEditable(false);   

        JScrollPane scrollPane = new JScrollPane(browser);    
         getContentPane().add(scrollPane, BorderLayout.CENTER);

	}
	

	public void open(ItemFormativoEsercizioAdv item) {
		this.myOpenedFile = item;
		if (myOpenedFile != null) reloadCurrentFile();
	}
	
	public void close() {
		dispose();
	}
	
	public void reloadFromDisk(String xmlAbsolutePath) {
		if (myOpenedFile == null) return;
		
		try {
			myOpenedFile = new ItemFormativoEsercizioAdv(xmlAbsolutePath, false, pathStructure);
		} catch (FileNotFoundException | FileAlreadyExistsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		reloadCurrentFile();
	}

	
	public void loadFile(ItemFormativoEsercizioAdv item) throws FileNotFoundException {
		myOpenedFile = item;
	    reloadCurrentFile();
	}

	public void reloadCurrentFile() {
		if (myOpenedFile != null) {
		    browser.setContentType("text/html");
			try {
				
				//TODO JEditorPane dovrebbe caricare le immagini locali da un Hashtable, ma questa soluzione NON funziona, risolvere
				//codice provvisorio, messo sia in in SwingExercisePreview sia in SwingExerciseEditor con opportuni adattamenti

				Dictionary cache=(Dictionary)browser.getDocument().getProperty("imageCache");

				if (cache==null) {
	            	cache=new Hashtable<URL, Image>();
	            	browser.getDocument().putProperty("imageCache",cache);
				} 
	            List<RisorseCollegateType> risorseCollegate = myOpenedFile.masterItem.getRisorseCollegate();
				if (risorseCollegate != null) {
					for (int i=0; i < risorseCollegate.size(); i++)
					{
						String imageFileName = ItemFormativoEsercizioAdv.nomeFileDaRisorsa(risorseCollegate.get(i));
						if (Tools.isImage(imageFileName)) {
							StringBuilder fixedAttachmentPath = new StringBuilder();
							if (pathStructure.imagePath != null && pathStructure.imagePath.isValid()) {
								fixedAttachmentPath.append(pathStructure.imagePath);
								if (!pathStructure.imagePath.getItemPath().endsWith(File.separator)) fixedAttachmentPath.append(File.separator); 
								fixedAttachmentPath.append(imageFileName);
								
								URL imageFile = new File(fixedAttachmentPath.toString()).toURI().toURL();
						        Image image = Toolkit.getDefaultToolkit().createImage(imageFile); 
						        cache.put(imageFile, image);
							}
						}
					}
				}

	            String output = myOpenedFile.toHtmlString(pathStructure);
				browser.setText(output);
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				StringWriter stringWriter = new StringWriter();
				e.printStackTrace(new PrintWriter(stringWriter));
				browser.setText("<html><body><pre>" + stringWriter.toString() + "</pre></body></html>");
			}
		}
	}
	
}
