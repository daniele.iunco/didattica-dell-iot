<!--<metadata>
<title>Indicatore di stato con Led RGB</title>
<tags>Base,DigitalInput,DigitalOutput,Rimbalzi</tags>
<prerequisites>digitalInput,digitalOutput,debouncing</prerequisites>
</metadata>-->
[OK]Indicatore di stato con Led RGB
====

Prerequisiti:{-}
----
- I/O digitale
- Gestione dei rimbalzi

Componenti:{-}
----
- Led RGB su modulo con resistori, oppure Led RGB + 3 resistori
- Pulsante

Scopo dell'esercizio:{-}
----
Mostrare uno stato tra più possibili grazie al colore del LED RGB.

Risultato atteso:{-}
----
Costruire un circuito che, premendo il pulsante, cambia il colore del LED; alla pressione del pulsante deve accendersi prima R poi G poi B.

Eventuali funzionalità aggiuntive:{-}
----
Definire una sequenza di combinazioni di colori, la cui esecuzione sarà avviata e interrotta con la pressione del pulsante.

Errori comuni:{-}
----
Inversione collegamenti LED

Possibile comportamento erratico:{-}
----
Possibile rimbalzo pulsante (se delay molto basso)

Risultato di apprendimento:{-}
----
Individuare la polarità dei LED e il modo corretto di gestire un LED RGB a catodo comune e anodo comune

Indicazioni per la soluzione:{-}
----
LED a Catodo comune:
Catodo connesso a Gnd
Ciascun terminale R, G, B, connesso con un resistore a un GPIO digitale.

LED ad Anodo comune:
Catodo connesso a Vcc
Ciascun terminale R, G, B, connesso con un resistore a un GPIO digitale.
