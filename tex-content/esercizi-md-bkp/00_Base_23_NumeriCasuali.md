<!--<metadata>
<title>Costruire una roulette a LED</title>
<tags>Avanzato,AnalogInput,Software</tags>
<prerequisites>analogInput,digitalOutput</prerequisites>
</metadata>-->
[OK]Costruire una roulette a LED
====

Prerequisiti:{-}
----
- Gestione dei rimbalzi

Componenti:{-}
----
- LED e relativi resistori
- Pulsante
- Registro a scorrimento, nel caso si vogliano usare più LED di quanti se ne possono gestire con i GPIO

Scopo dell'esercizio:{-}
----
Le schede di sviluppo non hanno componenti specifici per generare numeri casuali e nemmeno un orologio che si possa usare per l'inizializzazione del generatore di numeri.
Nella piattaforma arduino i numeri casuali generati con random() sono presi da una sequenza predeterminata, e per non ottenere la stessa sequenza ad ogni avvio si dovrebbe inizializzare il generatore di numeri casuali con randomSeed(n) ma manca l'hardware adatto.

Risultato atteso:{-}
----
Costruire una roulette a LED opportunamente disposti, con un pulsante per lanciare il dado, che dovrà far fermare la roulette virtuale su un numero accendendo uno dei LED.
Inizializzare in modo corretto il generatore di numeri casuali, una soluzione al problema è ottenere una lettura che non è prevedibile con analogRead() su un pin analogico non collegato, che dovrebbe ricevere interferenze, e valutare l'entropia prima di considerare la lettura valida.

Generare automaticamente il primo lancio del dado all'avvio, per accertarsi che non si ripeta sempre lo stesso numero.

Eventuali funzionalità aggiuntive:{-}
----
Utilizzare i LED della roulette come se fossero una barra LED, per rappresentare il livello di entropia dal minimo al massimo.

Errori comuni:{-}
----
Inversione collegamenti dei LED

Possibile comportamento erratico:{-}
----
In caso di metodo setup non corretto, il lancio del dado all'avvio genererà sempre lo stesso numero.

Risultato di apprendimento:{-}
----
Ripetendo le misurazioni si riscontrerà che non sempre la lettura varia, potrebbero ripetersi misurazioni uguali per carenza di interferenze e quindi come conseguenza all'accensione si potrebbe ripetere il primo numero della sequenza.
Quindi l'input analogico non sempre è variabile in modo imprevedibile e, se è critico ottenere una sequenza di numeri casuali sempre diversa, bisogna avere entropia sufficiente.

Indicazioni per la soluzione:{-}
----
In setup(), leggere il valore con analogRead e calcolare l'entropia, misurando la deviazione standard dei valori ottenuti, ed eventualmente collegare un resistore di valore elevato (es. 1 MOhm) tra pin analogico e massa per ricevere più rumore.
Un'alternativa per Arduino, da valutare, può essere utilizzare il sensore di temperatura integrato nel microcontrollore sfruttando l'instabilità del valore restituito, ed usare quel valore per inizializzare il generatore di numeri casuali.

Sarà importante il metodo setup() che dovrà misurare l'input analogico e attendere di avere entropia sufficiente per poter inizializzare il generatore di numeri casuali, tale stato di attesa si può notificare all'utente facendo lampeggiare il LED integrato sulla scheda di sviluppo.
