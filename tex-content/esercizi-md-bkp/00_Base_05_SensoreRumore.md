<!--<metadata>
<title>Interruttore acustico o monitor per livello di rumore ambientale</title>
<tags>Medio,DigitalOutput,AnalogInput</tags>
<prerequisites>analogInput,digitalOutput</prerequisites>
</metadata>-->
[OK]Interruttore acustico o monitor per livello di rumore ambientale
====

Prerequisiti:{-}
----
- Funzionamento sensore di rumore

Componenti:{-}
----
- Sensore di rumore con uscita analogica (es. KY-038)
- Relè

Scopo dell'esercizio:{-}
----
Misurare una grandezza analogica, distinguendo tra le misure quelle che corrispondono ad un particolare evento.

Risultato atteso:{-}
----
Costruire un oggetto che, al rilevamento di un rumore come un battito di mani, cambia lo stato di accensione di un relè tra On e Off.
Individuare un valore di soglia adeguato e codificarlo nel software.

Eventuali funzionalità aggiuntive:{-}
----
Registrare suoni nel tempo, stampando una media mobile delle ampiezze

Se il sensore è abbastanza sensibile, costruire un decoder morse, che ad esempio distingue punti e linee rilevando la durata dei suoni emessi da un altro oggetto (buzzer).

Errori comuni:{-}
----
Errori di collegamento.

Possibile comportamento erratico:{-}
----
L'esempio del kit può non essere adeguato
https://www.youtube.com/watch?v=CbovaHqvdsM

Individuare una soglia errata può portare a falsi positivi, se l'ambiente è rumoroso si può pensare a una procedura di setup che misura il rumore dell'ambiente.

Risultato di apprendimento:{-}
----
Utilizzare un sensore analogico attivo.

Indicazioni per la soluzione:{-}
----
Collegare l'output analogico del sensore a un pin di input analogico della scheda di sviluppo, collegare il pin di input del relè a un GPIO scelto, seguendo eventuali indicazioni sui moduli.
