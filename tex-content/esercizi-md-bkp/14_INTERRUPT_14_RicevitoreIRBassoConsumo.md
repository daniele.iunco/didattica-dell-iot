<!--<metadata>
<title>Ricevitore a basso consumo per telecomando a raggi infrarossi</title>
<tags>Base,DigitalInput,Software,Risparmio energetico</tags>
<prerequisites>digitalInput,digitalOutput,swpowersave</prerequisites>
</metadata>-->
[OK]Ricevitore a basso consumo per telecomando a raggi infrarossi
====

Prerequisiti:{-}
----
- Funzionamento del sensore VS1838
- Interrupt
- Modalità di risparmio energetico

Componenti:{-}
----
- ricevitore a raggi infrarossi VS1838
- telecomando a raggi infrarossi
- resistore 10 kOhm (resistore di pullup)

Scopo dell'esercizio:{-}
----
Quando la fonte di alimentazione è una batteria è importante saper individuare opportunità per l'utilizzo delle modalità di risparmio energetico disponibili e massimizzare così l'autonomia.

Risultato atteso:{-}
----
Costruire un ricevitore a raggi infrarossi che:
- consente di accendere e spegnere il led integrato con il telecomando;
- quando non arriva un segnale per più di 5 secondi entra in modalità a basso consumo e ci rimane fino alla ricezione di un nuovo segnale.

Eventuali funzionalità aggiuntive:{-}
----
N/A

Errori comuni:{-}
----
N/A

Possibile comportamento erratico:{-}
----
La scelta di un GPIO non abilitato agli interrupt o non corretto per il risveglio dalla modalità a basso consumo scelta può rendere irreversibile il passaggio in modalità a basso consumo e richiedere il reset per il risveglio.

Risultato di apprendimento:{-}
----
In questo caso, dato che che l'azione va svolta solo in presenza dell'input dell'utente, e tale input è un segnale esterno che si può può rilevare con gli interrupt, si può utilizzare la modalità power down, o altra modalità equivalente prevista dal microcontrollore utilizzato.

Indicazioni per la soluzione:{-}
----
La linea dati del sensore cambia livello logico quando arriva un segnale e si può collegare a un pin abilitato all'utilizzo degli interrupt.
E' sufficiente mettere il microcontrollore in modalità power down disattivando tutto il possibile (es. per Arduino: LowPower.powerDown(SLEEP_FOREVER, ADC_OFF, BOD_OFF) ) e farlo riattivare sfruttando gli interrupt.
