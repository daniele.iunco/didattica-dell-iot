<!--<metadata>
<title>Ohmmetro</title>
<tags>Base,AnalogInput</tags>
<prerequisites>ohmlaw,analogInput</prerequisites>
</metadata>-->
[OK]Ohmmetro
====

Prerequisiti:{-}
----
- legge di Ohm e partitore di tensione
- funzionamento dell'ADC e istruzione analogRead

Componenti:{-}
----
- R1 - 1 resistore da 1kOhm, il resistore di valore noto
- R2 - resistore di altro valore da misurare

Scopo dell'esercizio:{-}
----
Comprendere come si applica la legge di Ohm con un esempio d'uso del partitore di tensione.

Risultato atteso:{-}
----
Misurare la resistenza del resistore incognito, disponendo di un altro resistore di valore noto.

Eventuali funzionalità aggiuntive:{-}
----
N/A

Errori comuni:{-}
----
Scambiare resistore noto e ignoto

Possibile comportamento erratico:{-}
----
N/A

Risultato di apprendimento:{-}
----
Utilizzare il partitore di tensione per ottenere il valore di un resistore incognito.

Indicazioni per la soluzione:{-}
----
Costruire un partitore di tensione, con il resistore noto tra massa e 

![Esempio](./images/partitore01_bb.png)

-
![Esempio](./images/schema_partitore.png)
