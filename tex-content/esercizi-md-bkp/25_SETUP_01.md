<!--<metadata>
<title>Setup di rete</title>
<tags>Avanzato,DigitalInput,Software,EEPROM</tags>
<prerequisites>digitalInput,eeprom</prerequisites>
</metadata>-->
[OK]Setup di rete
====

Prerequisiti:{-}
----
- Uso della memoria EEPROM integrata o altra memoria non volatile equivalente
- Conoscenza delle librerie software per l'impostazione dell'interfaccia di rete del dispositivo

Componenti:{-}
----
- Scheda di sviluppo con scheda di rete wifi o ethernet, in base alla disponibilità

Scopo dell'esercizio:{-}
----
La produzione di oggetti che saranno utilizzati da terzi richiede di predisporre procedure opportune per la configurazione iniziale, ad esempio per i parametri di rete, non si possono utilizzare parametri hard coded.

Risultato atteso:{-}
----
Ipotizzando di dover realizzare una procedura di inizializzazione software per la configurazione di rete, ideare un sistema per accedere alla modalità di configurazione e creare un'interfaccia minimale per inserire i parametri di rete.
La stessa modalità servirà in caso di cambiamenti imprevisti ai parametri di rete, per non dover azzerare completamente ogni informazione memorizzata ma avere la possibilità di intervenire esclusivamente sui parametri da modificare.

Eventuali funzionalità aggiuntive:{-}
----
N/A

Errori comuni:{-}
----
N/A

Possibile comportamento erratico:{-}
----
Se si decide di utilizzare un GPIO già usato per altro, scegliere per errore qualcosa connesso a un sensore che può causare il rilevamento di un input anche in fase di avvio.

Validità per PBL:{-}
----
Problema da definire e analizzare: Si
Necessaria individuazione nuovi obiettivi di apprendimento: Si
Dettagli obiettivo di apprendimento atteso:
Studiare un sistema per ottenere un identificatore unico dell'oggetto sulla rete, e trovare un sistema compatibile con il microcontrollore in uso per consentire all'utente di accedere alla procedura di configurazione ed impostare i parametri di rete, con tutte le opzioni ragionevoli per il tipo di interfaccia di rete (es. wifi in modalità access point o client, con e senza DHCP, oppure ethernet con e senza DHCP

Risultato di apprendimento:{-}
----
Gestire parametri di configurazione tramite un'interfaccia utente creata ad hoc accessibile grazie ad una specifica modalità di avvio dell'oggetto

Indicazioni per la soluzione:{-}
----
Scegliere un GPIO da usare per consentire all'utente di selezionare la modalità di configurazione, in setup() impostare quel GPIO in modalità INPUT_PULLUP, verificare se è su LOW e solo in quel caso far eseguire la procedura di configurazione della connessione di rete.  
L'utente che dispone del prototipo, per accedere alla modalità di configurazione dovrà portare a LOW il pin scelto con un ponticello e premere il pulsante reset.  
E' importante il concetto delle modalità di avvio multiple, non la tecnica che poi sceglierà chi produrrà l'hardware definitivo per accedere alla configurazione premendo un pulsante.

Modalità di configurazione rete WiFi:

Con avvio in modalità setup il software deve:

- individuare un identificativo unico dell'oggetto, ad esempio leggere il MAC address;
- impostare la rete in modalità access point utilizzando l'identificativo appena individuato come SSID, con una password predefinita e DHCP.
- mettere a disposizione dell'utente un'interfaccia minimale dove impostare i parametri di accesso a una rete WiFi esistente, oppure i parametri per far funzionare l'oggetto da access point indipendente, a discrezione dell'utente.
- si potrebbe anche inserire nell'interfaccia una funzione di scanner di rete, per cercare le reti WiFi esistenti e permettere all'utente di sceglierne una.
- se non si dispone di una libreria server DHCP o non c'è spazio per caricarla, scegliere un IP statico.
- tutti i parametri predefiniti poi dovranno essere inseriti nella documentazione dell'oggetto finale.

Estensione:
Individuare un'area di memoria EEPROM su cui l'oggetto non dovrà mai scrivere, da utilizzare per la password WiFi predefinita, leggendola da li quando serve, questo sarà utile perché consentirà in fase di produzione di preconfigurare gli oggetti con password uniche diverse per ogni dispositivo, programmando il chip della memoria con un programmatore progettato a questo scopo.

Modalità di setup in presenza di una scheda di rete ethernet:

Il criterio sarà lo stesso, con opportuni adattamenti data l'assenza della parte wireless e dell'access point.
