<!--<metadata>
<title>Charlieplexing</title>
<tags>Laboratorio di saldatura</tags>
<prerequisites>diode</prerequisites>
</metadata>-->
[OK]Charlieplexing
====

Prerequisiti:{-}
----
- Funzionamento dei LED

Componenti:{-}
----
- LED
- Resistori
- Saldatore
- Filo per ponticelli
- Piastra millefori

Scopo dell'esercizio:{-}
----
Dato il numero limitato di GPIO si devono trovare soluzioni per gestire più linee di I/O con meno pin.
Il Charlieplexing è una soluzione valida per gestire più LED.

Risultato atteso:{-}
----
Costruire un array di LED charlieplexed

Eventuali funzionalità aggiuntive:{-}
----
N/A

Errori comuni:{-}
----
Saldature fredde

Possibile comportamento erratico:{-}
----
LED che non si accendono in caso di errori di saldatura o inversione della polarità.

Risultato di apprendimento:{-}
----
Effettuare saldature corrette costruendo qualcosa che si può provare.

Indicazioni per la soluzione:{-}
----
https://en.wikipedia.org/wiki/Charlieplexing

Fare attenzione al carico massimo che si può alimentare con i GPIO del microcontrollore, dato il gran numero di LED.
