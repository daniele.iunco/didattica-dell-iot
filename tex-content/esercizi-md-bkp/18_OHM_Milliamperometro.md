<!--<metadata>
<title>Milliamperometro</title>
<tags>Base,AnalogInput</tags>
<prerequisites>ohmlaw,analogInput</prerequisites>
</metadata>-->
[OK]Milliamperometro
====

Prerequisiti:{-}
----
- Legge di Ohm

Componenti:{-}
----
- 1 Resistore 1 Ohm
- LED + resistore da 220 Ohm (massimo 5)

Scopo dell'esercizio:{-}
----
Tra i possibili input analogici da misurare c'è la corrente, quindi è utile provare a costruire un milliamperometro per misurare anche questa grandezza.

Risultato atteso:{-}
----
Costruire un milliamperometro per misurare il consumo massimo 5 LED in parallelo, ciascuno con un resistore da 220 Ohm.

Per misurare una corrente si deve utilizzare un resistore attraverso cui farla circolare, per poi misurarne la tensione ai capi, un resistore utilizzato a questo scopo è detto di shunt.
Il resistore di shunt dovrebbe avere un valore molto basso per generare una caduta di tensione minima, nell'ordine dei milliVolt, così da non rendere necessario dissipare potenze elevate, tuttavia la tensione di riferimento dell'ADC del microcontrollore non può essere inferiore al volt.

I 5 LED in parallelo consumeranno meno di 100 mA, quindi l'esercizio si potrà svolgere con un resistore da 1 Ohm 1/4W, è un valore insolito ma considerata una caduta di tensione massima di 100 mV, non si supereranno i limiti di funzionamento del componente.

Eventuali funzionalità aggiuntive:{-}
----
N/A

Errori comuni:{-}
----
Errori di connessione, possono causare danni

Possibile comportamento erratico:{-}
----
N/a

Risultato di apprendimento:{-}
----
Misurare la corrente

Indicazioni per la soluzione:{-}
----
Collegare il resistore di shunt tra il polo negativo dei 5V di alimentazione del microcontrollore e il negativo del LED da testare, e collegare l'ingresso analogico del microcontrollore tra il resistore di shunt e il LED da testare.
