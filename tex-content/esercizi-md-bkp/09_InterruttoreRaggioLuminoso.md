<!--<metadata>
<title>Interruttore a raggio luminoso</title>
<tags>Base,DigitalOutput,AnalogInput,Software</tags>
<prerequisites>ohmlaw,analogInput,digitalOutput</prerequisites>
</metadata>-->
[OK]Interruttore a raggio luminoso
====

Prerequisiti:{-}
----
- legge di Ohm e partitore di tensione
- funzionamento dell'ADC e istruzione analogRead
- funzionamento della fotoresistenza

Componenti:{-}
----
- 1 resistore di valore noto
- 1 fotoresistenza
- 1 potenziometro da almeno 1k
- (opzionale) modulo con relè

Scopo dell'esercizio:{-}
----
Il concetto di rilevare un segnale luminoso, oltre che ai raggi infrarossi, anche a una normale fonte di luce come la torcia dello smartphone, se si utilizza una fotoresistenza al posto del fotodiodo.
In questo caso il rilevamento è soggetto ad interferenze ambientali proprio per la luce ambientale e si deve definire una soglia sotto la quale considerare la luce rilevata un'interferenza. 

Risultato atteso:{-}
----
Costruire un oggetto che controlla il livello di luminosità rilevato dalla fotoresistenza per determinare quando questa è illuminata con un fascio di luce, come ad esempio con la torcia dello smartphone.
L'oggetto deve funzionare in un ambiente con una normale illuminazione, si può scegliere una soglia ragionevole da impostare nel codice, va quindi escluso il caso del funzionamento sotto diretta esposizione al sole.

Eventuali funzionalità aggiuntive:{-}
----
Con un pin digitale si può controllare un relè, utilizzando un modulo già pronto come il KY-019, per accendere e spegnere un dispositivo esterno.

Errori comuni:{-}
----
N/A

Possibile comportamento erratico:{-}
----
La luce della torcia dello smartphone è abbastanza forte tale da essere facilmente distinguibile dalla luce dell'ambiente, tuttavia scegliere una soglia errata causerà rilevamenti errati.

Risultato di apprendimento:{-}
----
In caso di uso di sensori analogici soggetti ad interferenze, quantificare l'interferenza per poi ignorarla, quando è possibile.

Indicazioni per la soluzione:{-}
----


![Esempio](./images/partitore02_fotoresistenza_bb.png)
