<!--<metadata>
<title>Timer a condensatore</title>
<tags>Medio,DigitalOutput,AnalogInput,Software</tags>
<prerequisites>ohmlaw,analogInput,digitalOutput</prerequisites>
</metadata>-->
[OK]Timer a condensatore
====

Prerequisiti:{-}
----
- Legge di Ohm
- Condensatore

Componenti:{-}
----
- Resistore 1 kOhm
- Condensatore elettrolitico da 100 microFarad, con tensione nominale >= 5.5V

Scopo dell'esercizio:{-}
----
In elettronica la misura di tempo si ottiene indirettamente, sfruttando componenti con comportamenti condizionati da altri parametri misurabili.

Risultato atteso:{-}
----
Costruire un timer che invia un messaggio sulla porta seriale ad intervalli di un secondo, utilizzando un circuito RC per la temporizzazione.
Semplificazioni:
- nel caso non siano disponibili i componenti indicati, scegliere componenti con valori multipli di 10;
- si può considerare il condensatore completamente scarico quando la tensione ai capi scende allo 0,7% di Vcc e completamente carico quando raggiunge il 99,3% di Vcc.

Cenni di teoria:
La tensione ai capi del condensatore collegato in serie a un resistore, in fase di carica o in fase di scarica, varia con un andamento esponenziale.
In un tempo t = R * C la tensione ai capi del condensatore varia del 63,2% di Vfinale-Viniziale, in fase di carica Vfinale sarà la tensione di alimentazione, in fase di scarica Vfinale sarà 0V.
Importante: il resistore R è necessario per evitare danni.

Eventuali funzionalità aggiuntive:{-}
----
N/A

Errori comuni:{-}
----
Collegamenti errati possono causare danni.

Possibile comportamento erratico:{-}
----
N/A

Risultato di apprendimento:{-}
----
Mettere in pratica quanto appreso dal capacimetro per misurare un tempo sfruttando il condensatore.

Indicazioni per la soluzione:{-}
----
Il condensatore si può caricare e scaricare utilizzando un pin GPIO, da impostare a livello HIGH per la fase di carica e a livello LOW per la fase di scarica.
Se i valori dei componenti sono multipli di 10, posso caricare e scaricare completamente il condensatore, alle % sopra indicate, considerando un tempo totale di 10 t, in caso contrario saranno necessari calcoli più complessi per ottenere l'intervallo di tempo desiderato.

Codice dello sketch Arduino: in /sketch/TimerACondensatore

![Esempio](./images/15_Condensatore_Timer_bb.png)
