<!--<metadata>
<title>Rappresentazione di colori su LED RGB</title>
<tags>Base,AnalogInput,PWM</tags>
<prerequisites>ohmLaw,analogInput,digitalOutput</prerequisites>
</metadata>-->
[OK]Rappresentazione di colori su LED RGB
====

Prerequisiti:{-}
----
- Legge di Ohm e partitore di tensione

Componenti:{-}
----
- Led RGB su modulo con resistori, oppure Led RGB + 3 resistori
- Potenziometro

Scopo dell'esercizio:{-}
----
Mostrare il funzionamento del potenziometro come partitore di tensione.
Ottenere un colore dalla combinazione di R, G e B, applicando una funzione matematica per il calcolo.

Risultato atteso:{-}
----
Consentire all'utente di regolare, tramite il potenziometro, la lunghezza d'onda della luce, da circa 380nm a circa 780nm, calcolando la combinazione di colori R, G, B necessaria.
La rotazione a sinistra del potenziometro deve ridurre la lunghezza d'onda fino ad arrivare al minimo, la rotazione a destra incrementarla fino al massimo.

Eventuali funzionalità aggiuntive:{-}
----
Definire una sequenza predeterminata di combinazioni di colori da riprodurre in sequenza ripetutamente, con una velocità regolabile dall'utente con il potenziometro.

Errori comuni:{-}
----
Mancato funzionamento per errore del collegamento dei terminali del LED RGB.

Possibile comportamento erratico:{-}
----
Funzionamento al contrario per connessione errata del potenziometro.

Risultato di apprendimento:{-}
----
Individuare la polarità dei LED e il modo corretto di gestire un LED RGB a catodo comune e anodo comune
Comprendere la relazione tra lunghezza d'onda e colore

Indicazioni per la soluzione:{-}
----
Costruire un partitore di tensione con il potenziometro, leggendo dall'input digitale il valore.
Individuare la formula per ottenere la combinazione di colori corretta partendo dalla lettura del potenziometro.

LED a Catodo comune:
Catodo connesso a Gnd
Ciascun terminale R, G, B, connesso con un resistore a un GPIO digitale con supporto PWM.

LED ad Anodo comune:
Catodo connesso a Vcc
Ciascun terminale R, G, B, connesso con un resistore a un GPIO digitale con supporto PWM.
