<!--<metadata>
<title>Contatore a due cifre con display 7 segmenti</title>
<tags>Medio,DigitalInput,DigitalOutput,Software,Rimbalzi</tags>
<prerequisites>digitalInput,digitalOutput,pullUpDown,debouncing,interrupt</prerequisites>
</metadata>-->
[OK]Contatore a due cifre con display 7 segmenti
====

Prerequisiti:{-}
----
- Interrupt
- Problema dei rimbalzi
- Resistore di pullup/pulldown

Componenti:{-}
----
- Display 7 segmenti da almeno 2 cifre, oppure 2 display da 1 cifra
- 2 pulsanti

Scopo dell'esercizio:{-}
----
Riconoscere il problema dei rimbalzi e scrivere il software in modo corretto per gestirlo.

Risultato atteso:{-}
----
Usando solo 2 cifre per rientrare nel numero di GPIO disponibili, costruire un contatore con aumento/decremento controllati da pulsanti.

Eventuali funzionalità aggiuntive:{-}
----
Utilizzare gli interrupt per rilevare la pressione dei pulsanti, questo renderà ancora più evidente il problema dei rimbalzi.

Nella versione con interrupt contare i rimbalzi in una variabile ed inviare il numero sulla porta seriale.

Fare in modo che, mantenendo un pulsante premuto più di 2 secondi, si ripeta l'incremento o il decremento periodicamente fino al rilascio del pulsante.

Scrivere il codice in modo ottimizzato, minimizzando cioè il codice della routine di gestione dell'interrupt.

Errori comuni:{-}
----
Inversione collegamenti LED

Possibile comportamento erratico:{-}
----
Incremento o decremento multiplo anomalo in caso non siano gestiti correttamente i rimbalzi

Validità per PBL:{-}
----
Problema da definire e analizzare: Si
Necessaria individuazione nuovi obiettivi di apprendimento: No
Dettagli obiettivo di apprendimento atteso:
Riscontrare che interruttori meccanici generano rimbalzi e che questo è un problema di tutti i pulsanti, fare un semplice polling del cambio di stato del pulsante può causare rilevazioni multiple se non si introduce un ritardo nel rilevamento.  
Per rilevare la pressione dei pulsanti in modo corretto quindi si hanno due opzioni, o fare il polling dell'input leggendo il valore con digitalRead ed introducendo un ritardo per distinguere i cambi di stato, oppure utilizzare una variabile cambiata da una routine di gestione degli interrupt, tenendo la prima lettura come provvisoria da confermare con una successiva lettura dopo un breve intervallo di tempo (10~20ms).

Risultato di apprendimento:{-}
----
Gli interruttori meccanici sono soggetti a rimbalzi, quindi per gestirli via software si deve introdurre un ritardo nella lettura del valore affinché si legga quello definitivo.

Indicazioni per la soluzione:{-}
----
Il display può essere ad Anodo comune o Catodo comune.

Per display a Catodo comune collegare:
- Catodo connesso a Gnd
- terminali di ciascun segmento connessi con un resistore a un GPIO digitale

Per display a Anodo comune collegare:
- Anodo connesso a Vcc
- terminali di ciascun segmento connessi con un resistore a un GPIO digitale
