<!--<metadata>
<title>Metro a ultrasuoni</title>
<tags>Medio,DigitalInput,DigitalOutput,Software,I2C,Rimbalzi</tags>
<prerequisites>digitalInput,digitalOutput,i2c,debouncing</prerequisites>
</metadata>-->
[OK]Metro a ultrasuoni
====

Prerequisiti:{-}
----
- Uso del display LCD, con connessione diretta oppure con modulo I2C
- Gestione dei rimbalzi

Componenti:{-}
----
- Display LCD a 2 righe (bus I2C)
- Sensore di distanza a ultrasuoni
- Pulsanti

Scopo dell'esercizio:{-}
----
Misurare una grandezza fisica e mostrarla all'utente in un formato facilmente leggibile ed utilizzabile per lo scopo previsto.

Risultato atteso:{-}
----
Costruire un metro che misura la distanza con il sensore a ultrasuoni e la visualizza sul display.

L'oggetto tiene traccia, in una variabile, dell'ultimo risultato dell'operazione matematica, che inizialmente sarà 0, mostrandolo sulla prima riga del display.

La pressione del pulsante 1 fa effettuare una nuova misura, che dovrà essere mostrata sulla seconda riga del display e memorizzata in una variabile dell'ultima misura.

A questo punto, premendo il pulsante 2 l'utente potrà scegliere se vuole sommare la nuova misura al risultato precedente o sottrarla, si dovrà mostrare come primo carattere della seconda riga + o - in base a quanto scelto.

Tenendo invece premuto a lungo il pulsante 2 (> 1 secondo) si dovrà effettuare l'operazione scelta tra somma e sottrazione, aggiornare la variabile dell'ultimo risultato dell'operazione matematica e mostrarla sulla prima riga del display.

Tenendo premuto invece il pulsante 1 a lungo (> 1 secondo) si dovranno azzerare ultima misura e ultimo risultato, riportando come conseguenza a 0 i valori e mostrando 0 su ciascuna riga del display.

Eventuali funzionalità aggiuntive:{-}
----
N/A

Errori comuni:{-}
----
N/A

Possibile comportamento erratico:{-}
----
Materiali particolarmente morbidi impediranno la misura portando a risultati sbagliati.

Risultato di apprendimento:{-}
----
Gestire, con i pochi GPIO a disposizione, più funzioni, studiando un sistema per consentire all'utente di scegliere tra le funzioni disponibili.

Indicazioni per la soluzione:{-}
----
Collegare il display ai pin previsti dal tipo di bus (es. I2C).

Collegare le linee dati del sensore di distanza ad ultrasuoni ai GPIO secondo le indicazioni sul modulo.

Collegare ciascun pulsante tra la massa e un GPIO a scelto, impostando quel GPIO in modalità INPUT_PULLUP.
