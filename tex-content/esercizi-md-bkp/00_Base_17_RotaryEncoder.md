<!--<metadata>
<title>Regolazione di precisione</title>
<tags>Medio,DigitalInput,PWM,Software,Verificare implementazione</tags>
<prerequisites>digitalInput,pwm</prerequisites>
</metadata>-->
[OK]Regolazione di precisione
====

Prerequisiti:{-}
----
- I/O digitale
- Gestione dei rimbalzi

Componenti:{-}
----
- Encoder rotativo
- Potenziometro
- Buzzer passivo
- Pulsante, nel caso non sia integrato nell'encoder

Scopo dell'esercizio:{-}
----
Input dal mondo fisico, consentendo all'utente di compiere una regolazione con un encoder rotativo con precisione maggiore di quella che si otterrebbe usando un potenziometro.

Risultato atteso:{-}
----
Definire un intervallo minimo e massimo di frequenze da utilizzare.

Generare un segnale acustico partendo da una frequenza intermedia nell'intervallo scelto, e consentire all'utente di incrementarla ruotando l'encoder in senso orario e decrementarla ruotandolo in senso antiorario.

Consentire all'utente, premendo il pulsante dell'encoder, di scegliere tra 3 possibili variazioni di frequenza per scatto: 1 Hz,10 Hz,100 Hz
La pressione del pulsante permette di iterare tra le opzioni, notificando all'utente la scelta fatta con l'emissione di un numero di bip corrispondente al numero dell'opzione, 1 Hz = 1 bip, 10 Hz = 2 bip, 100 Hz = 3 bip.

Notificare le modifiche ai parametri inviando messaggi sulla porta seriale, in modo da consentire all'utente di comprendere come sta cambiando la variabile con il monitor seriale.

Eventuali funzionalità aggiuntive:{-}
----
Ripetere l'esercizio utilizzando un potenziometro, mappando l'intero intervallo di frequenze sui valori misurabili dall'input analogico, con la frequenza minima abbinata alla completa rotazione a sinistra, e quella massima alla completa rotazione a destra.

(Alternativa al buzzer passivo) Pur non disponendo di hardware specifico, si può utilizzare come alternativa al buzzer passivo un altoparlante con in serie un resistore, ad esempio un altoparlante da 8 ohm 0.5w e un resistore da 220 Ohm, non meno, per limitare la corrente.

Errori comuni:{-}
----
Possibile rilevamento della rotazione al contrario, se l'interfaccia dell'encoder permette di invertire la connessione.

Possibile comportamento erratico:{-}
----
N/A

Risultato di apprendimento:{-}
----
Utilizzare un encoder rotativo per consentire all'utente di regolare con precisione un parametro.

Indicazioni per la soluzione:{-}
----
Collegare l'encoder rotativo secondo le indicazioni sul datasheet.

Nel caso si abbia un encoder senza pulsante, collegare un pulsante tra massa e un pin GPIO, impostando quel GPIO in modalità INPUT_PULLUP.

Collegare il polo positivo del buzzer passivo a un GPIO in modalità OUTPUT e il negativo a massa, se il buzzer ha un consumo > di 20mA, servirà un resistore tra il GPIO e il positivo del buzzer, oppure si dovrà utilizzare un transistor BJT come driver.
In caso di modulo buzzer seguire le indicazioni sul modulo.
