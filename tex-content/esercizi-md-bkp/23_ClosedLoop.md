<!--<metadata>
<title>Controllo closed loop</title>
<tags>Medio,AnalogInput,PWM</tags>
<prerequisites>ohmlaw,analogInput,pwm,transistorswitch,multipsu</prerequisites>
</metadata>-->
[OK]Controllo closed loop
====

Prerequisiti:{-}
----
- Partitore di tensione
- PWM
- Circuito con più alimentazioni

Componenti:{-}
----
- Fotoresistenza + Resistore per partitore
- Potenziometro o trimmer
- MOSFET N (es. IRLZ44N o equivalente) + Resistore 10 Ohm
- Striscia LED alimentata a 12V (o 24V) e alimentatore adeguato

Scopo dell'esercizio:{-}
----
I sensori misurano determinate grandezze, tra un minimo e un massimo, entrambi parametri documentati, ma non basta fare affidamento su questi valori limite perché non è detto che si possano raggiungere.
In generale si deve selezionare un intervallo di valori adeguato a ciò che si vuole misurare.

Risultato atteso:{-}
----
Costruire un oggetto che regola il livello di luminosità di una striscia LED, al diminuire della luminosità dell'ambiente l'oggetto dovrà incrementare la luminosità della striscia LED partendo dal minimo fino al massimo.

Eventuali funzionalità aggiuntive:{-}
----
N/A

Errori comuni:{-}
----
N/A

Possibile comportamento erratico:{-}
----
L'errato posizionamento della fotoresistenza rispetto alla striscia LED, nel caso la fotoresistenza possa rilevare la luce emessa dalla striscia LED, può causare continue variazioni della luminosità.

Validità per PBL:{-}
----
Problema da definire e analizzare: Si
Necessaria individuazione nuovi obiettivi di apprendimento: No
Dettagli obiettivo di apprendimento atteso:
Per far funzionare il circuito come da descrizione va stabilito un legame tra la variazione di luminosità dell'ambiente e quella della striscia LED entro un intervallo adatto all'ambiente in cui si installa l'oggetto.

Considerare una relazione dove la luminosità ambientale massima corrisponde all'illuminazione della striscia LED allo 0% e viceversa la luminosità ambientale minima corrisponde all'illuminazione al 100% della striscia LED non è sufficiente, perché:
- senza esposizione diretta al sole la luce ambientale potrebbe non abbastanza forte da essere vista come quella massima, tale da far spegnere la striscia LED, e quest'ultima potrebbe rimanere sempre accesa;
- anche imporre come prerequisito generico il buio per far raggiungere alla striscia LED la massima luminosità è un errore, perché anche puntando la fotoresistenza con attenzione in una direzione diversa, non è escluso che la striscia LED possa illuminarla indirettamente anche se di poco, non permettendo di raggiungere il valore atteso.

Risultato di apprendimento:{-}
----
Valutare l'intervallo di valori per la grandezza fisica oggetto di verifica adatto al caso concreto.

Indicazioni per la soluzione:{-}
----
Combinare, per l'output per il controllo della striscia LED l'esercizio sul dimmer per LED a MOSFET, e per la rilevazione del livello di luminosità ambientale quello della fotoresistenza, definendo però un intervallo di valori adatti all'ambiente e non gli estremi indicati nel datasheet della fotoresistenza.
