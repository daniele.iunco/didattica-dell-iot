<!--<metadata>
<title>Sensore fiamma con allarme sonoro</title>
<tags>Base,DigitalInput,DigitalOutput,Software</tags>
<prerequisites>digitalInput,digitalOutput</prerequisites>
</metadata>-->
[OK]Sensore fiamma con allarme sonoro
====

Prerequisiti:{-}
----
- I/O digitale
- (Opzionale) Interrupt
- (Opzionale) Risparmio energetico

Componenti:{-}
----
- Rilevatore di fiamma
- Buzzer passivo
- Pulsante

Scopo dell'esercizio:{-}
----
Rilevare eventi anomali da notificare all'utente con apposite segnalazioni.

Risultato atteso:{-}
----
Costruire un rilevatore di fuoco spento in modo anomalo che:
- parte in stato inattivo e resta in attesa della pressione del pulsante
- rilevata una pressione del pulsante verifica se c'è una fiamma accesa e solo in quel caso passa in modalità monitoraggio, altrimenti genera un segnale sonoro di errore
- in modalità monitoraggio controlla la presenza della fiamma e, in caso di spegnimento, genera un suono con effetto sirena poi torna inattivo
- in caso di pressione del pulsante in modalità monitoraggio, il monitoraggio si annulla e l'oggetto torna in stato inattivo

Eventuali funzionalità aggiuntive:{-}
----
(Opzionale) Gestire via software il risparmio energetico, rilevando la pressione del pulsante con un interrupt e mantenendo l'oggetto in modalità power down (o equivalente in base al MCU) quando non è in modalità monitoraggio.

L'uso delle modalità a basso consumo può richiedere la generazione di eventi per il risveglio su GPIO specifici scelti dal costruttore, scegliere una modalità a basso consumo che consente il risveglio tramite interrupt.

Errori comuni:{-}
----
N/A

Possibile comportamento erratico:{-}
----
L'eventuale gestione errata dei rimbalzi può compromettere il funzionamento dell'oggetto.

Risultato di apprendimento:{-}
----
Imparare a monitorare l'interazione con l'utente anche mentre si sta svolgendo un altro compito.

Indicazioni per la soluzione:{-}
----
Collegare il polo positivo del buzzer passivo a un GPIO in modalità OUTPUT e il negativo a massa, se il buzzer ha un consumo > di 20mA, servirà un resistore tra il GPIO e il positivo del buzzer, oppure si dovrà utilizzare un transistor BJT come driver.
In caso di modulo buzzer seguire le indicazioni sul modulo.

Collegare il pulsante tra la massa e un pin GPIO, impostando quel GPIO in modalità INPUT_PULLUP.
