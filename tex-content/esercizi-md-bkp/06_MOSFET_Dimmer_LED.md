<!--<metadata>
<title>Costruire un dimmer per LED di potenza</title>
<tags>Base,DigitalOutput,AnalogInput,PWM,Software,Circuiti con più alimentazioni</tags>
<prerequisites>ohmlaw,analogInput,digitalOutput,pwm,transistorswitch,multipsu</prerequisites>
</metadata>-->
[OK]Costruire un dimmer per LED di potenza
====

Prerequisiti:{-}
----
- legge di Ohm e partitore di tensione
- Funzionamento del MOSFET come interruttore
- Circuiti con più alimentazioni
- PWM

Componenti:{-}
----
- MOSFET di tipo N a 5V, ad esempio IRLZ44N o equivalente
- Resistore da 10 Ohm
- Resistore da 10 kOhm
- Batteria da 9V e connettore specifico
- LED da alimentare con la batteria: 3 LED dello stesso tipo (in serie) con un resistore da 220 Ohm per limitare la corrente.
- Potenziometro da almeno 1 kOhm

Scopo dell'esercizio:{-}
----
Nei sistemi embedded il microcontrollore può avere una tensione diversa da quella del resto del circuito, oppure si può avere un carico da controllare che consuma troppo.

Un lampeggiatore, se fatto con un relè e lasciato acceso per lunghi periodi, non è una buona soluzione, per via dell'usura dei contatti presenti all'interno.

Nel caso del dimmer per LED poi è richiesta un'elevata frequenza di commutazione (kHz) e il relè non è in alcun modo utilizzabile a tali frequenze.

Come già visto il BJT è poco efficiente e se si ha in progetto di controllare LED ad alta potenza si dovrà usare il MOSFET.

Risultato atteso:{-}
----
Evoluzione dell'esercizio precedente sul lampeggiatore con MOSFET, per simulare il funzionamento di un dimmer per LED che consente all'utente di regolare la luminosità.

Eventuali funzionalità aggiuntive:{-}
----
N/A

Errori comuni:{-}
----
Attenzione: eventuali errori di connessione causano danni ai componenti.

Possibile comportamento erratico:{-}
----
N/A

Risultato di apprendimento:{-}
----
Gestire un carico con alimentazione esterna diversa da quella del microcontrollore, sempre in bassa tensione, collegando in modo corretto, insieme, le masse delle fonti di alimentazione.

Indicazioni per la soluzione:{-}
----
Costruire un partitore con il potenziometro e collegare la relativa uscita a un pin di input analogico per consentire all'utente di regolare il duty cycle.
Collegare il GPIO scelto, che deve supportare PWM, al gate del MOSFET come indicato nell'immagine (da sinistra Gate, Drain, Source).

![Esempio](./images/MOSFET_Dimmer_bb.png)
