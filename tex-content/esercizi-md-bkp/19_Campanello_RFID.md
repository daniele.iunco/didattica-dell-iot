<!--<metadata>
<title>Campanello con suoneria personalizzata e autenticazione RFID</title>
<tags>Avanzato,DigitalInput,DigitalOutput,Software,Rimbalzi,Memorie di massa,SoftwareSerial,SPI</tags>
<prerequisites>digitalInput,digitalOutput,debouncing,interrupt,softwareserial,spi</prerequisites>
</metadata>-->
[OK]Campanello con suoneria personalizzata e autenticazione RFID
====

Prerequisiti:{-}
----
- Uso dei tag RFID
- Bus SPI
- Libreria SoftwareSerial
- Pulsanti e rimbalzi
- Memorie di massa

Componenti:{-}
----
- Lettore RFID
- Pulsante
- Lettore di memorie microSD
- Buzzer passivo a 5V su modulo

Scopo dell'esercizio:{-}
----
I microcontrollori si possono installare in molti oggetti per aggiungere funzionalità, con questo esercizio si deve identificare l'utente per poi rispondere in modo personalizzato all'evento.

Risultato atteso:{-}
----
Costruire un campanello per la porta di casa con suonerie personalizzate, l'oggetto deve avere due modalità di funzionamento:

- attivazione con il pulsante: eseguire la suoneria predefinita alla pressione del pulsante;
- attivazione con RFID: rilevare il passaggio di un tag RFID e verificare se il codice è in un elenco di tag degli amici e in caso affermativo eseguire una suoneria diversa;

Cenni di teoria:

- i tag RFID a 125kHz sono memorie wireless senza sistemi di protezione o codifica, i normali tag RFID di questo tipo sono prodotti con già memorizzato un codice unico e sono accessibili in sola lettura con l'apposito lettore;
- la tecnologia RFID a 125kHz ha una portata massima di 10 centimetri, quindi il ricevitore si può inserire senza problemi in un contenitore di plastica, o anche nel contenitore del campanello di casa, facendo molta attenzione all'isolamento per evitare contatti accidentali e danni.

Semplificazioni:
- l'esercizio si può svolgere utilizzando il buzzer passivo e tone() per generare due suoni con frequenza diversa;
- vanno bene anche i tag RFID a 13.56 Mhz e il relativo lettore, adattando il codice per utilizzare il codice UID di questi tag.

Eventuali funzionalità aggiuntive:{-}
----
Leggere la lista amici da un file di testo memorizzato su una memoria microSD.

Errori comuni:{-}
----
N/A

Possibile comportamento erratico:{-}
----
Problemi di incompatibilità tra periferiche SPI: pare esistano lettori di memorie microSD su bus SPI che non funzionano insieme ad altre periferiche SPI.

Risultato di apprendimento:{-}
----
Utilizzare tag RFID per l'identificazione dell'utente.
(Espansione) Utilizzare una memoria di massa per gestire file esterni da leggere in fase di esecuzione del software

Indicazioni per la soluzione:{-}
----
Collegare i vari moduli all'alimentazione.

Collegare poi il lettore RFID e lettore di memorie SD sui GPIO previsti per il tipo di bus in uso, tipicamente hanno bus SPI.

Scegliere un GPIO per la linea dati del buzzer passivo.
