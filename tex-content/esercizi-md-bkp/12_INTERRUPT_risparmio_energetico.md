<!--<metadata>
<title>Suoneria a basso consumo per avviso apertura porta</title>
<tags>Avanzato,DigitalInput,DigitalOutput,Software,Rimbalzi,Risparmio energetico,PBL</tags>
<prerequisites>digitalInput,digitalOutput,debouncing,swpowersave</prerequisites>
</metadata>-->
[TODO]Suoneria a basso consumo per avviso apertura porta
====

Prerequisiti:{-}
----
- Modalità di risparmio energetico
- Interrupt
- Problema dei rimbalzi

Componenti:{-}
----
- Relè reed
- Buzzer attivo (Es. KY-012)

Scopo dell'esercizio:{-}
----
Quando si utilizzano le modalità a basso consumo con eventi generati da interruttori meccanici di qualche tipo, i rimbalzi possono causare comportamenti anomali

Risultato atteso:{-}
----
Costruire un oggetto che rileva l'apertura di un contatto e, al verificarsi dell'evento, fa suonare un buzzer per generare un breve suono.

Progettare il software affinché sia possibile alimentare l'oggetto anche a batteria, il microcontrollore quindi deve rimanere in modalità a basso consumo e riattivarsi solo all'apertura del contatto e limitatamente al tempo in cui deve suonare il buzzer.

Partire dal presupposto che l'utente deve anche poter decidere di lasciare la porta aperta, tale azione dovrà rendere inattivo l'oggetto senza costringere l'utente a spegnerlo per impedire che si scarichi la batteria.

Eventuali funzionalità aggiuntive:{-}
----
N/A

Errori comuni:{-}
----
N/A

Possibile comportamento erratico:{-}
----
Vedi dettagli PBL

Validità per PBL:{-}
----
Problema da definire e analizzare: Si
Necessaria individuazione nuovi obiettivi di apprendimento: Si
Dettagli obiettivo di apprendimento atteso:
La criticità sta nel modo di rilevare l'apertura e la chiusura della porta, qualora l'evento che genera il risveglio del microcontrollore sia l'apertura della porta, si potrebbe tornare alla modalità a basso consumo subito dopo l'emissione del suono tornando in attesa di una nuova apertura, però se si chiudesse la porta sbattendola con forza si potrebbe generare un rimbalzo del relè reed visto come nuova apertura che causerebbe l'emissione di un altro suono che, per requisiti dell'esercizio, non deve esserci.

Il microcontrollore, dovrà tornare sempre alla modalità a basso consumo migliore disponibile in base alla situazione, gestendo anche l'eventuale scelta dell'utente di lasciare la porta aperta per poi chiuderla in seguito e gestendo l'anomalia del rimbalzo in chiusura.

Una tra le possibili opzioni è rilevare l'apertura, emettere il suono, aggiornare una variabile di stato, poi fare un polling dello stato della porta per attendere che si chiuda ma in modalità "basso consumo" cioè sostituendo il classico delay con un breve passaggio a standby ripetuto fino al rilevamento della chiusura della porta.
Questo breve standby si ottiene abilitando un apposito watchdog, che consente di far risvegliare dallo standby il microcontrollore dopo un numero di secondi scelto (da 1 a 8).

Risultato di apprendimento:{-}
----
Imparare ad individuare momenti di inattività in cui sfruttare le modalità di risparmio energetico per minimizzare i consumi.

Indicazioni per la soluzione:{-}
----
Utilizzare la modalità per il risparmio energetico massimo prevista che consente il risveglio con interrupt sul microcontrollore in uso.

Vedi dettagli PBL
\todo{codice per arduino e esp8266}
