<!--<metadata>
<title>Auto che si ferma in caso di urti</title>
<tags>Base,DigitalInput,DigitalOutput,Software,Rimbalzi,Dimensionamento elettrico</tags>
<prerequisites>digitalInput,digitalOutput,debouncing,multipsu,psudimensioning</prerequisites>
</metadata>-->
[OK]Auto che si ferma in caso di urti
====

Prerequisiti:{-}
----
- Dimensionamento elettrico
- Circuiti con più alimentazioni
- Gestione dei rimbalzi

Componenti:{-}
----
- motore
- relè
- rilevatore di urti

Scopo dell'esercizio:{-}
----
Nell'interazione con il mondo fisico possono verificarsi eventi inattesi che richiedono azioni correttive.

Risultato atteso:{-}
----
Far muovere una macchina, la macchina si dovrà fermare in caso di urto.

Eventuali funzionalità aggiuntive:{-}
----
N/A

Errori comuni:{-}
----
N/A

Possibile comportamento erratico:{-}
----
Se il sensore è molto sensibile l'avvio della macchina potrebbe generare il rilevamento di un urto.

Risultato di apprendimento:{-}
----
Utilizzo del relè per controllare un carico con propria alimentazione.

Indicazioni per la soluzione:{-}
----
Il rilevatore di urti funziona come un pulsante, scegliere un GPIO, da utilizzare in modalità INPUT_PULLUP, collegare il sensore tra il GPIO scelto e la massa.
Se si dispone del sensore su un modulo, collegarlo seguendo le indicazioni del costruttore.
