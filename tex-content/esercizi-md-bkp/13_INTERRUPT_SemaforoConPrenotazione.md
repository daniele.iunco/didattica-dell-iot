<!--<metadata>
<title>Semaforo con pulsanti per la prenotazione</title>
<tags>Medio,DigitalOutput,Software,DigitalInput</tags>
<prerequisites>diode,digitalInput,digitalOutput,interrupt</prerequisites>
</metadata>-->
[OK]Semaforo con pulsanti per la prenotazione
====

Prerequisiti:{-}
----
- Pulsante
- Resistore di pullup/pulldown
- Interrupt
- Funzionamento e polarità dei diodi

Componenti:{-}
----
- 2 LED rossi
- 2 LED gialli
- 2 LED verdi
- 6 resistori da 220 Ohm
- 2 pulsanti normalmente aperti
- Buzzer attivo
- Interruttore o ponticello

Scopo dell'esercizio:{-}
----
Saper interagire con l'utente con gli interrupt senza fare il classico polling delle linee di input.
Gestire in modo efficiente i pochi GPIO disponibili.

Risultato atteso:{-}
----
Costruire un semaforo per regolare il traffico in prossimità di un passaggio pedonale.
Il semaforo deve avere due pulsanti.
Il primo pulsante serve a prenotare il passaggio pedonale in anticipo, portando il semaforo lato strada in stato giallo.
Il secondo pulsante serve a richiedere la notifica sonora del verde per i pedoni, questa richiesta è valida solo per il successivo passaggio di stato a verde, quindi il pulsante va premuto ogni volta che si vuole sia notificato il successivo verde per i pedoni.

Gestire il controllo dei LED a coppie in modo da utilizzare un solo pin digitale ogni 2 LED di ciascun colore.

Gestire l'interazione con l'utente sfruttando gli interrupt.

Eventuali funzionalità aggiuntive:{-}
----
Prevedere una modalità con passaggio pedonale solo su prenotazione, selezionabile tramite l'interruttore o il ponticello sulla breadboard.

Errori comuni:{-}
----
N/A

Possibile comportamento erratico:{-}
----
N/A

Risultato di apprendimento:{-}
----
Scrivere codice in cui un evento esterno non prevedibile può alterare lo stato dell'oggetto, gestendo correttamente tale evento.

Indicazioni per la soluzione:{-}
----
Collegare le coppie di LED di ciascun colore come nell'esercizio sull'uso di due LED per GPIO

Collegare ciascun pulsante tra un GPIO e massa, utilizzando il pin in modalità INPUT_PULLUP.

Collegare il polo positivo del buzzer attivo a un GPIO in modalità OUTPUT e il negativo a massa, se il buzzer ha un consumo > di 20mA, servirà un resistore tra il GPIO e il positivo del buzzer, oppure si dovrà utilizzare un transistor BJT come driver.
In caso di modulo buzzer seguire le indicazioni sul modulo.
