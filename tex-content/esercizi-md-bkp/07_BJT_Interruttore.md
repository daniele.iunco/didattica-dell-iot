<!--<metadata>
<title>Lampeggiatore per tensioni > 5V</title>
<tags>Base,DigitalOutput,PWM,Software,Circuiti con più alimentazioni</tags>
<prerequisites>diode,digitalOutput,pwm,transistorswitch,multipsu</prerequisites>
</metadata>-->
[TODO]Lampeggiatore per tensioni > 5V
====

Prerequisiti:{-}
----
- Funzionamento del transistor BJT NPN come interruttore
- Circuiti con più alimentazioni

Componenti:{-}
----
- Transistor NPN, ad esempio S8050 o equivalente
- 1 resistore da 1 kOhm
- 1 resistore da 220 Ohm
- 3 LED dello stesso tipo.
- Batteria da 9V e connettore specifico

Scopo dell'esercizio:{-}
----
Nei sistemi embedded il microcontrollore può avere una tensione diversa da quella del resto del circuito, oppure si può avere un carico da controllare che consuma troppo.
Se ci si aspetta di dover accendere e spegnere continuamente il carico il relè non è adatto perché si usura, quindi serve un interruttore elettronico (BJT o MOSFET).

Risultato atteso:{-}
----
Utilizzare un transistor BJT come interruttore per accendere e spegnere 3 LED collegati in serie con un'alimentazione esterna, una batteria da 9V.
3 LED in serie, pur consumando meno di 20 mA, richiedono una tensione maggiore dei 5V del microcontrollore, quindi si deve usare un'alimentazione esterna.

Eseguire lo sketch blink sostituendo LED_BUILTIN con il GPIO a cui si collega il BJT

Eventuali funzionalità aggiuntive:{-}
----
N/A

Errori comuni:{-}
----
Attenzione: eventuali errori di connessione causano danni ai componenti.

Possibile comportamento erratico:{-}
----
N/A

Risultato di apprendimento:{-}
----
Gestire un carico con alimentazione esterna diversa da quella del microcontrollore, sempre in bassa tensione, collegando in modo corretto, cioè insieme, le masse delle fonti di alimentazione.

Indicazioni per la soluzione:{-}
----
Il BJT richiede un resistore corretto alla base affinché il componente sia in stato di saturazione, ma calcolarlo non è un obiettivo del corso.
I componenti indicati consentono di costruire il circuito proposto senza problemi.
\todo{Immagine circuito con BJT}
