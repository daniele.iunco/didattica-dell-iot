<!--<metadata>
<title>Interazione con l'utente tramite pulsanti, per pilotare un LED RGB</title>
<tags>Medio,AnalogInput,DigitalInput,PWM</tags>
<prerequisites>ohmLaw,analogInput,digitalInput,digitalOutput,debouncing</prerequisites>
</metadata>-->
[OK]Interazione con l'utente tramite pulsanti, per pilotare un LED RGB
====

Prerequisiti:{-}
----
- Legge di Ohm e partitore di tensione
- I/O digitale
- Gestione dei rimbalzi
- macchina a stati

Componenti:{-}
----
- Led RGB su modulo con resistori, oppure Led RGB + 3 resistori
- Potenziometro
- 2 pulsanti

Scopo dell'esercizio:{-}
----
La naturale carenza di GPIO non permette di collegare molti pulsanti, quindi servono soluzioni ingegnose per utilizzarne meno.

Risultato atteso:{-}
----
La pressione breve del pulsante 1 (meno di 1 secondo) cambia il colore selezionato oggetto di modifica, nell'ordine R, G, B.
Subito dopo la selezione far lampeggiare il LED con il colore oggetto di modifica, l'utente a quel punto dovrà portare al centro il potenziometro, con una tolleranza del 10% sul valore, il LED deve continuare a lampeggiare finché il potenziometro non è in posizione corretta.
Quando il potenziometro è posizionato come richiesto, tornare alla visualizzazione del colore risultante dalla somma dei valori R,G,B, la rotazione a sinistra dovrà ridurre il livello del colore selezionato in modo relativo al valore precedente, la rotazione a destra incrementarlo in modo relativo, entro i minimi e massimi consentiti.
La pressione prolungata (> 5 secondi) dovrà far lampeggiare alternativamente i colori R, G, B, una sola volta, impostare i livelli di ciascun colore a 50%, riportare la selezione su R e riportare l'oggetto in attesa del posizionamento del potenziometro al centro.
Una pressione di durata intermedia va ignorata.

Eventuali funzionalità aggiuntive:{-}
----
Nella fase di attesa del posizionamento del potenziometro al centro, sostituire il lampeggio con la variazione della luminosità del colore, che dovrà essere inversamente proporzionale allo scostamento rispetto al valore atteso

Errori comuni:{-}
----
Inversione collegamenti LED

Possibile comportamento erratico:{-}
----
Possibile rimbalzo pulsante (se delay molto basso)

Risultato di apprendimento:{-}
----
Individuare la polarità dei LED e il modo corretto di gestire un LED RGB a catodo comune e anodo comune

Indicazioni per la soluzione:{-}
----
Costruire un partitore di tensione con il potenziometro, collegando il terminale centrare all'ingresso analogico scelto.
Collegare ciascun pulsante tra massa e pin GPIO scelto per l'input digitale, impostando quel GPIO in modalità INPUT_PULLUP.

LED a Catodo comune:
Catodo connesso a Gnd
Ciascun terminale R, G, B, connesso con un resistore a un GPIO digitale con supporto PWM.

LED ad Anodo comune:
Catodo connesso a Vcc
Ciascun terminale R, G, B, connesso con un resistore a un GPIO digitale con supporto PWM.
