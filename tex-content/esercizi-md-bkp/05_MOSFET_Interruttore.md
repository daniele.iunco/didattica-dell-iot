<!--<metadata>
<title>Lampeggiatore per LED di potenza con MOSFET</title>
<tags>Base,DigitalOutput,PWM,Software,Circuiti con più alimentazioni</tags>
<prerequisites>digitalOutput,pwm,transistorswitch,multipsu</prerequisites>
</metadata>-->
[OK]Lampeggiatore per LED di potenza con MOSFET
====

Prerequisiti:{-}
----
- Funzionamento del MOSFET N come interruttore
- Circuiti con più alimentazioni
- PWM

Componenti:{-}
----
- MOSFET di tipo N a 5V, ad esempio IRLZ44N o equivalente
- Resistore da 10 Ohm
- Resistore da 10 kOhm
- Batteria da 9V e connettore specifico
- LED da alimentare con la batteria: 3 LED dello stesso tipo (in serie) con un resistore da 220 Ohm per limitare la corrente.

Scopo dell'esercizio:{-}
----
Nei sistemi embedded il microcontrollore può avere una tensione diversa da quella del resto del circuito, oppure si può avere un carico da controllare che consuma troppo.

Un lampeggiatore, se fatto con un relè e lasciato acceso per lunghi periodi, non è una buona soluzione, per via dell'usura dei contatti presenti all'interno.

Il BJT è poco efficiente, se si ha in progetto di controllare LED ad alta potenza si dovrà usare il MOSFET.

Risultato atteso:{-}
----
Utilizzare Arduino per far lampeggiare una serie di LED alimentati da una batteria a 9V, utilizzando come interruttore un MOSFET per ottenere la massima efficienza energetica e dover così dissipare meno calore.

Eventuali funzionalità aggiuntive:{-}
----
N/A

Errori comuni:{-}
----
Attenzione: eventuali errori di connessione causano danni ai componenti.

Possibile comportamento erratico:{-}
----
N/A

Risultato di apprendimento:{-}
----
Gestire un carico con alimentazione esterna diversa da quella del microcontrollore, sempre in bassa tensione, collegando in modo corretto, insieme, le masse delle fonti di alimentazione.

Indicazioni per la soluzione:{-}
----
Collegare il GPIO scelto al gate del MOSFET come indicato nell'immagine (da sinistra Gate, Drain, Source).

![Esempio](./images/MOSFET_Switch_bb.png)
