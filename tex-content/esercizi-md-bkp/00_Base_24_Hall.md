<!--<metadata>
<title>Rilevatore del senso di marcia e velocità di un carrello</title>
<tags>Medio,AnalogInput,Software</tags>
<prerequisites>analogInput,digitalOutput</prerequisites>
</metadata>-->
[OK]Rilevatore del senso di marcia e velocità di un carrello
====

Prerequisiti:{-}
----
- I/O analogico
- I/O digitale

Componenti:{-}
----
- Sensore ad effetto Hall analogico + magnete
- (Alternativa) Sensore di distanza a ultrasuoni
- Buzzer passivo

Scopo dell'esercizio:{-}
----
Combinare le misure di più sensori semplici che restituiscono una singola misura, in modo da ottenere un sensore complesso la cui combinazione delle misure da informazioni aggiuntive.

Risultato atteso:{-}
----
Posizionare più sensori Hall lungo una retta su cui si farà muovere un carrello con fissato un magnete, i sensori devono avere essere tutti alla stessa distanza predeterminata.

Rilevare la direzione in cui si muove il carrello monitorando l'ordine in cui i sensori rilevano il passaggio del magnete.

Calcolare un valore approssimativo di velocità individuando quando il magnete del carrello è passa esattamente sopra a ciascun sensore e misurando il tempo trascorso nel passaggio da un punto di monitoraggio al successivo.

N.B. Considerato il funzionamento dei sensori hall più comuni, la distanza massima non supererà i 4cm, ma è più che sufficiente ai fini dell'esercizio.

Eventuali funzionalità aggiuntive:{-}
----
Applicando lo stesso criterio ma con più sensori Hall disposti su un cerchio, si può costruire un segnavento, tuttavia dato il numero limitato di GPIO analogici l'unica opzione sarà considerare la distanza del magnete tra coppie di sensori per ottenere le posizioni intermedie.

Errori comuni:{-}
----
N/A

Possibile comportamento erratico:{-}
----
N/A

Risultato di apprendimento:{-}
----
Utilizzare sensori hall analogici, misurando e gestendo via software l'input per ottenere più informazioni dell'equivalente digitale.

Indicazioni per la soluzione:{-}
----
Collegare ciascun sensore hall, alla fonte di alimentazione e a un GPIO analogico, nel caso si disponga di sensori sfusi, non su modulo, accertarsi dell'eventuale necessità di componenti aggiuntivi per la connessione e procedere in modo opportuno.
