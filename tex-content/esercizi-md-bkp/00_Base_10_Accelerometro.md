<!--<metadata>
<title>Accelerometro</title>
<tags>Medio,DigitalOutput,Software,SPI,I2C</tags>
<prerequisites>diode,digitalOutput,spi,i2c</prerequisites>
</metadata>-->
[OK]Accelerometro
====

Prerequisiti:{-}
----
- Bus SPI o I2C

Componenti:{-}
----
- Matrice LED 8x8 su modulo con bus I2C
- Accelerometro a 3 assi
- Registro a scorrimento e 8 resistori da 220 ohm, se non si dispone del modulo I2C

Scopo dell'esercizio:{-}
----
Rilevare un particolare evento del mondo fisico, ad esempio lo spostamento dell'oggetto su uno degli assi, e svolgere un'azione come conseguenza.

Risultato atteso:{-}
----
Partendo dall'esercizio sulla matrice LED; far ruotare la riga rispetto al centro, in base allo spostamento rilevato dall'accelerometro

Eventuali funzionalità aggiuntive:{-}
----
Abbinare lo spostamento sui vari assi ad effetti diversi

Errori comuni:{-}
----
Inversione polarità LED, in caso di utilizzo della matrice senza modulo I2C

Possibile comportamento erratico:{-}
----
N/A

Risultato di apprendimento:{-}
----
Reazione ad eventi ambientali

Indicazioni per la soluzione:{-}
----
Se non si dispone del modulo indicato ma di una normale matrice LED, collegarla come nell'esercizio precedente sulla matrice LED, sarà necessario almeno un registro a scorrimento per gestire o le righe o le colonne.

Collegare l'accelerometro sui pin appropriati per il tipo di bus (SPI o I2C)
