\section{Regolatori di tensione lineari e a commutazione (switching)}
Gli studenti potrebbero dover collegare al microcontrollore componenti che richiedono una tensione di alimentazione diversa dai 5V della porta USB, ad esempio 12V, in questo caso le opzioni sono due, o utilizzare un alimentatore che può fornire le tensioni necessarie o utilizzare un regolatore di tensione per ottenere la tensione mancante a partire da quella disponibile.

Ci sono due tipi di regolatore di tensione:
\begin{compactitem}
\item lineare, può solo ridurre la tensione, ad esempio da 12V a 5V;
\item a commutazione (\textit{switching}), può ridurre o elevare la tensione, ad esempio da 12V a 5V oppure da 5V a 12V.
\end{compactitem}

Non è questa la sede per spiegare come funzionano gli alimentatori, tuttavia è importante una breve introduzione al problema della potenza dissipata, per evitare di bruciare i componenti a causa di scelte errate.

Tutti i regolatori di tensione consumano corrente per funzionare, questa corrente si chiama corrente di quiescenza (I\textsubscript{Q}) e di norma è di pochi mA, ma ai fini dell'obiettivo prefissato, che è non bruciare i componenti, è un parametro trascurabile.

\bigskip

\titoloparagrafo{Regolatore di tensione lineare}

\begin{figure}
	\begin{center}
		\includegraphics[width=.7\textwidth]{schema_regolatore_lineare}
		\caption{Schema regolatore di tensione lineare}
		\label{fig:regolatorelineare}
	\end{center}
\end{figure}

\begin{figure}
	\begin{center}
		\includegraphics[width=.2\textwidth]{L7805(lineare)}
		\caption{Foto regolatore di tensione lineare L7805}
		\label{fig:foto7805}
	\end{center}
\end{figure}

Il regolatore di tensione lineare (figure \ref{fig:regolatorelineare}, \ref{fig:foto7805}), genera in uscita una tensione V\textsubscript{OUT} partendo da una tensione in ingresso V\textsubscript{IN}, la tensione in uscita è sempre minore di V\textsubscript{IN} ed è stabile nel tempo, questa riduzione di tensione è ottenuta introducendo una una caduta di tensione:

\begin{equation}
V\textsubscript{DROP} = V\textsubscript{OUT} - V\textsubscript{IN}
\end{equation}

Ogni regolatore di tensione introduce una caduta di tensione minima (V\textsubscript{DROPOUT}) e V\textsubscript{DROP} non deve essere inferiore a V\textsubscript{DROPOUT}.
Il componente ha poi un parametro \textbf{RtjJA} (\textit{Thermal resistance junction-ambient}) che indica l'incremento di temperatura per ogni W di potenza da dissipare nell'ambiente in assenza sistemi di raffreddamento.
Se si supera il valore massimo di \textit{Operating junction temperature} il componente brucia.

Come da premessa, posta la corrente di quiescenza $I_Q = 0$, la potenza dissipata P sarà:

\begin{equation}
P = V\textsubscript{DROP} * I\textsubscript{LOAD}
\end{equation}

\titoloparagrafo{Esempio con regolatore di tensione 7805:}
Per ottenere V\textsubscript{OUT} = 5V, con I\textsubscript{LOAD} = 1A e V\textsubscript{IN} = 12V, si ha una potenza dissipata:

\begin{equation}
P = V\textsubscript{DROP} * I\textsubscript{LOAD} = (12V-5V) * 1A = 7W
\end{equation}

Il regolatore di tensione 7805 ST Microelectronics ha come massimo valore di "Operating junction temperature" 125°C e RtjJA = 60°C/W, a queste condizioni l'incremento di temperatura T senza un sistema di raffreddamento è:

\begin{equation}
T = 7W * 60\degree C/W = 420\degree C
\end{equation}

Il componente brucia.

\bigskip

\titoloparagrafo{Regolatore di tensione a commutazione}
Il regolatore di tensione a commutazione (figure \ref{fig:regolatoreswitching}, \ref{fig:foto2596}) utilizza un metodo di conversione che minimizza la potenza dissipata ed il parametro per la sua valutazione è l'efficienza.

\begin{figure}
	\begin{center}
		\includegraphics[width=.7\textwidth]{Schema_regolatore_a_commutazione}
		\caption{Schema regolatore switching}
		\label{fig:regolatoreswitching}
	\end{center}
\end{figure}

\begin{figure}
	\begin{center}
		\includegraphics[width=.7\textwidth]{LM2596(switching)}
		\caption{Modulo con regolatore di tensione switching LM2596}
		\label{fig:foto2596}
	\end{center}
\end{figure}

La potenza dissipata da un regolatore di tensione a commutazione non dipende dalla differenza di tensione tra ingresso e uscita ma dall'efficienza, che indica la \% di potenza a disposizione sull'uscita partendo da quella in ingresso.

Il produttore del regolatore di tensione a commutazione indica nelle specifiche, oltre all'efficienza, la tensione minima e massima in ingresso e la corrente massima del carico in Ampere, o la potenza massima del carico in Watt, è quindi sufficiente fare attenzione a questi parametri.

L'efficienza è un parametro utile per determinare quanta energia si spreca ed è molto importante se la fonte di energia è una batteria.

\titoloparagrafo{Esempio di calcolo, regolatore di tensione con efficienza 75\%:}

Con gli stessi parametri del regolatore di tensione lineare visto prima, con V\textsubscript{OUT} = 5V e I\textsubscript{LOAD} = 1A, P\textsubscript{OUT} = 5W, calcolo la potenza dissipata:

\begin{equation}
Efficienza = P\textsubscript{OUT} / P\textsubscript{IN} * 100
\end{equation}

\begin{equation}
P\textsubscript{IN} = P\textsubscript{OUT} / Efficienza * 100
\end{equation}

\begin{equation}
P\textsubscript{DISSIPATA} = P\textsubscript{IN} - P\textsubscript{OUT}
\end{equation}

\begin{equation}
P\textsubscript{DISSIPATA} = P\textsubscript{OUT} / Efficienza * 100 - P\textsubscript{OUT}
\end{equation}

Con efficienza del 75\% la potenza dissipata è 1,67W, un valore molto inferiore ai 7W del regolatore di tensione lineare a parità di condizioni.
