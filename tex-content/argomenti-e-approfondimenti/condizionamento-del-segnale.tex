\section{Il condizionamento del segnale}

Al problema dell'incertezza della misura visto prima si aggiunge quello del riferimento di tensione, quando i valori di tensione sono tali da non poter ottenere l'accuratezza voluta con il riferimento di tensione integrato, oppure non rientrano nell'intervallo di tensioni consentito.
Una soluzione sarebbe cambiare il riferimento di tensione, utilizzando un riferimento di tensione di poco superiore al valore massimo da misurare, naturalmente entro i limiti di funzionamento del microcontrollore in uso, ma questo non è sempre possibile.

Quando il livello di tensione non è misurabile nemmeno cambiando la tensione di riferimento dell'ADC è necessario procedere al condizionamento del segnale.
Condizionare il segnale significa utilizzare un circuito che trasforma un segnale che ha caratteristiche inadatte alla sua misura in un segnale con caratteristiche tali da essere misurabile. In generale si deve utilizzare un circuito che, data tensione incompatibile in ingresso V\textsubscript{IN}, applicando una determinata funzione di trasferimento f\textsubscript{T}(), ha in uscita una tensione V\textsubscript{OUT} compatibile.

Ai fini del corso sarà sufficiente considerare l'esistenza del problema ed i casi principali, dove la tensione in ingresso può essere maggiore della tensione massima consentita oppure così bassa, nell'ordine delle decine/centinaia di milliVolt, da non essere misurabile, in questi caso l'operazione da svolgere sarà la moltiplicazione della tensione in ingresso V\textsubscript{IN} per un valore k tale da ottenere in uscita una tensione V\textsubscript{OUT} compatibile.

V\textsubscript{OUT} = k * V\textsubscript{IN}

\bigskip

\titoloparagrafo{Tensione troppo alta, da ridurre ($k < 1$):}

Questo problema è risolvibile con il partitore di tensione di figura \ref{fig:partitore}.
\begin{figure}
	\begin{center}
		\includegraphics[width=.6\textwidth]{schema_partitore}
		\caption{Schema partitore di tensione}
		\label{fig:partitore}
\end{center}
\end{figure}
Dato che l'ingresso del microcontrollore ha un'alta impedenza sarà sufficiente una corrente nell'ordine dei milliAmpere e, ai fini del calcolo della corrente, i resistori si potranno considerare come se fossero in serie con una resistenza totale $R_S$.

Posto $k = \frac{R_2}{R_1 + R_2}$ e $R_S = R_1 + R_2 \rightarrow R_2 = R_S - R_1$, risolvendo il sistema di equazioni con incognita $R_1$ si ottiene $R_1 = R_S*(1-k)$ , scegliendo un valore per $R_S$ si possono calcolare i valori dei resistori.

Esempio (non si considerano le tolleranze dei componenti):
Per ottenere V\textsubscript{OUT} = 5V da V\textsubscript{IN} = 10V, serve k = 0.5.
Per limitare la corrente a un valore vicino a 1mA, 0.001A, la somma dei resistori $R_S$ dovrà avere un valore vicino a $10k\Omega$.
Risolvendo l'equazione sopra si ottiene $R_1 = R_2 = 5k\Omega$, il valore commerciale del resistore più vicino utilizzabile è $4.7k\Omega$.

I componenti, resistori compresi, hanno delle tolleranze, per evitare danni sarà opportuno calcolare i valori per ottenere un partitore con un livello di tensione in uscita leggermente inferiore al massimo consentito.

\bigskip

\titoloparagrafo{Tensione troppo bassa, da amplificare ($k > 1$):}

In una situazione di questo tipo si può risolvere il problema con un componente che si utilizza in molti circuiti, l'amplificatore operazionale (figura \ref{fig:opamp}).
\begin{figure}
	\begin{center}
		\includegraphics[width=.7\textwidth]{opamp}
		\caption{Amplificatore operazionale}
		\label{fig:opamp}
\end{center}
\end{figure}
L'amplificatore operazionale è un componente che consente di effettuare operazioni matematiche.
L'utilizzo dell'amplificatore operazionale è complesso perché l'operazione matematica svolta dipende sia dall'ingresso sia da una rete di retroazione posta tra l'uscita e l'ingresso; la progettazione della rete di retroazione quindi è l'aspetto più critico.

L'amplificatore operazionale è trattato in modo approfondito nel corso "Progetto di Sistemi a Sensore" del professor Pedersini.

Ai fini dell'amplificazione di tensione serve una configurazione standard, la configurazione non invertente, per questo motivo si può costruire il circuito seguendo le indicazioni delle note applicative del componente presenti nei datasheet (figura \ref{fig:opamplm358ds}).

\bigskip

\begin{figure}
	\begin{center}
\includegraphics[width=.9\textwidth]{schema_op_amp_non_invertente}.
		\caption{Amplificatore operazionale configurazione non invertente}
		\label{fig:opamplm358ds}
\end{center}
\end{figure}

La tensione V\textsubscript{OUT} è pari alla tensione in ingresso V\textsubscript{IN} moltiplicata per il guadagno G (\textbf{G}ain)) così definito:

\begin{equation}
G = 1 + \frac{R_2}{R_1}
\end{equation}

ottenendo quindi:

\begin{equation}
V\textsubscript{OUT} = G * V\textsubscript{IN}
\end{equation}

\noindent Importante, in un amplificatore operazionale reale:
\begin{compactitem}
\item il valore massimo in uscita non potrà mai essere pari alla tensione di alimentazione ma sarà leggermente più basso;
\item con un'alimentazione singola il valore 0 sull'uscita non sarà mai 0V ma ci si avvicinerà soltanto.
\end{compactitem}
Per questo esempio questi aspetti sono trascurabili.

Per testare il circuito avrò bisogno di una tensione di poche decine/centinaia di milliVolt da amplificare, che si può ottenere con un partitore di tensione costruito con resistori o, per ottenere una tensione variabile, con un potenziometro.

Come esercizio si può costruire il circuito utilizzando un amplificatore operazionale LM358 e come resistori R1 = 100k{\textohm}, R2 = 1M{\textohm}, per ottenere un guadagno di 11, e la tensione in ingresso si può ottenere creando un partitore di tensione con un potenziometro da 5k{\textohm}.

\begin{figure}
	\begin{center}
		\includegraphics[width=.8\textwidth]{opamp_x11_example}
		\caption{Connessione ad Arduino, su breadboard}
		\label{fig:connessionebreadboard}
	\end{center}
\end{figure}

\begin{figure}
	\begin{center}
		\includegraphics[width=.9\textwidth]{opampLM358_schem}
		\caption{Schema connessione Arduino}
		\label{fig:connessionearduino}
	\end{center}
\end{figure}