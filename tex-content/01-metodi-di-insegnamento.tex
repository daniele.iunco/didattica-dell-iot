\chapter{Analisi metodi di insegnamento e materiale didattico}

\section{Metodo di insegnamento classico, la lezione frontale}
Nella didattica tradizionale il docente presenta i contenuti della lezione agli studenti, questo avviene in modo unidirezionale, seguendo un programma preciso; gli studenti quindi seguono la lezione e prendono appunti.

L'insegnamento è affidato interamente al docente, lo stesso vale per eventuali lezioni di laboratorio, dove gli studenti applicano le teorie apprese per svolgere degli esercizi assegnati.

Una lezione frontale può essere poco efficace, l'attenzione degli studenti, anche se massima nei primi minuti, diminuisce considerevolmente con il passare del tempo.
Secondo i dati raccolti da CONFAO ``immediatamente dopo una lezione (di 50 minuti), gli studenti ricordano circa il 70\% di quanto presentato nei primi 10 minuti, e il 20\% del contenuto presentato negli ultimi 10 minuti.'' \cite{CONFAO}
Questo tipo di lezione, valida per apprendere le basi teoriche, può non essere ottimale dove è importante anche la creatività dello studente; come nel caso dei sistemi embedded, per la risoluzione di problemi reali.

\section{Il costruzionismo}
L'idea di incentivare l'interazione dello studente, rendendolo con la sua creatività parte attiva nel processo di apprendimento, ha basi nel costruzionismo.

Il concetto di costruzionismo è stato definito da Seymour Papert, noto matematico, informatico, pedagogista e ideatore del linguaggio di programmazione a scopo didattico LOGO;
il costruzionismo ha origine dalla teoria della psicologia dello sviluppo costruttivista di Piaget e dalle opportunità messe a disposizione dalla tecnologia; nel costruzionismo si punta alla costruzione di conoscenza piuttosto che alla sua trasmissione, Papert ritiene che l'insegnamento sia più efficace quando diventa parte di un'attività che mette lo studente nella condizione di costruire un oggetto significativo \cite{papert1986constructionism}.

Quindi si deve generare un interesse nello studente, che lo incentiva a proseguire con un apprendimento che non è più unidirezionale; per fare questo lo studente deve avere la possibilità di costruire oggetti.
Per queste ragioni anche l'errore diventa parte del processo di apprendimento; l'errore commesso non va visto solo come un ostacolo che impedisce di raggiungere l'obiettivo; correggere l'errore deve diventare parte del processo di apprendimento, ad esempio un esito inatteso deve diventare un motivo di riflessione per individuarne la causa e poi trovare una soluzione.

\section{Problem-Based Learning e Project-Based Learning}
Il Problem-Based Learning è un metodo didattico che cambia il ruolo dello studente in problem solver; ha origine dalla teoria del filosofo John Dewey definita come ``learning by doing'', secondo cui lo studio sperimentale deve partire da un problema da risolvere \cite[p.~13]{WileyPBLHandbook}.
Il Problem-Based Learning è stato utilizzato per la prima volta nella facoltà di educazione alle scienze della salute all'università McMaster in Canada (1969) \cite[p.~4]{WileyPBLHandbook}.
Risolvere un problema reale è diverso da svolgere un normale esercizio perché, negli esercizi, le teorie da applicare e le soluzioni possono essere già determinate.
Lo studente dovrà prima comprendere il problema e poi individuare una soluzione, per fare questo dovrà definire degli obiettivi da raggiungere e facendo questo individuerà anche le nuove conoscenze necessarie.
Con questo metodo di insegnamento diventa importante anche la collaborazione con gli altri studenti che lavoreranno in gruppo sul problema, il docente fornirà supporto agli studenti nelle varie fasi che portano alla risoluzione del problema.

Il Project-Based Learning è un caso particolare di Problem-Based Learning, dove si parte da un progetto con obiettivi di apprendimento già determinati \cite[p.~92]{WileyPBLHandbook}.

Con entrambi i metodi è necessario scegliere progetto o un problema di complessità tale da garantire, in caso di collaborazione tra più studenti, che tutti debbano collaborare per il raggiungimento degli obiettivi.

\subsection{Corso sperimentale dell'università di Helsinki}
Un caso di applicazione del Problem-Based Learning a un corso di Internet of Things è quello del corso sperimentale dell'università di Helsinki \cite{PBLHelsinki}.

Il corso è durato 7 settimane ed è stato diviso in più fasi, aveva come scopo quello di individuare e realizzare soluzioni per supportare la gestione di una serra, considerando anche il contenimento dei costi di realizzazione.
Pur definendo dei concetti base da apprendere, gli obiettivi non erano definiti dall'inizio; gli studenti dovevano sviluppare soluzioni per supportare la gestione della serra senza vincoli su metodi utilizzati per affrontare i problemi.

Gli studenti hanno:
\begin{compactitem}
\item analizzato le caratteristiche della serra e individuato ciò che poteva essere utile per la sua gestione;
\item valutato l'hardware a disposizione e studiato i progetti da realizzare;
\item lavorato sui progetti, dividendosi in gruppi;
\item messo a punto i prototipi e al termine ne hanno dimostrato il funzionamento.
\end{compactitem}
Gli studenti proseguendo con il lavoro hanno acquisito conoscenze aggiuntive relative all'ambiente in cui lavoravano, e anche individuato altri problemi come ad esempio quello dell'efficienza energetica o della precisione delle misurazioni.

Il corso ha avuto successo e sono stati raggiunti gli obiettivi prefissati.

\subsection{Metodo Problem-Based Learning con didattica capovolta, Università di Aarhus, Danimarca}
Un caso particolare interessante è quello dell'abbinamento del metodo Problem-Based Learning alla didattica capovolta.

Nell'università di Aarhus in Danimarca, è stato studiato un corso di laurea in ingegneria elettronica dove si utilizza il metodo della didattica capovolta \cite{AarhusDK}, inoltre, per agevolare l'acquisizione di competenze professionali, sono assegnati progetti finalizzati alla risoluzione di problemi aperti, utilizzando così anche il metodo Problem-Based Learning, ad oggi il corso è disponibile solo in lingua danese.
Gli studenti del corso sono divisi in tre gruppi:
\begin{compactitem}
\item studenti che seguono le lezioni all'università;
\item studenti che seguono le lezioni online in tempo reale, in modo sincrono;
\item studenti che seguono le lezioni in un momento diverso, in modo asincrono.
\end{compactitem}
Lo studio individuale è un punto chiave, gli studenti, con il supporto di video lezioni, studiano autonomamente gli argomenti assegnati, svolgono poi degli esercizi e, per concludere, rispondono ad un questionario predisposto dal docente.
Le lezioni in aula hanno uno scopo diverso da quello a cui si è abituati, e sono divise in tre parti:
\begin{compactitem}
\item la prima parte è dedicata alla valutazione dell'apprendimento, i questionari permettono al docente di individuare gli argomenti che creano maggiori difficoltà e, grazie ai dati raccolti, predisporre anche tutorial, esercizi o piccoli progetti da far svolgere agli studenti, a cui seguirà come sempre una fase di discussione in aula;
\item la seconda parte è finalizzata allo svolgimento di esercizi, anche come lavoro di gruppo, il docente in questo caso fornisce supporto quando necessario;
\item la terza e ultima parte è dedicata ad una discussione finale su quanto fatto a lezione.
\end{compactitem}
Le lezioni in aula sono registrate e messe a disposizione degli studenti che seguono il corso in modalità asincrona.

Questo corso di laurea è stato oggetto di uno studio finalizzato a determinare l'impatto della combinazione della didattica capovolta e del metodo Problem-Based Learning sull'apprendimento degli studenti \cite{AarhusDKEvaluation}.
L'autore dello studio analizza in modo approfondito questo tipo di organizzazione per raccogliere dati sulla distribuzione del tempo tra le varie attività e monitorare l'andamento semestre dopo semestre, con il fine ultimo di determinare se c'è una contraddizione tra didattica capovolta e Problem-Based Learning.

Uno dei principali problemi è legato a come gli studenti considerano questo metodo di apprendimento, dato che uno dei loro maggiori dubbi è l'incertezza sul tempo a disposizione per studiare, avendo l'impressione di non averne a sufficienza; i dati raccolti però non evidenziano difficoltà per gli studenti nel seguire il corso così come è stato progettato.

Dall'analisi fatta emerge che la maggioranza degli studenti crede di poter influenzare l'esito del proprio apprendimento, inoltre semestre dopo semestre questi hanno sempre più controllo sul lavoro svolto, questo è in linea con gli obiettivi del metodo di insegnamento scelto.
L'autore riscontra però che gli studenti non sono coinvolti nella pianificazione dell'apprendimento per la parte teorica, perché è il docente ad occuparsi di questo, concludendo che ci si potrebbe spingere ancora di più verso l'approccio orientato ai problemi, sempre abbinato alla didattica capovolta.

\subsection{Conclusioni}
In questo metodo di apprendimento la parte pratica è rilevante ai fini dell'apprendimento, tuttavia l'assenza di conoscenze di base di elettronica a inizio corso potrebbe rendere necessario un maggior impiego di tempo per acquisirle, e limitare il tempo a disposizione di esercitazioni su argomenti più complessi, per ottenere buoni risultati diventa quindi necessario massimizzare la produttività di queste lezioni e sfruttare al meglio le ore di studio individuale degli studenti.

Lo svolgimento di esercizi ha fondamenti nella teoria del ciclo di apprendimento sperimentale di Kolb (1984) \cite{KolbExperimentalLearning}, ma è ipotizzabile che il limitato numero di ore dedicate alle lezioni di laboratorio possa limitarne l'applicazione.

Uno studio estensivo del metodo di apprendimento sperimentale di Kolb pubblicato sul Journal of Engineering Education \cite{ApplyingKolbExperimentalLearningCycle}, teorizza che sia possibile agevolare il raggiungimento degli obiettivi didattici del lavoro di laboratorio con l'ausilio di sessioni di preparazione virtuali prima delle sessioni di laboratorio e con l'utilizzo di strumenti di simulazione.
L'idea è far si che gli studenti possano acquisire in anticipo conoscenze utili a svolgere il lavoro di laboratorio; i dati raccolti dagli autori hanno confermato che questo tipo di organizzazione ha portato al miglioramento della comprensione dei concetti da parte degli studenti; questa risulta quindi un'opzione da tenere in considerazione nella progettazione delle lezioni di laboratorio.


\section{Analisi di due diversi corsi di Sistemi Embedded e Internet of Things dell'università di Berkeley}

Come ultimo punto analizzerò il programma di due corsi dell'università di Berkeley, ``Introduction to Embedded Systems'' \cite{EECS149} e ``Electronics for the Internet of Things'' \cite{EE49}, questi corsi sono molto diversi tra loro, il primo è un corso più teorico dove le basi di elettronica sono un prerequisito, il secondo invece parte dall'elettronica.


\subsection{Introduction to Embedded Systems - EECS 149}

Questo corso tratta i seguenti argomenti:
\begin{compactitem}
\item sistemi cyber fisici;
\item analisi e progettazione di sistemi embedded;
\item modelli di computazione, controllo, analisi e verifica;
\item interfacciamento con il mondo fisico;
\item sistemi real-time;
\item sistemi embedded distribuiti.
\end{compactitem}
Nel corso si studiano in modo approfondito gli aspetti teorici (fisica e modellazione, macchine a stati finiti, sistemi deterministici, architettura della memoria), la realizzazione del software e gli strumenti utilizzati a tal scopo.
E' un corso interessante dal punto di vista dello studio delle problematiche di progettazione dei sistemi embedded.


\subsection{Electronics for the Internet of Things - EE49}

Questo corso tratta i seguenti argomenti:
\begin{compactitem}
\item basi di elettronica e componenti
\item segnali analogici e digitali
\item microcontrollori
\item sensori
\item attuatori
\item efficienza energetica
\item protocolli di comunicazione
\end{compactitem}
Questo corso a differenza del precedente parte dalle basi di elettronica, e ha come obiettivo insegnare agli studenti come realizzare dispositivi che interagiscono con il mondo fisico e ad utilizzare l'elettronica per risolvere problemi, con lezioni di laboratorio dove si mette in pratica quanto appreso nelle lezioni di teoria.


\subsection{Conclusioni sui corsi}

Questi due corsi riguardano aspetti diversi ma comunque importanti della materia.
Nel mio lavoro li terrò in considerazione per fare un confronto con il corso di Sistemi Embedded del corso di laurea in Informatica.
L'obiettivo sarà individuare eventuali argomenti da approfondire per fornire agli studenti le nozioni di base per poter collaborare a progetti di sistemi embedded, così che si possano interfacciare con chi si occupa degli aspetti di elettronica comprendendo le modalità operative e le problematiche.

\section{L'elettronica nel corso di laurea in Informatica}
Per lavorare sui sistemi embedded, oltre che opportune conoscenze di programmazione e di fisica, sono fondamentali molte conoscenze di elettronica, perché è necessario sapere come connettere sensori ed attuatori ai microcontrollori e come gestire input e output, cosa ben diversa dal software su cui di norma lavora un informatico.
Uno degli obiettivi del corso sarà quindi far acquisire agli studenti le conoscenze di base di elettronica per metterli nelle condizioni di utilizzare ciò che esiste in modo corretto e con successo, senza commettere errori che possono causare danni.

Gli studenti, completati i corsi ``Programmazione I'', ``Architettura degli Elaboratori I'' e ``Algoritmi e strutture dati'', potranno proseguire studiando le differenze tra CPU e MCU per poi essere nelle condizioni di iniziare a sviluppare software per sistemi embedded.

Il corso di ``Architettura degli Elaboratori I'' tratta concetti che stanno alla base dell'elettronica digitale:
\begin{compactitem}
\item Logica combinatoria
\item Logica sequenziale
\item Registri
\item Macchine a stati finiti
\item Memoria
\item Bus e I/O
\item Circuiti combinatori
\end{compactitem}
Anche se questi concetti sono applicabili ai sistemi embedded, il livello di astrazione con cui sono stati studiati non consente di passare direttamente alla pratica ed al montaggio fisico, inoltre, dovendo interagire con il mondo fisico, non ci si può concentrare solo sull'elettronica digitale ed ignorare l'elettronica analogica.
La prima parte del corso quindi dovrà essere dedicata allo studio dei concetti di base di elettronica e dei principali componenti utilizzati, per fornire le conoscenze di base necessarie ad operare su questi oggetti; sarà necessario individuare i componenti elettronici più spesso utilizzati ed i problemi più comuni da risolvere, e, in una fase più avanzata del corso procedere con un approfondimento sui circuiti combinatori con esempi d'uso dei circuiti integrati in commercio.

Un altro problema per l'informatico è che, in caso di un funzionamento inatteso tenderà a considerare come prima causa un bug software, tuttavia nei sistemi embedded esiste anche la possibilità che il bug sia nell'hardware, cioè che il comportamento inatteso sia la conseguenza di un errore nella progettazione dell'hardware o nell'assemblaggio dei componenti.\\
Già nella vita di tutti i giorni si possono commettere errori che causano anomalie poi valutate in modo errato, l'alimentatore di un dispositivo posizionato in un posto poco ventilato potrebbe surriscaldarsi e spegnersi in modo anomalo o danneggiarsi; ad una prima osservazione si potrebbe pensare a un difetto dell'alimentatore, ma se non si individua la causa si ripresenterà lo stesso problema anche dopo la sua sostituzione.
Può capitare lo stesso nei sistemi embedded, ad esempio un modulo connesso con un cavo di lunghezza eccessiva potrebbe apparire come non funzionante o causare errori di comunicazione; il malfunzionamento, dovuto all'attenuazione del segnale, non si potrebbe rilevare provando i componenti singolarmente e questo potrebbe far pensare erroneamente a un bug del software.

Inizierò con un'analisi degli argomenti del libro ``Sistemi Embedded: Teoria e pratica" \cite{sistemiembedded} in particolare dei capitoli relativi all'elettronica, anche con lo scopo di individuare eventuali argomenti non trattati che però possono essere necessari nella progettazione di sistemi embedded e quindi utili ai fini del corso.
Poi, dato che gli studenti acquisteranno dei kit di sensori/attuatori e alcuni componenti standard, analizzerò ciò che si trova nei kit in commercio per valutare che utilizzo farne per gli esercizi e anche per il progetto da realizzare.

Ritengo poi opportuna sia l'inclusione di esercizi sia di un progetto diviso in più parti da realizzare in momenti diversi del corso.
Gli esercizi serviranno a provare volta per volta quanto studiato nelle lezioni di teoria, includendo anche alcuni esempi dove si mostrano risultati inattesi causati da bug hardware o da scelte di progettazione errate, per incentivare gli studenti a capire quest'altro tipo di anomalia che nell'informatica non esiste.
Il progetto servirà ad applicare i concetti studiati per la risoluzione di problemi reali, volta per volta i nuovi argomenti studiati consentiranno di introdurre funzionalità e migliorie; pur fornendo delle linee guida saranno gli studenti a dover trovare il modo di realizzare quanto richiesto per ottenere le funzionalità volute.
Si inizierà con il semplice collegamento di sensori ed attuatori per utilizzarli singolarmente, fino ad arrivare alla costruzione di un oggetto con funzionalità complesse ottenute utilizzando più componenti e moduli.

Per questo lavoro intendo scegliere hardware e software libero, la disponibilità di piattaforme come Arduino e di MCU più avanzate come ESP8266, economiche e con disponibilità di numerosi moduli e librerie per l'interfacciamento, rende possibile realizzare oggetti anche complessi e provare quanto studiato senza barriere d'ingresso, disporre poi della documentazione e degli schemi degli oggetti consente di studiare come questi funzionano ed è un altro aspetto molto importante.

\import{tex-content/argomenti-e-approfondimenti/}{argomenti-libro-sistemi-embedded}
