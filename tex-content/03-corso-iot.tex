\chapter{Premesse per l'organizzazione del corso}\label{cap:premesse}

Per poter procedere con lo studio dell'organizzazione del corso ci serve la definizione delle tecnologie da utilizzare per quanto riguarda l'hardware, del materiale da acquistare o di cui sarà consigliato l'uso negli esercizi, e infine un breve esame dell'approccio alla comunicazione tra oggetti, aspetto anch'esso da considerare nel contesto dell'IoT.

\section{Hardware}
Nell'Internet of Things e nei sistemi embedded l'hardware ha un ruolo chiave, tutto l'interfacciamento con il mondo fisico è possibile grazie a sensori e attuatori da collegare a un microcontrollore e quindi non basta la sola programmazione, un oggetto va visto come un insieme di hardware e software e con il software si comunica direttamente con l'hardware.

Il corso sarà orientato al physical computing e gli studenti, acquisite le basi di elettronica necessarie, dovranno passare al montaggio fisico, per arrivare così a costruire dei prototipi utilizzando moduli e componenti che si trovano in commercio.

Quindi è necessario scegliere l'hardware da utilizzare, ed in particolare decidere tra hardware libero e hardware proprietario valutandone i pro e i contro.

\subsection{Hardware proprietario e brevetti}

L'hardware proprietario, il più comune, è un hardware di cui non sono disponibili informazioni dettagliate sul funzionamento, per l'utente è una black box e la documentazione è limitata a quanto necessario per l'interfacciamento e l'uso.
Questo hardware è spesso brevettato o è costruito con componenti brevettati e non può essere disassemblato, studiato o riprodotto liberamente.

\subsubsection*{Brevetti}
Il brevetto è uno strumento che concede all'inventore un diritto esclusivo di sfruttamento della sua invenzione e di conseguenza la facoltà di decidere anche come possa essere utilizzata e a chi altro eventualmente consentire la produzione e la commercializzazione.

Il brevetto si può rinnovare pagando annualmente i diritti di mantenimento, per un tempo massimo che, per i brevetti di invenzioni industriali in Europa e Stati Uniti è 20 anni; quando un brevetto scade l'invenzione non è più tutelata e diventa di pubblico dominio.

Il brevetto può riguardare funzionalità che possono anche essere utilizzate in diversi oggetti, se una tecnologia brevettata sta alla base di un particolare oggetto e non si può sostituire con altro, non sarà possibile produrre quell'oggetto, salvo ottenere dal detentore del brevetto l'autorizzazione, cosa che se possibile avrà sicuramente un costo.

I brevetti possono portare a una maggior competitività se si vede la cosa dal punto di vista che, volendo realizzare un prodotto dello stesso tipo di uno brevettato, non sarà possibile partire dalla tecnologia su cui si basa e si dovrà creare qualcosa di nuovo che lo supera, che poi si possa brevettare per trarne profitto, tuttavia il fatto di non poter iniziare della tecnologia esistente può anche limitare il progresso tecnologico.

\subsubsection*{Vantaggi e svantaggi}
L'indubbio vantaggio dell'hardware proprietario è che ha caratteristiche precise, una documentazione completa e dettagliata, valida per tutti gli esemplari dello stesso oggetto e un supporto tecnico fornito dal produttore stesso.

Lo svantaggio è che l'hardware proprietario è una black box, quindi imparare ad utilizzare un oggetto di un produttore che svolge una particolare operazione non vuol dire imparare ad utilizzare una categoria di oggetti che compiono quell'operazione, né capire come funziona quella categoria di oggetti, anche perché un secondo produttore non potrebbe comunque realizzare un oggetto copiandone un altro brevettato.


\subsection{Hardware libero - Open hardware}
Nell'hardware libero i progetti (schemi, layout del PCB e dettagli del funzionamento) sono distribuiti con licenze \cite{WikipediaOpenHardware} che permettono di studiarne in dettaglio il funzionamento, rendendo possibile anche la costruzione di copie uguali o di nuove varianti da ridistribuire.

La diffusione dell'hardware libero, a differenza del software, dipende anche dai costi dei componenti utilizzati e del processo di produzione.

Tra l'hardware libero più diffuso ci sono schede di sviluppo e moduli, l'esempio più famoso per il successo che ha ottenuto è Arduino.

In generale però, a differenza del software, nella produzione e distribuzione di hardware libero si devono comunque tenere in considerazione i brevetti; il progetto di un particolare componente hardware libero potrebbe contenere, anche involontariamente, funzionalità già brevettate da qualcun altro, pubblicarlo o anche modificarlo e ridistribuirlo potrebbe portare a una violazione del brevetto esistente.


\subsubsection*{Vantaggi e svantaggi}

Disporre di hardware libero porta numerosi vantaggi:
\begin{compactitem}
	\item grande disponibilità di documentazione che consente di studiare in modo approfondito anche come funzionano gli oggetti e non solo come si usano;
	\item possibilità di modificare i progetti di oggetti esistenti in base alle necessità;
	\item forum e community dove trovare supporto da altri utenti e a volte anche dai produttori stessi dell'hardware;
	\item costi bassi grazie all'esistenza di più produttori e quindi alla maggiore concorrenza;
	\item gli studenti possono studiare concetti validi per categorie di oggetti e non specifici per il funzionalità specifiche degli oggetti di un singolo produttore;
\end{compactitem}

Senza dubbio l'hardware open source può creare anche difficoltà, ipotizzando di voler acquistare sensori, attuatori o schede di sviluppo basate su un progetti open source, capiterà di trovarne sul mercato varianti di produttori diversi. Queste varianti hardware si potrebbero paragonare ai \textit{fork} del software open source, le differenze potrebbero renderle più o meno adatte all'uso previsto, quindi va considerato anche questo nella scelta, senza contare che, a differenza del software, un hardware già costruito non si può aggiornare, aggiornarlo vorrebbe dire sostituire o modificare ogni esemplare.
Nella scelta di una particolare versione di un hardware, nel caso lo si voglia acquistare già pronto e non produrlo, sarà necessario anche considerare la disponibilità a lungo termine sul mercato o trovare chi può produrlo.

\subsection{Relazione hardware e middleware nel contesto open source}
Il concetto di middleware è applicabile anche ai sistemi embedded, avendo il software un ruolo chiave per interfacciarsi a componenti hardware diversi, con librerie standard o specifiche per singoli componenti hardware.
Questo richiede quindi un confronto sul diverso approccio alle librerie prima di trarre conclusioni sulla scelta della tipologia di hardware.

L'hardware proprietario è abbinato a librerie anch'esse proprietarie fornite dal produttore e specifiche per i suoi prodotti, questo vuol dire avere il vantaggio di ottenere software e supporto dal produttore, ma allo stesso tempo non poter utilizzare quel software per scopi diversi da quanto previsto o comunque nel caso servano modifiche che non sono consentite.

Nel caso dell'hardware libero invece si avrà l'enorme vantaggio di poter disporre anche dei sorgenti del middleware, ed avere la possibilità di studiarli, oppure in caso di necessità, di modificarli come serve per adattarli ad un particolare progetto o ad un altro hardware.

\subsection{Conclusioni generali sulla scelta dell'hardware}

Il corso dovrà fornire conoscenze valide per lavorare in generale sui sistemi embedded, quindi sarà importante saper leggere la documentazione di ciò su cui si deve lavorare ed essere in grado di sviluppare il software interfacciandosi all'hardware seguendo quella documentazione, questo è applicabile sia a hardware proprietario sia a hardware libero.
Però ritengo opportuno privilegiare conoscenze valide in generale e non specifiche per oggetti di un particolare produttore, e in quest'ottica avere anche la possibilità di capire come funzionano gli oggetti senza limitarsi a saperne utilizzare solo alcuni è un punto a favore dell'hardware libero.

Naturalmente nella scelta dell'hardware ci sono anche aspetti commerciali, dal punto di vista della produzione industriale, i vantaggi sopra esposti dell'hardware proprietario potrebbero fare la differenza nella scelta, ma, acquisito un metodo valido per procedere nel caso generale, non sarà un problema studiare la documentazione dell'hardware di un particolare produttore.

Per questi motivi ritengo che l'uso di hardware e software open source sia un vantaggio per l'apprendimento.

\import{tex-content/argomenti-e-approfondimenti/}{panoramica-kit-moduli-in-commercio}


\section{Comunicazioni, sistemi distribuiti e cloud computing}
Gli oggetti sono spesso parte di un sistema più grande per cui diventa evidente l'importante ruolo delle tecniche di comunicazione, che non devono essere limitate all'uso di una connessione internet; si devono quindi studiare anche mezzi di trasmissione diversi e protocolli specifici per l'IoT, come MQTT.

Serviranno esercizi specifici legati alla scelta del mezzo di comunicazione, e di conseguenza scelta dei moduli da utilizzare, partendo da dei vincoli come:
\begin{compactitem}
	\item distanza tra gli oggetti da connettere
	\item resistenza alle interferenze
	\item sicurezza delle comunicazioni senza fili, come ad esempio il rischio di \textit{replay-attack}
	\item fonte di alimentazione e consumi massimi consentiti
\end{compactitem}

\subsection{Sistemi distribuiti e cloud computing}
L'esistenza di più oggetti che svolgono compiti specifici come parte di un sistema più complesso porta allo studio dei sistemi distribuiti e all'utilizzo del cloud computing, i dati possono essere raccolti ed elaborati da un sistema centrale.
Si dovranno quindi riprendere i concetti studiati nelle altre materie del corso di Laurea in Informatica ed applicarli, possibilmente con progetti specifici, ai sistemi embedded.

Per quanto riguarda questo punto, progetti specifici sono quelli del monitoraggio ambientale, dove grazie a numerose stazioni di monitoraggio si raccolgono dati dell'area monitorata e si inviano su un server centrale per l'elaborazione; questo tipo di progetto coprirebbe molti punti trattati in precedenza, però con costi di realizzazione elevati dovuti in buona parte al costo dei sensori necessari.
Per trattare l'aspetto teorico e dare un'idea pratica del funzionamento con un progetto da svolgere nel corso, si potrebbe pensare al monitoraggio delle reti WiFi, costruendo un oggetto con la scheda di rete WiFi e un sensore GPS, che periodicamente scansiona le reti attive nell'area ed invia i dati raccolti a un server centrale.
In alternativa, al posto delle reti WiFi, si potrebbero monitorare temperatura e umidità.

Indipendentemente da quale sia la misura effettuata, l'obiettivo sarà far comprendere come costruire un sistema complesso, in cui ciascun nodo raccoglie dei dati e li invia a un server centrale che poi li elaborerà, espandibile semplicemente aggiungendo nodi.
Anche il software non sarà banale, volendolo realizzare per sfruttare le caratteristiche di risparmio energetico dell'hardware, non sarà sufficiente fare un polling dell'output del sensore scelto ed inviarlo al server centrale, ma sarà necessario pianificare come e quando raccogliere e trasferire i dati in modo da ridurre i consumi.
Questo progetto, se realizzato da più studenti, potrà consentire di disegnare una mappa di un'area evidenziando la concentrazione delle reti WiFi oppure con la temperatura/umidità, e di fare poi anche qualche analisi statistica su questi dati.