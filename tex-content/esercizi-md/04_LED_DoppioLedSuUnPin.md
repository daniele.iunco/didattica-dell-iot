<!--<metadata>
<title>Funzionamento dei diodi</title>
<tags>Base,DigitalInput,DigitalOutput</tags>
<prerequisites>diode,digitalOutput</prerequisites>
</metadata>-->
Funzionamento dei diodi
====

Prerequisiti:{-}
----
- funzionamento dei diodi ed in particolare dei LED
- funzionamento dei pin GPIO

Componenti:{-}
----
- 2 resistori da 220 Ohm
- 2 diodi LED

Descrizione e risultato atteso:{-}
----
I diodi sono componenti polarizzati, collegando i LED come nell'immagine se ne potrà accendere solo uno per volta, in base allo stato del pin digitale.
Sfruttare i 3 stati (OUTPUT LOW, OUTPUT HIGH, INPUT) per per lampeggiare uno dei LED e lasciare spento l'altro.

Scopo dell'esercizio:{-}
----
Lo stato di INPUT ad alta impedenza, senza resistore di pullUp, può essere utile per accendere o spegnere qualche componente collegato ai GPIO.

Eventuali funzionalità aggiuntive:{-}
----
N/A

Errori comuni:{-}
----
L'inversione di polarità dei LED fa funzionare il circuito al contrario.

Possibile comportamento erratico:{-}
----
N/A

Risultato di apprendimento:{-}
----
Se si vuole spegnere un componente alimentato da un GPIO, ma lo stato OUTPUT LOW porta ad un risultato indesiderato, si può sfruttare la modalità INPUT che ha un'alta impedenza, tale da comportarsi da circuito aperto.

N.B. Provando la modalità INPUT_PULLUP con il tester si rileva una tensione, questo potrebbe far pensare che in questo circuito abbia lo stesso effetto dello stato OUTPUT HIGH, ma non è così, data la bassissima corrente la luminosità del LED sarebbe appena percettibile.

Indicazioni per la soluzione:{-}
----
Impostando il pin digitale in modalità INPUT i LED rimangono spenti.

![Esempio](./images/2ledstessopin_bb.png)

-
![Esempio](./images/04-2ledsamepin_schem.png)
