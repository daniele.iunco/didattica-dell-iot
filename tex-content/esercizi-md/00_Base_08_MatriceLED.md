<!--<metadata>
<title>Grafica su matrice LED</title>
<tags>Medio,DigitalOutput</tags>
<prerequisites>diode,digitalOutput</prerequisites>
</metadata>-->
Grafica su matrice LED
====

Prerequisiti:{-}
----
- I/O digitale

Componenti:{-}
----
- Matrice LED 8x8
- 8 resistori da 220 ohm per alimentazione a 5V

Descrizione e risultato atteso:{-}
----
Disegnare una riga al centro, e farla ruotare sempre rispetto al centro.

Scopo dell'esercizio:{-}
----
Dato l'esiguo numero di GPIO si devono trovare metodi adeguati a gestire l'I/O, ad esempio con componenti esterni come il registro a scorrimento.

Eventuali funzionalità aggiuntive:{-}
----
Fare un "charlieplexing" con la matrice

Errori comuni:{-}
----
Inversione collegamenti LED

Possibile comportamento erratico:{-}
----
N/A

Risultato di apprendimento:{-}
----
Utilizzo di pin analogici per l'I/O digitale, se si esauriscono questi ultimi, oppure utilizzo del registro a scorrimento (shift register)

Indicazioni per la soluzione:{-}
----
Prima soluzione: non disponendo di abbastanza pin digitali, sarà necessario togliere qualche riga/colonna
Seconda soluzione: un'alternativa è usare i pin analogici come digitali, tuttavia anche questo porterà all'esaurimento di tutti i GPIO e comunque con alcune schede di sviluppo potrebbe non bastare.
Terza soluzione: Quanto provato fa capire la necessità di un componente esterno per controllare la matrice LED, a tal scopo si usa il registro a scorrimento (shift register)

N.B. Collegare un resistore in serie al pin di ciascuna riga della matrice.
