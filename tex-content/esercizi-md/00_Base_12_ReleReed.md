<!--<metadata>
<title>Relè reed + Buzzer attivo</title>
<tags>Base,DigitalInput,DigitalOutput,Software,Rimbalzi,Knocking protocol</tags>
<prerequisites>digitalInput,digitalOutput,debouncing</prerequisites>
</metadata>-->
Relè reed + Buzzer attivo
====

Prerequisiti:{-}
----
- I/O digitale
- Gestione dei rimbalzi

Componenti:{-}
----
- Relè reed
- Magnete
- Buzzer attivo

Descrizione e risultato atteso:{-}
----
Sensore per apertura porta/finestra, a contatto aperto fa suonare un buzzer attivo

Scopo dell'esercizio:{-}
----
Monitorare lo stato di un oggetto per rilevare un particolare evento da notificare all'utente

Eventuali funzionalità aggiuntive:{-}
----
Il buzzer suona solo se rileva una precisa sequenza di attivazioni (applicabile anche in altri casi, es. pulsante/rilevatore urti)

Errori comuni:{-}
----
Errori di collegamento

Possibile comportamento erratico:{-}
----
Sbattere la porta con forza può causare rimbalzi ed eventi anomali.

Risultato di apprendimento:{-}
----
Reazione ad eventi ambientali, tenendo in considerazione eventuali disturbi, nel caso specifico i rimbalzi

Indicazioni per la soluzione:{-}
----
Impostare un GPIO in modalità INPUT_PULLUP, collegare il relè reed tra quel GPIO e massa.
Collegare il polo positivo del buzzer attivo a un GPIO in modalità OUTPUT e il negativo a massa, se il buzzer ha un consumo > di 20mA, servirà un resistore tra il GPIO e il positivo del buzzer, oppure si dovrà utilizzare un transistor BJT come driver.
In caso di modulo buzzer seguire le indicazioni sul modulo.
