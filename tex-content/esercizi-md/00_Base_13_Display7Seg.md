<!--<metadata>
<title>Display 7 segmenti (singolo) + Buzzer attivo</title>
<tags>Base,DigitalOutput,Software</tags>
<prerequisites>digitalOutput</prerequisites>
</metadata>-->
Display 7 segmenti (singolo) + Buzzer attivo
====

Prerequisiti:{-}
----
- I/O digitale

Componenti:{-}
----
- Display 7 segmenti
- Buzzer
- 7 resistori da 220 ohm, per alimentazione a 5V

Descrizione e risultato atteso:{-}
----
Costruire un timer che fa il conto alla rovescia, da 9 a 0 e suona al termine

Scopo dell'esercizio:{-}
----
Mostrare all'utente il progresso di un'operazione in corso e non solo l'esito.

Eventuali funzionalità aggiuntive:{-}
----
N/A

Errori comuni:{-}
----
Inversione collegamenti LED

Possibile comportamento erratico:{-}
----
N/A

Risultato di apprendimento:{-}
----
Individuare la polarità dei LED e il modo corretto di gestire un display a 7 segmenti a catodo comune o anodo comune.

Indicazioni per la soluzione:{-}
----
Collegare il polo positivo del buzzer attivo a un GPIO in modalità OUTPUT e il negativo a massa, se il buzzer ha un consumo > di 20mA, servirà un resistore tra il GPIO e il positivo del buzzer, oppure si dovrà utilizzare un transistor BJT come driver.
In caso di modulo buzzer seguire le indicazioni sul modulo.


Il display può essere ad Anodo comune o Catodo comune.

Per display a Catodo comune collegare:
- Catodo connesso a Gnd
- terminali di ciascun segmento connessi con un resistore a un GPIO digitale

Per display a Anodo comune collegare:
- Anodo connesso a Vcc
- terminali di ciascun segmento connessi con un resistore a un GPIO digitale
