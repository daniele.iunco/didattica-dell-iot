<!--<metadata>
<title>Voltmetro per tensioni maggiori di 3.3V o 5V</title>
<tags>Base,AnalogInput,Software,Circuiti con più alimentazioni</tags>
<prerequisites>ohmlaw,analogInput,multipsu</prerequisites>
</metadata>-->
Voltmetro per tensioni maggiori di 3.3V o 5V
====

Prerequisiti:{-}
----
- legge di Ohm e partitore di tensione
- funzionamento dell'ADC e istruzione analogRead
- (opzionale) cambio del riferimento di tensione dell'ADC

Componenti:{-}
----
- R1 = Resistore da 100 kOhm
- R2 = Resistore da 10 kOhm
- (metacomponente) fonte di tensione adeguata >5V

Descrizione e risultato atteso:{-}
----
Costruire un partitore di tensione con i resistori indicati per ridurre la tensione sull'ingresso analogico del microcontrollore, in modo da ottenere la massima riduzione della tensione in ingresso con i resistori disponibili.  
Individuare la nuova tensione massima misurabile.  
Scrivere sulla porta seriale il valore misurato convertito in Volt, tenendo conto della funzione di trasferimento del partitore di tensione costruito.

Scopo dell'esercizio:{-}
----
Riconoscere il problema della misura di un livello di tensione da una fonte esterna che, in condizioni normali, supererebbe il massimo consentito come input del microcontrollore.

Eventuali funzionalità aggiuntive:{-}
----
In caso di realizzazione su Arduino, rifare l'esercizio utilizzando il riferimento di tensione integrato da 1.1V per ottenere misure più accurate.

Errori comuni:{-}
----
N/A

Possibile comportamento erratico:{-}
----
La tolleranza dei componenti potrebbe portare a misure poco precise in confronto a quanto misurato con il multimetro digitale.

Risultato di apprendimento:{-}
----
Applicare il partitore per misurare una tensione da una fonte esterna, sempre in bassa tensione, per rendere possibile la misurazione si devono connettere insieme le masse delle fonti di alimentazione.

Indicazioni per la soluzione:{-}
----
Costruire un partitore di tensione per ridurre la tensione in ingresso, come indicato nell'immagine

![Esempio](./images/partitore_voltmetro_bb.png)
