<!--<metadata>
<title>Suoneria a basso consumo per avviso apertura porta</title>
<tags>Avanzato,DigitalInput,DigitalOutput,Software,Rimbalzi,Risparmio energetico,PBL</tags>
<prerequisites>digitalInput,digitalOutput,debouncing,swpowersave</prerequisites>
</metadata>-->
Suoneria a basso consumo per avviso apertura porta
====

Prerequisiti:{-}
----
- Modalità di risparmio energetico
- Interrupt
- Problema dei rimbalzi

Componenti:{-}
----
- Relè reed (Es. KY-019)
- Buzzer attivo (Es. KY-012)

Descrizione e risultato atteso:{-}
----
Costruire un oggetto che rileva l'apertura di un contatto e, al verificarsi dell'evento, fa suonare un buzzer per generare un breve suono.

Progettare il software affinché sia possibile alimentare l'oggetto anche a batteria, il microcontrollore quindi deve rimanere in modalità a basso consumo e riattivarsi solo all'apertura del contatto e limitatamente al tempo in cui deve suonare il buzzer.

Partire dal presupposto che l'utente deve anche poter decidere di lasciare la porta aperta, tale azione dovrà rendere inattivo l'oggetto senza costringere l'utente a spegnerlo per impedire che si scarichi la batteria.

Scopo dell'esercizio:{-}
----
Quando si utilizzano le modalità a basso consumo con eventi generati da interruttori meccanici di qualche tipo, i rimbalzi possono causare comportamenti anomali.
L'esercizio, principalmente legato allo sviluppo software, mette in evidenza l'interazione tra uso delle varie modalità a basso consumo e problema dei rimbalzi.

Eventuali funzionalità aggiuntive:{-}
----
Per ESP8266, utilizzare una soluzione per gestire la modalità deep sleep aggiungendo un transistor come indicato nel secondo schema.

Errori comuni:{-}
----
N/A

Possibile comportamento erratico:{-}
----
Vedi dettagli PBL

Validità per PBL:{-}
----
Problema da definire e analizzare: Si
Necessaria individuazione nuovi obiettivi di apprendimento: Si
Dettagli obiettivo di apprendimento atteso:
La criticità sta nel modo di rilevare l'apertura e la chiusura della porta, qualora l'evento che genera il risveglio del microcontrollore sia l'apertura della porta, si potrebbe tornare alla modalità a basso consumo subito dopo l'emissione del suono tornando in attesa di una nuova apertura, però se si chiudesse la porta sbattendola con forza si potrebbe generare un rimbalzo del relè reed visto come nuova apertura che causerebbe l'emissione di un altro suono che, per requisiti dell'esercizio, non deve esserci.

Il microcontrollore, dovrà tornare sempre alla modalità a basso consumo migliore disponibile in base alla situazione, gestendo anche l'eventuale scelta dell'utente di lasciare la porta aperta per poi chiuderla in seguito e gestendo l'anomalia del rimbalzo in chiusura.

Una tra le possibili opzioni è rilevare l'apertura, emettere il suono, aggiornare una variabile di stato, poi fare un polling dello stato della porta per attendere che si chiuda ma in modalità "basso consumo" cioè sostituendo il classico delay con un breve passaggio a standby, ripetuto fino al rilevamento della chiusura della porta.
Questo breve standby si ottiene abilitando un apposito watchdog, che consente di far risvegliare dallo standby il microcontrollore dopo un numero di secondi scelto (da 1 a 8).

Risultato di apprendimento:{-}
----
Imparare ad individuare momenti di inattività in cui sfruttare le modalità di risparmio energetico per minimizzare i consumi.

Indicazioni per la soluzione:{-}
----
Utilizzare la modalità per il risparmio energetico massimo prevista che consente il risveglio con interrupt sul microcontrollore in uso.

Vedi dettagli PBL


Codice d'esempio (campanello_porta.ino)
----
```cpp

/*
 * Versione per Arduino (ATmega328P).
 * 
 * Definisco un tempo ATTESA_MINIMA_CAMBIO_STATO_MS per evitare rilevamenti errati causa eventi ambientali, tempo di blocco della serratura, etc.
 * 
 */

#include "LowPower.h"

#define PIN_RELE_REED 2
#define PIN_BUZZER 5
#define ATTESA_MINIMA_CAMBIO_STATO_MS 500

volatile bool ignoraEvento = false;
volatile unsigned long ultimaAperturaPorta = 0;

void isrAperturaPorta(){
  if (ignoraEvento) return;
  if (ultimaAperturaPorta > 0) return;
  ultimaAperturaPorta = millis();
}

void notificaApertura(){
  digitalWrite(PIN_BUZZER, HIGH);
  delay(1000);
  digitalWrite(PIN_BUZZER, LOW);
}

void setup() {
  pinMode(PIN_RELE_REED, INPUT_PULLUP);
  pinMode(PIN_BUZZER, OUTPUT);
  attachInterrupt(digitalPinToInterrupt(PIN_RELE_REED), isrAperturaPorta, RISING);
}

void loop() {
  if (ultimaAperturaPorta > 0){
    if (millis() > ultimaAperturaPorta + ATTESA_MINIMA_CAMBIO_STATO_MS) { // se è trascorso abbastanza tempo posso proseguire
      if (digitalRead(PIN_RELE_REED) == HIGH){
        notificaApertura();
      } else { // porta non più aperta, possibili micro spostamenti del magnete causa eventi atmosferici, devo ignorare l'evento.
        ultimaAperturaPorta = 0;
        LowPower.powerDown(SLEEP_FOREVER, ADC_OFF, BOD_OFF);
        return;
      }
    } else { // ancora in attesa
      delay(1); 
      return;
    }
  }

  // porta aperta e notifica emessa, o porta aperta dall'accensione, attendo chiusura
  ignoraEvento = true; // senza disattivare interrupt, ignoro altri eventi per non generare notifiche
  while (digitalRead(PIN_RELE_REED) == HIGH) {
    // N.B. un eventuale evento fa terminare l'attesa
    LowPower.powerDown(SLEEP_1S, ADC_OFF, BOD_OFF);
  }
  delay(ATTESA_MINIMA_CAMBIO_STATO_MS);
  ignoraEvento = false;

  // se dopo il delay la porta si è riaperta, considero l'evento una nuova apertura da verificare
  if (digitalRead(PIN_RELE_REED) == HIGH) {
    ultimaAperturaPorta = millis();
    return;
  }
  
  LowPower.powerDown(SLEEP_FOREVER, ADC_OFF, BOD_OFF);
}
```


Codice d'esempio (campanello_porta_esp8266.ino)
----
```cpp

/*
 * Versione per ESP8266.
 * 
 * Definisco un tempo ATTESA_MINIMA_CAMBIO_STATO_MS per evitare rilevamenti errati causa eventi ambientali, tempo di blocco della serratura, etc.
 * 
 * su ESP8266 in modalità deep sleep il risveglio è possibile solo generando un segnale di reset con componenti esterni, uso quindi la modalità light sleep
 */

#include <ESP8266WiFi.h>
 
#define PIN_RELE_REED D2
#define PIN_BUZZER D5
#define ATTESA_MINIMA_CAMBIO_STATO_MS 500

void setSleepWaitHigh() { //sleep in attesa di livello alto
  WiFi.mode(WIFI_OFF);
  wifi_fpm_set_sleep_type(LIGHT_SLEEP_T);
  gpio_pin_wakeup_enable(GPIO_ID_PIN(PIN_RELE_REED), GPIO_PIN_INTR_HILEVEL);
  wifi_fpm_set_wakeup_cb(callbackAperturaPorta); // callback al risveglio
  wifi_fpm_open();
  wifi_fpm_do_sleep(0xFFFFFFF);  // attesa in sleep massima con risveglio da evento GPIO
  delay(10);  // attesa mentre va in sleep
}

void setSleepWaitLow() { //sleep in attesa di livello basso
  WiFi.mode(WIFI_OFF);
  wifi_fpm_set_sleep_type(LIGHT_SLEEP_T);
  gpio_pin_wakeup_enable(GPIO_ID_PIN(PIN_RELE_REED), GPIO_PIN_INTR_LOLEVEL);
  //wifi_fpm_set_wakeup_cb(callbackChiusuraPorta);
  wifi_fpm_open();
  wifi_fpm_do_sleep(0xFFFFFFF); // attesa in sleep massima con risveglio da evento GPIO
  delay(10);  // attesa mentre va in sleep
}

volatile bool ignoraEvento = false;
volatile unsigned long ultimaAperturaPorta = 0;

void callbackAperturaPorta(){
  if (ignoraEvento) return;
  if (ultimaAperturaPorta > 0) return;
  ultimaAperturaPorta = millis();
}

void notificaApertura(){
  digitalWrite(PIN_BUZZER, HIGH);
  delay(1000);
  digitalWrite(PIN_BUZZER, LOW);
}

void setup() {
  pinMode(PIN_RELE_REED, INPUT_PULLUP);
  pinMode(PIN_BUZZER, OUTPUT);
}

void loop() {
  if (ultimaAperturaPorta > 0){
    if (millis() > ultimaAperturaPorta + ATTESA_MINIMA_CAMBIO_STATO_MS) { // se è trascorso abbastanza tempo posso proseguire
      if (digitalRead(PIN_RELE_REED) == HIGH){
        notificaApertura();
      } else { // porta non più aperta, possibili micro spostamenti del magnete causa eventi atmosferici, devo ignorare l'evento.
        ultimaAperturaPorta = 0;
        setSleepWaitHigh();
        return;
      }
    } else { // ancora in attesa
      delay(1); 
      return;
    }
  }

  // porta aperta e notifica emessa, o porta aperta dall'accensione, attendo chiusura
  ignoraEvento = true; // senza disattivare interrupt, ignoro altri eventi per non generare notifiche
  while (digitalRead(PIN_RELE_REED) == HIGH) {
    setSleepWaitLow();
  }
  delay(ATTESA_MINIMA_CAMBIO_STATO_MS);
  ignoraEvento = false;

  // se dopo il delay la porta si è riaperta, considero l'evento una nuova apertura da verificare
  if (digitalRead(PIN_RELE_REED) == HIGH) {
    ultimaAperturaPorta = millis();
    return;
  }
  
  setSleepWaitHigh();
}
```

