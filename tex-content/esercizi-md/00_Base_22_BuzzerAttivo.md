<!--<metadata>
<title>Convertitore morse da input seriale</title>
<tags>Base,DigitalOutput,Software,Seriale</tags>
<prerequisites>digitalOutput,serial</prerequisites>
</metadata>-->
Convertitore morse da input seriale
====

Prerequisiti:{-}
----
- I/O su porta seriale
- I/O digitale

Componenti:{-}
----
- Buzzer attivo

Descrizione e risultato atteso:{-}
----
Leggere l'input dalla porta seriale, convertire i caratteri in codice Morse generando l'output sonoro corrispondente con il buzzer.

Scopo dell'esercizio:{-}
----
Gli oggetti possono anche diventare periferiche di un computer, quindi devono dialogarci e per fare questo la porta seriale va usata anche per l'input.

Eventuali funzionalità aggiuntive:{-}
----
N/A

Errori comuni:{-}
----
N/A

Possibile comportamento erratico:{-}
----
N/A

Risultato di apprendimento:{-}
----
Comprendere come dialogare con un computer tramite la porta seriale, ricevendo comandi direttamente dall'utente tramite tastiera e serial monitor.

Indicazioni per la soluzione:{-}
----
Collegare il polo positivo del buzzer attivo a un GPIO in modalità OUTPUT e il negativo a massa, se il buzzer ha un consumo > di 20mA, servirà un resistore tra il GPIO e il positivo del buzzer, oppure si dovrà utilizzare un transistor BJT come driver.
In caso di modulo buzzer seguire le indicazioni sul modulo.
