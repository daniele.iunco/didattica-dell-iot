<!--<metadata>
<title>Grafica su matrice LED con input da sensore di capovolgimento</title>
<tags>Medio,DigitalOutput,Software,DigitalInput,Rimbalzi</tags>
<prerequisites>digitalInput,digitalOutput,debouncing</prerequisites>
</metadata>-->
Grafica su matrice LED con input da sensore di capovolgimento
====

Prerequisiti:{-}
----
- I/O digitale
- Gestione dei rimbalzi

Componenti:{-}
----
- Sensore di capovolgimento
- Matrice LED 8x8 su modulo con bus I2C
- Registro a scorrimento e 8 resistori da 220 ohm, se non si dispone del modulo I2C

Descrizione e risultato atteso:{-}
----
Disegno un'immagine sulla matrice LED, ad esempio una freccia, in caso di capovolgimento capovolgo l'immagine per farla vedere sempre nel verso giusto.

Scopo dell'esercizio:{-}
----
Rilevare un particolare evento del mondo fisico ed utilizzarlo come input per controllare il comportamento dell'oggetto.

Eventuali funzionalità aggiuntive:{-}
----
Al posto dell'immagine mostro un numero.
Costruisco un timer che quando è in posizione predefinita conta da 0 a 9 quando è capovolto da 9 a 0.

Errori comuni:{-}
----
Inversione polarità LED, in caso di utilizzo della matrice senza modulo I2C

Possibile comportamento erratico:{-}
----
L'eventuale gestione errata dei rimbalzi può compromettere il funzionamento dell'oggetto.

Risultato di apprendimento:{-}
----
Interazione con il mondo fisico mediante eventi e non tramite tastiera o in generale pulsanti.
Espansione: monitorare l'interazione con l'utente anche mentre si sta svolgendo un altro compito.

Indicazioni per la soluzione:{-}
----
Se non si dispone del modulo indicato ma di una normale matrice LED, collegarla come nell'esercizio precedente sulla matrice LED, sarà necessario almeno un registro a scorrimento per gestire o le righe o le colonne.

Collegare l'accelerometro sui pin appropriati per il tipo di bus (SPI o I2C)

Il sensore di capovolgimento funziona come un pulsante, scegliere un GPIO, da utilizzare in modalità INPUT_PULLUP, collegare il sensore tra il GPIO scelto e la massa.
