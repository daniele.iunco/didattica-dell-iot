<!--<metadata>
<title>VU meter per rumore ambientale</title>
<tags>Medio,DigitalOutput,AnalogInput,Software</tags>
<prerequisites>analogInput,digitalOutput</prerequisites>
</metadata>-->
VU meter per rumore ambientale
====

Prerequisiti:{-}
----
- Funzionamento sensore di rumore

Componenti:{-}
----
- Sensore di rumore con uscita analogica (es. KY-038)
- Barra LED oppure Led, con relativi resistori, in base alla disponibilità

Descrizione e risultato atteso:{-}
----
Costruire un oggetto che fa accendere un numero di LED variabile in base al livello di rumore, utilizzando più LED singoli oppure una barra LED.
Individuare a ogni misurazione l'eventuale nuovo massimo e mappare il livello del segnale sui LED a disposizione in modo proporzionale.

Scopo dell'esercizio:{-}
----
Misurare le variazioni di una grandezza analogica e mostrarle all'utente con un'interfaccia adeguata, in questo caso usando dei LED o una barra LED.

Eventuali funzionalità aggiuntive:{-}
----
Introdurre un effetto tipo scia al variare del livello di rumore.

Errori comuni:{-}
----
Errori di collegamento.

Possibile comportamento erratico:{-}
----
L'esempio base nella documentazione del kit può non essere adeguato
https://www.youtube.com/watch?v=CbovaHqvdsM

Risultato di apprendimento:{-}
----
Utilizzare un sensore analogico attivo.
Abbinare una rappresentazione grafica a una grandezza fisica.

Indicazioni per la soluzione:{-}
----
Collegare l'output analogico del sensore all'input analogico.
Collegare ciascun LED con il suo resistore a un GPIO scelto per l'output.
