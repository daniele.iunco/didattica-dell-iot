<!--<metadata>
<title>Capacimetro</title>
<tags>Base,DigitalOutput,AnalogInput,Software</tags>
<prerequisites>Prerequisiti: ohmlaw,analogInput,digitalOutput</prerequisites>
</metadata>-->
Capacimetro
====

Prerequisiti:{-}
----
- Legge di Ohm
- Condensatore

Componenti:{-}
----
- Resistore 1 kOhm
- Condensatore elettrolitico da misurare, tra 10 e 100 microFarad, con tensione nominale >= 5.5V

Descrizione e risultato atteso:{-}
----
Costruire un circuito che misura la capacità del condensatore, sfruttando la formula che mette in relazione i valori di capacità del condensatore e resistenza del resistore al tempo di carica.

Cenni di teoria:
La tensione ai capi del condensatore collegato in serie a un resistore, in fase di carica o in fase di scarica, varia con un andamento esponenziale.
In un tempo t = R * C la tensione ai capi del condensatore varia del 63,2% di Vfinale-Viniziale, in fase di carica Vfinale sarà la tensione di alimentazione, in fase di scarica Vfinale sarà 0V.
Importante: il resistore R è necessario per evitare danni.

Come semplificazione si può considerare il condensatore completamente scarico quando la tensione ai capi scende allo 0,7% di Vcc.

Scopo dell'esercizio:{-}
----
Trattare una grandezza fisica non direttamente misurabile né direttamente convertibile in una tensione, per cui si rende necessario un calcolo più complesso.

Eventuali funzionalità aggiuntive:{-}
----
N/A

Errori comuni:{-}
----
Collegamenti errati possono causare danni.

Possibile comportamento erratico:{-}
----
N/A

Risultato di apprendimento:{-}
----
Una delle grandezze fisiche che ci si può trovare a dover misurare è la capacità di un condensatore e ci vuole un approccio completamente diverso rispetto ad altre misure.
La capacità va infatti ricavata da resistenza, tempo di carica e tensione raggiunta dal condensatore.

Indicazioni per la soluzione:{-}
----
Il condensatore si può caricare e scaricare utilizzando un pin GPIO, da impostare a livello HIGH  per la fase di carica e a livello LOW per la fase di scarica.
In fase di setup si deve scaricare il condensatore, se non è già scarico.
In fase di loop va messo in carica il condensatore, per misurare il tempo t impiegato a raggiungere il 63.2% di Vcc e ricavare C da R e t.
Non è necessario misurare il tempo di scarica.

![Esempio](./images/15_Condensatore_Timer_bb.png)
