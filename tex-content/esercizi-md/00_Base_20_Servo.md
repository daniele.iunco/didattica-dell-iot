<!--<metadata>
<title>Servo controllato dall'utente</title>
<tags>Base,AnalogInput,PWM</tags>
<prerequisites>ohmlaw,analogInput,pwm</prerequisites>
</metadata>-->
Servo controllato dall'utente
====

Prerequisiti:{-}
----
- Partitore di tensione

Componenti:{-}
----
- Potenziometro
- Servo
- 2 pulsanti normalmente aperti

Descrizione e risultato atteso:{-}
----
Far muovere il servo ruotando il potenziometro, con la posizione predefinita centrale del servo che deve corrispondere alla posizione centrale del potenziometro.

Scopo dell'esercizio:{-}
----
Applicare il comportamento dell'utente a un oggetto, rilevando l'input con un sensore ed applicando l'azione corrispondente con un attuatore.

Eventuali funzionalità aggiuntive:{-}
----
Ripetere l'esercizio senza il potenziometro, utilizzando due pulsanti in modo da abbinare la breve pressione del pulsante 1 alla rotazione verso sinistra e la breve pressione del pulsante 2 alla rotazione verso destra, con lo spostamento minimo consentito dal servo.
Fare in modo che la pressione simultanea dei pulsanti riporti riporti il servo al centro.

Errori comuni:{-}
----
N/A

Possibile comportamento erratico:{-}
----
Rotazione inversa, in caso di inversione del collegamento del partitore fatto con il potenziometro.

Risultato di apprendimento:{-}
----
Utilizzare un servo e controllarlo con un potenziometro, associandone la rotazione alla corrispondente rotazione del servo.
Gestire tramite pochi pulsanti più comandi monitorando combinazioni di eventi.

Indicazioni per la soluzione:{-}
----
Costruire un partitore con il potenziometro.

Collegare i pulsanti come nell'esercizio sul contatore con incremento/decremento.

Collegare il GPIO scelto per il servo al relativo pin di input, e l'alimentazione alla sorgente (5v della porta USB o altro alimentatore).
