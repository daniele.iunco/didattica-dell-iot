<!--<metadata>
<title>Convertitore di testo in codice morse con output su display LCD e sonoro</title>
<tags>Medio,DigitalInput,DigitalOutput,Software</tags>
<prerequisites>digitalInput,digitalOutput,debouncing,i2c</prerequisites>
</metadata>-->
Convertitore di testo in codice morse con output su display LCD e sonoro
====

Prerequisiti:{-}
----
- I/O digitale
- Bus I2C o altro bus usato dal display
- Gestione dei rimbalzi
- I/O su porta seriale

Componenti:{-}
----
- Encoder rotativo
- Display LCD a due righe
- Pulsante, nel caso non sia integrato nell'encoder
- Buzzer attivo

Descrizione e risultato atteso:{-}
----
Definire un array di stringhe con i possibili valori tra cui l'utente dovrà scegliere, ciascuna stringa sarà una parola da convertire poi in codice morse, definire inoltre una variabile per l'indice dell'elemento dell'array attualmente selezionato, con valore iniziale 0, corrispondente al primo elemento dell'array.

In esecuzione mostrare sempre, sulla prima riga del display, la stringa all'indice selezionato, e sulla seconda riga del display la conversione della stringa in codice morse, utilizzando quando serve la funzione scorrimento testo del display per rientrare nello spazio a disposizione.
Consentire all'utente, ruotando l'encoder, di cambiare la selezione portandosi avanti o indietro nell'array di stringhe.

Rilevare l'evento di pressione del pulsante, e, quando si verifica, generare l'audio corrispondente alla conversione in codice morse della stringa all'indice selezionato.

Scopo dell'esercizio:{-}
----
L'utente, nell'interazione con sistemi embedded, può dover compiere scelte tra le possibili opzioni da visualizzare e confermare per procedere.
La dimensione ridotta dei display e l'assenza di tastiera e mouse richiede accorgimenti per rendere possibile questo tipo di interazione.

Eventuali funzionalità aggiuntive:{-}
----
N/A

Errori comuni:{-}
----
Possibile rilevamento della rotazione al contrario, se l'interfaccia dell'encoder permette di invertire la connessione.

Possibile comportamento erratico:{-}
----
N/A

Risultato di apprendimento:{-}
----
Utilizzare un encoder rotativo come interfaccia per scegliere tra le opzioni possibili.


Indicazioni per la soluzione:{-}
----
Collegare l'encoder rotativo secondo le indicazioni sul datasheet, e il display ai pin corretti per il suo bus (es. I2C).

Nel caso si abbia un encoder senza pulsante, collegare un pulsante tra massa e un pin GPIO, impostando quel GPIO in modalità INPUT_PULLUP.

Collegare il polo positivo del buzzer attivo a un GPIO in modalità OUTPUT e il negativo a massa, se il buzzer ha un consumo > di 20mA, servirà un resistore tra il GPIO e il positivo del buzzer, oppure si dovrà utilizzare un transistor BJT come driver.
In caso di modulo buzzer seguire le indicazioni sul modulo.
