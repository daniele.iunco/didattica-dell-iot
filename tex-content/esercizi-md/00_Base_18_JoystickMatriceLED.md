<!--<metadata>
<title>Grafica interattiva su matrice LED</title>
<tags>Medio,DigitalOutput,AnalogInput,Software,DigitalInput,Rimbalzi,Interrupt</tags>
<prerequisites>ohmlaw,analogInput,digitalInput,digitalOutput,pullUpDown,debouncing,interrupt</prerequisites>
</metadata>-->
Grafica interattiva su matrice LED
====

Prerequisiti:{-}
----
- Partitore di tensione
- Gestione dei rimbalzi
- Interrupt

Componenti:{-}
----
- Matrice LED 8x8 su modulo con bus I2C
- Joystick
- Registro a scorrimento e 8 resistori da 220 ohm, se non si dispone del modulo I2C

Descrizione e risultato atteso:{-}
----
Disegnare un punto posizionato al centro che si sposta in base allo spostamento del joystick, con uscita e rientro dal bordo (spazio toroidale).
La velocità deve essere proporzionale allo spostamento del joystick.
L'evento di pressione del joystick deve fermare il punto dove si trova e farlo lampeggiare.

Scopo dell'esercizio:{-}
----
L'input dal mondo fisico può essere un insieme di input proveniente da più sensori, e, nel caso specifico del joystick, della posizione sui due assi X e Y e dello stato premuto/non premuto.

Eventuali funzionalità aggiuntive:{-}
----
N/A

Errori comuni:{-}
----
N/A

Possibile comportamento erratico:{-}
----
Comportamento al contrario, se si collegano al contrario i pin dei potenziometri del joystick.


Risultato di apprendimento:{-}
----
Gestire una combinazione di eventi che rende complesso fare il polling di un pin di input, comprendere il vantaggio nell'uso degli interrupt.

Indicazioni per la soluzione:{-}
----
Se non si dispone del modulo indicato ma di una normale matrice LED, collegarla come nell'esercizio precedente sulla matrice LED, sarà necessario almeno un registro a scorrimento per gestire o le righe o le colonne.

Costruire con i potenziometri che fanno parte del joystick due partitori di tensione, uno per l'asse X e uno per l'asse Y, collegando le relative uscite a pin di input analogico.
Collegare il pulsante tra un GPIO e la massa, ed impostare nel codice tale GPIO in modalità INPUT_PULLUP.
