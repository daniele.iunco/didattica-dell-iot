<!--<metadata>
<title>Sistema di illuminazione automatico</title>
<tags>Avanzato,DigitalInput,DigitalOutput,AnalogInput,PWM,Software,Rimbalzi,PBL</tags>
<prerequisites>ohmlaw,analogInput,digitalInput,digitalOutput,pwm,debouncing,transistorswitch</prerequisites>
</metadata>-->
Sistema di illuminazione automatico
====

Prerequisiti:{-}
----
- Partitore di tensione
- Funzionamento della Fotoresistenza
- Funzionamento del sensore PIR
- PWM
- MOSFET o BJT
- Interrupt
- Problema dei rimbalzi
- Macchina a stati

Componenti:{-}
----
- sensore PIR
- fotoresistenza + resistore di valore adeguato a costruire il partitore
- pulsante
- MOSFET di tipo N
- resistore da 10 Ohm

Descrizione e risultato atteso:{-}
----
Costruire un oggetto per gestire l'illuminazione a LED di ingresso/scale/etc di un edificio, in modalità automatica al passaggio delle persone e anche su richiesta premendo un pulsante.  
Per garantire l'illuminazione necessaria mantenendo sotto controllo i consumi, il comportamento dell'oggetto sarà influenzato anche dal livello di luminosità dell'ambiente.

Stato dell'oggetto e modalità di funzionamento:{-}
----

Stato 1: Giornata soleggiata{-}
----
- la luce è normalmente spenta;
- in questo stato il sensore PIR non è utilizzato;
- l'utente, premendo il pulsante, accende la luce con un livello di luminosità del 50%;
- dopo l'accensione della luce, l'oggetto funziona da timer, attende che sia trascorso un tempo prefissato e spegne la luce;
- ogni pressione del pulsante resetta il timer.

Stato 2: Giornata nuvolosa{-}
----
- la luce è normalmente spenta;
- la luce si accende al passaggio dell'utente nell'area monitorata dal sensore PIR con un livello di luminosità del 50%;
- l'utente, premendo il pulsante, accende la luce con un livello di luminosità del 100%, l'impostazione è mantenuta fino allo spegnimento della luce;
- dopo l'accensione della luce, l'oggetto funziona da timer, attende che sia trascorso un tempo prefissato e spegne la luce;
- ogni pressione del pulsante o passaggio dell'utente nell'area monitorata resetta il timer.

Stato 3: Notte{-}
----
- la luce è normalmente accesa con un livello di luminosità del 10%;
- la luce si accende al passaggio dell'utente nell'area monitorata dal sensore PIR con un livello di luminosità del 100%;
- l'utente, premendo il pulsante, accende la luce con un livello di luminosità del 100%;
- dopo l'accensione della luce, l'oggetto funziona da timer, attende che sia trascorso un tempo prefissato e spegne la luce;
- ogni pressione del pulsante o passaggio dell'utente nell'area monitorata resetta il timer.

Scopo dell'esercizio:{-}
----
Gestire una macchina a stati in cui il passaggio di stato dipende dalla misura di una grandezza analogica.

Eventuali funzionalità aggiuntive:{-}
----
N/A

Errori comuni:{-}
----
N/A

Possibile comportamento erratico:{-}
----
Il sensore PIR appena acceso può rilevare falsi positivi per 30-60 secondi (in base al modello), quindi serve un delay iniziale.

Validità per PBL:{-}
----
Problema da definire e analizzare: Si
Necessaria individuazione nuovi obiettivi di apprendimento: Si
Dettagli obiettivo di apprendimento atteso:
Applicare il concetto di macchina a stati finiti ai sistemi embedded, in un sistema dove il cambio di stato non è gestibile con gli interrupt perché non è legato a un input digitale, ma dipende dalla variazione di una grandezza analogica, dove però la combinazione di eventi è la complessità può incentivare l'uso degli interrupt per l'interazione con l'utente.

Risultato di apprendimento:{-}
----
L'esercizio è progettato per mettere lo studente nelle condizioni di lavorare su una macchina a stati dove il cambio di stato non è gestibile con gli interrupt, quindi dove diventa necessario il polling del sensore analogico, tuttavia si devono rilevare altri eventi e per quelli si dovrebbero utilizzare gli interrupt.

Indicazioni per la soluzione:{-}
----
Costruire un partitore di tensione con la fotoresistenza e collegare l'output PWM per pilotare la luce LED come nei relativi esercizi.
Collegare il pulsante tra un GPIO e massa, utilizzando il pin in modalità INPUT_PULLUP, collegare la linea dati del sensore PIR a un GPIO in modalità INPUT. 
