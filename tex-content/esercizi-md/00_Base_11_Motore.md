<!--<metadata>
<title>Uso del motore</title>
<tags>Base,DigitalOutput,Dimensionamento elettrico</tags>
<prerequisites>digitalOutput,multipsu,psudimensioning</prerequisites>
</metadata>-->
Uso del motore
====

Prerequisiti:{-}
----
- Dimensionamento elettrico
- Circuiti con più alimentazioni

Componenti:{-}
----
- motore
- relè (es. KY-019)
- pile/batterie adeguate al motore da alimentare

Descrizione e risultato atteso:{-}
----
Controllare via software l'accensione e lo spegnimento di un motore, ad esempio costruendo un blink che accende e spegne il motore al posto del LED integrato.

Dato che il motore non si può alimentare con i GPIO perché il consumo è eccessivo (>> 20 mA) si deve alimentare con delle pile/batterie con la tensione necessaria e servirà un relè come interruttore.

Scopo dell'esercizio:{-}
----
I GPIO possono erogare solo una corrente limitata, di norma intorno ai 20 mA o poco più, quindi per carichi elevati si devono utilizzare attuatori pilotati con un input digitale e un'alimentazione apposita.

Eventuali funzionalità aggiuntive:{-}
----
N/A

Errori comuni:{-}
----
Errori di connessione

Possibile comportamento erratico:{-}
----
Inversione accensione e spegnimento, in caso il modulo relè abbia uno switch per invertire gli effetti dell'input HIGH/LOW

Risultato di apprendimento:{-}
----
Utilizzo del relè per controllare un carico con propria alimentazione.

Indicazioni per la soluzione:{-}
----
Collegare motore e relè come indicato nello schema, fare attenzione a scegliere un pacco batterie adeguato alla tensione richiesta dal motore e al suo consumo, verificandone prima le caratteristiche.

![Esempio](./images/arduino-motor-relay_bb.png)
