<!--<metadata>
<title>Regolatore di velocità per ventole</title>
<tags>Avanzato,DigitalInput,DigitalOutput,PWM,Software,Interrupt,Rimbalzi</tags>
<prerequisites>digitalInput,debouncing,pullUpDown,interrupt,pwm,multipsu,eeprom</prerequisites>
</metadata>-->
Regolatore di velocità per ventole
====

Prerequisiti:{-}
----
- Resistore di pullup/pulldown
- Interrupt
- Problema dei rimbalzi
- Circuiti con più alimentazioni
- PWM
- Uso della memoria EEPROM

Componenti:{-}
----
- Ventola per PC a 4 pin (non si possono usare quelle a 3 pin)
- display LCD
- LED e resistore da 1 kOhm

Descrizione e risultato atteso:{-}
----
L'oggetto deve individuare la velocità massima della ventola, il duty cycle minimo a cui può funzionare e quindi la velocità minima, e permettere poi all'utente di variare la velocità tra il minimo al massimo, in RPM, con due pulsanti +/-.

Mantenere premuto un pulsante dovrà inoltre ripetere 'incremento/decremento sempre più velocemente.

Visualizzare sul display LCD:
- la velocità richiesta dall'utente, modificabile con i pulsanti +/-;
- la velocità attuale effettiva in RPM;
- l'intervallo di velocità a cui può funzionare ventola.

Scopo dell'esercizio:{-}
----
Utilizzare un sistema di controllo PID per far funzionare la ventola alla velocità impostata dall'utente.
Le ventole a 4 pin per PC hanno un'interfaccia che consente di simulare questo tipo di controllo senza componenti aggiuntivi, lasciando allo studente la possibilità di concentrarsi sull'interazione tramite software.

Eventuali funzionalità aggiuntive:{-}
----
Aggiungere una funzionalità di rilevamento del funzionamento non ottimale della ventola, per individuare se la ventola non raggiunge più la velocità massima attesa.
Per fare questo memorizzare il valore di velocità massima rilevato nella memoria EEPROM, una sola volta facendo attenzione a non riscriverlo ripetutamente, e avvertire, facendo lampeggiare un LED, se la velocità massima raggiunta scende sotto una soglia predeterminata, ad esempio sotto il 90%.
Consentire di resettare la velocità massima misurata, rilevando la pressione di entrambi i pulsanti in fase di setup come comando scelto a tal scopo.

Errori comuni:{-}
----
N/A

Possibile comportamento erratico:{-}
----
N/A

Validità per PBL:{-}
----
Problema da definire e analizzare: Si
Necessaria individuazione nuovi obiettivi di apprendimento: Si
Dettagli obiettivo di apprendimento atteso:
Studiare il funzionamento delle ventole a 4 pin per PC ( https://noctua.at/pub/media/wysiwyg/Noctua_PWM_specifications_white_paper.pdf )

Con Arduino la frequenza del segnale PWM generato normalmente non rientra nelle specifiche delle ventole a 4 pin, la soluzione sarà agire sui registri per poter generare un segnale che rientra nei parametri (tra 21 kHz e 28 kHz).

Con ESP8266 esiste l'istruzione analogWriteFreq(frequenzaHz) per modificare la frequenza;

In aggiunta, far lampeggiare il LED di allarme mentre si eseguono anche altre operazioni non permetterà di usare il comando delay() e si dovrà gestire diversamente la cosa.

Risultato di apprendimento:{-}
----
Generare un segnale PWM con una frequenza diversa da quella predefinita prevista dal microcontrollore.

Indicazioni per la soluzione:{-}
----
Possibile soluzione per individuare la velocità minima e massima della ventola:
In fase di setup() avviare la ventola al 100% e calcolare la velocità, ridurre poi il duty cycle al 33% e proseguire con decrementi di 1 per determinare il duty cycle minimo necessario affinché la ventola continui a ruotare e la velocità minima ottenuta, riportare poi la ventola al 100%.
