<!--<metadata>
<title>Ohmmetro migliorato</title>
<tags>Base,AnalogInput,DigitalOutput,Software,PBL</tags>
<prerequisites>ohmlaw,analogInput,digitalOutput</prerequisites>
</metadata>-->
Ohmmetro migliorato
====

Prerequisiti:{-}
----
- legge di Ohm e partitore di tensione
- funzionamento dell'ADC e istruzione analogRead

Componenti:{-}
----
- Resistori noti di valore diverso, es 1k, 4.7k, 10k

Descrizione e risultato atteso:{-}
----
Modificare l'esercizio sull'Ohmmetro affinché possa misurare valori di resistenza anche molto diversi tra loro con un errore accettabile (<= 10%).

Scopo dell'esercizio:{-}
----
Trovare il modo ottimale per utilizzare più resistori tra cui costruire il partitore, utilizzando i pin GPIO per accenderli e spegnerli quando serve.

Eventuali funzionalità aggiuntive:{-}
----
N/A

Errori comuni:{-}
----
N/A

Possibile comportamento erratico:{-}
----
Intuitivamente si potrebbe pensare di accendere e spegnere i resistori con stato HIGH/LOW, ma lo stato LOW non è adatto allo scopo perché con più resistori tra input analogico e GPIO LOW questi andrebbero in parallelo alterando la misura.

Validità per PBL:{-}
----
Problema da definire e analizzare: Si
Necessaria individuazione nuovi obiettivi di apprendimento: Si
Dettagli obiettivo di apprendimento atteso:
Rilevare che non è possibile costruire un partiore con più possibili resistori tra cui scegliere, disattivando quelli inutilizzati con lo stato LOW del pin GPIO, perché a quel punto si creerebbe un parallelo di resistori con quello incognito che altererebbe la misurazione, serve uno stato in cui i pin GPIO dei resistori disattivati non conducono, si può utilizzare quanto appreso dall'esercizio sull'accensione di due LED con un GPIO, quindi utilizzare lo stato INPUT che ha alta impedenza, ed è adatto allo scopo.

Risultato di apprendimento:{-}
----
Utilizzare il partitore di tensione per ottenere il valore di un resistore incognito, sfruttando lo stato di INPUT dei GPIO per consentire di scegliere tra più resistori quale utilizzare.

Indicazioni per la soluzione:{-}
----
Costruire il partitore, sostituendo il resistore noto che si trovava tra il pin di input analogico e la massa con vari resistori, ciascuno connesso tra il pin di input analogico e un diverso GPIO, i GPIO scelti serviranno poi da interruttore per selezionare il resistore da usare.

![Esempio](./images/multi-ohmmetro_bb.png)

-
![Esempio](./images/multi-ohmmetro_schem.png)
