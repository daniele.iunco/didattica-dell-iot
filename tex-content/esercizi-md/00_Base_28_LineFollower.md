<!--<metadata>
<title>Line follower</title>
<tags>Avanzato,DigitalInput,DigitalOutput,Software,Dimensionamento elettrico</tags>
<prerequisites>digitalInput,digitalOutput,multipsu,psudimensioning</prerequisites>
</metadata>-->
Line follower
====

Prerequisiti:{-}
----
- Dimensionamento elettrico
- Circuiti con più alimentazioni

Componenti:{-}
----
- 2 rilevatori di linee/bordi a infrarossi
- 1 motore e 1 servo, oppure 2 motori
- Relè, uno per motore
- Modulo con integrato L293D o equivalente
- Sensore di distanza ad ultrasuoni

Descrizione e risultato atteso:{-}
----
Costruire una macchina che deve seguire una linea, utilizzando per lo sterzo 2 motori oppure un motore e un servo, in base al materiale disponibile nel kit.

Scopo dell'esercizio:{-}
----
Nell'interazione con il mondo fisico può essere necessario correggere il comportamento di un oggetto in base a fattori esterni non predeterminati.

Eventuali funzionalità aggiuntive:{-}
----
Utilizzare un modulo specifico per il controllo del motore, ad esempio un modulo con l'integrato L293D, per poter regolare la velocità di rotazione del motore.

Aggiungere un sensore di distanza ad ultrasuoni, per rilevare la presenza di un ostacolo di fronte e rallentare proporzionalmente alla distanza fino a fermarsi per evitare la collisione.

Errori comuni:{-}
----
Una velocità eccessiva o il posizionamento errato dei rilevatori di linea può far andare la macchina fuori dal percorso.

Possibile comportamento erratico:{-}
----
L'eventuale messa in attesa con delay(), se serve ma si gestisce male, può far perdere l'evento di uscita dalla linea per troppo tempo facendo finire la macchina fuori strada.

Risultato di apprendimento:{-}
----
Gestire azione e reazione per correggere un errore o impedire un evento non voluto, nel caso specifico seguire un percorso con delle curve correggendo la traiettoria della macchina ed evitare un possibile urto (espansione).

Indicazioni per la soluzione:{-}
----
Collegare i sensori di linea ai GPIO scelti secondo le indicazioni sul modulo.
Fare lo stesso con il sensore di distanza ad ultrasuoni.

Per l'alimentazione dei motori utilizzare delle batterie e un relè per controllare l'accensione, come nel relativo esercizio, oppure il modulo specifico, da collegare seguendo le indicazioni.

Servono due rilevatori di linee posizionati con una certa distanza, così che in caso di curva sia possibile individuare quale tra i due sensori rileva la linea per sterzare nella direzione giusta.
