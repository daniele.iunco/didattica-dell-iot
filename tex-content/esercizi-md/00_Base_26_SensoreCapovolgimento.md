<!--<metadata>
<title>Rilevatore di corriere distratto</title>
<tags>Avanzato,DigitalInput,DigitalOutput,Software,EEPROM</tags>
<prerequisites>digitalInput,digitalOutput,eeprom</prerequisites>
</metadata>-->
Rilevatore di corriere distratto
====

Prerequisiti:{-}
----
- Uso memoria EEPROM
- Macchina a stati

Componenti:{-}
----
- Sensore di capovolgimento
- Pulsante o ponticello

Descrizione e risultato atteso:{-}
----
Costruire un oggetto che messo in un pacchetto non deve essere capovolto.
L'oggetto dovrà rilevare il capovolgimento e, al verificarsi dell'evento far lampeggiare un LED.

Salvare l'informazione sul capovolgimento nella memoria EEPROM integrata, per non perderla in caso di spegnimento, ricaricandola in fase di setup.
Per resettare l'oggetto per il successivo trasporto prevedere l'uso di un pulsante che, se tenuto premuto in fase di setup, dovrà far cancellare l'informazione dalla memoria EEPROM; come alternativa si può usare un ponticello.

Scopo dell'esercizio:{-}
----
Comprendere come salvare dei dati, affinché non siano persi in assenza di alimentazione.

Eventuali funzionalità aggiuntive:{-}
----
N/A

Errori comuni:{-}
----
Esaurimento rapido scritture su EEPROM in caso di errore software, fare attenzione.

Possibile comportamento erratico:{-}
----
N/A

Risultato di apprendimento:{-}
----
In caso di salvataggio dello stato dell'oggetto su memoria non volatile, saper prevedere una modalità per resettare lo stato dell'oggetto affinché si possa riutilizzare senza doverlo azzerare con l'apposito programmatore.

Indicazioni per la soluzione:{-}
----
Il sensore di capovolgimento funziona come un pulsante, scegliere un GPIO, da utilizzare in modalità INPUT_PULLUP, collegare il sensore tra il GPIO scelto e la massa.

N.B. Il numero di scritture possibili sulla memoria EEPROM è limitato, fare attenzione a scriverci solo quando serve; un errore come riscrivere di continuo lo stesso valore renderebbe inutilizzabile quella memoria in poco tempo.
