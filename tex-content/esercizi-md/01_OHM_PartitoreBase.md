<!--<metadata>
<title>Ohmmetro</title>
<tags>Base,AnalogInput</tags>
<prerequisites>ohmlaw,analogInput</prerequisites>
</metadata>-->
Ohmmetro
====

Prerequisiti:{-}
----
- legge di Ohm e partitore di tensione
- funzionamento dell'ADC e istruzione analogRead

Componenti:{-}
----
- R1 - 1 resistore da 1kOhm, il resistore di valore noto
- R2 - resistore di altro valore da misurare

Descrizione e risultato atteso:{-}
----
Misurare la resistenza del resistore incognito, disponendo di un altro resistore di valore noto.

Scopo dell'esercizio:{-}
----
Comprendere come si applica la legge di Ohm con un esempio d'uso del partitore di tensione.

Eventuali funzionalità aggiuntive:{-}
----
N/A

Errori comuni:{-}
----
Scambiare resistore noto e ignoto senza modificare in modo opportuno la formula farà calcolare in modo errato il valore di resistenza.

Possibile comportamento erratico:{-}
----
N/A

Risultato di apprendimento:{-}
----
Utilizzare il partitore di tensione per ottenere il valore di un resistore incognito.

Indicazioni per la soluzione:{-}
----
Costruire un partitore di tensione, con il resistore noto tra Vcc e il pin di input analogico, ed il resistore incognito tra il pin di input analogico e la massa.

![Esempio](./images/01-partitore01_bb.png)

-
![Esempio](./images/01-partitore01_schem.png)
