Prima parte: introduzione e quadro teorico
==========================================

- Relazione sul metodo di insegnamento classico, dove il docente è fonte della conoscenza e lo studente la acquisisce.
~Work in progress~

- Valutazione di un diverso metodo di insegnamento, dove si punta ad insegnare allo studente le basi da cui partire ed in seguito incentivarlo ad apprendere facendo lui stesso qualcosa, come attività singola, o anche di gruppo per creare un modello collaborativo.
~Work in progress~

Costruzionismo (riferimenti a quanto teorizzato da Papert nel suo libro Constructionism) per rendere l'apprendimento più efficiente costruendo oggetti.
~Work in progress~

Partendo dal costruzionismo arriviamo a due pattern per l'apprendimento:
- apprendimento basato sulla realizzazione di progetti (Project based learning)
- apprendimento basato sulla risoluzione di problemi (Problem based learning)
~Work in progress~


Relazione su corsi esistenti
----------------------------
Internet of Things (IoT) - Berkeley University (2019)
https://people.eecs.berkeley.edu/~boser/courses/49_sp_2019/index.html

EMBEDDED SYSTEMS 1 - Politecnico di Milano (2018)
https://www4.ceda.polimi.it/manifesti/manifesti/controller/ManifestoPublic.do?EVN_DETTAGLIO_RIGA_MANIFESTO=evento&aa=2018&k_cf=225&k_corso_la=481&k_indir=T2A&codDescr=095905&lang=IT&semestre=1&idGruppo=3756&idRiga=227400

Embedded Systems - Berkeley University (2017)
https://bcourses.berkeley.edu/courses/1464526

Embedded System Design Laboratory - Stanford University (2002)
https://web.stanford.edu/class/ee281/

-> Introduzione e capitolo 1 su "Didattica-dell-iot.tex"







Revisione concetti di base di elettronica, dal libro SISTEMI EMBEDDED: TEORIA E PRATICA
=======================================================================================

Argomenti non trattati di elettronica
-------------------------------------
- componenti: riferimenti di tensione e note sull'uso di Arduino con un riferimento di tensione esterno
- componenti: fotoaccoppiatori
- uso di diodi di bloccaggio (clamping) in caso di input da oggetti con alimentazione indipendente, possibile soluzione alternativa con mosfet
- interfacciamento a oggetti che richiedono più corrente di quella che può fornire il microcontrollore, esempi di switch e driver con BJT NPN, PNP e FET -> Pattern "Consapevolezza della potenza"

Oggetti alimentati a batteria
-----------------------------
Introduzione: Problema dell'autonomia se si alimenta l'oggetto con una batteria (qualche esempio, anche il caso della ricarica con pannello fotovoltaico)
Soluzione: Progettare l'hardware in modo opportuno (breve introduzione) e il software tenendo conto delle modalità di risparmio energetico del microcontrollore (idle / power down / power save / standby / extended standby), con esempi di codice


[Input dal mondo fisico] (./files/input-dal-mondo-fisico-sensori-analogici.md)
----------------------
- sensori analogici attivi, variazione della tensione sull'uscita
- sensori analogici passivi, variazione della resistenza
- pulsanti, problema dei rimbalzi e soluzioni software e hardware
- esercizi sull'interfacciamento


Input digitale e debouncing
---------------------------
Analisi delle soluzioni e relativa implementazione:
- Hardware (rete RC + Trigger di Schmitt)
  - Pro: non occupa il processore
  - Contro: prerequisiti di elettronica, richiede componenti aggiuntivi
- Software
  - Pro: non ci sono prerequisiti di elettronica, non servono componenti aggiuntivi
  - Contro: uso del processore



Attuatori controllati in PWM
----------------------------
- servo


Panoramica su sensori, attuatori e altri oggetti interessanti in commercio
==========================================================================
[oggetti_interessanti_in_commercio.md](./files/oggetti_interessanti_in_commercio.md)


Prerequisiti e conoscenze necessarie
====================================

Fisica
------
[Misura di grandezze fisiche](./files/misura_di_grandezze_fisiche.md)

Elettronica
-----------
[Approfondimento argomenti di elettronica](./files/elettronica.md)


Pattern trovati e approfondimenti
=================================
Pattern "Consapevolezza della potenza"
[Variante del pattern MVC per controllo closed loop](./files/pattern_closed_loop_corrispondenza_mvc.md)


Imparare dagli errori
=====================
Esercizi che, per un errore di progettazione dovuto a limitazioni o caratteristiche specifiche dei componenti utilizzati, non hanno l'esito previsto, e soluzione.

[Interfacciamento a sensori analogici e valori restituiti da analogRead](./files/imparare-dagli-errori-analogread.md)


Possibili progetti
==================

Con Arduino o ESP32
-------------------
- termometro/igrometro con sensore wireless da installare all'esterno (comunicazione con NRF24)
- telecomando programmabile, per copiarne uno esistente da emulare
- cercametalli

Con ESP32
---------
- telecomando WiFi, per controllare un dispositivo da remoto tramite un'interfaccia web, emulando il telecomando IR originale




Terza parte - Corso di Sistemi Embedded e Internet of Things
==============================================================
Lo studio dei sistemi embedded richiede conoscenze di più materie, oltre a programmazione ed architettura degli elaboratori e reti servono fisica ed elettronica, dato che nel corso di laurea in informatica non ci sono corsi di elettronica, questa sarà una parte importante del programma del corso di sistemi embedded.

1. Hardware
Nell'Internet of Things l'hardware ha un ruolo chiave che lo differenzia dalla semplice programmazione, tutto l'interfacciamento è possibile grazie a sensori, attuatori da collegare a un microcontrollore.
Sotto questo aspetto gli studenti devono avere le basi di elettronica necessarie per capire come procedere trovandosi di fronte a componenti di vario tipo, sia di tipo analogico sia di tipo digitale, dovranno conoscere i componenti hardware più utilizzati e le tecniche di interfacciamento.
Il corso quindi sarà orientato al physical computing.

2. Middleware
Pur non avendo un sistema operativo, possiamo riapplicare il concetto di middleware anche ai sistemi embedded, avendo il software un ruolo chiave per interfacciarsi a componenti hardware diversi con le proprie librerie per l'interfacciamento, lo scopo del middleware quindi è permettere al microcontrollore di comunicare con sensori ed attuatori.
Gli studenti devono comprendere le differenze rispetto a ciò su cui sono abituati a programmare, computer con veri sistemi operativi, nel caso dei sistemi embedded infatti lavoreranno su oggetti che hanno risorse limitate e svolgono compiti specifici per interfacciarsi al mondo reale.

3. Comunicazioni
Gli oggetti sono spesso parte di un sistema più grande e le tecniche di comunicazione hanno un ruolo chiave.

4. Sistemi distribuiti e cloud computing
L'esistenza di più oggetti che svolgono compiti specifici come parte di un sistema più complesso porta allo studio dei sistemi distribuiti e all'utilizzo del cloud computing, i dati possono essere raccolti ed elaborati da un sistema centrale.


Intendo partire dall'hardware, affiancando alle lezioni di teoria esercizi e piccoli progetti che possano mettere gli studenti nelle condizioni di provare cose effettivamente utilizzabili con esempi di dispositivi che già ci circondano, mettendo in evidenza concetti chiave che è necessario conoscere nella progettazione di sistemi embedded.

1 - Hardware
------------
- [Diagramma di flusso passaggi interfacciamento sensori](./files/diagramma-connessione-sensore.png)
- [Teoria ed esercizio sulla fotoresistenza](./files/sensori-resistivi-fotoresistenza.md)
- [PWM, per controllare la luminosità di un singolo led o più led, problema del carico da controllare e soluzione con l'uso del transistor](./files/pwm-carichi-elevati.md)
- [Controllo closed loop, combinazione dei punti 1 e 2 per risolvere un problema pratico](./files/controllo-closed-loop.md)
- [Condizionamento del segnale proveniente da sensori analogici, amplificatore operazionale ed esempio d'uso](./files/condizionamento-segnale-analogico.md)
- [Utilizzo di sensori capacitivi, misurazione della capacità](./files/sensori-capacitivi.md)
- [Approfondimento su hardware proprietario, brevetti, hardware libero](./files/brevetti_e_hardware_libero.md)
.....
- Utilizzo di sensori in cui l'output è una corrente
- Teoria: problema del ground loop e soluzione con connessione a stella
- Interfacciamento tra oggetti con livelli di tensione differenti (es. 3.3v e 5v)
- Alimentazione di oggetti a batteria e problema del consumo energetico

2 - Middleware
--------------
- Teoria: confronto di ciò che intendiamo come periferiche e driver in un normale computer con l'equivalente nei sistemi embedded.

3 - Comunicazioni
-----------------
- Teoria: scelta del mezzo di comunicazione in base alle caratteristiche e vincoli ambientali e requisiti del progetto (fonte di alimentazione e consumi massimi consentiti, distanza, resistenza alle interferenze, sicurezza della trasmissione).

4 - Sistemi distribuiti e progetto finale
-----------------------------------------
Per quanto riguarda il punto 4, progetti specifici sono quelli del monitoraggio ambientale, dove grazie a numerose stazioni di monitoraggio si raccolgono dati dell'area monitorata e si elaborano; questo tipo di progetto coprirebbe molti punti trattati in precedenza, però con costi di realizzazione elevati dovuti in buona parte al costo dei sensori necessari.
Per trattare l'aspetto teorico e dare un'idea pratica del funzionamento, ho pensato al monitoraggio delle reti WiFi, costruendo un oggetto con la scheda di rete WiFi e un sensore GPS, che periodicamente scansiona le reti attive nell'area ed invia i dati raccolti a un server centrale.
Questo progetto, se realizzato da più studenti, può permettere di rileavare la concentrazione di reti wifi su un'area più ampia o di fare qualche analisi statistica sulla presenza di reti protette o non protette, reti wifi condivise per gli utenti dei vari operatori di telefonia, etc.



Quarta parte - Framework per l'insegnamento
===========================================
Con questo framework intendo creare un metodo per individuare le conoscenze essenziali e dividerle tra più categorie e in base al livello di difficoltà per poi selezionare gli argomenti da trattare a lezione.

Gli argomenti, catalogati per livello di difficoltà (4), sono divisi in 4 categorie:
1. Elettronica: l'elettronica è alla base del corso essendo necessario lavorare sull'hardware
2. Microcontrollori: l'uso di microcontrollori, alla base dei sistemi embedded, richiede competenze specifiche a causa delle differenze rispetto alle CPU e dei computer su cui lavoriamo
3. I/O tra oggetti o con il mondo fisico: Un aspetto chiave dei sistemi embedded è l'interazione con il mondo fisico e anche con altri oggetti, quindi vanno trattati in dettaglio i componenti e le tecnologie utilizzate per fare questo
4. Utilizzo degli oggetti: Questa categoria riguarda esercizi e progetti da svolgere dopo aver completato lo studio della teoria.

L'idea è disporre gli argomenti su una griglia che consente di individuare il punto di partenza e di arrivo, l'organizzazione degli argomenti nella griglia dipende da una precisa relazione:
Elettronica -> Microcontrollori -> I/O con il mondo fisico -> Realizzazione pratica

Per realizzare un particolare progetto (Cat.4) si deve interagire in qualche modo con il mondo fisico (Cat.3), per fare questo servono funzionalità specifiche dei microcontrollori e componenti appositi (Cat.2), per utilizzare questi componenti servono conoscenze di elettronica (Cat.1).

Lo studio dei sistemi embedded però è complesso e comprende molti argomenti, non è possibile trattare ogni argomento nel corso, ho quindi scelto quello che a seguito delle mie ricerche ho ritenuto essenziale, ed esercizi e progetti che riguardano applicazioni pratiche di quanto studiato.

Gli argomenti sono organizzati in modo da disporli con livelli di difficoltà crescenti, e prerequisiti che dipendono dal livello di difficoltà raggiunto e dai precedenti, sostanzialmente il corso può essere svolto seguendo una diagonale dal punto 1,1 al punto 4,4.

Per aggiungere degli argomenti, ed individuare a che punto del corso inserirli, sarà necessario indiviuare i prerequisiti il relativo livello di difficoltà.

Lo stesso schema è utile per catalogare gli esercizi, e, raccogliendo esercizi specifici per categoria e livello di difficoltà, si possono pianificare lezioni di laboratorio da svolgere dopo le lezioni di teoria.

[Tabella argomenti e livelli di difficoltà](./files/Tabella-argomenti-e-livelli-difficolta.pdf)




Bibliografia
============
01. [PDF] What Is Embedded Systems and How Should It Be Taught? Results from a Didactic Analysis
http://doi.acm.org/10.1145/1086519.1086528

02. [PDF] Building a Framework for the Senior Capstone Experience in an Information Computer Technology Program
https://dl.acm.org/doi/pdf/10.1145/1631728.1631804

03. [PDF] Nexos: A Next Generation Embedded Systems Laboratory
http://doi.acm.org/10.1145/1534480.1534487

04. [PDF] Internet of Things (IoT): A vision, architectural elements, and security issues
https://www.researchgate.net/publication/320250009_Internet_of_Things_IoT_A_vision_architectural_elements_and_security_issues

05. [PDF] Implementation and Evaluation of Flipped Classroom as IoT Element into Learning Process of Computer Network Education
https://www.researchgate.net/publication/324141978_Implementation_and_Evaluation_of_Flipped_Classroom_as_IoT_Element_into_Learning_Process_of_Computer_Network_Education

06. [PDF] Information and Communication Technology inside the black box
https://books.google.it/books?id=U9R-rO-x0nEC&printsec=frontcover&hl=it&sa=X&ved=2ahUKEwij8uCRpKnqAhWO4YUKHad9AlIQ6AEwBXoECAUQAg#v=onepage&q&f=false

07. [PDF] Climbing to understanding: lessons from an experimental learning environment for adjudicated youth
https://www.semanticscholar.org/paper/Climbing-to-understanding%3A-lessons-from-an-learning-Cavallo-Papert/051330054e07452c9105c943c30ef9c9abceb62f

08. Situating Constructionism , By Seymour Papert and Idit Harel
https://web.archive.org/web/20160304015549/http://www.papert.org/articles/SituatingConstructionism.html

09. Open-Source Lab: How to Build Your Own Hardware and Reduce Research Costs by Joshua M. Pearce

10. Sistemi Embedded: Teoria e pratica di Alexjan Carraturo, Andrea Trentini

11. [PDF] Blending Problem- and Project-Based Learning in Internet of Things Education: Case Greenhouse Maintenance
https://www.researchgate.net/publication/279533985_Blending_Problem-_and_Project-Based_Learning_in_Internet_of_Things_Education_Case_Greenhouse_Maintenance
