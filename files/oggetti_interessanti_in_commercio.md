Panoramica su sensori, attuatori e altri oggetti interessanti in commercio
==========================================================================

Oggetti interessanti pronti all'uso
-----------------------------------
- Sensori di temperatura e umidità
- Relè reed
- Kit Ricevitore IR (VS1838B) + Telecomando
- Microfono, modulo con amplificatore integrato
- Joystick
- Rilevatore di movimento con sensore PIR
- Rilevatore di ostacoli a infrarossi
- Termometro (es. DHT11)
- Rilevatore di urti
- Sensore di inclinazione
- Lettore RFID (versioni 13.56Mhz e 125khz)
- Moduli rilevatori di gas o altre sostanze nocive
- Led RGB, modulo con le resistenze
- Buzzer passivo, modulo con transistor
- Display LCD con interfaccia SPI
- Relè su modulo con transistor o fotoaccoppiatore
- Servo
- Display LCD, anche con modulo per comunicazione su I2C
- Modulo RTC, con chip DS1302(SPI) o DS1307(I2C)
- Modulo per schede micro SD
- Modulo NRF24

Componenti vari
---------------
- Fotoresistenza
- Termistore
- Sensore di temperatura con output analogico (es. LM35)
- Diodo usato come sensore di temperatura, solo a scopo didattico
- Sensore effetto Hall
- Ricevitore IR o in alternativa fotodiodo
- Diodo IR
- Diodo Led (classico o RGB)
- Buzzer attivo e passivo
- Relè
- Ventola

[Indietro](../schema.md)
