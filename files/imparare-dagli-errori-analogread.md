Interfacciamento a sensori analogici e valori restituiti da analogRead
======================================================================

Tensione di riferimento Aref
----------------------------
Con l'impostazione predefinita il microcontrollore utilizzerà come riferimento di tensione analogico (Aref) la tensione di alimentazione, però questo non è un riferimento di tensione preciso.

Se l'alimentazione arriva da una porta USB avrò, oltre alla tolleranza del 5% della porta USB, la caduta di tensione sul diodo tra i 5V USB e i 5V del microcontrollore e l'eventuale caduta di tensione dovuta al cavo USB che dipende dalla lunghezza del cavo stesso.

Prendere il valore restituto da analogRead considerando Aref 5V esatti può portare ad errori nelle misurazioni.

Esercizio:
Utilizzando un sensore di temperatura analogico LM35, rilevare la temperatura, inviarla al computer via porta seriale o visualizzarla su un display, accendere poi una ventola utilizzando come driver un transistor oppure con un relè e l'alimentazione della porta USB.

Risultato errato:
(La tensione di alimentazione, quindi anche Aref, che dovrebbe essere 5V potrebbe scendere fino a 4.7V)

Considerando nei calcoli come Aref 5V, il valore di temperatura sarà errato e, con le tensioni previste, più alto di ~2 gradi, inoltre, con un cavo USB lungo (o con una prolunga), l'accensione della ventola causerà una ulteriore caduta di tensione e di conseguenza il rilevamento di una temperatura ancora più alta.

Soluzione:
Utilizzare il riferimento di tensione interno (es. 1.1V su Arduino), la temperatura rilevata sarà corretta e la misurazione sarà anche più precisa.

[Indietro](../schema.md)
