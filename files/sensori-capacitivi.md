Utilizzo di sensori capacitivi, misurazione della capacità
==========================================================
Un caso particolare di sensori analogici, i sensori capacitivi.

Oltre agli input digitali, ciò che possiamo misurare con il microcontrollore sono tensioni, e di conseguenza resistenze e correnti ricorrendo alla legge di Ohm e ai componenti appropriati per ottenere un valore di tensione che dipende dalla resistenza o dalla corrente.

Tuttavia esistono sensori il cui funzionamento dipende da altre caratteristiche come i sensori capacitivi, ad esempio un sensore di umidità può essere capacitivo quindi comportarsi come un condensatore, un condensatore particolare costruito con un materiale dielettrico che varia le sue caratteristiche al variare dell'umidità.

Per ciò che non può essere trasformato in una tensione, si possono cercare componenti specifici per effettuare l'interfacciamento, o sensori pronti all'uso con tutta l'elettronica necessaria per mettere a disposizione un output digitale.

Un metodo semplice per misurare un la capacità di un condensatore, che allo stesso tempo è un buon esempio dell'utilizzo di un componente esterno per trasformare un segnale da misurare, è utilizzare il condensatore in un generatore di onda quadra; l'obiettivo sarà ottenere un segnale ad onda quadra con duty cycle o frequenza che dipende dalla capacità, questo segnale può essere letto da qualunque microcontrollore essendo sufficiente un ingresso digitale.

In commercio è disponibile un componente che ci permette di fare questa operazione, il circuito integrato NE555, che possiamo collegare in configurazione astabile come indicato nel datasheet.
[ne555_astabile](./schema_ne555_astabile.png)

A questo punto sarà sufficiente rilevare i fronti di salita e discesa del segnale e fare i calcoli opportuni.

Il circuito è di facile realizzazione ed economico, inoltre utilizzando questo componente si ha allo stesso tempo anche un esempio di come utilizzare gli interrupt per individuare i fronti di salita/discesa di un segnale ad onda quadra.

TODO: Allegare schema del circuito, valori dei componenti, codice dello sketch.

TODO(2): Esercizio, l'integrato NE555 può funzionare anche in configurazione monostabile, in presenza di un inpulso che porta a 0 il pin trigger, l'uscita passa a livello alto per un tempo che dipende dai valori di C, R1, R2.
Come si potrebbe utilizzare questa configurazione per misurare la capacità?

Soluzione: Si collega l'ingresso TRIGGER del NE555 a un'uscita digitale del microcontrollore con valore iniziale HIGH, poi si invia un inpulso impostando quel pin a LOW e si calcola il tempo in cui l'uscita del NE555 rimane alta.

[Indietro](../schema.md)
