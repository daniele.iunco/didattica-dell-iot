Il condizionamento del segnale
==============================
Un sensore analogico può avere in uscita una tensione il cui valore dipende dalla grandezza fisica rilevata, questa tensione però potrebbe essere troppo bassa per poterla misurare con precisone utilizzando l'ADC del microcontrollore.

Quando ci si trova con una tensione che non rientra nel range dell'input analogico del microcontrollore, è necessario procedere al condizionamento del segnale, condizionare il segnale significa utilizzare un circuito che trasforma il segnale disponibile in un segnale con caratteristiche adatte all'ingresso del dispositivo che lo dovrà misurare.

Il caso che ritengo utile trattare è quello in cui si deve misurare una tensione molto bassa (nell'ordine delle decine/centinaia di milliVolt) e la risoluzione dell'ADC non permette di ottenere la precisione voluta.

In una situazione di questo tipo si può risolvere il problema con un componente che si utilizza in molti circuiti, l'amplificatore operazionale.

L'amplificatore operazionale è un componente che consente di effettuare operazioni matematiche, in questo esempio lo utilizzerò nella configurazione non invertente [Schema](./schema_op_amp_non_invertente.png), la configurazione non invertente serve ad ottenere in uscita (Vout) un'amplificazione della tensione in ingresso (Vin), pari a:

Vout = Guadagno * Vin, con Guadagno = 1 + R2/R1

Potrò collegare poi l'uscita dell'amplificatore operazionale all'ingresso analogico del microcontrollore, in modo da misurare il segnale amplificato.

Per provare quanto descritto avrò bisogno di una tensione di poche decine/centinaia di milliVolt da amplificare, per ottenerla costruire un primo partitore (es. con resistenze 1k e 10k, per ottenere 0.45V dai 5V di alimentazione), seguito da un secondo partiore con costruito con un potenziometro che mi permetterà di ottenere una tensione variabile tra 0 e 0.45V.

Posso costruire il circuito dell'esempio utilizzando un amplificatore operazionale LM358, R1 = 100k, R2 = 1M, per ottenere un guadagno di 11, e provare poi altre coppie di resistenze.

Note, in un amplificatore operazionale reale:
- il valore massimo in uscita non potrà mai essere pari alla tensione di alimentazione ma sarà leggermente più basso;
- con un'alimentazione singola il valore 0 sull'uscita non sarà mai 0V ma ci si avvicinerà.
Per questo esempio non è un problema.

TODO: Allegare schema del circuito, con valori dei componenti

[Indietro](../schema.md)
