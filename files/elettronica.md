
Argomenti di elettronica da trattare
====================================
- Introduzione a encoder e decoder
- Amplificatori operazionali
- Tolleranza dei componenti
- Comportamento ideale e reale dei sensori
- Taratura e calibrazione, metodo classico per i circuiti analogici (es. con trimmer), e per i circuiti digitali con microcontrollore (via software, salvando i parametri nella EEPROM)
- Leggere e comprendere datasheet e note applicative dei componenti utilizzati


Progetti collegati
------------------
Amplificatore operazionale:
- Rilevare variazioni di tensione troppo basse per l'ADC.

Taratura e calibrazione:
- Interruttore crepuscolare
- Interruttore acustico
- Interruttore temporizzato

[Indietro](../schema.md)
