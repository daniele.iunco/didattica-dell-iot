Un sensore resistivo comune, la fotoresistenza
==============================================
Prerequisiti:
- legge di Ohm
- partitore di tensione

In un sensore resistivo la resistenza varia al variare di quanto rilevato, quindi misurare quanto rilevato significa misurare la resistenza del sensore e poi secondo le indicazioni del datasheet del componente calcolare il valore corrispondente della grandezza misurata.

Quindi dobbiamo utilizzare il nostro microcontrollore come se fosse un ohmetro, questo si può fare utilizzando un partitore di tensione con una resistenza nota e un'incognita che è la resistenza del sensore.

[schema_partitore.png](./schema_partitore.png)

Con incognita R2, ricaviamo:

R2 = R1 * Vout (Vin - Vout)

Per rilevare il livello di luminosità dell'ambiente possiamo utilizzare una fotoresistenza che è un sensore resistivo, la cui resistenza varia in modo inversamente proporzionale alla luce che la colpisce.

TODO: Allegare schema del circuito e codice dello sketch

Possiamo quindi mettere la fotoresistenza al posto di R2 di cui rileveremo il valore in ohm, e, il rapporto LUX / variazione di resistenza presente nel datasheet del componente, ottenere il livello di luminosità dell'ambiente.

[Indietro](../schema.md)
