Controllo closed loop
=====================
Rif. teoria sul controllo closed loop.

Utilizziamo quanto fatto nei punti 1 e 2 per controllare il livello di luminosità di una striscia led, deve essere possibile impostare il livello desiderato agendo su un potenziometro, il microcontrollore al diminuire della luminosità dell'ambiente dovrà incrementare la luminosità della striscia led.

TODO: Allegare schema del circuito e codice dello sketch.

Quanto costruito si potrebbe utilizzare anche per controllare l'illuminazione dell'ingresso di un edificio, per accendere la luce se necessario quando è buio, e, per minimizzare i consumi, tenendo la luminosità a un livello minimo da aumentare solo quando si avvicina qualcuno al palazzo.
Per rilevare la presenza possiamo aggiungere un sensore PIR per monitorare il passaggio di persone nell'area di fronte all'edificio, il comportamento del sensore PIR è intuitivo, quando rileva il passaggio di una persona porta l'uscita a livello alto, che torna a livello basso dopo un tempo preimpostato.

Esercizio da svolgere:
Cambiamo il circuito costruito nella prima parte per:
- mantenere un livello minimo di luminosità, se necessario (luminosità ambiente < luminosità richiesta), in assenza di persone;
- portare il livello di luminosità al massimo, se necessario (luminosità ambiente < luminosità richiesta) e per un tempo preimpostato, quando qualcuno entra nell'area monitorata dal sensore PIR.

TODO: Allegare schema del circuito e codice dello sketch.

[Indietro](../schema.md)
