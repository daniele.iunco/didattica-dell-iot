Approfondimento di fisica
=========================
Misura delle grandezze fisiche, sensibilità, cifre significative, come tutto questo si applica ad un ADC.

Quando si utilizzano sensori analogici, la misurazione sarà effettuata rilevando una tensione sull'input analogico del microcontrollore.

Per fare questo il microcontrollore utilizza un ADC (Analog-digital converter), con un suo riferimento di tensione (Aref), che converte un un valore analogico in un valore discreto codificato con un numero finito di bit.

Con un ADC non si avranno cifre significative ma bit significativi, ma sono applicabili gli stessi concetti validi nella misura di grandezze fisiche.

La sensibilità dell'ADC è pari alla variazione di tensione corrispondente alla variazione da 0 a 1 del bit meno significativo del valore restituito dall'ADC.

Nell'interfacciamento a sensori analogici è necessario considerare:
- la variazione di tensione minima da rilevare;
- il valore di tensione massimo possibile.

In base al sensore analogico che si utilizza, potrebbe essere necessario cambiare il riferimento di tensione (Aref) dell'ADC;

Inoltre in caso di operazioni matematiche sul valore restituito dall'ADC, il risultato non potrà avere più cifre significative di quelle che aveva il valore rilevato.

[Indietro](../schema.md)
