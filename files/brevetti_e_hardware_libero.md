Hardware proprietario, brevetti, hardware libero
================================================

Hardware proprietario
---------------------
L'hardware proprietario, il pi� comune, � un hardware di cui non sono disponibili informazioni dettagliate sul funzionamento, per l'utente � una black box e la documentazione � limitata a quanto necessario per l'interfacciamento e l'uso.
Questo hardware � spesso brevettato o � costruito con componenti brevettati e non pu� essere disassemblato studiato o riprodotto liberamente.


Brevetti
--------
Il brevetto � uno strumento che concede all'inventore un diritto esclusivo di sfruttamento della sua invenzione e di conseguenza la facolt� di decidere come possa essere utilizzata e a chi eventualmente consentire la produzione e la commercializzazione.

Il brevetto si pu� rinnovare pagando annualmente i diritti di mantenimento, per un tempo massimo che, per i brevetti per invenzione industriale, � 20 anni (verificato per Europa e Stati Uniti); dopo la scadenza l'invenzione non sar� pi� tutelata.

Il brevetto pu� riguardare funzionalit� che possono anche essere utilizzate in diversi oggetti.


Hardware libero - Open hardware
-------------------------------
I progetti (schemi, layout del PCB e dettagli del funzionamento) sono distribuiti con licenze (https://it.wikipedia.org/wiki/Hardware_libero#Licenze) che permettono di studiare in dettaglio il funzionamento dell'hardware, costruirne copie uguali o anche varianti e ridistribuirle.

La diffusione dell'hardware libero, a differenza del software, dipende anche dai costi dei componenti utilizzati e del processo di produzione.

Tra l'hardware libero pi� diffuso ci sono schede di sviluppo e moduli, l'esempio pi� famoso per il successo ottenuto � Arduino.

In generale per�, a differenza del software, nella produzione e distribuzione di hardware libero si devono comunque tenere in considerazione i brevetti; il progetto di un particolare componente hardware libero potrebbe comunque contenere, anche involontariamente, funzionalit� gi� brevettate da qualcun altro, modificarlo e ridistribuirlo potrebbe portare a una violazione del brevetto.


Vantaggi dell'utilizzo di hardware libero per l'insegnamento di sistemi embedded e internet of things
-----------------------------------------------------------------------------------------------------
Disporre di hardware libero porta numerosi vantaggi:
- grande disponibilit� di documentazione, questo consente di studiare in modo approfondito il funzionamento degli oggetti;
- possibilit� di modificare i progetti di oggetti esistenti in base alle necessit�;
- forum e community dove trovare supporto da altri utenti e a volte anche dai produttori stessi dell'hardware;
- costi bassi grazie all'esistenza di pi� produttori e quindi alla maggiore concorrenza;
- gli studenti possono utilizzare tecnologie valide per pi� oggetti e non limitate a funzionalit� specifiche degli oggetti di un singolo produttore;


[Indietro](../schema.md)


