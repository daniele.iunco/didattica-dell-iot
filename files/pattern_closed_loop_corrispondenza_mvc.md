Rappresentare un sistema di controllo closed loop con una variante del pattern MVC
================================================================================================
In sistemi embedded dove abbiamo sensori ed attuatori e un sistema di controllo closed loop, l'interazione tra microcontrollore e mondo fisico si può rappresentare con una variante del pattern MVC, dove l'utente è sostituito dal microcontrollore e il relativo software.

In particolare considerando:
- come modello il mondo fisico, limitato a ciò che si intende monitorare e su cui si deve agire;
- come controllo l'insieme degli attuatori;
- come vista l'insieme dei sensori;
- come utente l'MCU programmato per svolgere il compito richiesto, che riceve i dati dai restituiti dai sensori (vista), agisce sul modello mediante gli attuatori (controllore).

Questo pattern può rappresentare l'interazione tra MCU e mondo fisico in un sistema embedded nel caso descritto, in un modo ben conosciuto dagli informatici.

[Immagine](./variante_mvc_controllo_closed_loop.png)

[Indietro](../schema.md)
