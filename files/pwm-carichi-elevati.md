PWM per il controllo di carichi elevati (illminazione con LED)
==============================================================
Prerequisiti:
- PWM
- Transistor in generale, ed in particolare i FET

Qualunque sia il microcontrollore, le correnti in uscita sono limitate quindi non si possono alimentare carichi elevati, Arduino ad esempio può erogare fino a 40mA per pin digitale, con un massimo complessivo come somma di tutti gli output di 200mA, se dovessimo alimentare dei comuni led che consumano sui 20mA, su un pin potremmo collegarne al massimo 2 per non danneggiare il microcontrollore.

Quando il carico da controllare supera il massimo consentito è necessario utilizzare una alimentazione apposita, che ad esempio si può controllare con un relè, il relè però non è utilizzabile per pilotare un carico con un segnale PWM, ad esempio per alimentare una serie di LED dove vogliamo variare la luminosità.

Per dare un'idea agli studenti del problema della potenza del carico da alimentare, possiamo fare una dimostrazione con:
- un convertitore DC-DC con limitatore di corrente
- alcuni led ad alta luminosità, ad esempio led blu

Regoliamo l'uscita del convertitore DC-DC entro il livello di tensione previsto dai led scelti (es. 3v) con il un limite di corrente più basso consentito, ad esempio 100mA.
Colleghiamo i led uno per volta, raggiunto il carico massimo, con l'aggiunta di altri led la luminosità diminuirà, questo significa che la corrente non è sufficiente ad alimentare tutti i led, e, cosa più importante, se avessimo preso l'alimentazione da un pin digitale del microcontrollore probabilmente si sarebbe danneggiato.

Se vogliamo alimentare più led regolando la luminosità con un segnale PWM, dovremo utilizzare un transistor (rif. teoria sul funzionamento dei transistor), il classico BJT però non è efficiente, la caduta di tensione può arrivare a 1.5 - 2V, con carichi nell'ordine degli ampere converrà utilizzare un mosfet (rif. utilizzo del mosfet)

TODO: Allegare schema del circuito con il mosfet.

[Indietro](../schema.md)
