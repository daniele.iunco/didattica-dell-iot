Input dal mondo fisico e sensori analogici
==========================================
I sistemi embedded possono avere il compito di raccogliere dati (con sensori), tuttavia questo significa avere sistemi digitali che devono interagire con l'ambiente che è analogico.

La disponibilità di sensori digitali semplifica notevolmente questo lavoro di interazione, tuttavia volendo mettere gli studenti nelle condizioni di poter lavorare con chi progetta sistemi embedded non si può non considerare l'aspetto analogico e le relative problematiche.



Parte 1 - I sensori
-------------------
Con i sistemi embedded si entra in contatto con il mondo fisico, ad esempio per monitorare una particolare caratteristica dell'ambiente e compiere azioni in risposta ad eventi particolari.

Nei casi più semplici si ha una caratteristica fisica del mondo esterno che varia con continuità, da misurare e trasformare in un valore discreto per poi utilizzarlo nel software, e per ottenere questo valore si utilizza un sensore.

In commercio si trovano vari kit di sensori, il più delle volte si tratta di oggetti con un output digitale, istruzioni dettagliate per la connessione e librerie software pronte all'uso.
Il compito per l'utilizzatore di questi kit diventa scegliere ciò che va monitorato, collegare i sensori necessari ai pin indicati nelle istruzioni, caricare le librerie e richiamare le funzioni già pronte per effettuare le misurazioni.
Semplificando, con questo livello di astrazione si ottiene una variabile con un valore che rappresenta una grandezza fisica senza avere alcuna idea di come questo sia stato possibile perché non è necessario.

Ritengo che studiare sistemi embedded iniziando in questo modo non sia molto utile dal punto di vista didattico, e, prima di iniziare ad utilizzare sensori digitali pronti all'uso, sia preferibile studiare come funzionano i sensori analogici.

Sostanzialmente dobbiamo rilevare grandezze fisiche che, per diventare un input per il nostro microcontrollore, devono essere trasformate in un segnale elettrico con caratteristiche che dipendono da ciò che si misura.

Ho pensato quindi ad alcuni esercizi da svolgere per comprendere:
- come funziona un sensore analogico
- come, in un sensore digitale, si trasforma una grandezza continua ed analogica in un valore numerico.
Per fare questo si dovranno utilizzare come sensori dei componenti passivi e un arduino (o altro microcontrollore scelto) per generare l'output digitale.

Esercizio 1:
TODO: - Costruire un sensore di temperatura con un termistore oppure un diodo

Esercizio 2:
TODO: - Costruire un luxmetro con una fotoresistenza

Esercizio 3:
TODO: - Costruire un ricevitore a infrarossi per segnali a 38 kHz con un fotodiodo

Per avere una misura corretta (esercizi 1,2) sarebbe necessario un processo di taratura, la cosa però non è essenziale perchè lo scopo non è produrre sensori digitali ma capirne la logica, quindi si possono prendere i parametri dei componenti dai datasheet senza considerare le tolleranze.

Con questi esercizi, in una prima fase si può restituire il valore rilevato sulla porta seriale, successivamente, nello studio delle tecniche di comunicazione, si può utilizzare il bit banging scrivendo il codice necessario per la comunicazione con un altro arduino.



Parte 2 - I pulsanti e il problema dei rimbalzi
-----------------------------------------------
Un caso particolare di input dal mondo fisico, che questa volta riguarda l'interazione con l'utente, è l'utilizzo di pulsanti; i pulsanti non sono sensori ma servono a permettere all'utente di inviare un input.

Per un programmatore intuitivamente il cambio di stato di un pulsante corrisponde al cambio di valore di una variabile booleana, ma i pulsanti sono oggetti meccanici e, per via di come funzionano, la chiusura dei contatti è soggetta a rimbalzi.
Quando si preme un pulsante la chiusura del circuito non è immediata e stabile, il contatto metallico rimbalza aprendo/chiudendo il circuito anche più volte prima di fermarsi sulla posizione definitiva; anche se tutto accade in pochi millisecondi, questi cambi di stato sono rilevati dal microcontrollore come eventi separati.

Il problema dei rimbalzi non riguarda solo i pulsanti ma qualunque tipo di oggetto con contatti metallici che aprono/chiudono un circuito, ad esempio anche un sensore di capovolgimento; anche la soluzione al problema è applicabile a tutti i componenti che funzionano in modo simile.

Spesso la pressione di un pulsante, quindi il cambio di stato da 0 a 1 dell'input, deve generare una reazione immediata di qualche tipo e, per ottenere questo risultato, si utilizzano gli interrupt; con gli interrupt ignorare i rimbalzi può causare gravi anomalie e quindi è necessario gestirli correttamente.

Un metodo semplice per mostrare l'esistenza del problema è quello di utilizzare un pulsante e un interrupt per incrementare il valore di una variabile ad ogni pressione; senza opportuni accorgimenti ciascuna pressione del pulsante farà incrementare il valore della variabile più volte.

Il problema dei rimbalzi può essere gestito direttamente dal software oppure si può utilizzare un hardware dedicato, la scelta dipende dal tipo di progetto e dai vincoli, ad esempio la necessità di risparmiare spazio e non utilizzare componenti esterni, o di risparmiare potenza di calcolo.

La soluzione software consiste nell'introdurre un ritardo nella lettura dello stato del pin digitale per attendere di avere quello definitivo, quindi, al rilevamento di un cambio di stato del pulsante, si attende un certo tempo (es. 20ms) prima di confermare il cambio di stato e compiere l'operazione prevista come reazione all'evento.

Una soluzione hardware è l'utilizzo di una rete RC e un Trigger di Schmitt, o più semplicemente di componenti già pronti progettati per risolvere questo problema come i MAX6816/MAX6817/MAX6818, quest'ultima opzione consente anche un risparmio di spazio sul circuito stampato.

Esercizio 1:
TODO: - Costruire un contatore a 2 pulsanti (incremento/decremento) con l'utilizzo degli interrupt e  gestire in modo corretto i rimbalzi via software.



[Indietro](../schema.md)
