SHELL := /bin/bash 

# Tools
LATEXMK = latexmk
RM = rm -f

# Project-specific settings
DOCS = Didattica-dell-iot.pdf Eserciziario.pdf
DOCNAME = Eserciziario

# Targets
all: clean doc
everything: clean all
doc: pdf
pdf: $(DOCNAME).pdf

# Rules
%.pdf: %.tex
#	$(LATEXMK) -r "latexmkrc" -pdf -pdflatex="xelatex %O %S" -M -MP -MF $*.deps $<
	$(LATEXMK) -xelatex -M -MP -MF $*.deps $<

eserciziario: clean $(DOCNAME)

evince: $(DOCS)
	evince $(DOCS) &

okular: $(DOCS)
	okular $(DOCS) &

atril: $(DOCS)
	atril $(DOCS) &

stats:
	texcount 		-col -merge -incbib $(DOCNAME).tex -out=texcount.stats
#	texcount 		-col -merge -incbib $(DOCNAME).tex -html -out=texcount.html
	texcount -char	-col -merge -incbib $(DOCNAME).tex -out=texcount-char.stats

mostlyclean:
	$(LATEXMK) -silent -c
	$(RM) *.bbl

clean: mostlyclean
	$(LATEXMK) -silent -C
	$(RM) *.run.xml *.synctex.gz *.glsdefs *.fdb_latexmk *.d *.deps *.fls *.mtc* *.maf *.tdo pdfl.log dep.list texcount* Eserciziario-dot2tex*

#.PHONY: all doc pdf evince detex okular shrink stats mostlyclean clean vol2
