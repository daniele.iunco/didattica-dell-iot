%versione=2
\section{Voltmetro per tensioni maggiori di 3.3V o 5V}
\index{ohmlaw!Voltmetro per tensioni maggiori di 3.3V o 5V}\index{analogInput!Voltmetro per tensioni maggiori di 3.3V o 5V}\index{multipsu!Voltmetro per tensioni maggiori di 3.3V o 5V}\titoloparagrafo{Prerequisiti:}
\adjustbox{max width=\textwidth}{
\begin{dot2tex}[dot,options=-t raw]
\input{grafi/esercizio-esercizio31.gv}
\end{dot2tex}
}

\titoloparagrafo{Componenti:}
\begin{compactitem}
\item R1 = Resistore da 100 kOhm
\item R2 = Resistore da 10 kOhm
\item (metacomponente) fonte di tensione adeguata >5V
\end{compactitem}


\titoloparagrafo{Descrizione e risultato atteso:}
Costruire un partitore di tensione con i resistori indicati per ridurre la tensione sull'ingresso analogico del microcontrollore, in modo da ottenere la massima riduzione della tensione in ingresso con i resistori disponibili.  
Individuare la nuova tensione massima misurabile.  
Scrivere sulla porta seriale il valore misurato convertito in Volt, tenendo conto della funzione di trasferimento del partitore di tensione costruito.

\titoloparagrafo{Scopo dell'esercizio:}
Riconoscere il problema della misura di un livello di tensione da una fonte esterna che, in condizioni normali, supererebbe il massimo consentito come input del microcontrollore.

\titoloparagrafo{Eventuali funzionalità aggiuntive:}
In caso di realizzazione su Arduino, rifare l'esercizio utilizzando il riferimento di tensione integrato da 1.1V per ottenere misure più accurate.

\titoloparagrafo{Errori comuni:}
N/A

\titoloparagrafo{Possibile comportamento erratico:}
La tolleranza dei componenti potrebbe portare a misure poco precise in confronto a quanto misurato con il multimetro digitale.

\titoloparagrafo{Risultato di apprendimento:}
Applicare il partitore per misurare una tensione da una fonte esterna, sempre in bassa tensione, per rendere possibile la misurazione si devono connettere insieme le masse delle fonti di alimentazione.

\titoloparagrafo{Indicazioni per la soluzione:}
Costruire un partitore di tensione per ridurre la tensione in ingresso, come indicato nell'immagine
Voltmetro, montaggio su breadboard in figura \ref{fig:f46e9195-beef-4169-bb41-de2073dd1939}.
\begin{figure}
	\begin{center}
\includegraphics[width=0.9\textwidth]{partitore_voltmetro_bb}
		\caption{Voltmetro, montaggio su breadboard}
		\label{fig:f46e9195-beef-4169-bb41-de2073dd1939}
\end{center}
\end{figure}
Schema voltmetro in figura \ref{fig:b9a43f6f-e025-4cf3-88d6-4c084e645622}.
\begin{figure}
	\begin{center}
\includegraphics[width=0.5\textwidth]{03-partitore_voltmetro_schem}
		\caption{Schema voltmetro}
		\label{fig:b9a43f6f-e025-4cf3-88d6-4c084e645622}
\end{center}
\end{figure}



