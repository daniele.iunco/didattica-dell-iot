# 1 - Led RGB con accensione sequenziale/singola premendo un pulsante
Tag: Base, DigitalInput, DigitalOutput, Rimbalzi
Prerequisiti: digitalInput,digitalOutput,debouncing

# 2 - Led RGB (non numerico o ex strip)
Tag: Base, AnalogInput, PWM
Prerequisiti: analogInput,digitalOutput
Regolo la combinazione di colori tutti insieme usando un potenziometro, o separatamente con 3 potenziometri.


# 3 - Led RGB + pulsante
Tag: Base, DigitalInput, AnalogInput, PWM
Prerequisiti: analogInput,digitalInput,digitalOutput,debouncing
Uso un solo potenziometro e un pulsante, con una pressione del pulsante cambio il colore selezionato e potrò regolare il livello di quel colore con il potenziometro.


# 4 - Fotoresistenza (già in elenco)
Tag: Base, DigitalOutput, AnalogInput
Prerequisiti: ohmlaw,analogInput,digitalOutput
Interruttore crepuscolare


# 5 - Sensore di rumore (es - KY-038) + Relè
Tag: Medio, DigitalOutput, AnalogInput
Prerequisiti: analogInput,digitalOutput
Se rilevo un rumore che supera una soglia preimpostata (es. battito di mani) cambio lo stato di accensione di un rele tra On e Off.
Devo usare il sensore in modalità analogica altrimenti l'esercizio non ha senso.
Alternativa: registro suoni nel tempo e stampo una media mobile delle ampiezze
Alternativa/espansione: decoder morse?


# 6 - Sensore di rumore (es. KY-038)
Tag: Medio, DigitalOutput, AnalogInput
Prerequisiti: analogInput,digitalOutput
Accendo un numero di LED variabile in base al livello di rumore, posso usare più LED singoli o una barra LED.


# 7 - Fotodiodo - TODO
Tag: Base, DigitalInput, DigitalOutput
Prerequisiti: digitalInput,digitalOutput
Rilevatore di telecamere a infrarossi, il fotodiodo conduce in presenza di un segnale IR, e significa che vicino c'è una telecamera puntata nella sua direzione.
Alternativa: usare un telecomando TV come sorgente, senza fare decoding, oppure led IR sempre acceso (contapezzi)
Nota: Verificare funzionamento del fotodiodo


# 8 - Matrice LED
Tag: Medio, DigitalOutput
Prerequisiti: digitalOutput
Prima parte: non ho abbastanza pin di I/O digitale, uso meno righe
Seconda parte: uso pin analogici come digitali, poi però non ne rimangono altri liberi
Terza parte: citazione circuito integrato adatto a controllare la matrice LED (registro a scorrimento)
Disegno una riga al centro, e la faccio ruotare sempre rispetto al centro.
Utile anche provando a usare tutti i GPIO dell'MCU (quindi tralasciando qualche riga/colonna), per capire la necessità dello shift register.
Alternativa: si riesce a fare un "charlieplexing" con la matrice?


# 9 - Charlieplexing
Tag: Laboratorio di saldatura
Prerequisiti: diode


# 10 - Accelerometro
Tag: Medio, DigitalOutput, Software, I2C
Prerequisiti: digitalOutput,i2c
Partendo dall'esercizio sulla matrice LED; faccio ruotare la riga in base allo spostamento rilevato dall'accelerometro


# 11 - Motore
Tag: Base, DigitalOutput, Dimensionamento elettrico
Prerequisiti: digitalOutput,multipsu,psudimensioning
Alimento il motore con delle pile, uso un relé per controllarne l'accensione.


# 12 - Relè reed + Buzzer attivo
Tag: Base, DigitalInput, DigitalOutput, Software, Rimbalzi, Knocking protocol
Prerequisiti: digitalInput,digitalOutput,debouncing
Sensore per apertura porta/finestra, a contatto aperto faccio suonare un buzzer attivo
Alternativa/espansione: il buzzer suona solo se rileva una precisa sequenza di attivazioni (applicabile anche in altri casi, es. pulsante/rilevatore urti)


# 13 - Display 7 segmenti (singolo) + Buzzer attivo
Tag: Base, DigitalOutput, Software
Prerequisiti: digitalOutput
Timer per conto alla rovescia, da 9 a 0, e suono al termine.


# 14 - Rilevatore di urti + motore
Tag: Base, DigitalInput, DigitalOutput, Software, Rimbalzi, Dimensionamento elettrico
Prerequisiti: digitalInput,digitalOutput,debouncing,multipsu,psudimensioning
Faccio muovere una macchina che si deve fermare in caso di urto.


# 15 - Sensore di distanza ad ultrasuoni + Motore
Tag: Medio, DigitalInput, DigitalOutput, PWM, Software, Dimensionamento elettrico
Prerequisiti: digitalInput,digitalOutput,pwm,multipsu,psudimensioning
Costruisco una macchina che avanza in assenza di ostacoli, in presenza di ostacoli rallenta proporzionalmente alla distanza


# 16 - Encoder rotativo + Display LCD (Seriale/I2C) + Pulsante
Tag: Medio, DigitalInput, DigitalOutput, Software
Prerequisiti: digitalInput,digitalOutput,debouncing,i2c
Definisco un array di stringhe e mostro sul diplay quella alla posizione determinata dalla rotazione dell'encoder,
Riga 1: stringa selezionata
Riga 2: vuoto o, premendo il pulsante, traduzione in codice morse della stringa (con funzione scorrimento del display)


# 17 - Encoder rotativo + Buzzer passivo
Tag: Medio, DigitalInput, PWM, Software, Verificare implementazione
Prerequisiti: digitalInput,pwm
valutare opzione altoparlante + resistenza
Eseguo tone(), con frequenza che varia con la rotazione dell'encoder
Espansione: confrontare con implementazione "a potenziometro" (gestione intervalli di resistenza)


# 18 - Joystick + Matrice LED
Tag: Medio, AnalogInput, DigitalInput, DigitalOutput, Software
Prerequisiti: ohmlaw,analogInput,digitalInput,digitalOutput
Disegno un punto inizialmente posizionato al centro, sposto il punto in base allo spostamento del joystick, premendo il joystick faccio lampeggiare il punto
Joystick varia la velocità di spostamento del punto
Espansione: con rientro dal bordo (spazio toroidale)


# 19 - Display 7 segmenti da 4 cifre + Pulsanti
Tag: Medio, DigitalInput, DigitalOutput, Software, Rimbalzi
Prerequisiti: digitalInput,digitalOutput,debouncing,interrupt
(Usiamo 2 cifre)
Contatore con aumento/decremento, questo fa vedere anche l'effetto dei rimbalzi, che è ancora più evidente con gli interrupt.


# 20 - Servo + Potenziometro
Tag: Base, AnalogInput, PWM
Prerequisiti: ohmlaw,analogInput,pwm
Faccio muovere il servo ruotando il potenziometro, con la posizione predefinita centrale che corrisponde alla posizione centrale del potenziometro


# 21 - Servo + IR decoder + Telecomando
Tag: Base, DigitalInput, PWM, Software
Prerequisiti: digitalInput,pwm,interrupt
Faccio muovere il servo in una direzione o nell'altra con i tasti + e - del telecomando.


# 22 - Buzzer attivo
Tag: Base, DigitalOutput, Software, Seriale
Prerequisiti: digitalOutput
Leggo l'input dalla porta seriale e converto i caratteri in codice Morse, genero l'output sonoro corrispondente con il buzzer.


# 23 - Costruire una roulette a LED, Arduino e numeri casuali
Tag: Medio, AnalogInput, Software
Prerequisiti: analogInput,digitalOutput
Per ottenere numeri casuali diversi ad ogni avvio va richiamato randomSeed(n) utilizzando come parametro n il valore letto con analogRead da un pin non collegato.
I numeri casuali generati con random() sono presi da una sequenza predeterminata e a ogni riavvio si ricomincia dall'inizio della sequenza.
Misurare l'entropia, in setup() il led lampeggia finché non ho rilevato entropia sufficiente, misuro deviazione standard dati input AnalogRead, barra led per rappresentare il livello di entropia.
atmega: sensore di temperatura integrato nel chip che funziona in modo non determinabile?
resistenza molto alta tra analog input e massa, riceve rumore


# 24 - Sensore ad effetto Hall con output analogico oppure sensore di distanza a ultrasuoni + Buzzer passivo
Tag: Medio, AnalogInput, PWM, Software
Prerequisiti: analogInput,pwm
Rilevo la distanza dal magnete e uso il valore per generare un suono con tonalità variabile.
Si potrebbero usare misuratori di distanza a ultrasuoni


# 25 - Sensore fiamma + Buzzer attivo
Tag: Base, DigitalInput, DigitalOutput, Software
Prerequisiti: digitalInput,digitalOutput
Fiamma che si accende come trigger, oppure timer per spegnimento
Se rilevo la presenza di una fiamma che non si spegne entro un intervallo predefinito, genero un segnale acustico con il buzzer.


# 26 - Sensore di capovolgimento, protezione da corriere distratto
Tag: Avanzato, DigitalInput, DigitalOutput, Software, EEPROM
Prerequisiti: digitalInput,digitalOutput,eeprom

Circuito iniziale: Rilevo il capovolgimento e faccio lampeggiare un LED
Espansione: il circuito va messo in un pacchetto da trasportare che non deve essere capovolto, serve a scoprire se si è verificato questo evento, per non perdere l'informazione sul capovolgimento in caso di spegnimento o riavvio la deve salvare nella memoria EEPROM (integrata su Arduino) per poi leggerla in fase di setup.

ATTENZIONE, il numero di scritture possibili sulla memoria EEPROM è limitato, fare attenzione a scriverci solo quando serve; un errore come riscrivere di continuo lo stesso valore renderebbe inutilizzabile quella memoria in poco tempo.

Per resettare il circuito per il successivo trasporto uso un pulsante che, tenuto premuto in fase di avvio, causa la cancellazione dell'informazione sul capovolgimento dalla memoria EEPROM se c'è in fase di setup().


# 27 - Sensore di capovolgimento + matrice LED
Tag: Medio, DigitalInput, DigitalOutput, Software
Prerequisiti: digitalInput,digitalOutput
Disegno un'immagine sulla matrice LED, in caso di capovolgimento capovolgo l'immagine per farla vedere sempre nel verso giusto.


# 28 - Line follower
Tag: Avanzato, DigitalInput, DigitalOutput, Software
Prerequisiti: digitalInput,digitalOutput
Costruisco una macchina che deve seguire, utilizzando per lo sterzo 2 motori o un servo.
Per l'alimentazione uso delle pile e un relé per controllarne l'accensione.

# 29 - Striscia LED (numerica)
Tag: Medio, DigitalOutput, Software
Prerequisiti: digitalOutput
Punto che si muove lungo una linea, ad esempio per segnalare un percorso da seguire.
Espansione: il punto si ferma e lampeggia in corrispondenza delle uscite di emergenza (configurabili)
Espansione2 effetti ottici ad libitum: muovere il punto lasciando coda (con fade out).


# 30 - Metro a ultrasuoni + LCD
Tag: Medio, DigitalInput, DigitalOutput, Software
Prerequisiti: digitalInput,digitalOutput



TODO: Questi vanno messi dopo gli esercizi base:

# 31 - Esercizio sui transistor BJT e MOSFET
Tag: Medio, Elettronica analogica, Da valutare
Prerequisiti: ohmlaw,digitalOutput
Per semplificare, uso questi componenti per accendere e spegnere dei LED (con in serie il resistore necessario), avrò un consumo di pochi milliAmpere, per il BJT serve un resistore tra GPIO e Base da 1 kOhm, per il MOSFET un resistore tra GPIO e Gate da 10 Ohm (Non essenziale ma consigliato, vedi note applicative Toshiba).

BJT NPN:
con GPIO LOW: non conduce
con GPIO HIGH conduce, con l'Emettitore collegato al - e il Collettore collegato al - del carico da alimentare

MOSFET N, funziona come il BJT NPN - Gate come Base, Source come Emettitore, Drain come Collettore, ma con resistore GPIO-Gate 10 Ohm
con GPIO LOW non conduce
con GPIO HIGH conduce come l'NPN

Cosa accade se controllo un carico con una propria alimentazione?
Con il BJT se spengo il micorontrollore non ho più corrente sulla base e il carico si spegne, il MOSFET invece rimane acceso, ci vuole un resistore di pulldown(MOSFET N) o pullup (MOSFET P)

2 led, 1 su base 1 su collettore
mostrare che con led su base e su emettitore, il passaggio di corrente è molto diverso, facendo lo stesso con MOSFET invece non passa corrente sul gate


# 32
Tag: Avanzato, DigitalInput, Software, Verificare implementazione
Prerequisiti: digitalInput
bit banging: leggere il segnale del numero composto con il telefono a impulsi

# 33 - Sensore ad effetto Hall con output analogico - Segnavento
Tag: Avanzato, AnalogInput, PWM, Software
Prerequisiti: analogInput
